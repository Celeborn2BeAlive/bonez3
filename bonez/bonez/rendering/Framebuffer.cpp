#include "Framebuffer.hpp"

namespace BnZ {

const char* GeometryFramebufferChannels::getName(size_t channel) {
    static const char* names[] = {
        "normalized_position",
        "shading_normal",
        "abs_shading_normal",
        "geometric_normal",
        "abs_geometric_normal",
        "normalized_distance",
        "uv",
        "texture_coords",
        "emitted_radiance"
    };
    return names[channel];
}

}
