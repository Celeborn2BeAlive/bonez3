#include "VeachBSDFSamplingPathtraceRenderer.hpp"
#include <bonez/scene/shading/BSDF.hpp>
#include <bonez/sys/DebugLog.hpp>

#include <bonez/scene/sensors/PixelSensor.hpp>

namespace BnZ {

void VeachBSDFSamplingPathtraceRenderer::preprocess() {
}

void VeachBSDFSamplingPathtraceRenderer::processSample(uint32_t threadID, uint32_t pixelID, uint32_t sampleID, uint32_t x, uint32_t y) const {
    for(auto i: range(getFramebufferChannelCount())) {
        accumulate(i, pixelID, Vec4f(0.f, 0.f, 0.f, 1.f));
    }

    PixelSensor pixelSensor(getSensor(), Vec2u(x, y), getFramebufferSize());

    auto pixelSample = getPixelSample(threadID, sampleID);
    auto lensSample = getFloat2(threadID);

    RaySample raySample;
    Intersection I;
    float positionPdf, directionPdf, intersectionPdfWrtArea,
            sampledPointToIncidentDirectionJacobian;
    auto We = pixelSensor.sampleExitantRay(getScene(), lensSample, pixelSample, raySample, positionPdf, directionPdf, I,
                                      sampledPointToIncidentDirectionJacobian, intersectionPdfWrtArea);

    if(We == zero<Vec3f>() || raySample.pdf == 0.f) {
        return;
    }
    We /= raySample.pdf;

    if(!I) {
        return;
    }

    BSDF bsdf { -raySample.value.dir, I, getScene() };

    auto L = zero<Vec3f>();

    if(acceptPathDepth(1) && I.Le != zero<Vec3f>()) {
        accumulate(DEPTH1, pixelID, Vec4f(I.Le, 0.f));
        L += We * I.Le;
    }

    auto Ls = zero<Vec3f>(); // Estimate of scattered radiance

    // Sample the BSDF, two sample for each light source to have the same total number of samples than the MIS test
    auto bsdfSampleCount = 2 * getScene().getLightContainer().size();
    for(auto i: range(bsdfSampleCount)) {
        Sample3f wo;
        float cosThetaOutDir;
        auto fs = bsdf.sample(getFloat3(threadID), wo, cosThetaOutDir);

        if(fs != zero<Vec3f>() && wo.pdf > 0.f) {
            auto hitPoint = getScene().intersect(Ray(I, wo.value));
            if(hitPoint) {
                Ls += We * fs * abs(cosThetaOutDir) * hitPoint.Le / wo.pdf;
            }
        }
    }
    Ls /= float(bsdfSampleCount);
    accumulate(DEPTH1 + 1, pixelID, Vec4f(Ls, 0.f));

    L += Ls;
    accumulate(FINAL_RENDER, pixelID, Vec4f(L, 0.f));
}

void VeachBSDFSamplingPathtraceRenderer::doExposeIO(GUI& gui) {
}

void VeachBSDFSamplingPathtraceRenderer::doLoadSettings(const tinyxml2::XMLElement& xml) {
}

void VeachBSDFSamplingPathtraceRenderer::doStoreSettings(tinyxml2::XMLElement& xml) const {
}

void VeachBSDFSamplingPathtraceRenderer::processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const {
    processTileSamples(viewport, [this, threadID](uint32_t x, uint32_t y, uint32_t pixelID, uint32_t sampleID) {
        processSample(threadID, pixelID, sampleID, x, y);
    });
}

void VeachBSDFSamplingPathtraceRenderer::initFramebuffer() {
    addFramebufferChannel("final_render");
    for(auto i : range(2)) {
        addFramebufferChannel("depth_" + toString(i + 1));
    }
}

}
