#include "VeachMISPathtraceRenderer.hpp"
#include <bonez/scene/shading/BSDF.hpp>
#include <bonez/sys/DebugLog.hpp>

#include <bonez/scene/sensors/PixelSensor.hpp>
#include <bonez/scene/lights/AreaLight.hpp>

namespace BnZ {

void VeachMISPathtraceRenderer::preprocess() {
    m_Sampler.initFrame(getScene());
}

void VeachMISPathtraceRenderer::processSample(uint32_t threadID, uint32_t pixelID, uint32_t sampleID, uint32_t x, uint32_t y) const {
    for(auto i: range(getFramebufferChannelCount())) {
        accumulate(i, pixelID, Vec4f(0.f, 0.f, 0.f, 1.f));
    }

    PixelSensor pixelSensor(getSensor(), Vec2u(x, y), getFramebufferSize());

    auto pixelSample = getPixelSample(threadID, sampleID);
    auto lensSample = getFloat2(threadID);

    RaySample raySample;
    Intersection I;
    float positionPdf, directionPdf, intersectionPdfWrtArea,
            sampledPointToIncidentDirectionJacobian;
    auto We = pixelSensor.sampleExitantRay(getScene(), lensSample, pixelSample, raySample, positionPdf, directionPdf, I,
                                      sampledPointToIncidentDirectionJacobian, intersectionPdfWrtArea);

    if(We == zero<Vec3f>() || raySample.pdf == 0.f) {
        return;
    }
    We /= raySample.pdf;

    if(!I) {
        return;
    }

    BSDF bsdf { -raySample.value.dir, I, getScene() };

    auto L = zero<Vec3f>();

    if(acceptPathDepth(1) && I.Le != zero<Vec3f>()) {
        accumulate(DEPTH1, pixelID, Vec4f(I.Le, 0.f));
        L += We * I.Le;
    }

    // Sample both the light sources and the BSDF to perform MIS

    auto bsdfSampleCount = getScene().getLightContainer().size();

    // Light source sampling
    for(const auto& pLight: getScene().getLightContainer()) {
        RaySample shadowRay;
        auto Le = pLight->sampleDirectIllumination(getScene(), getFloat2(threadID), I, shadowRay);

        if (Le != zero<Vec3f>() && shadowRay.pdf > 0.f && !getScene().occluded(shadowRay.value)) {
            auto contrib = We * bsdf.eval(shadowRay.value.dir) * abs(dot(shadowRay.value.dir, I.Ns))
                * Le / shadowRay.pdf;

            auto bsdfPDF = bsdf.pdf(shadowRay.value.dir);
            auto weight = shadowRay.pdf / (shadowRay.pdf + bsdfSampleCount * bsdfPDF); // Balance heuristic

            accumulate(DEPTH1 + 1, pixelID, Vec4f(weight * contrib, 0.f));
            L += weight * contrib;
        }
    }

    auto Ls = zero<Vec3f>(); // Estimate of scattered radiance
    // BSDF sampling
    for(auto i: range(bsdfSampleCount)) {
        Sample3f wo;
        float cosThetaOutDir;
        auto fs = bsdf.sample(getFloat3(threadID), wo, cosThetaOutDir);

        if(fs != zero<Vec3f>() && wo.pdf > 0.f) {
            auto hitPoint = getScene().intersect(Ray(I, wo.value));
            if(hitPoint && hitPoint.Le != zero<Vec3f>()) {
                auto cosAtHitPoint = abs(dot(hitPoint.Ns, -wo.value));

                if(cosAtHitPoint > 0) {
                    auto meshID = hitPoint.meshID;
                    auto lightID = getScene().getGeometry().getMesh(meshID).m_nLightID;
                    const auto& pLight = getScene().getLightContainer().getLight(lightID);
                    const auto* pAreaLight = static_cast<const AreaLight*>(pLight.get());

                    float pointPdfWrtArea, directionPdfWrtSolidAngle;
                    pAreaLight->pdf(hitPoint, -wo.value, getScene(),
                                    pointPdfWrtArea, directionPdfWrtSolidAngle);

                    auto lightPDF = pointPdfWrtArea * sqr(hitPoint.distance) / cosAtHitPoint;
                    auto weight = bsdfSampleCount * wo.pdf / (bsdfSampleCount * wo.pdf + lightPDF); // Balance heuristic

                    auto contrib = We * fs * abs(cosThetaOutDir) * hitPoint.Le / wo.pdf;

                    accumulate(DEPTH1 + 1, pixelID, Vec4f(weight * contrib, 0.f));
                    Ls += weight * contrib;
                }
            }
        }
    }
    Ls /= float(bsdfSampleCount);
    accumulate(DEPTH1 + 1, pixelID, Vec4f(Ls, 0.f));

    L += Ls;
    accumulate(FINAL_RENDER, pixelID, Vec4f(L, 0.f));
}

void VeachMISPathtraceRenderer::doExposeIO(GUI& gui) {
}

void VeachMISPathtraceRenderer::doLoadSettings(const tinyxml2::XMLElement& xml) {
}

void VeachMISPathtraceRenderer::doStoreSettings(tinyxml2::XMLElement& xml) const {
}

void VeachMISPathtraceRenderer::processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const {
    processTileSamples(viewport, [this, threadID](uint32_t x, uint32_t y, uint32_t pixelID, uint32_t sampleID) {
        processSample(threadID, pixelID, sampleID, x, y);
    });
}

void VeachMISPathtraceRenderer::initFramebuffer() {
    addFramebufferChannel("final_render");
    for(auto i : range(2)) {
        addFramebufferChannel("depth_" + toString(i + 1));
    }
}

}
