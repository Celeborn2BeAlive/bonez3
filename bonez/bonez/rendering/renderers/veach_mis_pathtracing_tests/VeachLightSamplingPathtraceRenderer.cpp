#include "VeachLightSamplingPathtraceRenderer.hpp"
#include <bonez/scene/shading/BSDF.hpp>
#include <bonez/sys/DebugLog.hpp>

#include <bonez/scene/sensors/PixelSensor.hpp>

namespace BnZ {

void VeachLightSamplingPathtraceRenderer::preprocess() {
    m_Sampler.initFrame(getScene());
}

void VeachLightSamplingPathtraceRenderer::processSample(uint32_t threadID, uint32_t pixelID, uint32_t sampleID, uint32_t x, uint32_t y) const {
    for(auto i: range(getFramebufferChannelCount())) {
        accumulate(i, pixelID, Vec4f(0.f, 0.f, 0.f, 1.f));
    }

    PixelSensor pixelSensor(getSensor(), Vec2u(x, y), getFramebufferSize());

    auto pixelSample = getPixelSample(threadID, sampleID);
    auto lensSample = getFloat2(threadID);

    RaySample raySample;
    Intersection I;
    float positionPdf, directionPdf, intersectionPdfWrtArea,
            sampledPointToIncidentDirectionJacobian;
    auto We = pixelSensor.sampleExitantRay(getScene(), lensSample, pixelSample, raySample, positionPdf, directionPdf, I,
                                      sampledPointToIncidentDirectionJacobian, intersectionPdfWrtArea);

    if(We == zero<Vec3f>() || raySample.pdf == 0.f) {
        return;
    }
    We /= raySample.pdf;

    if(!I) {
        return;
    }

    BSDF bsdf { -raySample.value.dir, I, getScene() };

    auto L = zero<Vec3f>();

    if(acceptPathDepth(1) && I.Le != zero<Vec3f>()) {
        accumulate(DEPTH1, pixelID, Vec4f(I.Le, 0.f));
        L += We * I.Le;
    }

    auto Ldir = zero<Vec3f>(); // Estimate of direct illumination

    // Sample the light sources, two times each to have the same total number of samples than the MIS test
    for(const auto& pLight: getScene().getLightContainer()) {
        for(auto i: range(2)) {
            RaySample shadowRay;
            auto Le = pLight->sampleDirectIllumination(getScene(), getFloat2(threadID), I, shadowRay);

            if (Le != zero<Vec3f>() && shadowRay.pdf > 0.f && !getScene().occluded(shadowRay.value)) {
                auto contrib = We * bsdf.eval(shadowRay.value.dir) * abs(dot(shadowRay.value.dir, I.Ns))
                    * Le / shadowRay.pdf;
                Ldir += contrib;
            }
        }
    }
    Ldir /= 2.f; // Divide by the number of samples of each source
    accumulate(DEPTH1 + 1, pixelID, Vec4f(Ldir, 0.f));

    L += Ldir;
    accumulate(FINAL_RENDER, pixelID, Vec4f(L, 0.f));
}

void VeachLightSamplingPathtraceRenderer::doExposeIO(GUI& gui) {
}

void VeachLightSamplingPathtraceRenderer::doLoadSettings(const tinyxml2::XMLElement& xml) {
}

void VeachLightSamplingPathtraceRenderer::doStoreSettings(tinyxml2::XMLElement& xml) const {
}

void VeachLightSamplingPathtraceRenderer::processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const {
    processTileSamples(viewport, [this, threadID](uint32_t x, uint32_t y, uint32_t pixelID, uint32_t sampleID) {
        processSample(threadID, pixelID, sampleID, x, y);
    });
}

void VeachLightSamplingPathtraceRenderer::initFramebuffer() {
    addFramebufferChannel("final_render");
    for(auto i : range(2)) {
        addFramebufferChannel("depth_" + toString(i + 1));
    }
}

}
