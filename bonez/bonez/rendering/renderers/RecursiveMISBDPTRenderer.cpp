#include "RecursiveMISBDPTRenderer.hpp"
#include <bonez/scene/lights/AreaLight.hpp>

#include <bonez/scene/sensors/PixelSensor.hpp>

#include "paths.hpp"

#include <bonez/sys/DebugLog.hpp>

namespace BnZ {

void RecursiveMISBDPTRenderer::preprocess() {
    BPT_STRATEGY_s0_t2 = FINAL_RENDER_DEPTH1 + m_nMaxDepth;
    m_LightSampler.initFrame(getScene());
}

void RecursiveMISBDPTRenderer::beginFrame() {
    m_AdaptativeSamplingBuffer.resize(getPixelCount());
    m_LocalVarianceImage.resize(getPixelCount());

    if(m_bAdaptativeSampling) {
        // Only works for 1 spp
        auto windowSize = 2;

        processTasksDeterminist(getPixelCount(), [&](uint32_t pixelID, uint32_t threadID) {
            Vec2u pixel = getPixel(pixelID, getFramebufferSize());
            auto localVariance = computeLocalVariance(getFramebuffer().getChannel(FINAL_RENDER), pixel, windowSize);
            m_LocalVarianceImage[pixelID] = localVariance;
        }, getThreadCount());

        processTasksDeterminist(getPixelCount(), [&](uint32_t pixelID, uint32_t threadID) {
            Vec2u pixel = getPixel(pixelID, getFramebufferSize());
            Vec4i window(int(pixel.x) - windowSize, int(pixel.y) - windowSize, 2 * windowSize + 1, 2 * windowSize + 1);
            auto localMeanVariance = computeLocalMean(m_LocalVarianceImage.data(), getFramebufferSize().x, getFramebufferSize().y, window);
            auto localVariance = m_LocalVarianceImage[pixelID];

            if(localVariance >= m_fAdaptativeThreshold * localMeanVariance) {
                m_AdaptativeSamplingBuffer[pixelID] = true;
            } else {
                m_AdaptativeSamplingBuffer[pixelID] = false; // don't process this pixel
            }
        }, getThreadCount());
    } else {
        std::fill(begin(m_AdaptativeSamplingBuffer), end(m_AdaptativeSamplingBuffer), true);
    }

    m_nAdaptativeLightPathCount = std::count_if(begin(m_AdaptativeSamplingBuffer), end(m_AdaptativeSamplingBuffer), [&](bool value) {
        return value;
    });

    sampleLightPaths();
    buildDirectImportanceSampleTilePartitionning();
}

void RecursiveMISBDPTRenderer::sampleLightPaths() {
    m_nLightPathCount = getPixelCount() * getSppCount();
    m_LightPathBuffer.resize(getMaxLightPathDepth(), m_nLightPathCount);

    auto mis = [&](float v) {
        return Mis(v);
    };

    processTasksDeterminist(m_nLightPathCount, [&](uint32_t pathID, uint32_t threadID) {
        ThreadRNG rng(*this, threadID);
        auto pLightPath = m_LightPathBuffer.getSlicePtr(pathID);
        if(m_AdaptativeSamplingBuffer[pathID]) {
            sampleLightPath(pLightPath, getMaxLightPathDepth(), getScene(),
                            m_LightSampler, getSppCount(), mis, rng);
        } else {
            invalidateLightPath(pLightPath, getMaxLightPathDepth());
        }
    }, getThreadCount());
}

void RecursiveMISBDPTRenderer::buildDirectImportanceSampleTilePartitionning() {
    ThreadRNG rng(*this, 0u);
    m_DirectImportanceSampleTilePartitionning.build(
                m_LightPathBuffer.size(),
                getScene(),
                getSensor(),
                getFramebufferSize(),
                getTileSize(),
                getTileCount2(),
                [&](std::size_t i) {
                    return m_LightPathBuffer[i].m_Intersection;
                },
                [&](std::size_t i) {
                    return m_LightPathBuffer[i].m_Intersection && m_LightPathBuffer[i].m_fPathPdf > 0.f;
                },
                rng);
}

void RecursiveMISBDPTRenderer::processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const {
    auto spp = getSppCount();

    TileProcessingRenderer::processTilePixels(viewport, [&](uint32_t x, uint32_t y) {
        auto pixelID = getPixelIndex(x, y);

        if(m_AdaptativeSamplingBuffer[pixelID]) {
            // Add a new sample to the pixel
            for(auto i : range(m_nMaxDepth)) {
                auto depth = i + 1;
                // Don't add new sample if the depth image is not to be estimated
                if(acceptPathDepth(depth)) {
                    accumulate(FINAL_RENDER_DEPTH1 + i, pixelID, Vec4f(0, 0, 0, 1));
                    auto vertexCount = depth + 1; // number of vertices in a path of depth i
                    for(auto s : range(vertexCount + 1)) {
                        auto strategyOffset = computeBPTStrategyOffset(vertexCount, s);
                        accumulate(BPT_STRATEGY_s0_t2 + strategyOffset, pixelID, Vec4f(0, 0, 0, 1));
                        if(m_bStoreUnweightedImages) {
                            accumulate(BPT_STRATEGY_s0_t2_unweighted + strategyOffset, pixelID, Vec4f(0, 0, 0, 1));
                        }
                    }
                }
            }
            setColor(ADAPTATIVE_SAMPLING_CHANNEL, pixelID, Vec4f(1));

            // Process each sample
            for(auto sampleID = 0u; sampleID < spp; ++sampleID) {
                processSample(threadID, tileID, pixelID, sampleID, x, y);
            }
        } else {
            setColor(ADAPTATIVE_SAMPLING_CHANNEL, pixelID, Vec4f(0, 0, 0, 1));
        }
    });

    // Compute contributions for 1-length eye paths (connection to sensor)
    connectLightVerticesToSensor(threadID, tileID, viewport);

    // Reconstruct final image from depth images, a little bit dirty but it works to avoid improve only some depth images
    // with the path depth mask
    TileProcessingRenderer::processTilePixels(viewport, [&](uint32_t x, uint32_t y) {
        auto pixelID = getPixelIndex(x, y);
        Vec3f estimate = zero<Vec3f>();
        for(auto i = 1u; i <= m_nMaxDepth; ++i) {
            auto depthEstimate = getFramebuffer().getChannel(i)[pixelID];
            if(depthEstimate.w) {
                depthEstimate /= depthEstimate.w;
            }
            estimate += Vec3f(depthEstimate);
        }
        setColor(FINAL_RENDER, pixelID, Vec4f(estimate, 1));
    });
}

void RecursiveMISBDPTRenderer::processSample(uint32_t threadID, uint32_t tileID, uint32_t pixelID, uint32_t sampleID,
                       uint32_t x, uint32_t y) const {
    auto mis = [&](float v) {
        return Mis(v);
    };
    ThreadRNG rng(*this, threadID);

    PixelSensor pixelSensor(getSensor(), Vec2u(x, y), getFramebufferSize());

    auto pixelSample = getPixelSample(threadID, sampleID);
    auto sensorSample = getFloat2(threadID);

    auto fImportanceScale = 1.f / getSppCount();
    BDPTPathVertex eyeVertex(getScene(), pixelSensor, sensorSample, pixelSample, m_nAdaptativeLightPathCount, mis);

    if(eyeVertex.m_Power == zero<Vec3f>() || !eyeVertex.m_fPathPdf) {
        return;
    }

    // The light path to connect with the eye path
    auto pLightPath = m_LightPathBuffer.getSlicePtr(pixelID * getSppCount() + sampleID);

    auto maxEyePathDepth = getMaxEyePathDepth();
    auto maxLightPathDepth = getMaxLightPathDepth();


    auto extendEyePath = [&]() -> bool {
        return eyeVertex.m_nDepth < maxEyePathDepth &&
            eyeVertex.extend(eyeVertex, getScene(), getSppCount(), false, rng, mis);
    };

    // Iterate over an eye path
    do {
        // Intersection with light source
        auto totalLength = eyeVertex.m_nDepth;
        if(acceptPathDepth(totalLength) && totalLength <= m_nMaxDepth) {
            float misWeight = 0.f;
            auto contrib = fImportanceScale * computeEmittedRadiance(eyeVertex, getScene(), m_LightSampler, getSppCount(), mis, misWeight);

            if(isInvalidMeasurementEstimate(contrib)) {
                reportInvalidContrib(threadID, tileID, pixelID, [&]() {
                    debugLog() << "s = " << 0 << ", t = " << (eyeVertex.m_nDepth + 1) << std::endl;
                });
            }

            //accumulate(FINAL_RENDER, pixelID, Vec4f(contrib, 0));
            accumulate(FINAL_RENDER_DEPTH1 + totalLength - 1u, pixelID, Vec4f(contrib, 0));

            auto strategyOffset = computeBPTStrategyOffset(totalLength + 1, 0u);
            accumulate(BPT_STRATEGY_s0_t2 + strategyOffset, pixelID, Vec4f(contrib, 0));
            if(misWeight && m_bStoreUnweightedImages) {
                accumulate(BPT_STRATEGY_s0_t2_unweighted + strategyOffset, pixelID, Vec4f(contrib / misWeight, 0));
            }
        }

        // Connections
        if(eyeVertex.m_Intersection) {
            // Direct illumination
            auto totalLength = eyeVertex.m_nDepth + 1;
            if(acceptPathDepth(totalLength) && totalLength <= m_nMaxDepth) {
                float lightPdf;
                auto pLight = m_LightSampler.sample(getScene(), getFloat(threadID), lightPdf);
                auto s2D = getFloat2(threadID);
                float misWeight = 0.f;
                auto contrib = fImportanceScale *
                        connectVertices(eyeVertex, EmissionVertex(pLight, lightPdf, s2D), getScene(), getSppCount(), mis, misWeight);

                if(isInvalidMeasurementEstimate(contrib)) {
                    reportInvalidContrib(threadID, tileID, pixelID, [&]() {
                        debugLog() << "s = " << 1 << ", t = " << (eyeVertex.m_nDepth + 1) << std::endl;
                    });
                }

                //accumulate(FINAL_RENDER, pixelID, Vec4f(contrib, 0));
                accumulate(FINAL_RENDER_DEPTH1 + totalLength - 1u, pixelID, Vec4f(contrib, 0));

                auto strategyOffset = computeBPTStrategyOffset(totalLength + 1, 1);
                accumulate(BPT_STRATEGY_s0_t2 + strategyOffset, pixelID, Vec4f(contrib, 0));
                if(misWeight && m_bStoreUnweightedImages) {
                    accumulate(BPT_STRATEGY_s0_t2_unweighted + strategyOffset, pixelID, Vec4f(contrib / misWeight, 0));
                }
            }

            // Connection with each light vertex
            for(auto j = 0u; j < maxLightPathDepth; ++j) {
                auto pLightVertex = pLightPath + j;
                auto totalLength = eyeVertex.m_nDepth + pLightVertex->m_nDepth + 1;

                if(pLightVertex->m_fPathPdf > 0.f && acceptPathDepth(totalLength) && totalLength <= m_nMaxDepth) {
                    float misWeight = 0.f;
                    auto contrib = fImportanceScale * connectVertices(eyeVertex, *pLightVertex, getScene(), getSppCount(), mis, misWeight);

                    if(isInvalidMeasurementEstimate(contrib)) {
                        reportInvalidContrib(threadID, tileID, pixelID, [&]() {
                            debugLog() << "s = " << (pLightVertex->m_nDepth + 1) << ", t = " << (eyeVertex.m_nDepth + 1) << std::endl;
                        });
                    }

                    //accumulate(FINAL_RENDER, pixelID, Vec4f(contrib, 0));
                    accumulate(FINAL_RENDER_DEPTH1 + totalLength - 1u, pixelID, Vec4f(contrib, 0));

                    auto strategyOffset = computeBPTStrategyOffset(totalLength + 1, pLightVertex->m_nDepth + 1);
                    accumulate(BPT_STRATEGY_s0_t2 + strategyOffset, pixelID, Vec4f(contrib, 0));
                    if(misWeight && m_bStoreUnweightedImages) {
                        accumulate(BPT_STRATEGY_s0_t2_unweighted + strategyOffset, pixelID, Vec4f(contrib / misWeight, 0));
                    }
                }
            }
        }
    } while(extendEyePath());
}

void RecursiveMISBDPTRenderer::connectLightVerticesToSensor(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const {
    auto rcpPathCount = 1.f / m_nAdaptativeLightPathCount;

    auto mis = [&](float v) {
        return Mis(v);
    };

    for(const auto& directImportanceSample: m_DirectImportanceSampleTilePartitionning[tileID]) {
        auto pLightVertex = &m_LightPathBuffer[directImportanceSample.m_nLightVertexIndex];

        auto totalDepth = 1 + pLightVertex->m_nDepth;
        if(!acceptPathDepth(totalDepth) || totalDepth > m_nMaxDepth) {
            continue;
        }

        auto pixel = directImportanceSample.m_Pixel;
        auto pixelID = BnZ::getPixelIndex(pixel, getFramebufferSize());

        if(!m_AdaptativeSamplingBuffer[pixelID]) {
            continue;
        }

        PixelSensor sensor(getSensor(), pixel, getFramebufferSize());

        float misWeight = 0.f;
        auto contrib = rcpPathCount * connectVertices(*pLightVertex,
                                       SensorVertex(&sensor, directImportanceSample.m_LensSample),
                                       getScene(), m_nAdaptativeLightPathCount, mis, misWeight);

        if(isInvalidMeasurementEstimate(contrib)) {
            reportInvalidContrib(threadID, tileID, pixelID, [&]() {
                debugLog() << "s = " << (pLightVertex->m_nDepth + 1) << ", t = " << 1 << std::endl;
            });
        }

        //accumulate(FINAL_RENDER, pixelID, Vec4f(contrib, 0.f));
        accumulate(FINAL_RENDER_DEPTH1 + totalDepth - 1u, pixelID, Vec4f(contrib, 0.f));

        auto strategyOffset = computeBPTStrategyOffset(totalDepth + 1, pLightVertex->m_nDepth + 1);
        accumulate(BPT_STRATEGY_s0_t2 + strategyOffset, pixelID, Vec4f(contrib, 0));

        if(misWeight && m_bStoreUnweightedImages) {
            accumulate(BPT_STRATEGY_s0_t2_unweighted + strategyOffset, pixelID, Vec4f(contrib / misWeight, 0));
        }
    }
}

uint32_t RecursiveMISBDPTRenderer::getMaxLightPathDepth() const {
    return m_nMaxDepth - 1; // We don't allow intersection with sensor so the light paths have one vertex less
}

uint32_t RecursiveMISBDPTRenderer::getMaxEyePathDepth() const {
    return m_nMaxDepth;
}

void RecursiveMISBDPTRenderer::doExposeIO(GUI& gui) {
    gui.addVarRW(BNZ_GUI_VAR(m_nMaxDepth));
}

void RecursiveMISBDPTRenderer::doLoadSettings(const tinyxml2::XMLElement& xml) {
    serialize(xml, "maxDepth", m_nMaxDepth);
    serialize(xml, "storeUnweightedImages", m_bStoreUnweightedImages);
    serialize(xml, "adaptativeSampling", m_bAdaptativeSampling);
    serialize(xml, "adaptativeThreshold", m_fAdaptativeThreshold);
}

void RecursiveMISBDPTRenderer::doStoreSettings(tinyxml2::XMLElement& xml) const {
    serialize(xml, "maxDepth", m_nMaxDepth);
    serialize(xml, "storeUnweightedImages", m_bStoreUnweightedImages);
    serialize(xml, "adaptativeSampling", m_bAdaptativeSampling);
    serialize(xml, "adaptativeThreshold", m_fAdaptativeThreshold);
}

void RecursiveMISBDPTRenderer::initFramebuffer() {
    addFramebufferChannel("final_render");
    for(auto i : range(m_nMaxDepth)) {
        addFramebufferChannel("depth_" + toString(i + 1));
    }
    for(auto i : range(m_nMaxDepth)) {
        auto depth = i + 1;
        auto vertexCount = depth + 1; // number of vertices in a path of depth i
        for(auto s : range(vertexCount + 1)) {
            addFramebufferChannel("strategy_s_" + toString(s) + "_t_" + toString(vertexCount - s));
        }
    }
    if(m_bStoreUnweightedImages) {
        BPT_STRATEGY_s0_t2_unweighted = getFramebufferChannelCount();
        for(auto i : range(m_nMaxDepth)) {
            auto depth = i + 1;
            auto vertexCount = depth + 1; // number of vertices in a path of depth i
            for(auto s : range(vertexCount + 1)) {
                addFramebufferChannel("strategy_s_" + toString(s) + "_t_" + toString(vertexCount - s) + "_unweighted");
            }
        }
    }
    if(m_bAdaptativeSampling) {
        ADAPTATIVE_SAMPLING_CHANNEL = getFramebufferChannelCount();
        addFramebufferChannel("adaptative_sampling");
    }
}

}
