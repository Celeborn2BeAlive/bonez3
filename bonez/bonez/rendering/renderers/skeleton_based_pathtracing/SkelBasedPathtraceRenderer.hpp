#pragma once

#include "../TileProcessingRenderer.hpp"
#include "../paths.hpp"

#include <bonez/scene/lights/PowerBasedLightSampler.hpp>
#include <bonez/utils/DijkstraAlgorithm.hpp>

#include <bonez/opengl/debug/GLDebugStream.hpp>

namespace BnZ {

class SkelBasedPathtraceRenderer: public TileProcessingRenderer {
public:
    PowerBasedLightSampler m_Sampler;

    uint32_t m_nMaxPathDepth = 2;

    float m_fSkelProb = 0.5f; // Probability of using the skeleton
    float m_fSkeletonStrength = 1.f; // Focus of rays sent toward importance directions

    size_t m_nImportanceRayCount = 32; // Number of rays traced from light sources to compute importance points

    void preprocess() override;

    void processSample(uint32_t threadID, uint32_t pixelID, uint32_t sampleID, uint32_t x, uint32_t y) const;

    void doExposeIO(GUI& gui) override;

    void doLoadSettings(const tinyxml2::XMLElement& xml) override;

    void doStoreSettings(tinyxml2::XMLElement& xml) const override;

    void processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const override;

    void initFramebuffer() override;

    void drawGLData(const ViewerData& viewerData) override;

    GraphNodeIndex getSkeletonNode(const PathVertex& vertex) const;

    Sample3f sampleSkeleton(float r1D, Vec2f r2D, const PathVertex& vertex, GraphNodeIndex nodeIdx,
                            Vec3f& throughtput, uint32_t& sampledEvent) const;

    float pdfSkeleton(PathVertex& vertex, const Vec3f& outDir, GraphNodeIndex nodeIdx) const;

    Sample3f sampleBSDF(float r1D, Vec2f r2D, const PathVertex& vertex,
                        Vec3f& throughtput, uint32_t& sampledEvent) const;

    float pdfBSDF(PathVertex& vertex, const Vec3f& outDir) const;

    void doStoreStatistics() const override;

    enum FramebufferTarget {
        FINAL_RENDER,
        SKELETON_NODE,
        DEPTH1
    };

    struct ImportancePoint {
        Vec3f position;
        float importance;
        ImportancePoint(Vec3f position, float importance):
            position(position), importance(importance) {
        }
    };
    using ImportancePointVector = std::vector<ImportancePoint>;
    std::vector<ImportancePointVector> m_SkeletonImportancePoints;
    MultiDimensionalArray<float, 2> m_SkeletonImportancePointsCDFs;

    GLDebugStreamData m_GLDebugStream; // For drawing importance points with opengl
    // For debug rendering only:
    std::vector<std::vector<RaySample>> m_ImportanceRays; // Ray sampled from light sources to compute importance points
    std::vector<std::vector<size_t>> m_ImportanceLightsIdx;
    std::vector<std::vector<Intersection>> m_ImportanceHits; // Hit points obtained from these rays
    std::vector<GraphNodeIndex> m_ImportanceNodes; // Node associated to these hits
    int m_nSelectedImportanceNodeForDebug = -1; // Selected node to show importance points, if -1 show nothing, if -2 show all
    size_t m_nDebugPathToTraceCount = 4;


    Microseconds m_PreprocessTime;
};


}
