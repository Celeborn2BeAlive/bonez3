#include "SkelBasedPathtraceRenderer.hpp"
#include <bonez/scene/shading/BSDF.hpp>
#include <bonez/sys/DebugLog.hpp>

#include <bonez/scene/sensors/PixelSensor.hpp>

#include <bonez/opengl/debug/GLDebugRenderer.hpp>

#include <bonez/sampling/distribution1d.h>
#include <bonez/scene/lights/AreaLight.hpp>

#include <unordered_map>

namespace BnZ {

void SkelBasedPathtraceRenderer::preprocess() {
    m_Sampler.initFrame(getScene());

    m_SkeletonImportancePoints.clear();
    m_ImportanceRays.clear();
    m_ImportanceHits.clear();
    m_ImportanceNodes.clear();
    m_ImportanceLightsIdx.clear();

    {
        Timer timer;

        const auto& skeleton = *getScene().getCurvSkeleton();
        auto computeImportancePoints = [&](GraphNodeIndex rootNodeIndex, float sourceImportance, const std::vector<Intersection>& points,
                                           ImportancePointVector& importancePoints) {
            if(rootNodeIndex != UNDEFINED_NODE) {
                std::vector<bool> isNodeVisibleFromAPoint;
                for(auto node: range(skeleton.size())) {
                    auto Pnode = skeleton.getNode(node).P;
                    bool isVisible = false;
                    for(auto& I: points) {
                        if(!getScene().occluded(Ray(Pnode, I))) {
                            isVisible = true;
                                break;
                        }
                    }
                    isNodeVisibleFromAPoint.emplace_back(isVisible);
                }

                auto shortestPaths = computeDijkstraShortestPaths(
                            skeleton.getGraph(),
                            rootNodeIndex,
                            [&](GraphNodeIndex n1, GraphNodeIndex n2) {
                                if(isNodeVisibleFromAPoint[n2]) {
                                    return 0.1f;
                                }
                                return 1.f;
                            });

                for(auto nodeIdx: range(skeleton.size())) {
                    auto currentNodeIdx = nodeIdx;
                    auto currentNodePosition = skeleton.getNode(currentNodeIdx).P;
                    auto sumPositions = currentNodePosition;
                    auto countPositions = 1u;

                    auto distance = shortestPaths[nodeIdx].distance;

                    auto nextNodeIdx = shortestPaths[nodeIdx].predecessor;

                    if(nextNodeIdx != UNDEFINED_NODE) {
                        auto nextNodePosition = skeleton.getNode(nextNodeIdx).P;

                        auto makePredecessorShadowRay = [&]() {
                            auto dir = nextNodePosition - currentNodePosition;
                            auto l = length(dir);
                            dir /= l;
                            return Ray(currentNodePosition, dir, 0, l);
                        };

                        while(nextNodeIdx != currentNodeIdx && !getScene().occluded(makePredecessorShadowRay())) {
                            sumPositions += nextNodePosition;
                            ++countPositions;

                            currentNodeIdx = nextNodeIdx;
                            nextNodeIdx = shortestPaths[currentNodeIdx].predecessor;
                            nextNodePosition = skeleton.getNode(nextNodeIdx).P;
                        }
                    } else {
                        distance = 1.f;
                    }

                    float importance = sourceImportance / (distance > 0.f ? sqr(distance) : 1.f);

                    auto barycenter = sumPositions / float(countPositions);
                    importancePoints.emplace_back(ImportancePoint{ barycenter, importance });
                }
            }
        };

        using ImportanceSource = std::tuple<float, std::vector<RaySample>, std::vector<Intersection>, std::vector<size_t>>;

        std::unordered_map<GraphNodeIndex, ImportanceSource> nodes;

        for(auto i: range(m_nImportanceRayCount)) {
            float lightPdf;
            uint32_t lightIdx;
            auto pLight = m_Sampler.sample(getScene(), getFloat(0), lightPdf, &lightIdx);

            if(pLight && lightPdf > 0.f) {
                RaySample raySample;
                float emissionVertexPdf;
                Vec2f emissionVertexRand = getFloat2(0), rayRand = getFloat2(0);
                auto Le = pLight->sampleExitantRay(getScene(), emissionVertexRand, rayRand, raySample, emissionVertexPdf);

                if(raySample.pdf > 0.f && Le != zero<Vec3f>()) {
                    auto I = getScene().intersect(raySample.value);
                    if(I) {
                        auto node = skeleton.getNearestNode(I.P, I.Ns, -raySample.value.dir);
                        if(node != UNDEFINED_NODE) {
                            auto& source = nodes[node];

                            //RaySample shadowRay;
                            //auto LeDir = pLight->sampleDirectIllumination(getScene(), emissionVertexRand, I, shadowRay) * abs(dot(I.Ns, -raySample.value.dir));
                            //LeDir /= shadowRay.pdf;
                            auto LeDir = Le / raySample.pdf;

                            std::get<0>(source) += luminance(LeDir);
                            std::get<1>(source).emplace_back(raySample);
                            std::get<2>(source).emplace_back(I);
                            std::get<3>(source).emplace_back(lightIdx);
                        }
                    }
                }
            }
        }

        std::vector<float> m_ImportanceNodesImportance;
        for(const auto& pair: nodes) {
            m_ImportanceNodes.emplace_back(pair.first);
            m_ImportanceNodesImportance.emplace_back(std::get<0>(pair.second));
            m_ImportanceRays.emplace_back(std::get<1>(pair.second));
            m_ImportanceHits.emplace_back(std::get<2>(pair.second));
            m_ImportanceLightsIdx.emplace_back(std::get<3>(pair.second));
        }

        m_SkeletonImportancePoints.resize(m_ImportanceNodes.size());
        processTasks(m_ImportanceNodes.size(), [&](size_t nodeIndex, size_t threadID) {
            computeImportancePoints(m_ImportanceNodes[nodeIndex], m_ImportanceNodesImportance[nodeIndex], m_ImportanceHits[nodeIndex],
                                    m_SkeletonImportancePoints[nodeIndex]);
        }, getThreadCount());

        m_SkeletonImportancePointsCDFs.resize(getDistribution1DBufferSize(m_SkeletonImportancePoints.size()), skeleton.size());

        // CDF computation
        processTasks(skeleton.size(), [&](size_t nodeIdx, size_t threadID) {
            for(auto i: range(m_SkeletonImportancePoints.size())) {
                m_SkeletonImportancePointsCDFs(i, nodeIdx) = m_SkeletonImportancePoints[i][nodeIdx].importance;
            }
            buildDistribution1D([&](size_t i) {
                return m_SkeletonImportancePointsCDFs(i, nodeIdx);
            }, m_SkeletonImportancePointsCDFs.getSlicePtr(nodeIdx), m_SkeletonImportancePoints.size());
        }, getThreadCount());

        m_PreprocessTime = timer.getMicroEllapsedTime();
    }
}

void SkelBasedPathtraceRenderer::drawGLData(const ViewerData& viewerData) {
    m_GLDebugStream.clearObjects();
    viewerData.debugRenderer.addStream(&m_GLDebugStream);

    if(!m_SkeletonImportancePoints.empty()) {
        const auto& skeleton = *getScene().getCurvSkeleton();

        if(m_nSelectedImportanceNodeForDebug == -2) {
            for(auto& importancePoints: m_SkeletonImportancePoints) {
                for(auto i: range(skeleton.size())) {
                    auto nodePosition = skeleton.getNode(i).P;
                    m_GLDebugStream.addLine(nodePosition, importancePoints[i].position, Vec3f(0,1,0), Vec3f(1,0,0), 5);
                }
            }
        } else if(m_nSelectedImportanceNodeForDebug >= 0 && m_nSelectedImportanceNodeForDebug < m_SkeletonImportancePoints.size()) {
            auto& importancePoints = m_SkeletonImportancePoints[m_nSelectedImportanceNodeForDebug];

            auto importanceNodeIdx = m_ImportanceNodes[m_nSelectedImportanceNodeForDebug];
            auto color = getColor(importanceNodeIdx);

            for(auto i: range(m_ImportanceRays[m_nSelectedImportanceNodeForDebug].size())) {
                const auto& raySample = m_ImportanceRays[m_nSelectedImportanceNodeForDebug][i];
                m_GLDebugStream.addArrow(raySample.value.org, raySample.value.dir,
                                         viewerData.debugRenderer.getArrowLength(),
                                         viewerData.debugRenderer.getArrowBase(),
                                         color);
                const auto& I = m_ImportanceHits[m_nSelectedImportanceNodeForDebug][i];
                m_GLDebugStream.addLine(raySample.value.org, I.P, color, getColor(i), 5);
                m_GLDebugStream.addArrow(I.P, I.Ns,
                                         viewerData.debugRenderer.getArrowLength(),
                                         viewerData.debugRenderer.getArrowBase(),
                                         getColor(i));

                m_GLDebugStream.addLine(skeleton.getNode(importanceNodeIdx).P, I.P, color, getColor(i), 5);
            }

            for(auto i: range(skeleton.size())) {
                auto nodePosition = skeleton.getNode(i).P;
                m_GLDebugStream.addLine(nodePosition, importancePoints[i].position, Vec3f(0,1,0), Vec3f(1,0,0), 5);
            }
        }

        if(viewerData.pickedIntersection) {
            auto nodeIdx = skeleton.getNearestNode(viewerData.pickedIntersection, viewerData.incidentDirection);
            if(nodeIdx != UNDEFINED_NODE) {
                auto nodePosition = skeleton.getNode(nodeIdx).P;
                for(auto& importancePoints: m_SkeletonImportancePoints) {
                    m_GLDebugStream.addLine(nodePosition, importancePoints[nodeIdx].position, Vec3f(0,1,0), Vec3f(1,0,0), 5);
                }
            }

            RandomGenerator rng;
            for(auto i: range(m_nDebugPathToTraceCount)) {
                PathVertex vertex(viewerData.pickedIntersection,
                                  BSDF(viewerData.incidentDirection, viewerData.pickedIntersection, getScene()),
                                  ScatteringEvent::Emission,
                                  Vec3f(1), 1, false);

                while(vertex.length() < m_nMaxPathDepth) {
                    if(!vertex.intersection()) {
                        // Cannot extend from infinity
                        vertex.invalidate();
                        break;
                    }

                    // Multiple importance sampling between BSDF and Skeleton
                    // Returns (samplingEvent, woSample, power)
                    auto samplingResult = [&]() {
                        auto nodeIdx = getSkeletonNode(vertex);

                        if(m_fSkelProb && nodeIdx != UNDEFINED_NODE) {
                            auto pSkel = m_fSkelProb;
                            auto pBSDF = 1.f - pSkel;
                            if(rng.getFloat() <= pSkel) {
                                // Use skeleton
                                Vec3f throughput;
                                uint32_t sampledEvent;
                                auto woSample = sampleSkeleton(rng.getFloat(), rng.getFloat2(), vertex, nodeIdx, throughput, sampledEvent);
                                auto weight = pSkel * woSample.pdf / (pSkel * pdfSkeleton(vertex, woSample.value, nodeIdx) + pBSDF * pdfBSDF(vertex, woSample.value)); // MIS
                                auto power = weight * vertex.power() * throughput / (pSkel * woSample.pdf);

                                return std::make_tuple(sampledEvent, woSample, power);
                            }
                            // Use BSDF
                            Vec3f throughput;
                            uint32_t sampledEvent;
                            auto woSample = sampleBSDF(rng.getFloat(), rng.getFloat2(), vertex, throughput, sampledEvent);
                            auto weight = pBSDF * woSample.pdf / (pBSDF * woSample.pdf + pSkel * pdfSkeleton(vertex, woSample.value, nodeIdx));  // MIS
                            auto power = weight * vertex.power() * throughput / (pBSDF * woSample.pdf);

                            return std::make_tuple(sampledEvent, woSample, power);
                        }
                        // No available node, use BSDF only
                        Vec3f throughput;
                        uint32_t sampledEvent;
                        auto woSample = sampleBSDF(rng.getFloat(), rng.getFloat2(), vertex, throughput, sampledEvent);
                        auto power = vertex.power() * throughput / woSample.pdf;

                        return std::make_tuple(sampledEvent, woSample, power);
                    }();

                    auto sampledEvent = std::get<0>(samplingResult);
                    auto woSample = std::get<1>(samplingResult);
                    auto power = std::get<2>(samplingResult);

                    if(woSample.pdf == 0.f || power == zero<Vec3f>()) {
                        vertex.invalidate();
                        break;
                    }

                    // Extend path
                    auto I = getScene().intersect(Ray(vertex.intersection(), woSample.value));
                    if(!I) {
                        m_GLDebugStream.addArrow(vertex.intersection().P, woSample.value,
                                                 3 * viewerData.debugRenderer.getArrowLength(),
                                                 0.5 * viewerData.debugRenderer.getArrowBase(),
                                                 Vec3f(0,0,1));
                        vertex = PathVertex {
                            I,
                            BSDF(),
                            sampledEvent,
                            power * I.Le,
							uint32_t(vertex.length() + size_t(1)),
                            false
                        };
                    } else {
                        m_GLDebugStream.addArrow(I.P, I.Ns,
                                                 viewerData.debugRenderer.getArrowLength(),
                                                 viewerData.debugRenderer.getArrowBase(),
                                                 Vec3f(0,0,1));
                        m_GLDebugStream.addLine(vertex.intersection().P, I.P, Vec3f(0,0,1), Vec3f(0,0,1), 5);
                        vertex = PathVertex(I,
                                            BSDF(-woSample.value, I, getScene()),
                                            sampledEvent,
                                            power,
                                            vertex.length() + 1u,
                                            false);
                    }
                }
            }
        }
    }
}

// One importance point only
//void SkelBasedPathtraceRenderer::processSample(uint32_t threadID, uint32_t pixelID, uint32_t sampleID, uint32_t x, uint32_t y) const {
//    for(auto i: range(getFramebufferChannelCount())) {
//        accumulate(i, pixelID, Vec4f(0.f, 0.f, 0.f, 1.f));
//    }

//    PixelSensor pixelSensor(getSensor(), Vec2u(x, y), getFramebufferSize());

//    auto pixelSample = getPixelSample(threadID, sampleID);
//    auto lensSample = getFloat2(threadID);

//    auto vertex = BnZ::samplePrimaryEyeVertex<PathVertex>(getScene(), pixelSensor,
//        lensSample, pixelSample);

//    if (vertex.length() == 0u && acceptPathDepth(1)) {
//        auto contrib = vertex.power();
//        // Hit on environment light, if exists
//        accumulate(FINAL_RENDER, pixelID, Vec4f(contrib, 0));
//        accumulate(DEPTH1, pixelID, Vec4f(contrib, 0));
//        return;
//    }

//    Vec3f L = Vec3f(0.f);

//    if(acceptPathDepth(1)) {
//        accumulate(DEPTH1, pixelID, Vec4f(vertex.intersection().Le, 0.f));
//        L += vertex.power() * vertex.intersection().Le;
//    }

//    float lightPdf;
//    auto pLight = m_Sampler.sample(getScene(), getFloat(threadID), lightPdf);

//    ThreadRNG rng(*this, threadID);
//    while(vertex.length() < m_nMaxPathDepth) {
//        // Next event estimation
//        if(vertex.intersection() && vertex.length() < m_nMaxPathDepth && acceptPathDepth(vertex.length() + 1)) {
//            RaySample shadowRay;
//            auto Le = pLight->sampleDirectIllumination(getScene(), getFloat2(threadID), vertex.intersection(), shadowRay);

//            if (Le != zero<Vec3f>() && shadowRay.pdf > 0.f && !getScene().occluded(shadowRay.value)) {
//                shadowRay.pdf *= lightPdf;
//                auto contrib = vertex.power() * vertex.bsdf().eval(shadowRay.value.dir) * abs(dot(shadowRay.value.dir, vertex.intersection().Ns))
//                    * Le / shadowRay.pdf;
//                accumulate(DEPTH1 + vertex.length(), pixelID, Vec4f(contrib, 0.f));
//                L += contrib;
//            }
//        }

//        // If the scattering was specular, add Le
//        if((vertex.sampledEvent() & ScatteringEvent::Specular) && acceptPathDepth(vertex.length())) {
//            auto contrib = vertex.power() * vertex.intersection().Le;
//            accumulate(DEPTH1 + vertex.length() - 1u, pixelID, Vec4f(contrib, 0.f));
//            L += contrib;
//        }

//        if(!vertex.intersection()) {
//            // Cannot extend from infinity
//            vertex.invalidate();
//            break;
//        }

//        uint32_t sampledEvent;
//        Sample3f woSample;
//        Vec3f power;

//        auto nodeIdx = getSkeletonNode(vertex);
//        if(nodeIdx != UNDEFINED_NODE) {
//            if(vertex.length() == 1) {
//                accumulate(SKELETON_NODE, pixelID, Vec4f(getColor(nodeIdx), 0.f));
//            }

//            auto pSkel = m_fSkelProb;
//            auto pBSDF = 1.f - pSkel;
//            if(rng.getFloat() <= pSkel) {
//                // Use skeleton
//                Vec3f throughput;
//                woSample = sampleSkeleton(rng, vertex, nodeIdx, throughput, sampledEvent);
//                auto weight = pSkel * woSample.pdf / (pSkel * woSample.pdf + pBSDF * pdfBSDF(vertex, woSample.value)); // MIS
//                power = weight * vertex.power() * throughput / (pSkel * woSample.pdf);


//            } else {
//                // Use BSDF
//                Vec3f throughput;
//                woSample = sampleBSDF(rng, vertex, throughput, sampledEvent);
//                auto weight = pBSDF * woSample.pdf / (pBSDF * woSample.pdf + pSkel * pdfSkeleton(vertex, woSample.value, nodeIdx));  // MIS
//                power = weight * vertex.power() * throughput / (pBSDF * woSample.pdf);
//            }
//        } else {
//            Vec3f throughput;
//            woSample = sampleBSDF(rng, vertex, throughput, sampledEvent);
//            power = vertex.power() * throughput / woSample.pdf;
//        }

//        if(woSample.pdf == 0.f || power == zero<Vec3f>()) {
//            vertex.invalidate();
//            break;
//        }

//        // Extend path
//        auto I = getScene().intersect(Ray(vertex.intersection(), woSample.value));
//        if(!I) {
//            vertex = PathVertex {
//                I,
//                BSDF(),
//                sampledEvent,
//                power * I.Le,
//                vertex.length() + 1u,
//                false
//            };
//        } else {
//            vertex = PathVertex(I,
//                                BSDF(-woSample.value, I, getScene()),
//                                sampledEvent,
//                                power,
//                                vertex.length() + 1u,
//                                false);
//        }
//    }

//    accumulate(FINAL_RENDER, pixelID, Vec4f(L, 0.f));
//}

//Sample3f SkelBasedPathtraceRenderer::sampleSkeleton(ThreadRNG& rng, const PathVertex& vertex, GraphNodeIndex nodeIdx,
//                        Vec3f& throughtput, uint32_t& sampledEvent) const {
//    auto importancePointSetIdx =
//            clamp(size_t(rng.getFloat() * m_PointLightImportancePoints.size()),
//                  size_t(0),
//                  m_PointLightImportancePoints.size() - 1);
//    auto importancePoint = m_PointLightImportancePoints[importancePointSetIdx][nodeIdx];
//    auto importanceDirection = normalize(importancePoint.position - vertex.intersection().P);

//    sampledEvent = ScatteringEvent::Other;
//    auto woSample = powerCosineSampleHemisphere(rng.getFloat(), rng.getFloat(), importanceDirection, m_fSkeletonStrength);
//    float cosThetaOutDir;
//    throughtput = vertex.bsdf().eval(woSample.value, cosThetaOutDir);
//    throughtput *= abs(cosThetaOutDir);

//    return woSample;
//}

//float SkelBasedPathtraceRenderer::pdfSkeleton(PathVertex& vertex, const Vec3f& outDir, GraphNodeIndex nodeIdx) const {
//    auto importancePointSetIdx =
//            clamp(size_t(0),
//                  size_t(0),
//                  m_PointLightImportancePoints.size() - 1);
//    auto importancePoint = m_PointLightImportancePoints[importancePointSetIdx][nodeIdx];
//    auto importanceDirection = normalize(importancePoint.position - vertex.intersection().P);

//    return powerCosineSampleHemispherePDF(outDir, importanceDirection, m_fSkeletonStrength);
//}

void SkelBasedPathtraceRenderer::processSample(uint32_t threadID, uint32_t pixelID, uint32_t sampleID, uint32_t x, uint32_t y) const {
    for(auto i: range(getFramebufferChannelCount())) {
        accumulate(i, pixelID, Vec4f(0.f, 0.f, 0.f, 1.f));
    }

    PixelSensor pixelSensor(getSensor(), Vec2u(x, y), getFramebufferSize());

    auto pixelSample = getPixelSample(threadID, sampleID);
    auto lensSample = getFloat2(threadID);

    auto vertex = BnZ::samplePrimaryEyeVertex<PathVertex>(getScene(), pixelSensor,
        lensSample, pixelSample);

    if (vertex.length() == 0u && acceptPathDepth(1)) {
        auto contrib = vertex.power();
        // Hit on environment light, if exists
        accumulate(FINAL_RENDER, pixelID, Vec4f(contrib, 0));
        accumulate(DEPTH1, pixelID, Vec4f(contrib, 0));
        return;
    }

    Vec3f L = Vec3f(0.f);

    if(acceptPathDepth(1)) {
        accumulate(DEPTH1, pixelID, Vec4f(vertex.intersection().Le, 0.f));
        L += vertex.power() * vertex.intersection().Le;
    }

    float lightPdf;
    auto pLight = m_Sampler.sample(getScene(), getFloat(threadID), lightPdf);

    ThreadRNG rng(*this, threadID);
    while(vertex.length() < m_nMaxPathDepth) {
        // Next event estimation
        if(vertex.intersection() && vertex.length() < m_nMaxPathDepth && acceptPathDepth(vertex.length() + 1)) {
            RaySample shadowRay;
            auto Le = pLight->sampleDirectIllumination(getScene(), getFloat2(threadID), vertex.intersection(), shadowRay);

            if (Le != zero<Vec3f>() && shadowRay.pdf > 0.f && !getScene().occluded(shadowRay.value)) {
                shadowRay.pdf *= lightPdf;
                auto contrib = vertex.power() * vertex.bsdf().eval(shadowRay.value.dir) * abs(dot(shadowRay.value.dir, vertex.intersection().Ns))
                    * Le / shadowRay.pdf;
                accumulate(DEPTH1 + vertex.length(), pixelID, Vec4f(contrib, 0.f));
                L += contrib;
            }
        }

        // If the scattering was specular, add Le
        if((vertex.sampledEvent() & ScatteringEvent::Specular) && acceptPathDepth(vertex.length())) {
            auto contrib = vertex.power() * vertex.intersection().Le;
            accumulate(DEPTH1 + vertex.length() - 1u, pixelID, Vec4f(contrib, 0.f));
            L += contrib;
        }

        if(!vertex.intersection()) {
            // Cannot extend from infinity
            vertex.invalidate();
            break;
        }

        // Multiple importance sampling between BSDF and Skeleton
        // Returns (samplingEvent, woSample, power)
        auto samplingResult = [&]() {
            auto nodeIdx = getSkeletonNode(vertex);

            if(m_fSkelProb && nodeIdx != UNDEFINED_NODE) {
                if(vertex.length() == 1) {
                    accumulate(SKELETON_NODE, pixelID, Vec4f(getColor(nodeIdx), 0.f));
                }

                auto pSkel = m_fSkelProb;
                auto pBSDF = 1.f - pSkel;
                if(rng.getFloat() <= pSkel) {
                    // Use skeleton
                    Vec3f throughput;
                    uint32_t sampledEvent;
                    auto woSample = sampleSkeleton(rng.getFloat(), rng.getFloat2(), vertex, nodeIdx, throughput, sampledEvent);
                    auto weight = pSkel * woSample.pdf / (pSkel * pdfSkeleton(vertex, woSample.value, nodeIdx) + pBSDF * pdfBSDF(vertex, woSample.value)); // MIS
                    auto power = weight * vertex.power() * throughput / (pSkel * woSample.pdf);

                    return std::make_tuple(sampledEvent, woSample, power);
                }
                // Use BSDF
                Vec3f throughput;
                uint32_t sampledEvent;
                auto woSample = sampleBSDF(rng.getFloat(), rng.getFloat2(), vertex, throughput, sampledEvent);
                auto weight = pBSDF * woSample.pdf / (pBSDF * woSample.pdf + pSkel * pdfSkeleton(vertex, woSample.value, nodeIdx));  // MIS
                auto power = weight * vertex.power() * throughput / (pBSDF * woSample.pdf);

                return std::make_tuple(sampledEvent, woSample, power);
            }
            // No available node, use BSDF only
            Vec3f throughput;
            uint32_t sampledEvent;
            auto woSample = sampleBSDF(rng.getFloat(), rng.getFloat2(), vertex, throughput, sampledEvent);
            auto power = vertex.power() * throughput / woSample.pdf;

            return std::make_tuple(sampledEvent, woSample, power);
        }();

        auto sampledEvent = std::get<0>(samplingResult);
        auto woSample = std::get<1>(samplingResult);
        auto power = std::get<2>(samplingResult);

        if(woSample.pdf == 0.f || power == zero<Vec3f>()) {
            vertex.invalidate();
            break;
        }

        // Extend path
        auto I = getScene().intersect(Ray(vertex.intersection(), woSample.value));
        if(!I) {
            vertex = PathVertex {
                I,
                BSDF(),
                sampledEvent,
                power * I.Le,
                uint32_t(vertex.length() + size_t(1)),
                false
            };
        } else {
            vertex = PathVertex(I,
                                BSDF(-woSample.value, I, getScene()),
                                sampledEvent,
                                power,
                                vertex.length() + 1u,
                                false);
        }
    }

    accumulate(FINAL_RENDER, pixelID, Vec4f(L, 0.f));
}

Sample3f SkelBasedPathtraceRenderer::sampleSkeleton(float r1D, Vec2f r2D, const PathVertex& vertex, GraphNodeIndex nodeIdx,
                        Vec3f& throughtput, uint32_t& sampledEvent) const {
    // Importance sampling of an importance point
    auto importancePointSample = sampleDiscreteDistribution1D(m_SkeletonImportancePointsCDFs.getSlicePtr(nodeIdx), m_SkeletonImportancePoints.size(), r1D);
    auto importancePointIdx = importancePointSample.value;
    auto importancePointPdf = importancePointSample.pdf;

    // Uniform sampling of an importance point
//    auto importancePointIdx =
//            clamp(size_t(r1D * m_PointLightImportancePoints.size()),
//                  size_t(0),
//                  m_PointLightImportancePoints.size() - 1);
//    auto importancePointPdf = 1.f / m_PointLightImportancePoints.size();

    auto importancePoint = m_SkeletonImportancePoints[importancePointIdx][nodeIdx];
    auto importanceDirection = normalize(importancePoint.position - vertex.intersection().P);

    sampledEvent = ScatteringEvent::Other;
    auto woSample = powerCosineSampleHemisphere(r2D.x, r2D.y, importanceDirection, m_fSkeletonStrength);
    woSample.pdf *= importancePointPdf;

    float cosThetaOutDir;
    throughtput = vertex.bsdf().eval(woSample.value, cosThetaOutDir);
    throughtput *= abs(cosThetaOutDir);

    return woSample;
}

float SkelBasedPathtraceRenderer::pdfSkeleton(PathVertex& vertex, const Vec3f& outDir, GraphNodeIndex nodeIdx) const {
    float pdf = 0.f;
    for(auto i: range(m_SkeletonImportancePoints.size())) {
        const auto& importancePointVector = m_SkeletonImportancePoints[i];
        auto importancePoint = importancePointVector[nodeIdx];
        auto importanceDirection = normalize(importancePoint.position - vertex.intersection().P);

//         auto importancePointPdf = 1.f / m_PointLightImportancePoints.size();
        auto importancePointPdf = pdfDiscreteDistribution1D(m_SkeletonImportancePointsCDFs.getSlicePtr(nodeIdx), i);

        pdf += importancePointPdf * powerCosineSampleHemispherePDF(outDir, importanceDirection, m_fSkeletonStrength);
    }
    return pdf;
}

Sample3f SkelBasedPathtraceRenderer::sampleBSDF(float r1D, Vec2f r2D, const PathVertex& vertex,
                    Vec3f& throughtput, uint32_t& sampledEvent) const {
    Sample3f woSample;
    float cosThetaOutDir;
    throughtput = vertex.bsdf().sample(Vec3f(r1D, r2D), woSample, cosThetaOutDir, &sampledEvent);
    throughtput *= abs(cosThetaOutDir);

    return woSample;
}

float SkelBasedPathtraceRenderer::pdfBSDF(PathVertex& vertex, const Vec3f& outDir) const {
    return vertex.bsdf().pdf(outDir);
}

GraphNodeIndex SkelBasedPathtraceRenderer::getSkeletonNode(const PathVertex& vertex) const {
    if(vertex.bsdf().isDelta()) {
        return UNDEFINED_NODE;
    }

    if(m_SkeletonImportancePoints.empty()) {
        return UNDEFINED_NODE;
    }

    auto nodeIdx = getScene().getCurvSkeleton()->getNearestNode(vertex.intersection(), vertex.bsdf().getIncidentDirection());

    if(nodeIdx != UNDEFINED_NODE) {
        return isDiscreteCDF(m_SkeletonImportancePointsCDFs.getSlicePtr(nodeIdx), m_SkeletonImportancePoints.size()) ? nodeIdx : UNDEFINED_NODE;
    }
    return UNDEFINED_NODE;
}

void SkelBasedPathtraceRenderer::doExposeIO(GUI& gui) {
    gui.addVarRW(BNZ_GUI_VAR(m_nMaxPathDepth));
    gui.addVarRW(BNZ_GUI_VAR(m_fSkeletonStrength));
    gui.addVarRW(BNZ_GUI_VAR(m_fSkelProb));
    gui.addVarRW(BNZ_GUI_VAR(m_nImportanceRayCount));

    gui.addSeparator();
    gui.addText("Debug:");
    gui.addVarRW(BNZ_GUI_VAR(m_nSelectedImportanceNodeForDebug));
    gui.addVarRW(BNZ_GUI_VAR(m_nDebugPathToTraceCount));
    m_nSelectedImportanceNodeForDebug = min(m_nSelectedImportanceNodeForDebug,
                                            m_ImportanceNodes.empty() ? 0 : int(m_ImportanceNodes.size() - 1));
    if(m_nSelectedImportanceNodeForDebug >= 0) {
        for(auto lightIdx: m_ImportanceLightsIdx[m_nSelectedImportanceNodeForDebug]) {
            if(lightIdx < getScene().getLightContainer().size()) {
                gui.addValue("LightIdx", lightIdx);
                UpdateFlag flag;
                getScene().getLightContainer().getLight(lightIdx)->exposeIO(gui, flag);
                gui.addSeparator();
            }
        }
    }
}

void SkelBasedPathtraceRenderer::doStoreStatistics() const {
    if(auto pStats = getStatisticsOutput()) {
        serialize(*pStats, "PreprocessTime", m_PreprocessTime);
        serialize(*pStats, "ImportancePointPerNodeCount", m_SkeletonImportancePoints.size());
    }
}

void SkelBasedPathtraceRenderer::doLoadSettings(const tinyxml2::XMLElement& xml) {
    serialize(xml, "maxDepth", m_nMaxPathDepth);
    serialize(xml, "skeletonStrength", m_fSkeletonStrength);
    serialize(xml, "skeletonProbability", m_fSkelProb);
    serialize(xml, "importanceRayCount", m_nImportanceRayCount);
}

void SkelBasedPathtraceRenderer::doStoreSettings(tinyxml2::XMLElement& xml) const {
    serialize(xml, "maxDepth", m_nMaxPathDepth);
    serialize(xml, "skeletonStrength", m_fSkeletonStrength);
    serialize(xml, "skeletonProbability", m_fSkelProb);
    serialize(xml, "importanceRayCount", m_nImportanceRayCount);
}

void SkelBasedPathtraceRenderer::processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const {
    processTileSamples(viewport, [this, threadID](uint32_t x, uint32_t y, uint32_t pixelID, uint32_t sampleID) {
        processSample(threadID, pixelID, sampleID, x, y);
    });
}

void SkelBasedPathtraceRenderer::initFramebuffer() {
    addFramebufferChannel("final_render");
    addFramebufferChannel("skeleton_node");
    for(auto i : range(m_nMaxPathDepth)) {
        addFramebufferChannel("depth_" + toString(i + 1));
    }
}

}
