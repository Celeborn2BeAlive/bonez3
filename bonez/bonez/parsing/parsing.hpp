#pragma once

#include <unordered_map>

#include <bonez/types.hpp>
#include <bonez/scene/lights/LightContainer.hpp>
#include <bonez/scene/sensors/ProjectiveCamera.hpp>

#include <bonez/scene/lights/PointLight.hpp>
#include <bonez/scene/lights/DirectionalLight.hpp>
#include <bonez/scene/lights/EnvironmentLight.hpp>

#include <bonez/sys/time.hpp>

#include "tinyxml/tinyxml2.h"

namespace BnZ {

inline tinyxml2::XMLNode* cloneXMLNode(const tinyxml2::XMLNode* pNode, tinyxml2::XMLDocument* pDocument) {
    if(!pNode) {
        return nullptr;
    }
    auto pClone = pNode->ShallowClone(pDocument);
    for(auto pChild = pNode->FirstChild(); pChild; pChild = pChild->NextSibling()) {
        pClone->InsertEndChild(cloneXMLNode(pChild, pDocument));
    }
    return pClone;
}

template<typename T>
tinyxml2::XMLElement* setChildAttribute(tinyxml2::XMLElement& elt, const char* name, const T& value);

inline bool getAttribute(const tinyxml2::XMLElement& elt, const char* name,
                         float& value) {
    return tinyxml2::XML_NO_ERROR == elt.QueryFloatAttribute(name, &value);
}

inline bool getAttribute(const tinyxml2::XMLElement& elt, const char* name,
                         double& value) {
    return tinyxml2::XML_NO_ERROR == elt.QueryDoubleAttribute(name, &value);
}

inline bool getAttribute(const tinyxml2::XMLElement& elt, const char* name,
                         int& value) {
    return tinyxml2::XML_NO_ERROR == elt.QueryIntAttribute(name, &value);
}

inline bool getAttribute(const tinyxml2::XMLElement& elt, const char* name,
                         uint32_t& value) {
    return tinyxml2::XML_NO_ERROR == elt.QueryUnsignedAttribute(name, &value);
}

inline bool getAttribute(const tinyxml2::XMLElement& elt, const char* name,
                         bool& value) {
    return tinyxml2::XML_NO_ERROR == elt.QueryBoolAttribute(name, &value);
}

inline bool getAttribute(const tinyxml2::XMLElement& elt, const char* name,
                         std::string& value) {
    auto ptr = elt.Attribute(name);
    if(ptr) {
        value = ptr;
        return true;
    }
    return false;
}

template<typename T>
inline bool getAttribute(const tinyxml2::XMLElement& elt, const char* name,
                         T& value) {
    std::string strValue;
    auto r = getAttribute(elt, name, strValue);
    std::stringstream ss;
    ss << strValue;
    ss >> value;
    return r;
}

template<typename T>
inline void setAttribute(tinyxml2::XMLElement& elt, const char* name, const T& value) {
    elt.SetAttribute(name, value);
}

inline void setAttribute(tinyxml2::XMLElement& elt, const char* name,
                         const std::string& value) {
    elt.SetAttribute(name, value.c_str());
}

inline void setAttribute(tinyxml2::XMLElement& elt, const char* name,
                         uint64_t value) {
    setAttribute(elt, name, toString(value));
}

inline void setAttribute(tinyxml2::XMLElement& elt, const char* name,
                         int64_t value) {
    setAttribute(elt, name, toString(value));
}

template<typename T>
inline bool getValue(const tinyxml2::XMLElement& elt, T& value) {
    return getAttribute(elt, "value", value);
}

template<typename T, glm::precision p>
inline bool getValue(const tinyxml2::XMLElement& elt, glm::tvec2<T, p>& value) {
    bool returnValue = true;
    returnValue = returnValue && getAttribute(elt, "x", value.x);
    returnValue = returnValue && getAttribute(elt, "y", value.y);
    return returnValue;
}

template<typename T, glm::precision p>
inline bool getValue(const tinyxml2::XMLElement& elt, glm::tvec3<T, p>& value) {
    bool returnValue = true;
    returnValue = returnValue && getAttribute(elt, "x", value.x);
    returnValue = returnValue && getAttribute(elt, "y", value.y);
    returnValue = returnValue && getAttribute(elt, "z", value.z);
    return returnValue;
}

template<typename T, glm::precision p>
inline bool getValue(const tinyxml2::XMLElement& elt, glm::tvec4<T, p>& value) {
    bool returnValue = true;
    returnValue = returnValue && getAttribute(elt, "x", value.x);
    returnValue = returnValue && getAttribute(elt, "y", value.y);
    returnValue = returnValue && getAttribute(elt, "z", value.z);
    returnValue = returnValue && getAttribute(elt, "w", value.w);
    return returnValue;
}

template<typename T>
inline bool getValue(const tinyxml2::XMLElement& elt, std::vector<T>& values) {
    values.clear();
    auto i = 0u;
    auto pChildElement = elt.FirstChildElement("v0");
    while(pChildElement) {
        T value;
        if(!getValue(*pChildElement, value)) {
            return false;
        }
        values.emplace_back(value);
        ++i;
        pChildElement = pChildElement->NextSiblingElement((std::string("v") + toString(i)).c_str());
    }
    return true;
}

template<typename T>
inline bool getValue(const tinyxml2::XMLElement& elt, std::unordered_map<std::string, T>& values) {
    values.clear();
    for(auto pChildElement = elt.FirstChildElement(); pChildElement; pChildElement = pChildElement->NextSiblingElement()) {
        if(!getValue(*pChildElement, values[pChildElement->Name()])) {
            return false;
        }
    }
    return true;
}

inline bool getValue(const tinyxml2::XMLElement& elt, Microseconds& microseconds) {
    uint64_t count;
    auto pChildElement = elt.FirstChildElement("Microseconds");
    if(pChildElement && getValue(*pChildElement, count)) {
        microseconds = Microseconds { count };
        return true;
    }
    return false;
}

template<typename T>
inline void setValue(tinyxml2::XMLElement& elt, const T& value) {
    setAttribute(elt, "value", value);
}

template<typename T, glm::precision p>
inline void setValue(tinyxml2::XMLElement& elt, const glm::tvec2<T, p>& value) {
    setAttribute(elt, "x", value.x);
    setAttribute(elt, "y", value.y);
}

template<typename T, glm::precision p>
inline void setValue(tinyxml2::XMLElement& elt, const glm::tvec3<T, p>& value) {
    setAttribute(elt, "x", value.x);
    setAttribute(elt, "y", value.y);
    setAttribute(elt, "z", value.z);
}

template<typename T, glm::precision p>
inline void setValue(tinyxml2::XMLElement& elt, const glm::tvec4<T, p>& value) {
    setAttribute(elt, "x", value.x);
    setAttribute(elt, "y", value.y);
    setAttribute(elt, "z", value.z);
    setAttribute(elt, "w", value.w);
}

template<typename T>
inline void setValue(tinyxml2::XMLElement& elt, const std::unordered_map<std::string, T>& values) {
    elt.DeleteChildren();
    for(const auto& pair: values) {
        setChildAttribute(elt, pair.first.c_str(), pair.second); // Not a good idea: what if the key contains space !!
    }
}

template<typename T>
inline void setValue(tinyxml2::XMLElement& elt, const std::vector<T>& values) {
    elt.DeleteChildren();
    for(const auto& i: range(size(values))) {
        setChildAttribute(elt, (std::string("v") + toString(i)).c_str(), values[i]);
    }
}

inline void setValue(tinyxml2::XMLElement& elt, const Microseconds& microseconds) {
    elt.DeleteChildren();
    setChildAttribute(elt, "Microseconds", microseconds.count());
    setChildAttribute(elt, "Milliseconds", us2ms(microseconds));
    setChildAttribute(elt, "Seconds", us2sec(microseconds));
}

inline void setValue(tinyxml2::XMLElement& elt, const TaskTimer& timer) {
    elt.DeleteChildren();

    auto taskDurations = computeTaskDurations<Microseconds>(timer);
    auto totalTime = std::accumulate(begin(taskDurations), end(taskDurations), Microseconds(0), std::plus<Microseconds>());
    setChildAttribute(elt, "TotalTime", totalTime);
    for(auto taskID: range(timer.getTaskCount())) {
        auto percentage = 100. * taskDurations[taskID].count() / totalTime.count();
        auto pChildElement = setChildAttribute(elt, timer.getTaskName(taskID).c_str(), taskDurations[taskID]);
        setAttribute(*pChildElement, "percentage", percentage);
    }
}


// Again not a really good idea: what if the name contains space !!
// Alternatives: <TYPE name="" value="" />
//				 <TYPE name="">value</TYPE>    : better i think, allows composite types
// Do not always need a name, <TYPE>value</TYPE> should be allowed --> cf vectors
//  For vectors:
// <Vector valueType="Int"><Int name="0">42</INT>....   Perhaps ignore name ? the order is specified by the XML
// 
// For maps:
// Or <Map *name* keyType="" valueType=""><Entry><KEYTYPE>key</KEYTYPE><VALUETYPE>value</VALUETYPE></Entry>...
// Other ideas:
//		Objects, or PropertyTree   -> maps where the key is always a string with no spaces, can be represented as:
//			<PropertyTree name=""><TYPE name="property1">value</TYPE><TYPE name="property2">value2</TYPE>...
// sources from other files:
//	<TYPE *name* src="relative_path_to_file" />
// With overloading: <TYPE *name* src="path"><Childoverload1 /><Childoverload1 />...</TYPE>
// Problem: what if the child overload has childs, replace them ? require more overload ?
//
// Example:
// File1.xml: define a renderer with generic settings
// <PropertyTree name="renderJob"> // Actually do not need a name, specified by includer
//		<PropertyTree name="renderer">
//			<string name="name">PathtraceRenderer</string>
//			<uint name="maxDepth">6</uint>
//			<PropertyTree name="sampler">
//				<uint name="spp">8</uint>
//				<string name="filter">gaussian blur</string>
//			</PropertyTree>
//		</PropertyTree>
// </PropertyTree>
//
// File2.xml: render images with the generic renderer but want to change parameters for each
// <Vector name="renderJobs" type="PropertyTree">
//		<PropertyTree src="File1.xml" /> // default render
//		<PropertyTree src="File1.xml">
//			<uint name=".renderer.sampler.spp">16</uint>     // Does not seem too bad, but need to think more about other cases, what if the src is a vector ?
//   Also, respecifying the type seems a bit like DRY problem....
//   Perhaps:   <set name=".renderer.sampler.spp">16</set>
//   Then we can also introduce: <get name=".renderer.sample.spp">16</set>
//       And then:     <auto name="copySpp"><get name=".renderer.sample.spp">16</set></auto>    => parser side: read auto type, need the content, red get, try to load the tag and deduce de type, then read the value
// <Vector>
// 
// name or id ?? 
//  access to child:   ${global_name}.key1.key2     if ${global_name} = empty, refer to the parent of the element; perhaps use a syntax "a la shell" => ./key1/key2; then it becomes possible to write ../../key1
//  access to vector elements:  ${global_name}.key1[i]    same as cpp, if ${global_name} = empty ==> .[0] .[42], or without the "." ?
//
// vector DRY:   <vector type=""><auto>v0</auto>....
//    easier syntax for simple types, such as int, float, string,...  <vector type="">v0, v1, v2, ...</vector>
//    even vec3, vec4, ... with (x,y,z), (x,y,z), ...
template<typename T>
inline tinyxml2::XMLElement* setChildAttribute(tinyxml2::XMLElement& elt, const char* name, const T& value) {
    auto pChildElement = elt.FirstChildElement(name);
    if(!pChildElement) {
        pChildElement = elt.GetDocument()->NewElement(name);
        elt.InsertEndChild(pChildElement);
    }
    setValue(*pChildElement, value);
    return pChildElement;
}

template<typename T>
inline const tinyxml2::XMLElement* getChildAttribute(const tinyxml2::XMLElement& elt, const char* name, T& value) {
    auto pChildElement = elt.FirstChildElement(name);
    if(pChildElement) {
        getValue(*pChildElement, value);
    }
    return pChildElement;
}

Vec3f loadVector(const tinyxml2::XMLElement& elt);

Vec3f loadColor(const tinyxml2::XMLElement& elt, const Vec3f& defaultColor = zero<Vec3f>());

void storeVector(tinyxml2::XMLElement& elt, const Vec3f& v);

void storeColor(tinyxml2::XMLElement& elt, const Vec3f& c);

PointLight loadPointLight(
        const tinyxml2::XMLElement& elt);

DirectionalLight loadDirectionalLight(
        const tinyxml2::XMLElement& elt);

void loadLights(Scene& scene, const tinyxml2::XMLElement* pLightElt);

void storeLight(
        tinyxml2::XMLElement& elt,
        const PointLight& light);

void storeLight(
        tinyxml2::XMLElement& elt,
        const DirectionalLight& light);

void storeLights(const LightContainer& lightContainer, tinyxml2::XMLElement& lightElt);

void loadPerspectiveCamera(
        const tinyxml2::XMLElement& elt,
        Vec3f& eye, Vec3f& point, Vec3f& up,
        float& fovY);

void storePerspectiveCamera(
        tinyxml2::XMLElement& elt,
        const ProjectiveCamera& camera);

template<typename T>
const tinyxml2::XMLElement* serialize(const tinyxml2::XMLElement& elt, const char* name, T& value) {
    return getChildAttribute(elt, name, value);
}

template<typename T>
tinyxml2::XMLElement* serialize(tinyxml2::XMLElement& elt, const char* name, const T& value) {
    return setChildAttribute(elt, name, value);
}

}
