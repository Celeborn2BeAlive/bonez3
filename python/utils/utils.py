import sys
import matplotlib.pyplot as plt
import numpy as np
import string
import skimage
import skimage.io
import skimage.viewer
import sys
import functools
import colorsys

def is_float(string):
    try:
        float(string)
        return True
    except ValueError:
        return False

def load_values(file):
    f = open(file)
    lines = [ line for line in f ]
    values = [ float(x) for x in lines[0].split(' ') if is_float(x) ]
    return np.array(values, dtype='float')

def get_color(n, max_n):
    return colorsys.hsv_to_rgb(float(n) / max_n, 1, 0.5)

error_label = {
    "nrmse": "NRMSE",
    "rmse": "RMSE",
    "mse": "L2 error",
    "mae": "L1 error",
    "absoluteError": "L1 error"
}

class ResultStatistics:
    def __init__(self, index, index_in_list, nb_index, basedir = "", label = ""):
        self.index = index
        self.color = get_color(index_in_list, nb_index)
        self.basename = str(index).zfill(3)
        if not label:
            self.label = self.basename
        else:
            self.label = label
        self.error_values = {
                "nrmse": load_values(basedir + self.basename + ".nrmseFloat"),
                "rmse": load_values(basedir + self.basename + ".rmseFloat"),
                #"mae": load_values(basedir + self.basename + ".maeFloat"),
                "absoluteError": load_values(basedir + self.basename + ".absErrorFloat") # same as previous, old convention
            }
        self.error_values["mse"] = np.square(self.error_values["rmse"])
        self.processing_times = load_values(basedir + self.basename + ".processingTimes") / 1000.
    
    def get_error_values(self, error_type):
        return self.error_values[error_type]

    def plot_error_curve_of_time(self, error_type, plot):
        plot.plot(self.processing_times, self.error_values[error_type], color=self.color, label = self.label, linewidth=1.5)
        
    def plot_error_curve_of_iterations(self, error_type, plot):
        plot.plot(range(len(self.error_values[error_type])), self.error_values[error_type], color=self.color, label = self.label, linewidth=1.5)

    def plot_efficiency(self, error_type):
        plt.plot(range(len(self.processing_times)), (1 / (self.error_values[error_type] * self.processing_times)), 
                 color=self.color, label = self.label)
    
    def plot_error_derivative(self, error_type):
        errors =self.get_error_values(error_type)
        derivative = [(errors[i + 1] - errors[i]) / self.processing_times[i + 1] for i in range(len(errors) - 1)]
        plt.plot(self.processing_times[1:], derivative, color=self.color, label = self.label)

def load_result(index, index_in_list, nb_results, label, basedir = ""):
    return ResultStatistics(index, index_in_list, nb_results, basedir, label)

def load_all_results(basedir = ""):
    l = []
    done = False
    count = 0
    while not done:
        try:
            load_result(count, 0, 1, "", basedir)
            count = count + 1
        except:
            done = True
    done = False
    i = 0
    while not done:
        try:
            r = load_result(i, i, count, "", basedir)
            l.append(r)
            i = i + 1
        except:
            done = True
    return l

def load_results(index_list, label_list, basedir = ""):
    return [load_result(index_list[i], i, len(index_list), label_list[i], basedir) for i in range(len(index_list))]

def plot_error_curves_of_time(result_list, error_type, time_range = [], error_range = [], title = ""):
    fig = plt.figure()
    plot = fig.add_subplot(111)
    for axis in [plot.xaxis, plot.yaxis]:
        for tick in axis.get_major_ticks():
            tick.label.set_fontsize(14)
    plot.grid(True)
    plot.set_ylabel(error_label[error_type], fontsize=16)
    plot.set_xlabel("time (seconds)", fontsize=16)
    if time_range:
        plot.set_xlim(time_range)
    if error_range:
        plot.set_ylim(error_range)
    for r in result_list:
        r.plot_error_curve_of_time(error_type, plot)
    legend = plot.legend(title = title, prop = {'size': 16})
    plt.setp(legend.get_title(),fontsize=32)

def plot_error_curves_of_iterations(result_list, error_type, it_range = [], error_range = [], title = ""):
    fig = plt.figure()
    plot = fig.add_subplot(111)
    for axis in [plot.xaxis, plot.yaxis]:
        for tick in axis.get_major_ticks():
            tick.label.set_fontsize(14)
    plot.grid(True)
    plot.set_ylabel(error_label[error_type], fontsize=16)
    plot.set_xlabel("iteration count", fontsize=16)
    if it_range:
        plot.set_xlim(it_range)
    if error_range:
        plot.set_ylim(error_range)
    for r in result_list:
        r.plot_error_curve_of_iterations(error_type, plot)
    legend = plot.legend(title = title, prop = {'size': 16})
    plt.setp(legend.get_title(),fontsize=32)

def compute_time_to_reach(results_list, error_type, target_error, ref_index = 0):
    times = []
    for result in results_list:
        error_values = result.get_error_values(error_type)
        time = -1
        for i in range(len(error_values)):
            if(error_values[i] < target_error):
                time = result.processing_times[i]
                break
        times.append(time)
    speedups = [ times[ref_index] / t for t in times]        
        
    return (times, speedups)

def compute_error_at_time(results_list, error_type, time):
    errors = []
    for result in results_list:
        times = result.processing_times
        error = -1
        for i in range(len(times)):
            if(times[i] > time):
                error = result.get_error_values(error_type)[i]
                break
        errors.append(error)
    return errors

def maximal_error(results_list, error_type):
    return max((r.get_error_values(error_type)[0] for r in results_list))

def minimal_error(results_list, error_type):
    return max((r.get_error_values(error_type)[-1] for r in results_list))

def plot_efficiency(result_list, error_type, iteration_count = -1):
    plt.grid(True)
    if iteration_count > 0:
        plt.xlim([0, iteration_count])
    for r in result_list:
        r.plot_efficiency(error_type)
    plt.legend()

def get_best_result(result_list, error_type):
    best_error = sys.float_info.max
    best_result = -1
    for i in range(len(result_list)):
        error = result_list[i].get_error_values(error_type)[-1]
        if error < best_error:
            best_error = error
            best_result = i
    if best_result == -1:
        return -1
    return result_list[best_result].index

def get_sorted_index_list_by_error(result_list, error_type):
    return sorted(range(len(result_list)), key=functools.cmp_to_key(lambda i,j: result_list[i].get_error_values(error_type)[-1] - result_list[j].get_error_values(error_type)[-1]))