# -*- coding: utf-8 -*-
"""
Created on Fri Oct  2 10:45:08 2015

@author: c2ba
"""

from utils import *
from utils.utils import *

error_type = "mae"

results = load_results([0, 1, 4], ["BPT", "ICBPT", "SkelBPT" ], "stats/")

mae_list = get_sorted_index_list_by_error(results, error_type)
plot_error_curves_of_time(results, error_type, title="Corridor")