# -*- coding: utf-8 -*-
"""
Created on Mon Apr 20 13:15:40 2015

@author: c2ba
"""

from utils import *
from utils.utils import *

results = load_all_results("stats/")
plot_error_curves_of_time(results, "mae")
plt.show()
plot_efficiency(results, "mse", min((len(result.processing_times) for result in results)))
plt.show()

print("L1 (MAE) error = ", [results[i].index for i in get_sorted_index_list_by_error(results, "mae")])
print("L2 (MSE) error = ", [results[i].index for i in get_sorted_index_list_by_error(results, "mse")])
print("RMSE error = ", [results[i].index for i in get_sorted_index_list_by_error(results, "rmse")])
print("NRMSE error = ", [results[i].index for i in get_sorted_index_list_by_error(results, "nrmse")])

mae_list = get_sorted_index_list_by_error(results, "mae")
plot_error_curves_of_time([results[mae_list[0]], results[0]], "mae") # compare BPT and the best result on a curve
plt.show()

results[0].label = "BPT";
results[2].label = "SkelBPT";
results[1].label = "ICBPT";

plot_error_curves_of_time([results[0], results[1], results[2]], "mae", title="Sponza", time_range=[0,300])
plt.show()

print(compute_time_to_reach(results, "mae", 0.025))

#plot_error_curves_of_iterations([results[0], results[1], results[2]], "mae", it_range=[0,1000], title="Sponza")
#plt.show()

#error_range = (maximal_error([results[0], results[1], results[2]], "mae")
#    , minimal_error([results[0], results[1], results[2]], "mae"))
#
#error_diff = abs(error_range[0] - error_range[1])
#
#target_error_count = 1000
#
#delta = error_diff / target_error_count
#
#times_list = []
#speedups_list = []
#
#for i in range(target_error_count):
#    target_error = error_range[0] - i * delta
#    times, speedups = compute_time_to_reach([results[0], results[1], results[2]], "mae", target_error);
#    times_list.append(times)
#    speedups_list.append(speedups)
#
#best_speedup_index = -1
#best_speedup = 0
#
#for i in range(len(speedups_list)):
#    if speedups_list[i][2] > best_speedup:
#        best_speedup_index = i
#        best_speedup = speedups_list[i][2]
#
#print("best speedup = ", speedups_list[best_speedup_index], " at time = ", times_list[best_speedup_index], " for error = ",
#      error_range[0] - best_speedup_index * delta)