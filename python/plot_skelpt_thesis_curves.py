from utils import *
from utils.utils import *

error_type = "absoluteError"

results = load_results([0, 1, 2, 3], ["PT", "SkelPT (s=4)", "SkelPT (s=32)", "SkelPT (s=64)" ], "stats/")

mae_list = get_sorted_index_list_by_error(results, error_type)
plot_error_curves_of_time(results, error_type, title="Sibenik 2 Lights")