# Bone'z Renderer #

### What is this repository for? ###

* A 3D hybrid (CPU & GPU) renderer I developed during my PhD, for light transport simulation
* Centered around the use of a curvilinear skeleton and other kind of tools from discrete shape analysis in order to guide rendering algorithms
* More information: http://www.laurentnoel.fr/index.php?section=research
* Version 3.x

### How to compile ###

* With CMake
* Should work on Linux with gcc 5+ build and on Windows with VS2015 build (64 bits only)
* Self contained, all dependencies are included
* The main app compiled is flat_scene_viewer
* A second main app is skeleton_shadow_mapping_refactored
* The app result_viewer allows to view rendered EXR images

### Warning ###

* The apps are not meant to be user friendly (many windows, with many options)
* The code is sometimes dirty (research code :D)
* There is no documentation
* But if you have any question, you can send me a mail: laurent.noel.c2ba (at) gmail.com

### Scenes ###
Go pull https://bitbucket.org/Celeborn2BeAlive/bonez3-scenes/