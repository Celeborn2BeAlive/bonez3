#include <gtest/gtest.h>
#include <bonez/rendering/renderers/recursive_mis_bdpt.hpp>

namespace BnZ {

TEST(RecurviseMISBPTTest, StrategyCountTest) {
    for(auto maxDepth = 1; maxDepth < 64; ++maxDepth) {
        auto count = ((maxDepth + 1) * maxDepth) / 2 + 2 * maxDepth;
        ASSERT_EQ(count, computeBPTStrategyCount(maxDepth + 1));
    }
}

}
