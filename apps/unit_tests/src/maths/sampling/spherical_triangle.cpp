#include <gtest/gtest.h>

#include <bonez/sampling/shapes.hpp>
#include "../../utils.hpp"

namespace BnZ {

static const size_t SAMPLING_GRID_WIDTH = 4096;
static const size_t SAMPLING_GRID_HEIGHT = 2048;

TEST(SphericalTriangleSamplingTest, UnitLengthTest) {
    auto A = Vec3f(1, 0, 0), B = Vec3f(0, 1, 0), C = normalize(Vec3f(0, -1, 1));

    for(const auto& uv: makeUVGridTest(SAMPLING_GRID_WIDTH, SAMPLING_GRID_HEIGHT)) {
        auto dirSample = uniformSampleSphericalTriangle(uv.x, uv.y, A, B, C);
        ASSERT_FLOAT_EQ(length(dirSample.value), 1.f);
    }
}

}
