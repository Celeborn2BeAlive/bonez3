#pragma once

#include <vector>
#include <bonez/maths/types.hpp>

namespace BnZ {

inline std::vector<Vec2f> makeUVGridTest(size_t width, size_t height) {
    std::vector<Vec2f> uvGrid;
    uvGrid.reserve(width * height);

    Vec2f delta(1.f / width, 1.f / height);

    for(auto j = size_t(0); j < height; ++j) {
        for(auto i = size_t(0); i < width; ++i) {
            uvGrid.emplace_back(Vec2f(i + 0.5f, j + 0.5f) * delta);
        }
    }

    return uvGrid;
}

}
