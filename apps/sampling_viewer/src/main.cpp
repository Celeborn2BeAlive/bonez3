#include <iostream>
#include <bonez/common.hpp>

#include <bonez/viewer/WindowManager.hpp>
#include <bonez/viewer/GUI.hpp>

#include <bonez/opengl/GLShaderManager.hpp>
#include <bonez/opengl/debug/GLDebugRenderer.hpp>

#include <bonez/sampling/shapes.hpp>
#include <bonez/sampling/Random.hpp>

#include <GLFW/glfw3.h>

INITIALIZE_EASYLOGGINGPP

namespace BnZ {

struct TrackBallCamera {
    float m_fDistance = 0.f;
    float m_fAngleX = 0.f, m_fAngleY = 0.f;

    TrackBallCamera(float distance = 0.f):
        m_fDistance(distance) {
    }

    void moveFront(float delta) {
        m_fDistance -= delta;
    }

    void rotateLeft(float degrees) {
        m_fAngleY -= degrees;
    }

    void rotateUp(float degrees) {
        m_fAngleX -= degrees;
    }

    glm::mat4 getViewMatrix() const {
        return glm::rotate(glm::rotate(glm::translate(glm::mat4(1.f), glm::vec3(0, 0, -m_fDistance)),
                                       m_fAngleX, glm::vec3(1, 0, 0)), m_fAngleY, glm::vec3(0, 1, 0));
    }
};

inline std::vector<Vec2f> makeUVGridTest(size_t width, size_t height) {
    std::vector<Vec2f> uvGrid;
    uvGrid.reserve(width * height);

    Vec2f delta(1.f / width, 1.f / height);

    for(auto j = size_t(0); j < height; ++j) {
        for(auto i = size_t(0); i < width; ++i) {
            uvGrid.emplace_back(Vec2f(i + 0.5f, j + 0.5f) * delta);
        }
    }

    return uvGrid;
}

int main(int argc, char** argv) {
    initEasyLoggingpp(argc, argv);

    WindowManager windowManager(1024, 748, "SamplingViewer");
    GLShaderManager shaderManager(FilePath(argv[0]).directory() + "shaders");
    GLDebugRenderer debugRenderer(shaderManager);
    GLScreenFramebuffer framebuffer;
    framebuffer.init(windowManager.getSize().x, windowManager.getSize().y);

    GUI gui(FilePath(argv[0]), windowManager);

    Mat4f projMatrix = perspective(45.f, float(windowManager.getSize().x) / windowManager.getSize().y, 0.01f, 100.f);
    TrackBallCamera camera(3.f);

    bool isLeftKeyPressed = false;
    Vec2i lastMousePosition;

    auto uvGrid = makeUVGridTest(20, 10);
    std::vector<Vec3f> points;
    Vec3f A(1, 0, 0), B(0, 1, 0), C(0, 0, 1);

    GLDebugStreamData pointStream;
    GLDebugStreamData axisStream;
    axisStream.addLine(zero<Vec3f>(), Vec3f(1, 0, 0), Vec3f(1, 0, 0), Vec3f(1, 0, 0), 1.f);
    axisStream.addLine(zero<Vec3f>(), Vec3f(0, 1, 0), Vec3f(0, 1, 0), Vec3f(0, 1, 0), 1.f);
    axisStream.addLine(zero<Vec3f>(), Vec3f(0, 0, 1), Vec3f(0, 0, 1), Vec3f(0, 0, 1), 1.f);

    GLDebugStreamData triangleStream;

    float m_fPointSize = 0.01f;

    auto fillPointStream = [&]() {
        pointStream.clearObjects();
        for(auto& point: points) {
            pointStream.addSphere(point, m_fPointSize, Vec3f(1, 0, 0));
        }
    };



    double frameTime = 0.f;

    RandomGenerator rng;

    while(windowManager.isOpen()) {
        auto startTime = windowManager.getSeconds();

        windowManager.handleEvents();

        gui.startFrame();

        if(auto window = gui.addWindow("Controls")) {
            gui.addValue("Frame Time", frameTime);
            gui.addValue("MousePosition", windowManager.getCursorPosition());
            gui.addVarRW(BNZ_GUI_VAR(m_fPointSize));

            gui.addButton("SphereMapping", [&]() {
                points.clear();
                std::transform(begin(uvGrid), end(uvGrid), std::back_inserter(points), [&](auto& uv) {
                    return sphericalMapping(uv);
                });
                fillPointStream();
            });

            gui.addButton("UniformSampleSphere", [&]() {
                points.clear();
                std::transform(begin(uvGrid), end(uvGrid), std::back_inserter(points), [&](auto& uv) {
                    return uniformSampleSphere(uv.x, uv.y).value;
                });
                fillPointStream();
            });

            gui.addButton("CosineSampleSphere", [&]() {
                points.clear();
                std::transform(begin(uvGrid), end(uvGrid), std::back_inserter(points), [&](auto& uv) {
                    return cosineSampleSphere(uv.x, uv.y).value;
                });
                fillPointStream();
            });

            gui.addVarRW(BNZ_GUI_VAR(A));
            gui.addVarRW(BNZ_GUI_VAR(B));
            gui.addVarRW(BNZ_GUI_VAR(C));
            gui.addButton("RandomTriangle", [&]() {
                A = uniformSampleSphere(rng.getFloat(), rng.getFloat()).value;
                B = uniformSampleSphere(rng.getFloat(), rng.getFloat()).value;
                C = uniformSampleSphere(rng.getFloat(), rng.getFloat()).value;
            });

            gui.addButton("UniformSphericalTriangleSample", [&]() {
                points.clear();
                std::transform(begin(uvGrid), end(uvGrid), std::back_inserter(points), [&](auto& uv) {
                    return uniformSampleSphericalTriangle(uv.x, uv.y, normalize(A), normalize(B), normalize(C)).value;
                });
                fillPointStream();
            });
        }

        framebuffer.bindForDrawing();

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        debugRenderer.addStream(&pointStream);
        debugRenderer.addStream(&axisStream);

        triangleStream.clearObjects();
        triangleStream.addLine(A, B, Vec3f(1, 1, 0), Vec3f(1, 1, 0), 1.f);
        triangleStream.addLine(B, C, Vec3f(1, 1, 0), Vec3f(1, 1, 0), 1.f);
        triangleStream.addLine(C, A, Vec3f(1, 1, 0), Vec3f(1, 1, 0), 1.f);

        triangleStream.addLine(normalize(A), normalize(B), Vec3f(1, 0, 1), Vec3f(1, 0, 1), 1.f);
        triangleStream.addLine(normalize(B), normalize(C), Vec3f(1, 0, 1), Vec3f(1, 0, 1), 1.f);
        triangleStream.addLine(normalize(C), normalize(A), Vec3f(1, 0, 1), Vec3f(1, 0, 1), 1.f);
        debugRenderer.addStream(&triangleStream);

        auto viewMatrix = camera.getViewMatrix();
        debugRenderer.render(projMatrix * viewMatrix, viewMatrix);

        framebuffer.blitOnDefaultFramebuffer(Vec4u(0, 0, windowManager.getSize()));

        auto guiHasFocus = gui.draw();

        if(!guiHasFocus) {
            auto isCurrentlyPressed = windowManager.isMouseButtonPressed(GLFW_MOUSE_BUTTON_LEFT);
            if(isCurrentlyPressed && !isLeftKeyPressed) {
                isLeftKeyPressed = true;
                lastMousePosition = windowManager.getCursorPosition();
            } else if(!isCurrentlyPressed && isLeftKeyPressed) {
                isLeftKeyPressed = false;
            }

            if(isLeftKeyPressed) {
                auto currentMousePosition = windowManager.getCursorPosition();
                camera.rotateLeft(-0.1f * (currentMousePosition.x - lastMousePosition.x));
                camera.rotateUp(0.1f * (currentMousePosition.y - lastMousePosition.y));

                lastMousePosition = currentMousePosition;
            }
        }

        windowManager.swapBuffers();

        frameTime = windowManager.getSeconds() - startTime;
    }

	return 0;
}

}

int main(int argc, char** argv) {
#ifdef _DEBUG
        LOG(INFO) << "DEBUG MODE ACTIVATED";
#endif

#ifdef DEBUG
        LOG(INFO) << "DEBUG MODE ACTIVATED";
#endif
    return BnZ::main(argc, argv);
}
