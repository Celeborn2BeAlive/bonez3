#include <iostream>

#include "SSMViewer.hpp"
#include "bonez/sys/easyloggingpp.hpp"
#include "bonez/parsing/tinyxml/tinyxml2.h"

#include "bonez/sampling/patterns.hpp"

INITIALIZE_EASYLOGGINGPP

void usage(char** argv)
{
    std::cerr << "Usage: " << argv[0] << " < XML configuration filepath > [flat]" << std::endl;
}

using namespace BnZ;

void generateDiskSamples() {
	auto w = 4, h = 4;
	//auto grid = makeUVGridTest(w, h);
	RandomGenerator rng;
	JitteredDistribution2D distrib(w, h);
	std::cout << "vec2 gDiskSampleOffset[" << (w * h) << "] = vec2[] (" << std::endl;
	for (auto y : range(h)) {
		for (auto x : range(w)) {
			auto diskSample = uniformSampleDisk(distrib.getSample(x, y, rng.getFloat2()), 1.f);
			std::cout << "vec2( " << diskSample.x << ", " << diskSample.y << ")," << std::endl;
		}
	}
	std::cout << ");";
}

/**
 * TODO:
 * - fixer timers !!
 * - fixer light leaks (difficile)
 * - fixer direct light shadow map (osef ?)
 * - intégrer ISM pour comparaison (trop long)
 * - animations sans flickering (sampler des VPL fixe, non prioritaire, pas besoin pour la thèse)
 */

int main(int argc, char** argv) {
    // Checking the configuration filepath
    if(argc < 2) {
        usage(argv);
        return -1;
    }

    BnZ::initEasyLoggingpp(argc, argv);

	//generateDiskSamples();
	//return 0;

    // Handles the XLMDocument
    tinyxml2::XMLDocument inputDocument;

    // Loading the XLM document
    if(tinyxml2::XML_NO_ERROR != inputDocument.LoadFile(argv[1])) {
        std::cerr << "Unable to open input file " << argv[1] << std::endl;
        return -1;
    }

    // Checking if the root can be retrieved
    auto pRootElement = inputDocument.RootElement();
    if(!pRootElement) {
        std::cerr << "No root element in input file " << argv[1] << std::endl;
        return -1;
    }

    // Retrieving the root name and launching the good viewer
    auto rootName = std::string(pRootElement->Name());
    if (rootName != "Viewer") {
        std::cerr << "Name of the root is not Viewer " << rootName << std::endl;
        return -1;
    }

    // Root's name is Viewer, creating it
    BnZ::SSMViewer viewer(argv[0], argv[1]);
    viewer.run();

    return 0;
}







