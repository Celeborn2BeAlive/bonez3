#pragma once

#include <unordered_map>

#include "FpsCounter.hpp"
#include "SkeletonViewerModule.hpp"
#include "VPLViewerModule.hpp"

#include <bonez/opengl/GLGBuffer.hpp>
#include <bonez/opengl/GLScreenFramebuffer.hpp>
#include <bonez/opengl/GLScene.hpp>

#include <bonez/opengl/GLGBufferRenderPass.hpp>
#include <bonez/opengl/debug/GLDebugRenderer.hpp>
#include <bonez/opengl/debug/GLDebugStream.hpp>
#include <bonez/viewer/Viewer.hpp>
#include <bonez/parsing/parsing.hpp>

#include "gpu_vpl_utilities.hpp"
#include "LightViewerModule.hpp"
#include "render_passes/RenderPass.hpp"
#include "render_passes/ShadowCubeMapComputationPass.hpp"
#include "render_passes/ShadowCubeMapContainer.hpp"

namespace BnZ {

class VPLViewerModule;

class SSMViewer: public Viewer {

public:
    SSMViewer(const FilePath& applicationPath, const FilePath& settingsFilePath);

    void setUp() override;

    void tearDown() override;

    float getArrowLength() const;

    float getArrowBase() const;

    inline GLDebugRenderer& getGLDebugRenderer() {
        return m_GLDebugRenderer;
    }

    inline const GLScene& getGLScene() const {
        assert(m_pGLScene);
        return *m_pGLScene;
    }

    inline const GLScreenTriangle& getScreenTriangle() const {
        return m_ScreenTriangle;
    }

    inline const Vec4u& getSelectedObjectID() const {
        return m_SelectedObjectID;
    }

    inline GLScreenFramebuffer& getScreenFramebuffer() {
        return m_GLFramebuffer;
    }

    inline const Intersection& getPickedIntersection() const {
        return m_PickedIntersection;
    }

    inline const Vec3f& getPickedIntersectionIncidentDirection() const {
        return m_PickedIntersectionIncidentDirection;
    }

    const GLGBuffer& getGBuffer() const {
        return m_GBuffer;
    }

    const Mat4f& getRcpProjMatrix() const {
        return m_Camera.getRcpProjMatrix();
    }

    const Mat4f& getViewMatrix() const {
        return m_Camera.getViewMatrix();
    }

    const Mat4f& getRcpViewMatrix() const {
        return m_Camera.getRcpViewMatrix();
    }

    const ShadowCubeMapContainer& getSkeletonNodeShadowCubeMapContainer() const {
        return m_SkeletonNodeShadowCubeMapContainer;
    }

    size_t getSelectedNodeIndex() const {
        return m_SkeletonViewerModule.getSelectedNodeIndex();
    }

    const std::vector<Vec3f>& getNodePositionArray() const {
        return m_NodePositionArray;
    }

    const std::vector<BufferedLightGPU>& getLights() const {
        return m_Lights;
    }

	const std::vector<SVPL>& getIndirectVPLs() const {
		return m_IndirectVPLs;
	}

    const PointLight& getLight() const {
        return m_Light;
    }

    const std::vector<BufferedLightGPU>& getDirectLights() const {
        return m_DirectLights;
    }

    inline size_t getRealPathCount() const {
        return m_nRealPathCount;
    }

    inline const CurvilinearSkeleton& getSkeleton() const {
        return *(getScene()->getCurvSkeleton());
    }

    void setLightPosition(const Vec3f& position);

	float getGamma() const {
		return m_fGamma;
	}

	bool computeShadows() const {
		return m_bComputeShadows;
	}

	bool computeDirectLighting() const {
		return m_bComputeDirectLighting;
	}

	float getGeometryFactorDistanceClamp() const {
		return m_fGeometryFactorDistanceClamp;
	}

	float getGeometryFactorBound() const {
		if (m_fGeometryFactorDistanceClamp <= 0) {
			return std::numeric_limits<float>::max();
		}
		return 1.f / sqr(m_fGeometryFactorDistanceClamp);
	}

private:
    bool initAll();

    void initViewports();

    void initGLData();

    void handleGlobalPreprocessParameters(const tinyxml2::XMLElement& input,
                                          tinyxml2::XMLElement& output) override;
    void drawFrame() override;

    void drawFinalRender();

    void doPicking();

    void setSelectedPixel(const Vec2u& pixel);

    void applySceneThinning();

    void exposeIO();

    void updateLightPosition();

    std::vector<std::string> m_RenderPassNames = {
        "FlatRenderPass",
        "SkeletonNodeShadowsRenderPass",
		"CPUVPLRenderPass"
    };

	float m_fGamma = 2.2f;
	bool m_bComputeShadows = true;
	bool m_bComputeDirectLighting = false;
	float m_fGeometryFactorDistanceClamp = 10; // Gets adapted to the scene in constructor

    size_t m_nCurrentRenderPassIndex = 0;
    size_t m_nRealPathCount = 0;
    size_t m_nPathCount = 666;
    size_t m_nMaxDepth = 3;
    bool m_bLightCanMove = false;
	bool m_bSetLightPosAtSelectedNode = true;

    RenderPass* createRenderPass(const std::string& name);

    // Light movement:
    std::size_t m_nPreviousNodeIndex = 0; // Node where the light was
    std::size_t m_nLightNodeIndex = 0; // Node where the light is
    std::size_t m_nNextNodeIndex = 0; // Node where the light want to go
    float m_fLightInterpolationParam = 0.f;
    float m_fLightSpeed = 20.f; // 10 unit of scene per second
    float m_fLightIntensity = 5000000;
    uint64_t m_LastTime;
    RandomGenerator m_LightMovementRng;
	bool m_bDisplayPointLight = true;

    PointLight m_Light { Vec3f(0), Vec3f(0) };

    // The FPS counter.
    FpsCounter m_FpsCounter;

    // The Gbuffer.
    GLGBuffer m_GBuffer;

    // The framebuffer.
    GLScreenFramebuffer m_GLFramebuffer;

    // Renders the scene.
    GLGBufferRenderPass m_GBufferRenderPass;

    // Triangle used to draw during the FlashShadingPass.
    GLScreenTriangle m_ScreenTriangle;

    // Viewport of the final render.
    Vec4f m_FinalRenderViewport;

    // Used to draw some debug informations.
    GLDebugRenderer m_GLDebugRenderer;

    // Shared pointer on the scene.
    Shared<const GLScene> m_pGLScene;

    GLDebugStreamData m_DisplayStream;

    SkeletonViewerModule m_SkeletonViewerModule;

    Vec4u m_SelectedObjectID = GLScreenFramebuffer::NULL_OBJECT_ID;

	std::vector<SVPL> m_IndirectVPLs;
	EmissionVPLContainer m_EmissionVPLContainer;
    std::vector<BufferedLightGPU> m_Lights;
    std::vector<BufferedLightGPU> m_DirectLights;


    // Intersections.
    Intersection m_PickedIntersection;
    Vec3f m_PickedIntersectionIncidentDirection;
    Vec2u m_SelectedPixel;

    size_t m_nThinningResolution = 200;
    bool m_bSegmentSkeleton = true;

    UpdateFlag m_OldCameraFlag;

    RenderPass* m_pCurrentRenderPass = nullptr;

    bool m_bInitFlag = false;

    std::vector<Vec3f> m_NodePositionArray;
    ShadowCubeMapComputationPass m_ShadowCubeMapComputationPass;
	size_t m_nSkeletonShadowMapResolution = 128; // This must be declared before the next variable since its initialization depends on m_nSkeletonShadowMapResolution
    ShadowCubeMapContainer m_SkeletonNodeShadowCubeMapContainer;

    struct ShadowCubeMapDrawPass {
        GLProgram m_Program;

        BNZ_GLUNIFORM(m_Program, float, uZNear);
        BNZ_GLUNIFORM(m_Program, float, uZFar);
        BNZ_GLUNIFORM(m_Program, GLuint, uMapIndex);
        BNZ_GLUNIFORM(m_Program, bool, uDrawDepth);
        BNZ_GLUNIFORM(m_Program, int, uCubeMapContainer);

        // GLCubeMapContainerUniform uCubeMapContainer { m_Program };

        ShadowCubeMapDrawPass(const GLShaderManager& shaderManager):
            m_Program(shaderManager.buildProgram({
                                                     "image.vs",
                                                     "skeleton_shadow_mapping_refactored/ShadowCubeMapDrawPass.fs"
                                                 })) {}
    };

    LightViewerModule m_LightViewerModule;

    // Pass used to draw shadow maps of skeleton nodes in GUI
    ShadowCubeMapDrawPass m_ShadowCubeMapDrawPass;
    GLFramebuffer2D<1, false> m_NodeShadowCubeMapDisplayFramebuffer;

    void drawSelectedNodeShadowCubeMap();

    std::unordered_map<std::string, Unique<RenderPass>> m_RenderPasses;

	// Screenshots
	std::string m_ScreenshotPrefix = toString(getMicroseconds());
	void storeScreenshotImage(const Image& image);
	void doScreenshot();
};
}
