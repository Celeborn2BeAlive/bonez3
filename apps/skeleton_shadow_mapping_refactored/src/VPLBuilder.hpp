#pragma once

#include "bonez/scene/lights/PowerBasedLightSampler.hpp"
#include "bonez/scene/shading/BSDF.hpp"
#include <bonez/scene/lights/VPLs.hpp>

namespace BnZ {

//BACK.
/// VPL generated by the VPLBuilder's algorithm;
struct SVPL {
    /// Position in world space;
    Intersection intersection;
    /// BSDF of the SPVL;
    BSDF bsdf;
    /// Normal in world space;
    Vec3f normal;
    /// Light intensity (also its color);
    Vec3f power;
    SVPL(const Intersection& I, const Vec3f wi, const Scene& scene, const Vec3f& power):
        intersection(I), bsdf(wi, intersection, scene), normal(I.Ns), power(power) {}
};

class VPLBuilder {

public:

    /// Constructors storing the scene;
    VPLBuilder(const Scene& scene);

    /// Generates an emission and surface VPLS and store them in the two given buffers.
    /// Scales the initiale power by fScale (usualy equals to lightPathCount).
    void generateVPL(std::size_t nLightPathCount, std::size_t maxDepth,
                     EmissionVPLContainer& emissionVPLBuffer,
                     std::vector<SVPL>& surfaceVPLBuffer) const;

    void generateVPL(std::size_t nLightPathCount, std::size_t& out_nbPath,
                     std::size_t maxDepth,
                     EmissionVPLContainer& emissionVPLBuffer,
                     std::vector<SVPL>& surfaceVPLBuffer,
                     const PointLight& source) const;

    /// Generates an emission and surface VPLS and store them in the two given buffers.
    /// Scales the initiale power by fScale (usualy equals to lightPathCount).
    void generateVPLWithDepthLayer(std::size_t nLightPathCount, std::size_t maxDepth,
                                   EmissionVPLContainer& emissionVPLBuffer,
                                   std::vector<std::vector<SVPL>>& surfaceVPLBuffers) const;

private:

    /// Scene used to generate VPLs.
    const Scene& m_Scene;

    /// The light sampler used on the scene to provide sampled light.
    PowerBasedLightSampler m_Sampler;

    mutable RandomGenerator rnd;
};
}
