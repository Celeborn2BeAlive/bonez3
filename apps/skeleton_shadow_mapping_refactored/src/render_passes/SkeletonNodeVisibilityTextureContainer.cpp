#include "SkeletonNodeVisibilityTextureContainer.hpp"
#include <bonez/utils/itertools/Range.hpp>
#include <bonez/opengl/utils/glGet.hpp>

namespace BnZ {

SkeletonNodeVisibilityTextureContainer::SkeletonNodeVisibilityTextureContainer(
        size_t nTextureWidth,
        size_t nTextureHeight,
        size_t nMaxLayerRequested):
    m_nTextureWidth(nTextureWidth), m_nTextureHeight(nTextureHeight),
    m_nLimitTextureArraySize(getMaxArrayTextureLayers()),
    m_nMaxGlobalSizeAllocated(nMaxLayerRequested) {

    auto nArrayNeeded = getNumberOfTextureArrayToCreate(nMaxLayerRequested);

    m_ShadowTexture2DArrays.clear();
    m_ShadowTexture2DArrays.resize(nArrayNeeded);

    for (auto i: range(nArrayNeeded)) {
        auto nLayerCount = m_nLimitTextureArraySize <= nMaxLayerRequested ?
                    m_nLimitTextureArraySize : nMaxLayerRequested;
        nMaxLayerRequested -= nLayerCount;
        generateTextureArray(i, nLayerCount);
    }
}

void SkeletonNodeVisibilityTextureContainer::generateTextureArray(size_t index, size_t nLayerCount) {
    m_ShadowTexture2DArrays[index].setImage(0, GL_R16F, m_nTextureWidth, m_nTextureHeight, nLayerCount,
                                            0, GL_RED, GL_FLOAT, nullptr);

    // Setting the texture array parameters.
    m_ShadowTexture2DArrays[index].setFilters(GL_NEAREST, GL_NEAREST);
    m_ShadowTexture2DArrays[index].setWrapS(GL_CLAMP_TO_EDGE);
    m_ShadowTexture2DArrays[index].setWrapR(GL_CLAMP_TO_EDGE);
    m_ShadowTexture2DArrays[index].setWrapT(GL_CLAMP_TO_EDGE);
}

}
