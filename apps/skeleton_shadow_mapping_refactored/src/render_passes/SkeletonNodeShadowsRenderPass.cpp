﻿#include "SkeletonNodeShadowsRenderPass.hpp"
#include "../SSMViewer.hpp"

#include <bonez/opengl/GLShaderManager.hpp>

namespace BnZ {

SkeletonNodeShadowsRenderPass::SkeletonNodeShadowsRenderPass(
        SSMViewer &viewer):
    m_nNodeCount(viewer.getScene()->getCurvSkeleton()->size()),
    m_SkeletonNodeVisibilityTextureComputationPass(viewer.getShaderManager()),
    m_SkeletonNodeVisibilityTextureContainer(viewer.getScreenFramebuffer().getWidth(), // Remplace par (1)
                                             viewer.getScreenFramebuffer().getHeight(),
                                             m_nNodeCount),
    m_Program(viewer.getShaderManager().buildProgram({ "deferredShadingPass.vs",
                                                       "skeleton_shadow_mapping_refactored/SkeletonNodeShadowsRenderPass.fs" })),
    /*m_NodesPositionsBuffer(m_nNodeCount, nullptr, GL_MAP_WRITE_BIT),*/
    m_DirectIlluminationComputationPass(viewer),
    m_ScalingComputationPass(viewer.getShaderManager()),
    m_TextureFilteringComputationPass(viewer.getShaderManager()),
    m_GeometryAwareBlurFilteringComputationPass(viewer.getShaderManager()),
    m_GammaPass(viewer.getShaderManager()) {
    /*
    auto pNodePosition = m_NodesPositionsBuffer.mapRange(0, m_nNodeCount, GL_MAP_WRITE_BIT);
    for(auto position: viewer.getNodePositionArray()) {
        *pNodePosition = Vec4f(position, 1.f);
        ++pNodePosition;
    }
    m_NodesPositionsBuffer.unmap();
*/
    auto fWidth = viewer.getScreenFramebuffer().getWidth();
    auto fHeight = viewer.getScreenFramebuffer().getHeight();

    // Framebuffers.
    m_AccumulationFramebuffer.init(fWidth, fHeight, { GL_RGBA32F });
    m_TransitionFramebuffer.init(fWidth, fHeight, { GL_RGBA32F });
    m_Transition2Framebuffer.init(fWidth, fHeight, { GL_RGBA32F });
    m_Transition3Framebuffer.init(fWidth, fHeight, { GL_RGBA32F });

    // Configuration of the VisibilityFramebuffer and its texture2D.
    m_VisibilityTexture2D.setImage(0, GL_R16F, fWidth, fHeight, 0, GL_RED, GL_FLOAT, nullptr);
    m_VisibilityTexture2D.setFilters(GL_NEAREST, GL_NEAREST);
    m_VisibilityTexture2D.setWrapS(GL_CLAMP_TO_EDGE);
    m_VisibilityTexture2D.setWrapR(GL_CLAMP_TO_EDGE);
    m_VisibilityTexture2D.setWrapT(GL_CLAMP_TO_EDGE);

    glBindFramebuffer(GL_FRAMEBUFFER, m_VisibilityFramebuffer.glId());
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
                           m_VisibilityTexture2D.glId(), 0);
    glNamedFramebufferDrawBuffer(m_VisibilityFramebuffer.glId(), GL_COLOR_ATTACHMENT0);

    GLenum status;
    if ((status = glCheckFramebufferStatus(GL_FRAMEBUFFER)) != GL_FRAMEBUFFER_COMPLETE) {
        std::cout << "NodeShadowContainer::generateFBOs : Framebuffer not complete !\n"
                  << GLFramebufferErrorString(status) << std::endl;
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}


void SkeletonNodeShadowsRenderPass::
updateLightsNearestNodeANDLightLists(const CurvilinearSkeleton& skeleton,
                                     const std::vector<BufferedLightGPU>& lights) {

    m_LightNearestNodes.clear();
    m_LightNearestNodes.reserve(lights.size());

    m_LightListForEachNodes.clear();
    m_LightListForEachNodes.resize(m_nNodeCount);

    auto i = 0;
    for (auto& light: lights) {
        // The normal.w equals 0 if not existing, 1 otherwise.
        size_t id;
        if (0 == light.normal.w) {
            id = skeleton.getNearestNode(Vec3f(light.positionWs));
        } else {
            id = skeleton.getNearestNode(Vec3f(light.positionWs), Vec3f(light.normal));
        }

        if(UNDEFINED_NODE == id) {
            ++i;
            continue;
        }
        m_LightNearestNodes.emplace_back(id);
        m_LightListForEachNodes[id].push_back(i);
        ++i;
    }
    m_LightNearestNodesBuffer = GLBufferStorage<unsigned int>(m_LightNearestNodes.size(),
                                                              m_LightNearestNodes.data());
}


void SkeletonNodeShadowsRenderPass::updateLightPerNodeANDCumulated() {

    // Clearing the entire vector.
    m_LightPerNodeANDCumulated = std::vector<Vec2u>(m_nNodeCount, Vec2u(0));

    // Updating the light per node value.
    for (auto id : m_LightNearestNodes) {
        m_LightPerNodeANDCumulated[id].x++;
    }

    // Calculating the cumulated value.
    for (auto i : range(m_nNodeCount - 1)) {
        m_LightPerNodeANDCumulated[i + 1].y = m_LightPerNodeANDCumulated[i].x +
                m_LightPerNodeANDCumulated[i].y;
    }

    m_LightPerNodeANDCumulatedBuffer = GLBufferStorage<Vec2u>(m_nNodeCount,
                                                              m_LightPerNodeANDCumulated.data());
}


void SkeletonNodeShadowsRenderPass::computeNodeUsedArray() {

    /* m_LightPerNodeANDCumulated has to be valid first ! */

    m_NodeUsed.clear();
    for (auto i : range(m_LightPerNodeANDCumulated.size())) {
        if (m_LightPerNodeANDCumulated[i].x >= m_nMinimumLightAttached) m_NodeUsed.push_back(i);
    }

    /* Sorting by number of light. */
    std::sort(m_NodeUsed.begin(), m_NodeUsed.end(),
              [&](auto id1, auto id2) {
        return m_LightPerNodeANDCumulated[id1].x > m_LightPerNodeANDCumulated[id2].x;
    });
}

void SkeletonNodeShadowsRenderPass::render(SSMViewer &viewer) {


    const auto& gBuffer = viewer.getGBuffer();
    const auto& rcpProjMatrix = viewer.getRcpProjMatrix();
    const auto& rcpViewMatrix = viewer.getRcpViewMatrix();

    m_nLightProcessed = viewer.getLights().size();
    {
        GLQueryScope timer(m_CPUComputationTimeQuery);
        /* Updating light's nearestNode vector and Buffer. */
        updateLightsNearestNodeANDLightLists(viewer.getSkeleton(), viewer.getLights());

        // Updating the count of light per node. Has to be done before other computations.
        updateLightPerNodeANDCumulated();

        updateGPULightInViewSpaceBuffer(viewer.getViewMatrix(), viewer.getLights());
        /* Getting the array of node used. */
        computeNodeUsedArray();
    }

	// m_NodeUsed has to be set.
	drawGUI(viewer);

    //std::cout << "In SkeletonNodeShadowsRenderPass count : " << viewer.getLights().size()
    //          << " And first intensity : " << viewer.getLights()[0].intensity << std::endl;
    //std::cout << "And Node used : " << m_NodeUsed.size() << std::endl;

    // (2) bind FBO associé à texture contenant shadows d'un noeud à la fois

    m_AccumulationFramebuffer.bindForDrawing();
    glClear(GL_COLOR_BUFFER_BIT);

    m_VisibilityQueries.clear();
    m_IrradianceQueries.clear();
    m_VisibilityQueries.resize(m_NodeUsed.size());
    m_IrradianceQueries.resize(m_NodeUsed.size());

    auto i = 0;
    for (auto& nodeIndex : m_NodeUsed) {
        m_VisibilityQueries[i].begin();

        if (viewer.computeShadows()) {

            auto& skeletonNodeShadowCubeMapContainer = viewer.getSkeletonNodeShadowCubeMapContainer();

            // Getting the 1 - shadow value.
            m_SkeletonNodeVisibilityTextureComputationPass
                    .renderOnePosition(viewer.getScreenTriangle(), gBuffer,
                                       viewer.getNodePositionArray()[nodeIndex],
                                       nodeIndex,
                                       skeletonNodeShadowCubeMapContainer,
                                       m_fBias, m_bUsingPCF, m_bPCFOpti, m_nPCFAlgo, m_fPCFRadius,
                                       skeletonNodeShadowCubeMapContainer.getShadowFar(),
                                       rcpProjMatrix, rcpViewMatrix,
                                       m_VisibilityFramebuffer, m_VisibilityTexture2D.getWidth(),
                                       m_VisibilityTexture2D.getHeight());
        }
        glEndQuery(GL_TIME_ELAPSED);

        m_IrradianceQueries[i].begin();
        if (m_bComputeIrradiance) {
            // Accumulating the irradiance.
            m_AccumulationFramebuffer.bindForDrawing();

            // Enabling the  progressive pass values.
            glEnablei(GL_BLEND, 0);
            glBlendFunci(0, GL_ONE, GL_ONE);
            glBlendEquationi(0, GL_FUNC_ADD);
            // The depth test is already done by the deferred pass.
            glDisable(GL_DEPTH_TEST);
            glViewport(0, 0, m_AccumulationFramebuffer.getWidth(), m_AccumulationFramebuffer.getHeight());

            m_Program.use();

            // Binding OpenGL storage buffers.
            m_LightBuffer.bindBase(GL_SHADER_STORAGE_BUFFER, 0);
            m_LightNearestNodesBuffer.bindBase(GL_SHADER_STORAGE_BUFFER, 1);
            m_LightPerNodeANDCumulatedBuffer.bindBase(GL_SHADER_STORAGE_BUFFER, 2);

            // Updating and sending the current node's light list to the GPU.
            m_LightListOfTheCurrentNodeBuffer = GLBufferStorage<size_t>(m_LightListForEachNodes[nodeIndex].size(),
                                                                        m_LightListForEachNodes[nodeIndex].data());
            m_LightListOfTheCurrentNodeBuffer.bindBase(GL_SHADER_STORAGE_BUFFER, 3);

            // Uniforms.
            ubComputeShadow.set(viewer.computeShadows());
            uLightNumber.set(m_LightListForEachNodes[nodeIndex].size());
            uGBuffer.set(gBuffer, 0, 1, 2);
            uNodeIndex.set(nodeIndex);
            uIntensity.set(m_Intensity);
            uRcpProjMatrix.set(rcpProjMatrix);
            uRcpViewMatrix.set(rcpViewMatrix);
            uGeometryTermBound.set(viewer.getGeometryFactorBound());
            uInterleavedWindowSize.set(m_nInterleavedWindowSize);

            auto nTexture2DUnit = 3;
            m_VisibilityTexture2D.bind(nTexture2DUnit);
            uShadowTexture2D.set(nTexture2DUnit);

            viewer.getScreenTriangle().render();

            // Disabling the progressive pass values.
            glDisablei(GL_BLEND, 0);
            glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
        }
        glEndQuery(GL_TIME_ELAPSED);
        ++i;
    }


    /* Post-treatments. */
    auto *p_Framebuffer = &m_AccumulationFramebuffer;
    if (m_bComputeGeometryBlur){
        GLQueryScope timer(m_GeometryAwareBlurComputationTimeQuery);
        m_GeometryAwareBlurFilteringComputationPass.render(p_Framebuffer->getColorBuffer(0), m_fGaussianTetaNormal,
                                                           m_fGaussianTetaPosition, viewer.getZFar(), m_nInterleavedWindowSize,
                                                           gBuffer.getColorBuffer(GBUFFER_NORMAL_DEPTH),
                                                           m_TransitionFramebuffer.getColorBuffer(0));
        p_Framebuffer = &m_TransitionFramebuffer;
    }
    if (m_bComputeTexture){
        GLQueryScope timer(m_textureComputationTimeQuery);
        m_TextureFilteringComputationPass.render(p_Framebuffer->getColorBuffer(0),
                                                 m_Transition2Framebuffer.getColorBuffer(0),
                                                 gBuffer.getColorBuffer(GBUFFER_DIFFUSE));
        p_Framebuffer = &m_Transition2Framebuffer;
    }
    if (m_bComputeScaling){
        GLQueryScope timer(m_ScalingComputationTimeQuery);
        m_ScalingComputationPass.render(p_Framebuffer->getColorBuffer(0),
                                        viewer.getRealPathCount() / (float) (m_nInterleavedWindowSize
                                                                         * m_nInterleavedWindowSize),
                                        m_Transition3Framebuffer.getColorBuffer(0));
        p_Framebuffer = &m_Transition3Framebuffer;
    }

    /* Accumulating direct lights. */
    if (viewer.computeDirectLighting()){
        GLQueryScope timer(m_DirectIlluminationComputationTimeQuery);
        m_DirectIlluminationComputationPass.render(*p_Framebuffer, viewer, m_fDirectLightingBias);
    }

    if(viewer.getGamma() != 1.f) {
        m_GammaPass.compute(p_Framebuffer->getColorBuffer(0), p_Framebuffer->getColorBuffer(0), viewer.getGamma());
    }

    m_ScalingComputationPass.render(p_Framebuffer->getColorBuffer(0), 1,
                                    viewer.getScreenFramebuffer().getColorBuffer());

    // Blit dorsn't work !!
   // viewer.getScreenFramebuffer().blitFramebuffer(*p_Framebuffer, GL_COLOR_BUFFER_BIT, GL_NEAREST);

    /*
    // Squelette boucle rendu:
    // Pour chaque noeud de nodeUsed {
    //     m_SkeletonNodeVisibilityTextureComputationPass.render(...., m_ShadowsFBO, width, height);
    //     bind m_ShadowsTexture2D
    //     bind buffer indices lumieres associé au noeud
    //     lancer rendu irradiance
    // }

    // Ensuite peut etre:
    // Squelette boucle rendu:
    // Pour chaque noeud de nodeUsed {
    //     m_SkeletonNodeVisibilityTextureComputationPass.render(...., m_ShadowsFBO, width, height);
    //     bind m_ShadowsTexture2D
    //     pour chaque profondeur d possible {
    //          bind buffer indices lumieres de profondeur d associé au noeud
    //          lancer rendu irradiance
    //     }
    // }
    */
}


void SkeletonNodeShadowsRenderPass::drawGUI(SSMViewer &viewer) {
    auto& gui = viewer.getGUI();
    if(auto window = gui.addWindow("SkeletonShadowsRenderPass")) {
        gui.addVarRW(BNZ_GUI_VAR(m_bComputeIrradiance));
        gui.addVarRW(BNZ_GUI_VAR(m_bComputeGeometryBlur));
        gui.addVarRW(BNZ_GUI_VAR(m_bComputeTexture));
        gui.addVarRW(BNZ_GUI_VAR(m_bComputeScaling));
        gui.addSeparator();
        gui.addVarRW(BNZ_GUI_VAR(m_nMinimumLightAttached));
        gui.addSeparator();
        gui.addVarRW(BNZ_GUI_VAR(m_bUsingPCF));
        gui.addVarRW(BNZ_GUI_VAR(m_bPCFOpti));
        gui.addValue("Basic PCF ", 0);
        gui.addValue("Disk PCF ", 1);
        gui.addValue("Matrix PCF ", 2);
        gui.addVarRW(BNZ_GUI_VAR(m_nPCFAlgo));
		m_nPCFAlgo = m_nPCFAlgo % 4;
        gui.addVarRW(BNZ_GUI_VAR(m_fPCFRadius));
        gui.addVarRW(BNZ_GUI_VAR(m_fBias));
        gui.addSeparator();
		gui.addVarRW(BNZ_GUI_VAR(m_fDirectLightingBias));
		gui.addSeparator();
        gui.addVarRW(BNZ_GUI_VAR(m_Intensity));
        gui.addVarRW(BNZ_GUI_VAR(m_nInterleavedWindowSize));
        gui.addVarRW(BNZ_GUI_VAR(m_fGaussianTetaNormal));
        gui.addVarRW(BNZ_GUI_VAR(m_fGaussianTetaPosition));
        gui.addSeparator();

        // Stats.
        auto irradianceTime = 0., visibilityTime = 0.;
        if (auto window = gui.addWindow("VisibilityTimers")) {
            for (auto i : range(m_VisibilityQueries.size())) {
                auto nodeIndex = m_NodeUsed[i];
                std::string s = "N:" + std::to_string(nodeIndex) + " L:"
                        + std::to_string((m_LightPerNodeANDCumulated[nodeIndex].x)) + " Time";
                auto time = ns2ms(m_VisibilityQueries[i].waitResult());
                visibilityTime += time;
                gui.addValue(s.data(), time);
            }
        } else {
            for (auto i : range(m_VisibilityQueries.size())) {
                auto time = ns2ms(m_VisibilityQueries[i].waitResult());
                visibilityTime += time;
            }
        }

        if (auto window = gui.addWindow("IrradianceTimers")) {
            for (auto i : range(m_IrradianceQueries.size())) {
                auto nodeIndex = m_NodeUsed[i];
                std::string s = "N:" + std::to_string(nodeIndex) + " L:"
                        + std::to_string((m_LightPerNodeANDCumulated[nodeIndex].x)) + " Time";
                auto time = ns2ms(m_IrradianceQueries[i].waitResult());
                irradianceTime += time;
                gui.addValue(s.data(), time);
            }
        } else {
            for (auto i : range(m_IrradianceQueries.size())) {
                auto time = ns2ms(m_IrradianceQueries[i].waitResult());
                irradianceTime += time;
            }
        }

        gui.addSeparator();
        gui.addText("STATISTICS");
        gui.addValue("Visibility  pass", visibilityTime);
        gui.addValue("Irradiance  pass", irradianceTime);
        gui.addValue("CPU         pass", ns2ms(m_CPUComputationTimeQuery.waitResult()));
        gui.addValue("DirectLight pass", ns2ms(m_DirectIlluminationComputationTimeQuery.waitResult()));
        gui.addValue("Blurr       pass", ns2ms(m_GeometryAwareBlurComputationTimeQuery.waitResult()));
        gui.addValue("Scaling     pass", ns2ms(m_ScalingComputationTimeQuery.waitResult()));
        gui.addValue("texture     pass", ns2ms(m_textureComputationTimeQuery.waitResult()));
        gui.addSeparator();
    }
}


void SkeletonNodeShadowsRenderPass::
updateGPULightInViewSpaceBuffer(const Mat4f& viewMatrix,
                                const std::vector<BufferedLightGPU>& lights) {

    // Mapping the GPU buffer.
    std::vector<BufferedLightGPU> tmpLights;
    tmpLights.reserve(lights.size());

    // Calculating ViewSpace positions.
    Vec4f tmp;
    for(auto &worldLight : lights) {
        tmpLights.emplace_back(worldLight);
        auto& light = tmpLights.back();
        light = worldLight;
        light.positionWs = worldLight.positionWs;
        light.position = viewMatrix * worldLight.position;

        // The normal fourth coordinate can carry interferring informations.
        tmp = worldLight.normal; tmp.w = 0;
        light.normal = normalize(viewMatrix * tmp);
        light.normal.w = worldLight.normal.w;
        light.incidentDir = normalize(viewMatrix * worldLight.incidentDir);
    }

    m_LightBuffer = GLBufferStorage<BufferedLightGPU>(lights.size(), tmpLights.data());
}
}
