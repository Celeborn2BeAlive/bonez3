#include "CPUVPLRenderPass.hpp"

#include <bonez/opengl/GLShaderManager.hpp>
#include <bonez/scene/sensors/PixelSensor.hpp>
#include "../SSMViewer.hpp"

namespace BnZ {

CPUVPLRenderPass::CPUVPLRenderPass(SSMViewer &viewer) {
	m_IndirectRenderImage.setSize(viewer.getGBuffer().getSize());
	m_IndirectRenderImage.fill(Vec4f(0));
	m_DirectRenderImage.setSize(viewer.getGBuffer().getSize());
	m_DirectRenderImage.fill(Vec4f(0));
	m_RenderImage.setSize(viewer.getGBuffer().getSize());
	m_RenderImage.fill(Vec4f(0));

	m_Rng.init(getSystemThreadCount(), 0);
}

void CPUVPLRenderPass::drawGUI(SSMViewer &viewer) {
	auto& gui = viewer.getGUI();
	if (auto window = gui.addWindow("CPUVPLRenderPass")) {
		gui.addVarRW(BNZ_GUI_VAR(m_nIterationCount));
		gui.addButton("Render Iterations", [&]() {
			const auto& vpls = viewer.getIndirectVPLs();
			float rcpPathCount = viewer.getRealPathCount();

			Timer timer;

			std::atomic_size_t processedPixel{ 0 };
			for (auto i = 0u; i < m_nIterationCount; ++i) {
				std::mutex printMutex;
				processTasks(m_IndirectRenderImage.getPixelCount(), [&](size_t pixelID, size_t threadID) {
					auto pixel = getPixel(pixelID, m_IndirectRenderImage.getSize());
					m_IndirectRenderImage[pixelID] += Vec4f(Vec3f(0), 1); // Add one sample
					m_DirectRenderImage[pixelID] += Vec4f(Vec3f(0), 1); // Add one sample
					m_RenderImage[pixelID] += Vec4f(Vec3f(0), 1); // Add one sample

					PixelSensor sensor(viewer.getCamera(), pixel, m_IndirectRenderImage.getSize());

					RaySample raySample;
					float positionPdf, directionPdf;
					auto We = sensor.sampleExitantRay(*viewer.getScene(), m_Rng.getFloat2(threadID), m_Rng.getFloat2(threadID), raySample, positionPdf, directionPdf);

					auto I = viewer.getScene()->intersect(raySample.value);
					if (I) {
						BSDF bsdf(-raySample.value.dir, I, *viewer.getScene());
						for (const auto& vpl : vpls) {
							auto dirToVPL = vpl.intersection.P - I.P;
							auto dist = length(dirToVPL);

							if (dist > 0.f) {
								dirToVPL /= dist;
								auto G = max(dot(dirToVPL, I.Ns), 0.f) * max(dot(-dirToVPL, vpl.intersection.Ns), 0.f) / sqr(dist);
								G = min(G, viewer.getGeometryFactorBound());

								if (G > 0.f) {
									auto shadowRay = Ray(I, vpl.intersection);

									if (!viewer.computeShadows() || !viewer.getScene()->occluded(shadowRay)) {
										auto materialFactor = bsdf.getDiffuseCoefficient() * vpl.bsdf.getDiffuseCoefficient();
										auto contrib = (1.f / rcpPathCount) * vpl.power * materialFactor * G;
										m_IndirectRenderImage[pixelID] += Vec4f(contrib, 0);
										m_RenderImage[pixelID] += Vec4f(contrib, 0);
									}
								}
							}
						}

						// Direct illumination
						const auto& pointLight = viewer.getLight();
						auto dirToLight = pointLight.m_Position -I.P;
						auto dist = length(dirToLight);
						if (dist > 0.f) {
							dirToLight /= dist;
							auto G = max(dot(dirToLight, I.Ns), 0.f)  / sqr(dist);

							if (G > 0.f) {
								auto shadowRay = Ray(I, dirToLight, dist);

								if (!viewer.computeShadows() || !viewer.getScene()->occluded(shadowRay)) {
									auto materialFactor = bsdf.getDiffuseCoefficient();
									auto contrib = pointLight.m_Intensity * materialFactor * G;
									m_DirectRenderImage[pixelID] += Vec4f(contrib, 0);
									m_RenderImage[pixelID] += Vec4f(contrib, 0);
								}
							}
						}
					}

					++processedPixel;
					auto count = processedPixel.load();
					if (count % 128 == 0u) {
						auto l = std::unique_lock<std::mutex>(printMutex);
						LOG(INFO) << 100. * count / (m_nIterationCount * m_RenderImage.getPixelCount()) << " %";
					}
				}, getSystemThreadCount());
			}

			m_ComputationTime += timer.getMicroEllapsedTime();
		});
		gui.addButton("Clear", [&]() {
			m_IndirectRenderImage.fill(Vec4f(0));
			m_DirectRenderImage.fill(Vec4f(0));
			m_RenderImage.fill(Vec4f(0));

			m_ComputationTime = Microseconds{ 0 };
		});
		gui.addValue("ComputationTime", us2ms(m_ComputationTime.count()));
	}
}

void CPUVPLRenderPass::render(SSMViewer &viewer) {
	drawGUI(viewer);

	viewer.getScreenFramebuffer().bindForDrawing();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	viewer.getScreenFramebuffer().setGLViewport();

	if (viewer.computeDirectLighting()) {
		viewer.getGLImageRenderer().drawImage(viewer.getGamma(), m_RenderImage);
	}
	else {
		viewer.getGLImageRenderer().drawImage(viewer.getGamma(), m_IndirectRenderImage);
	}
}

}