#include "TextureFilteringComputationPass.hpp"


namespace BnZ {

TextureFilteringComputationPass::
TextureFilteringComputationPass (const GLShaderManager& shaderManager) :
    m_Program(shaderManager.buildProgram(
{ "skeleton_shadow_mapping_refactored/TextureFilteringComputationPass.cs" })) {}


void TextureFilteringComputationPass ::render(GLTexture2D& input, GLTexture2D& output,
                                              const GLTexture2D& diffuseSampler) {
    m_Program.use();

    input.bindImage(0, 0, GL_READ_ONLY, GL_RGBA32F);
    uInitialRenderImage.set(0);

    output.bindImage(1, 0, GL_WRITE_ONLY, GL_RGBA32F);
    uFinalRenderImage.set(1);

    diffuseSampler.bind(2);
    uDiffuseSampler.set(2);

    const Vec2u groupSize(32, 32);
    glDispatchCompute(1 + input.getWidth() / groupSize.x,
                      1 + input.getHeight() / groupSize.y,
                      1);
    glMemoryBarrier(GL_ALL_BARRIER_BITS);
}
}
