﻿#include "SkeletonNodeVisibilityTextureComputationPass.hpp"

namespace BnZ {

SkeletonNodeVisibilityTextureComputationPass::SkeletonNodeVisibilityTextureComputationPass(const GLShaderManager& shaderManager) :
    m_Program(shaderManager.buildProgram({ "deferredShadingPass.vs",
                                           "skeleton_shadow_mapping_refactored/SkeletonNodeVisibilityTextureComputationPass.fs"})) {
}

void SkeletonNodeVisibilityTextureComputationPass::render(const GLScreenTriangle& triangle,
                                                          const GLGBuffer& gBuffer,
                                                          const std::vector<Vec3f>& positions,
                                                          const ShadowCubeMapContainer& shadowContainer,
                                                          float fBias,
                                                          size_t nCount,
                                                          const Mat4f& rcpProjMatrix,
                                                          const Mat4f& rcpViewMatrix,
                                                          SkeletonNodeVisibilityTextureContainer& container) {

    m_Program.use();

    glBindFramebuffer(GL_FRAMEBUFFER, m_FBO.glId());
    glDisable(GL_DEPTH_TEST);

    glViewport(0, 0, container.getTextureWidth(), container.getTextureHeight());

    uGBuffer.set(gBuffer, 0, 1, 2);
    uDepthMap.set(3);
    uBias.set(fBias);
    uRcpProjMatrix.set(rcpProjMatrix);
    uRcpViewMatrix.set(rcpViewMatrix);

    //nCount = 1;

    for (auto i : range(nCount)) {
        // Getting the array containing the layer of the current node.
        // auto& textureArray = m_ShadowTexture2DArrays[0];
        // Binding the right layer of the right array.
        glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                                  container.getTextureArray(container.getTextureArrayIndexForTheLayer(i)).glId(), 0,
                                  container.getTextureArrayLocalLayerIndex(i));

        glNamedFramebufferDrawBuffer(m_FBO.glId(), GL_COLOR_ATTACHMENT0);

        // Checking the framebuffer status.
        GLenum status;
        if ((status = glCheckFramebufferStatus(GL_FRAMEBUFFER)) != GL_FRAMEBUFFER_COMPLETE) {
            std::cout << "NodeShadowContainer::generateFBOs : Framebuffer not complete !\n"
                      << GLFramebufferErrorString(status) << std::endl;
        }

        glClear(GL_COLOR_BUFFER_BIT);

        // Sending the position of the current node.
        uNodePosition.set(positions[i]);
        uNodeLayer.set(shadowContainer.
                       getDepthMapTextureLocalLayerIndex(i));


        // Binding the current shadow cube map array.
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_CUBE_MAP_ARRAY,
                      shadowContainer.getDepthMapTextureForTheLayer(i));

        // Drawing the whole scene.
        triangle.render();

        // Disabling the current SCMA texture.
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_CUBE_MAP_ARRAY, 0);
    }

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void SkeletonNodeVisibilityTextureComputationPass::render(const GLScreenTriangle& triangle,
                                                          const GLGBuffer& gBuffer,
                                                          const std::vector<Vec3f>& positions,
                                                          const ShadowCubeMapContainer& shadowContainer,
                                                          float fBias,
                                                          const size_t* pIndices, // Indices of visibility textures to render
                                                          size_t nCount, // Number of indices
                                                          const Mat4f& rcpProjMatrix,
                                                          const Mat4f& rcpViewMatrix,
                                                          SkeletonNodeVisibilityTextureContainer& container) {

    m_Program.use();

    glBindFramebuffer(GL_FRAMEBUFFER, m_FBO.glId());
    glDisable(GL_DEPTH_TEST);

    glViewport(0, 0, container.getTextureWidth(), container.getTextureHeight());

    uGBuffer.set(gBuffer, 0, 1, 2);
    uDepthMap.set(3);
    uBias.set(fBias);
    uRcpProjMatrix.set(rcpProjMatrix);
    uRcpViewMatrix.set(rcpViewMatrix);

    for (auto j : range(nCount)) {
        auto i = pIndices[j];

        // Getting the array containing the layer of the current node.
        // auto& textureArray = m_ShadowTexture2DArrays[0];
        // Binding the right layer of the right array.
        glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                                  container.getTextureArray(container.getTextureArrayIndexForTheLayer(i)).glId(), 0,
                                  container.getTextureArrayLocalLayerIndex(i));

        glNamedFramebufferDrawBuffer(m_FBO.glId(), GL_COLOR_ATTACHMENT0);

        // Checking the framebuffer status.
        GLenum status;
        if ((status = glCheckFramebufferStatus(GL_FRAMEBUFFER)) != GL_FRAMEBUFFER_COMPLETE) {
            std::cout << "NodeShadowContainer::generateFBOs : Framebuffer not complete !\n"
                      << GLFramebufferErrorString(status) << std::endl;
        }

        glClear(GL_COLOR_BUFFER_BIT);

        // Sending the position of the current node.
        uNodePosition.set(positions[i]);
        uNodeLayer.set(shadowContainer.
                       getDepthMapTextureLocalLayerIndex(i));


        // Binding the current shadow cube map array.
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_CUBE_MAP_ARRAY,
                      shadowContainer.getDepthMapTextureForTheLayer(i));

        // Drawing the whole scene.
        triangle.render();

        // Disabling the current SCMA texture.
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_CUBE_MAP_ARRAY, 0);
    }

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void SkeletonNodeVisibilityTextureComputationPass::
renderOnePosition(const GLScreenTriangle& triangle,
                  const GLGBuffer& gBuffer,
                  const Vec3f& nodePosition, size_t nodeIndex,
                  const ShadowCubeMapContainer& shadowContainer,
                  float fBias, bool bUsingPCF, bool bPCFOpti, size_t nPCFAlgo,
                  float fPCFRadius, float fFarPlane,
                  const Mat4f& rcpProjMatrix,
                  const Mat4f& rcpViewMatrix,
                  GLFramebufferObject& framebuffer,
                  float fFramebufferWidth, float fFramebufferHeight) {

    m_Program.use();

    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer.glId());
    glDisable(GL_DEPTH_TEST);
    glViewport(0, 0, fFramebufferWidth, fFramebufferHeight);
    glClear(GL_COLOR_BUFFER_BIT);

    ubPCFOpti.set(bPCFOpti);
    uGBuffer.set(gBuffer, 0, 1, 2);
    uDepthMap.set(3);
    uBias.set(fBias);
    uRcpProjMatrix.set(rcpProjMatrix);
    uRcpViewMatrix.set(rcpViewMatrix);
    ubUsingPCF.set(bUsingPCF);
    unPCFAlgo.set(nPCFAlgo);
    ufPCFRadius.set(fPCFRadius);
    ufFarPlane.set(fFarPlane);

    // Sending the position of the current node.
    uNodePosition.set(nodePosition);

    uNodeLayer.set(shadowContainer.
                   getDepthMapTextureLocalLayerIndex(nodeIndex));


    // Binding the current shadow cube map array.
    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_CUBE_MAP_ARRAY,
                  shadowContainer.getDepthMapTextureForTheLayer(nodeIndex));

    // Drawing the whole scene.
    triangle.render();

    // Disabling the current SCMA texture.
    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_CUBE_MAP_ARRAY, 0);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}
}
