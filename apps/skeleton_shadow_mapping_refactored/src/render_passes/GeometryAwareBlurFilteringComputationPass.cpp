#include "GeometryAwareBlurFilteringComputationPass.hpp"


namespace BnZ {

GeometryAwareBlurFilteringComputationPass::
GeometryAwareBlurFilteringComputationPass(const GLShaderManager& shaderManager) :
    m_Program(shaderManager.buildProgram(
{ "skeleton_shadow_mapping_refactored/GeometryAwareBlurFilteringPass.cs" })) {}


void GeometryAwareBlurFilteringComputationPass::render(const GLTexture2D& input,
                                                       float gaussianTetaN,
                                                       float gaussianTetaP,
													   float zFar,
                                                       size_t windowSize,
                                                       const GLTexture2D& normalDepthSampler,
                                                       GLTexture2D& output) {
    m_Program.use();

    input.bind(0);
    uAccumulatedTexture.set(0);

    output.bindImage(0, 0, GL_WRITE_ONLY, GL_RGBA32F);
    uFinalRenderImage.set(0);

    normalDepthSampler.bind(2);
    uNormalDepthSampler.set(2);

    uTetaN.set(gaussianTetaN);
    uTetaP.set(gaussianTetaP);
	uZFar.set(zFar);

    uWindowSize.set(windowSize);

    const Vec2u groupSize(32, 32);
    glDispatchCompute(1 + input.getWidth() / groupSize.x,
                      1 + input.getHeight() / groupSize.y,
                      1);
    glMemoryBarrier(GL_ALL_BARRIER_BITS);
}
}
