#pragma once

#include "bonez/maths/types.hpp"
#include "bonez/opengl/utils/GLBuffer.hpp"
#include "bonez/opengl/utils/GLTexture.hpp"


namespace BnZ {

///
/// \brief The ShadowCubeMapContainer class contains every informations about shadow maps
/// like FBOs, SCM arrays, and buffers.
///
class ShadowCubeMapContainer {
    static const size_t NUMBER_OF_FACE = 6;
public:

    ///
    /// \brief ShadowContainer
    /// \param cameraNear
    /// \param cameraFar
    ///
    /// TODO: this class should not care about zNear and zFar, only the computation pass should.
    ShadowCubeMapContainer(float cameraNear, float cameraFar, size_t width);

    ///
    /// \brief init initializes the current shadowPass by allocating cube
    /// map textures made of layers.
    /// \param nMaxLayerCountRequested number maximum of layer to handle.
    ///
    void init(size_t nMaxLayerCountRequested);

    ///
    /// \brief getShadowFar
    /// \return the far plane used during these computations.
    ///
    inline float getShadowFar() const {
        return m_fFar;
    }

    ///
    /// \brief getShadowNear
    /// \return the near plane used during these computations.
    ///
    inline float getShadowNear() const {
        return m_fNear;
    }

    ///
    /// \brief getDepthMaps
    /// \return shadow maps lastly computed.
    ///
    inline GLuint getDepthMapId(size_t index) const {
        return m_DistanceCubeMapArrays[index].glId();
    }

    ///
    /// \brief getNumberOfFBOsToPool Calculates the number of depth map Array to
    /// pool, to request the exact number of layer requested.
    /// \param nLayerCountRequested
    /// \return the number of depth map necessary to retrieve nLayerCountRequested CSM.
    ///
    inline size_t getNumberOfDepthMapTextureToPool(size_t nLayerCountRequested) const {
        return (nLayerCountRequested / m_nLimitArraySizePerFBOs) +
                (nLayerCountRequested % m_nLimitArraySizePerFBOs == 0 ? 0 : 1);
    }

    inline GLuint getDepthMapTextureForTheLayer(size_t nLayerIndexRequested) const {
        return getDepthMapId(nLayerIndexRequested / m_nLimitArraySizePerFBOs);
    }

    inline size_t getDepthMapTextureLocalLayerIndex(size_t nLayerIndexRequested) const  {
        return nLayerIndexRequested % m_nLimitArraySizePerFBOs;
    }

    ///
    /// \brief getLayerCount
    /// \param index
    /// \return the number of layer for this depth map array.
    ///
    inline size_t getLayerCount(size_t index) const {
        return m_DepthCubeMapArrays[index].getLayerCount();
    }

    ///
    /// \brief getShadowMatricesBuffer
    /// \return the buffer containing the VPmatrices of the lights processed.
    ///
    inline const GLBuffer<Mat4f>& getLightMatricesBuffer(size_t index) const {
        return m_LightMatricesBuffers[index];
    }

    ///
    /// \brief getlightPositionsBuffer
    /// \return the buffer of light positions.
    ///
    inline const GLBuffer<Vec4f>& getlightPositionsBuffer(size_t index) const {
        return m_LightPositionsBuffers[index];
    }

    ///
    /// \brief getFBO
    /// \return the FBO used for the current container.
    ///
    inline GLuint getFBO(size_t index) const {
        return m_FBOs[index];
    }

    ///
    /// \brief initShadowTransforms initializes lightPositions::size
    /// shadows transform using lightPositions.
    /// \param lightPositions
    ///
    void buildShadowTransforms(const std::vector<Vec3f>& lightPositions);
    void buildShadowTransforms2(const std::vector<Vec3f>& lightPositions);

    inline size_t getShadowMapWidth() const {
        return m_nWidth;
    }

    inline size_t getDepthMapMaxLayerCount() const {
        return m_nLimitArraySizePerFBOs;
    }

private:

    ///
    /// \brief generateShadowUnit generates a
    /// \param nLayerCount
    /// \param index
    ///
    void generateFBOs(size_t index, size_t nLayerCount);

    ///
    /// \brief generateRangeShadowTransforms Stores at index in our buffer the shadow
    /// transforms generated for positions from it -> to the end of the current buffer,
    /// or stops as soon as end is reached.
    /// \param index
    /// \param projectionMatrix
    /// \param it
    /// \param end
    ///
    void generateRangeShadowTransforms(int index, const Mat4f& projectionMatrix,
                                       std::vector<Vec3f>::const_iterator& it,
                                       const std::vector<Vec3f>::const_iterator& end);
    void generateRangeShadowTransforms2(int index, const Mat4f& projectionMatrix,
                                       std::vector<Vec3f>::const_iterator& it,
                                       const std::vector<Vec3f>::const_iterator& end);

    /// The maximum ShadowCubeMap array size for the current system's GPU.
    size_t m_nLimitArraySizePerFBOs;

    /// The maximum global size to contains all shadow cube maps.
    size_t m_nMaxGlobalSizeAllocated;

    /// Near & far used to build the projection matrix of the shadowMapFaces.
    float m_fNear, m_fFar;

    /// Texture size.
    size_t m_nWidth;

    /// Indexes.
    std::vector<GLuint> m_FBOs;

    /// Textures array.
    std::vector<GLTextureCubeMapArray> m_DistanceCubeMapArrays;
    std::vector<GLTextureCubeMapArray> m_DepthCubeMapArrays;

    /// ViewProjection matrices buffer used to send m_ShadowTransforms to GPU.
    std::vector<GLBuffer<Mat4f>> m_LightMatricesBuffers;

    /// Light positions buffer used to send lights' position to GPU.
    std::vector<GLBuffer<Vec4f>> m_LightPositionsBuffers;
};

}
