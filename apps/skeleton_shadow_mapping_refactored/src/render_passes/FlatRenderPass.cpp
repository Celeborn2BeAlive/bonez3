#include "FlatRenderPass.hpp"
#include "../SSMViewer.hpp"

namespace BnZ {

FlatRenderPass::FlatRenderPass(const GLShaderManager& shaderManager):
    m_FlatShadingPass(shaderManager) {
}

void FlatRenderPass::render(SSMViewer &viewer) {
    if(auto window = viewer.getGUI().addWindow("FlatRenderPass")) {
        m_FlatShadingPass.exposeIO(viewer.getGUI());
    }

    viewer.getScreenFramebuffer().bindForDrawing();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    viewer.getScreenFramebuffer().setGLViewport();

    m_FlatShadingPass.render(viewer.getGBuffer(), viewer.getRcpProjMatrix(),
                             viewer.getViewMatrix(), viewer.getScreenTriangle());
}

}

