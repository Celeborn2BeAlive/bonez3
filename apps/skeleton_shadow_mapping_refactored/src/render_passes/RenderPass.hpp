#pragma once

namespace BnZ {

class SSMViewer;

// The SSMViewer uses RenderPass instances to render on screen
class RenderPass {
public:
    virtual ~RenderPass() = default;

    // Must render in viewer.getScreenFramebuffer()
    virtual void render(SSMViewer& viewer) = 0;

private:
};

}
