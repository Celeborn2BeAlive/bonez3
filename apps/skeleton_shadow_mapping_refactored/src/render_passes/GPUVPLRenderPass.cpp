#include "GPUVPLRenderPass.hpp"

#include <bonez/opengl/GLShaderManager.hpp>
#include "../SSMViewer.hpp"

namespace BnZ {

	GPUVPLRenderPass::GPUVPLRenderPass(SSMViewer &viewer) {
	}

	void GPUVPLRenderPass::drawGUI(SSMViewer &viewer) {
		auto& gui = viewer.getGUI();
		if (auto window = gui.addWindow("GPUVPLRenderPass")) {
			
		}
	}

	void GPUVPLRenderPass::render(SSMViewer &viewer) {
		drawGUI(viewer);

		viewer.getScreenFramebuffer().bindForDrawing();
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		viewer.getScreenFramebuffer().setGLViewport();

	}

}