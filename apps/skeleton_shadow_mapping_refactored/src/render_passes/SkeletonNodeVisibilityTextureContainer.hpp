#pragma once

#include <bonez/opengl/utils/GLObject.hpp>
#include <bonez/opengl/utils/GLTexture.hpp>

namespace BnZ {

class SkeletonNodeVisibilityTextureContainer {
public:
    SkeletonNodeVisibilityTextureContainer(size_t nTextureWidth,
                                           size_t nTextureHeight,
                                           size_t nMaxLayerRequested);

    inline size_t getTextureArrayCount() const {
        return m_ShadowTexture2DArrays.size();
    }

    inline size_t getLimitTextureArraySize() const {
        return m_nLimitTextureArraySize;
    }

    inline const GLTexture2DArray& getTextureArray(size_t textureIndex) const {
        return m_ShadowTexture2DArrays[textureIndex];
    }

    inline size_t getTextureArrayIndexForTheLayer(size_t nLayerIndexRequested) const {
        return nLayerIndexRequested / m_nLimitTextureArraySize;
    }

    inline size_t getTextureArrayLocalLayerIndex(size_t nLayerIndexRequested) const  {
        return nLayerIndexRequested % m_nLimitTextureArraySize;
    }

    inline auto getTextureWidth() {
        return m_nTextureWidth;
    }

    inline auto getTextureHeight() {
        return m_nTextureHeight;
    }

    inline size_t getNumberOfTextureArrayToCreate(size_t nLayerCountRequested) const {
        return nLayerCountRequested / m_nLimitTextureArraySize + (nLayerCountRequested % m_nLimitTextureArraySize) ? 1 : 0;
    }
private:

    void generateTextureArray(size_t index, size_t nLayerCount);

    size_t m_nTextureWidth;
    size_t m_nTextureHeight;

    /// The maximum texture array size
    size_t m_nLimitTextureArraySize;

    /// The max global size. Can not be exceeded.
    size_t m_nMaxGlobalSizeAllocated;

    /// The shadow textures.
    std::vector<GLTexture2DArray> m_ShadowTexture2DArrays;
};

}
