#include "ShadowCubeMapContainer.hpp"
#include "ShadowCubeMapComputationPass.hpp"
#include "../gpu_vpl_utilities.hpp"
#include "../SSMViewer.hpp"

namespace BnZ {

class GLShaderManager;

class DirectIlluminationComputationPass {

public:

    DirectIlluminationComputationPass(SSMViewer &viewer);

    void render(GLFramebuffer2D<1, false> &framebuffer, SSMViewer &viewer, float shadowBias);

private:

    void renderVPLShadowCubeMap(const GLScreenTriangle& triangle, GUI& gui);

    GLProgram m_Program;

    GLGBufferUniform uGBuffer { m_Program };
    BNZ_GLUNIFORM(m_Program, Mat4f, uRcpProjMatrix);
    BNZ_GLUNIFORM(m_Program, Mat4f, uRcpViewMatrix);
    BNZ_GLUNIFORM(m_Program, GLint, uDepthMapArray);
    BNZ_GLUNIFORM(m_Program, GLfloat, ufBias);

    /* Point light uniforms. */
    BNZ_GLUNIFORM(m_Program, Vec4f, uLightPositionWs);
    BNZ_GLUNIFORM(m_Program, Vec4f, uLightPositionVs);
    BNZ_GLUNIFORM(m_Program, Vec3f, uLightIntensity);
    BNZ_GLUNIFORM(m_Program, Vec3f, uLightKD);

    ShadowCubeMapComputationPass m_ShadowPass;
    ShadowCubeMapContainer m_ShadowContainer;
    GLFramebuffer2D<1, false> m_DepthMapDisplayFramebuffer;

    /// Our LightBuffer bridging with OpenGL to send world lights.
    GLBufferStorage<BufferedLightGPU> m_LightBuffer;


    struct DrawPass {
        GLProgram m_Program;

        BNZ_GLUNIFORM(m_Program, float, uZNear);
        BNZ_GLUNIFORM(m_Program, float, uZFar);
        BNZ_GLUNIFORM(m_Program, GLuint, uMapIndex);
        BNZ_GLUNIFORM(m_Program, bool, uDrawDepth);
        BNZ_GLUNIFORM(m_Program, int, uCubeMapContainer);

        // GLCubeMapContainerUniform uCubeMapContainer { m_Program };

        DrawPass(const GLShaderManager& shaderManager):
            m_Program(shaderManager.buildProgram({
                                                     "skeleton_shadow_mapping/image.vs",
                                                     "skeleton_shadow_mapping/drawCubeMap.fs"
                                                 })) {}
    };

    DrawPass m_DrawDepthPass;
};
}
