#include "RenderPass.hpp"

#include <bonez/image/Image.hpp>
#include <bonez/sampling/Random.hpp>
#include <bonez/sys/time.hpp>

namespace BnZ {

	class CPUVPLRenderPass : public RenderPass {
	public:
		CPUVPLRenderPass(SSMViewer &viewer);

		virtual void render(SSMViewer &viewer) override;

	private:
		void drawGUI(SSMViewer &viewer);

		Image m_IndirectRenderImage;
		Image m_DirectRenderImage;
		Image m_RenderImage;
		ThreadsRandomGenerator m_Rng;

		size_t m_nIterationCount = 1u;
		Microseconds m_ComputationTime{ 0 };
	};

}
