#include "SkeletonNodeShadowsRenderPassOriginal.hpp"
#include "../SSMViewer.hpp"

#include <bonez/opengl/GLShaderManager.hpp>

namespace BnZ {

SkeletonNodeShadowsRenderPassOriginal::SkeletonNodeShadowsRenderPassOriginal(
        SSMViewer &viewer):
    m_nNodeCount(viewer.getScene()->getCurvSkeleton()->size()),
    m_SkeletonNodeVisibilityTextureComputationPass(viewer.getShaderManager()),
    m_SkeletonNodeVisibilityTextureContainer(viewer.getScreenFramebuffer().getWidth(),
                                             viewer.getScreenFramebuffer().getHeight(),
                                             m_nNodeCount),
    m_Program(viewer.getShaderManager().buildProgram({ "deferredShadingPass.vs",
                                                       "skeleton_shadow_mapping_refactored/SkeletonNodeShadowsRenderPassOriginal.fs" })),
    m_NodesPositionsBuffer(m_nNodeCount, nullptr, GL_MAP_WRITE_BIT) {

    auto pNodePosition = m_NodesPositionsBuffer.mapRange(0, m_nNodeCount, GL_MAP_WRITE_BIT);
    for(auto position: viewer.getNodePositionArray()) {
        *pNodePosition = Vec4f(position, 1.f);
        ++pNodePosition;
    }
    m_NodesPositionsBuffer.unmap();
}


void SkeletonNodeShadowsRenderPassOriginal::render(SSMViewer &viewer) {

    drawGUI(viewer);

    const auto nodeIndex = viewer.getSelectedNodeIndex();
    const auto& gBuffer = viewer.getGBuffer();

    m_SkeletonNodeVisibilityTextureComputationPass.render(viewer.getScreenTriangle(), gBuffer,
                                                          viewer.getNodePositionArray(),
                                                          viewer.getSkeletonNodeShadowCubeMapContainer(), m_fBias, &nodeIndex, 1,
                                                          viewer.getRcpProjMatrix(), viewer.getRcpViewMatrix(),
                                                          m_SkeletonNodeVisibilityTextureContainer);

    glDisable(GL_DEPTH_TEST);

    auto& framebuffer = viewer.getScreenFramebuffer();
    framebuffer.bindForDrawing();
    glViewport(0, 0, framebuffer.getWidth(), framebuffer.getHeight());

    glClear(GL_COLOR_BUFFER_BIT);

    m_Program.use();

    uGBuffer.set(gBuffer, 0, 1, 2);
    uNodeIndex.set(nodeIndex);

    auto nTexture2DUnit = 3;
    uShadowTexture2DArray.set(nTexture2DUnit);
    m_SkeletonNodeVisibilityTextureContainer.getTextureArray(0)
            .bind(nTexture2DUnit);
    uIntensity.set(m_Intensity);

    viewer.getScreenTriangle().render();
}


void SkeletonNodeShadowsRenderPassOriginal::drawGUI(SSMViewer &viewer) {
    auto& gui = viewer.getGUI();
    if(auto window = gui.addWindow("SkeletonShadowsRenderPass")) {
        gui.addVarRW(BNZ_GUI_VAR(m_fBias));
        gui.addVarRW(BNZ_GUI_VAR(m_Intensity));
    }
}
}
