#include "ShadowCubeMapContainer.hpp"
#include "bonez/opengl/opengl.hpp"
#include "bonez/opengl/utils/GLFramebuffer.hpp"

namespace BnZ {

ShadowCubeMapContainer::ShadowCubeMapContainer(float cameraNear, float cameraFar, size_t width) :
    m_fNear(cameraNear), m_fFar(cameraFar), m_nWidth(width) {
    GLint nMaxSize;
    glGetIntegerv(GL_MAX_ARRAY_TEXTURE_LAYERS, &nMaxSize);
    m_nLimitArraySizePerFBOs = nMaxSize / NUMBER_OF_FACE;
}


void ShadowCubeMapContainer::init(size_t nMaxLayerCountRequested) {

    m_nMaxGlobalSizeAllocated = nMaxLayerCountRequested;

    // Getting the number of FBOs needed.
    size_t nFBOGeneratedCount = getNumberOfDepthMapTextureToPool(m_nMaxGlobalSizeAllocated);

    // Clearing all.
    std::cout << "ShadowCubeMapContainer::Vector size :" << m_LightMatricesBuffers.size()
              << std::endl;

    m_FBOs.clear();
    m_DistanceCubeMapArrays.clear();
    m_DepthCubeMapArrays.clear();
    m_LightMatricesBuffers.clear();
    m_LightPositionsBuffers.clear();

    // Reserving.
    m_FBOs.resize(nFBOGeneratedCount);
    m_DistanceCubeMapArrays.resize(nFBOGeneratedCount);
    m_DepthCubeMapArrays.resize(nFBOGeneratedCount);
    m_LightMatricesBuffers.resize(nFBOGeneratedCount);
    m_LightPositionsBuffers.resize(nFBOGeneratedCount);

    // Generating every FBOs. The last has its buffers' length of the exact size.
    for (size_t i = 0; i < nFBOGeneratedCount; ++i) {
        auto nLayerCount = m_nLimitArraySizePerFBOs <= nMaxLayerCountRequested ?
                    m_nLimitArraySizePerFBOs : nMaxLayerCountRequested;
        nMaxLayerCountRequested -= nLayerCount;
        generateFBOs(i, nLayerCount);
    }
}


void ShadowCubeMapContainer::generateFBOs(size_t index, size_t nLayerCount) {

    // Setting buffers' size. 6 faces for each layer in the Matrices one.
    m_LightMatricesBuffers[index].setSize(NUMBER_OF_FACE * nLayerCount, GL_DYNAMIC_DRAW);
    m_LightPositionsBuffers[index].setSize(nLayerCount, GL_DYNAMIC_DRAW);

    // Settings texture cube map.
    auto& distanceCubeMapArray = m_DistanceCubeMapArrays[index];
    auto& depthCubeMapArray = m_DepthCubeMapArrays[index];

    distanceCubeMapArray.setImage(0, GL_R32F, m_nWidth, m_nWidth, nLayerCount,
                                  0, GL_RED, GL_FLOAT, nullptr);

    // Setting the texture parameters.
    distanceCubeMapArray.setFilters(GL_NEAREST, GL_NEAREST);
    distanceCubeMapArray.setWrapS(GL_CLAMP_TO_EDGE);
    distanceCubeMapArray.setWrapR(GL_CLAMP_TO_EDGE);
    distanceCubeMapArray.setWrapT(GL_CLAMP_TO_EDGE);

    // Generating 6 cubemap faces. They are 2D depth-valued texture images.
    depthCubeMapArray.setImage(0, GL_DEPTH_COMPONENT32F, m_nWidth, m_nWidth,
                               nLayerCount, 0, GL_DEPTH_COMPONENT,
                               GL_FLOAT, nullptr);

    // Setting the texture parameters.
    depthCubeMapArray.setFilters(GL_NEAREST, GL_NEAREST);
    depthCubeMapArray.setWrapS(GL_CLAMP_TO_EDGE);
    depthCubeMapArray.setWrapR(GL_CLAMP_TO_EDGE);
    depthCubeMapArray.setWrapT(GL_CLAMP_TO_EDGE);

    // Attaching the entire cubemap as the depth texture.
    // We'll process each face in the GS.
    auto& depthMapFBO = m_FBOs[index];
    glGenFramebuffers(1, &depthMapFBO);
    glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, distanceCubeMapArray.glId(), 0);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthCubeMapArray.glId(), 0);

    // No Color buffer needed.
    GLenum buff = GL_COLOR_ATTACHMENT0;
    glDrawBuffers(1, &buff);

    GLenum status;
    if ((status = glCheckFramebufferStatus(GL_FRAMEBUFFER)) != GL_FRAMEBUFFER_COMPLETE) {
        std::cout << "Shadowpass Init : Framebuffer not complete!\n" <<
                     GLFramebufferErrorString(status) << std::endl;
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}


void ShadowCubeMapContainer::buildShadowTransforms(const std::vector<Vec3f>& lightPositions) {

    auto nLightNumber = lightPositions.size();

    // Checking if the maximum global size is large enaugh to retrieve
    // lightPositions.size lights.
    if (nLightNumber > m_nMaxGlobalSizeAllocated) {
        std::cerr << "ShadowContainer : Warning, light number > max size allocated : "
                  << nLightNumber << std::endl;
    }

    // Constructing the light space transform matrix (VPMatrix from light position).
    GLfloat aspect = 1.f;

    // Same projection matrix (which is a perspective one).
    Mat4f projectionMatrix(glm::perspective(radians(90.0f), aspect, m_fNear, m_fFar));

    // Retrieving the number of indexes needed to build lightPositions.size() shadowTransform.
    auto nbIndexes = getNumberOfDepthMapTextureToPool(nLightNumber);

    auto it = lightPositions.begin();
    const auto end = lightPositions.end();

    // Storing shadow transforms.
    for (int i = 0; i < nbIndexes; ++i) {
        // Creating each view matrices (we are sure of these iterators, because of
        // our previous checks).
        generateRangeShadowTransforms(i, projectionMatrix, it, end);
    }
}


void ShadowCubeMapContainer::generateRangeShadowTransforms(int index, const Mat4f& projectionMatrix,
                                                           std::vector<Vec3f>::const_iterator& it,
                                                           const std::vector<Vec3f>::const_iterator& end) {

    Vec4f *plighPositions = m_LightPositionsBuffers[index].map(GL_WRITE_ONLY);
    Mat4f *pShadowTransforms = m_LightMatricesBuffers[index].map(GL_WRITE_ONLY);

    // Generating shadow transforms to get the ViewProjectionMatrices of each face.
    for (int i = 0; i < m_LightPositionsBuffers[index].size() && it != end; ++i, ++it) {
        auto& vPos = *it;

        Vec4f v4Pos(vPos, 1.0f);
        Mat4f xPlusView(glm::lookAt(vPos, vPos + Vec3f(1.0, 0.0, 0.0), Vec3f(0.0, -1.0, 0.0)));
        Mat4f xMinusView(glm::lookAt(vPos, vPos + Vec3f(-1.0, 0.0, 0.0), Vec3f(0.0, -1.0, 0.0)));
        Mat4f yPlusView(glm::lookAt(vPos, vPos + Vec3f(0.0, 1.0, 0.0), Vec3f(0.0, 0.0, 1.0)));
        Mat4f yMinusView(glm::lookAt(vPos, vPos + Vec3f(0.0, -1.0, 0.0), Vec3f(0.0, 0.0, -1.0)));
        Mat4f zPlusView(glm::lookAt(vPos, vPos + Vec3f(0.0, 0.0, 1.0), Vec3f(0.0, -1.0, 0.0)));
        Mat4f zMinusView(glm::lookAt(vPos, vPos + Vec3f(0.0, 0.0, -1.0), Vec3f(0.0, -1.0, 0.0)));

        (*plighPositions++) = v4Pos;
        (*pShadowTransforms++) = projectionMatrix * xPlusView;
        (*pShadowTransforms++) = projectionMatrix * xMinusView;
        (*pShadowTransforms++) = projectionMatrix * yPlusView;
        (*pShadowTransforms++) = projectionMatrix * yMinusView;
        (*pShadowTransforms++) = projectionMatrix * zPlusView;
        (*pShadowTransforms++) = projectionMatrix * zMinusView;
    }

    m_LightMatricesBuffers[index].unmap();
    m_LightPositionsBuffers[index].unmap();
}


void ShadowCubeMapContainer::buildShadowTransforms2(const std::vector<Vec3f>& lightPositions) {

    auto nLightNumber = lightPositions.size();

    // Checking if the maximum global size is large enaugh to retrieve
    // lightPositions.size lights.
    if (nLightNumber > m_nMaxGlobalSizeAllocated) {
        std::cerr << "ShadowContainer : Warning, light number > max size allocated : "
                  << nLightNumber << std::endl;
    }

    // Constructing the light space transform matrix (VPMatrix from light position).
    GLfloat aspect = 1.f;

    // Same projection matrix (which is a perspective one).
    Mat4f projectionMatrix(glm::perspective(radians(90.0f), aspect, m_fNear, m_fFar));

    // Retrieving the number of indexes needed to build lightPositions.size() shadowTransform.
    auto nbIndexes = getNumberOfDepthMapTextureToPool(nLightNumber);

    auto it = lightPositions.begin();
    const auto end = lightPositions.end();

    // Storing shadow transforms.
    for (int i = 0; i < nbIndexes; ++i) {
        // Creating each view matrices (we are sure of these iterators, because of
        // our previous checks).
        generateRangeShadowTransforms(i, projectionMatrix, it, end);
    }
}

void ShadowCubeMapContainer::generateRangeShadowTransforms2(int index, const Mat4f& projectionMatrix,
                                                            std::vector<Vec3f>::const_iterator& it,
                                                            const std::vector<Vec3f>::const_iterator& end) {

    std::vector<Vec4f> positions;
    std::vector<Mat4f> transforms;
//    Vec4f *plighPositions = m_LightPositionsBuffers[index].map(GL_WRITE_ONLY);
//    Mat4f *pShadowTransforms = m_LightMatricesBuffers[index].map(GL_WRITE_ONLY);

    // Generating shadow transforms to get the ViewProjectionMatrices of each face.
    for (int i = 0; i < m_LightPositionsBuffers[index].size() && it != end; ++i, ++it) {

        auto& vPos = *it;

        Vec4f v4Pos(vPos, 1.0f);
        Mat4f xPlusView(glm::lookAt(vPos, vPos + Vec3f(1.0, 0.0, 0.0), Vec3f(0.0, -1.0, 0.0)));
        Mat4f xMinusView(glm::lookAt(vPos, vPos + Vec3f(-1.0, 0.0, 0.0), Vec3f(0.0, -1.0, 0.0)));
        Mat4f yPlusView(glm::lookAt(vPos, vPos + Vec3f(0.0, 1.0, 0.0), Vec3f(0.0, 0.0, 1.0)));
        Mat4f yMinusView(glm::lookAt(vPos, vPos + Vec3f(0.0, -1.0, 0.0), Vec3f(0.0, 0.0, -1.0)));
        Mat4f zPlusView(glm::lookAt(vPos, vPos + Vec3f(0.0, 0.0, 1.0), Vec3f(0.0, -1.0, 0.0)));
        Mat4f zMinusView(glm::lookAt(vPos, vPos + Vec3f(0.0, 0.0, -1.0), Vec3f(0.0, -1.0, 0.0)));

        positions.push_back(v4Pos);
        transforms.push_back(projectionMatrix * xPlusView);
        transforms.push_back(projectionMatrix * xMinusView);
        transforms.push_back(projectionMatrix * yPlusView);
        transforms.push_back(projectionMatrix * yMinusView);
        transforms.push_back(projectionMatrix * zPlusView);
        transforms.push_back(projectionMatrix * zMinusView);
        /*
        (*plighPositions++) = v4Pos;
        (*pShadowTransforms++) = projectionMatrix * xPlusView;
        (*pShadowTransforms++) = projectionMatrix * xMinusView;
        (*pShadowTransforms++) = projectionMatrix * yPlusView;
        (*pShadowTransforms++) = projectionMatrix * yMinusView
                ;
        (*pShadowTransforms++) = projectionMatrix * zPlusView;
        (*pShadowTransforms++) = projectionMatrix * zMinusView;
        */
    }

    m_LightMatricesBuffers[index].setData(transforms, GL_DYNAMIC_DRAW);
    m_LightPositionsBuffers[index].setData(positions, GL_DYNAMIC_DRAW);
   // m_LightMatricesBuffers[index].unmap();
   // m_LightPositionsBuffers[index].unmap();
}

}
