#pragma once

#include "ShadowCubeMapContainer.hpp"
#include "SkeletonNodeVisibilityTextureContainer.hpp"

#include <vector>
#include <bonez/opengl/utils/GLTexture.hpp>
#include <bonez/opengl/GLGBuffer.hpp>
#include <bonez/opengl/utils/GLutils.hpp>
#include <bonez/opengl/GLShaderManager.hpp>

namespace BnZ {
///
/// \brief The NodeShadowContainer class Allows to contain shadow
/// FBO for each node of the Skeleton.
///
class SkeletonNodeVisibilityTextureComputationPass {
public:

    SkeletonNodeVisibilityTextureComputationPass(const GLShaderManager& shaderManager);

    // Render ALL visibility textures (costly)
    void render(const GLScreenTriangle& triangle,
                const GLGBuffer& gBuffer,
                const std::vector<Vec3f>& positions,
                const ShadowCubeMapContainer& shadowContainer,
                float fBias,
                size_t nCount, // Number of nodes to process in array "positions"
                const Mat4f& rcpProjMatrix,
                const Mat4f& rcpViewMatrix,
                SkeletonNodeVisibilityTextureContainer& container);

    // Render only some visibility textures, specified by an indices array
    void render(const GLScreenTriangle& triangle,
                const GLGBuffer& gBuffer,
                const std::vector<Vec3f>& positions,
                const ShadowCubeMapContainer& shadowContainer,
                float fBias,
                const size_t* pIndices, // Indices of visibility textures to render
                size_t nCount, // Number of indices
                const Mat4f& rcpProjMatrix,
                const Mat4f& rcpViewMatrix,
                SkeletonNodeVisibilityTextureContainer& container);

    void renderOnePosition(const GLScreenTriangle& triangle,
                           const GLGBuffer& gBuffer,
                           const Vec3f& nodePosition, size_t nodeIndex,
                           const ShadowCubeMapContainer& shadowContainer,
                           float fBias, bool bUsingPCF, bool bPCFOpti, size_t nPCFAlgo,
                           float fPCFRadius, float fFarPlane,
                           const Mat4f& rcpProjMatrix,
                           const Mat4f& rcpViewMatrix,
                           GLFramebufferObject& framebuffer,
                           float fFramebufferWidth, float fFramebufferHeight);


    // Ajoute d'une methode render pour dessiner dans un seul FBO et pour un seul noeud

private:
    GLProgram m_Program;

    GLGBufferUniform uGBuffer { m_Program };
    BNZ_GLUNIFORM(m_Program, Vec3f, uNodePosition);
    BNZ_GLUNIFORM(m_Program, GLuint, uNodeLayer);
    BNZ_GLUNIFORM(m_Program, int, uDepthMap); // TODO try GLint instead.
    BNZ_GLUNIFORM(m_Program, GLfloat, uBias);
    BNZ_GLUNIFORM(m_Program, bool, ubUsingPCF);
    BNZ_GLUNIFORM(m_Program, bool, ubPCFOpti);
    BNZ_GLUNIFORM(m_Program, GLuint, unPCFAlgo);
    BNZ_GLUNIFORM(m_Program, GLfloat, ufPCFRadius);
    BNZ_GLUNIFORM(m_Program, GLfloat, ufFarPlane);


    BNZ_GLUNIFORM(m_Program, Mat4f, uRcpViewMatrix);
    BNZ_GLUNIFORM(m_Program, Mat4f, uRcpProjMatrix);

    GLFramebufferObject m_FBO;

/*
    struct PCFFilteringComputationPass {
        GLProgram m_Program;

        BNZ_GLUNIFORM(m_Program, float, uZNear);
        BNZ_GLUNIFORM(m_Program, float, uZFar);
        BNZ_GLUNIFORM(m_Program, GLuint, uMapIndex);
        BNZ_GLUNIFORM(m_Program, bool, uDrawDepth);
        BNZ_GLUNIFORM(m_Program, int, uCubeMapContainer);

        // GLCubeMapContainerUniform uCubeMapContainer { m_Program };

        ShadowCubeMapDrawPass(const GLShaderManager& shaderManager):
            m_Program(shaderManager.buildProgram({ "skeleton_shadow_mapping_refactored/PCFFilteringComputationPass.cs"})) {}
    };*/
};
}
