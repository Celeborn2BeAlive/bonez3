#include "RenderPass.hpp"

namespace BnZ {

	class GPUVPLRenderPass : public RenderPass {
	public:
		GPUVPLRenderPass(SSMViewer &viewer);

		virtual void render(SSMViewer &viewer) override;

	private:
		void drawGUI(SSMViewer &viewer);
	};

}
