#include "RenderPass.hpp"

#include <bonez/opengl/GLFlatShadingPass.hpp>
#include <bonez/image/Image.hpp>

namespace BnZ {

class FlatRenderPass: public RenderPass {
public:
    FlatRenderPass(const GLShaderManager& shaderManager);

    virtual void render(SSMViewer &viewer) override;

private:
    GLFlatShadingPass m_FlatShadingPass;
};

}
