#include "GammaPass.hpp"

namespace BnZ {

GammaPass::GammaPass(const GLShaderManager& shaderManager):
    m_Program(shaderManager.buildProgram({ "skeleton_shadow_mapping_refactored/GammaPass.cs" })){

}

void GammaPass::compute(const GLTexture2D& input,
                       GLTexture2D& output,
                       float gamma) {
    m_Program.use();

    input.bindImage(0, 0, GL_READ_ONLY, GL_RGBA32F);
    uInput.set(0);

    output.bindImage(1, 0, GL_WRITE_ONLY, GL_RGBA32F);
    uOutput.set(0);

    uGamma.set(gamma);

    const Vec2u groupSize(32, 32);
    glDispatchCompute(1 + output.getWidth() / groupSize.x,
                      1 + output.getHeight() / groupSize.y,
                      1);
    glMemoryBarrier(GL_ALL_BARRIER_BITS);
}

}
