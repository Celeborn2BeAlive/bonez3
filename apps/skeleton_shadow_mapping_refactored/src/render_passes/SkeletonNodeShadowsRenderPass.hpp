#include "DirectIlluminationComputationPass.hpp"
#include "RenderPass.hpp"
#include "../gpu_vpl_utilities.hpp"

#include "SkeletonNodeVisibilityTextureComputationPass.hpp"
#include "ScalingComputationPass.hpp"
#include "TextureFilteringComputationPass.hpp"
#include "GeometryAwareBlurFilteringComputationPass.hpp"
#include "GammaPass.hpp"

namespace BnZ {

class GLShaderManager;

class SkeletonNodeShadowsRenderPass: public RenderPass {

public:

    SkeletonNodeShadowsRenderPass(SSMViewer &viewer);

    virtual void render(SSMViewer &viewer) override;

private:

    void drawGUI(SSMViewer &viewer);
    void updateLightPerNodeANDCumulated();
    void updateLightsNearestNodeANDLightLists(const CurvilinearSkeleton& skeleton,
                                              const std::vector<BufferedLightGPU>& lights);
    void updateGPULightInViewSpaceBuffer(const Mat4f& viewMatrix,
                                         const std::vector<BufferedLightGPU>& lights);
    void computeNodeUsedArray();

    size_t m_nNodeCount = 0;
    size_t m_nLightProcessed = 0;
    Vec3f m_Intensity = Vec3f(1.f);
    float m_fGaussianTetaNormal = 0.01f;
    float m_fGaussianTetaPosition = 0.05f;
    size_t m_nInterleavedWindowSize = 1; // No interleaved for now, need to be optimized first
    size_t m_nMinimumLightAttached = 2;

    // Compute passes.
    bool m_bComputeIrradiance = true;
    bool m_bComputeGeometryBlur = true;
    bool m_bComputeScaling = true;
    bool m_bComputeTexture = true;

    // PCF.
    bool m_bUsingPCF = true;
    bool m_bPCFOpti = false;
    size_t m_nPCFAlgo = 2; // Seems to be the best
    float m_fPCFRadius = 1.f; // Does nothing by default (it is a scale factor on the radius actually, and not the true radius)
    float m_fBias = 0.f; // Seems to work well without bias when using PCF

	float m_fDirectLightingBias = 1.f;

    SkeletonNodeVisibilityTextureComputationPass m_SkeletonNodeVisibilityTextureComputationPass;
    SkeletonNodeVisibilityTextureContainer m_SkeletonNodeVisibilityTextureContainer;

    GLProgram m_Program;

    GLGBufferUniform uGBuffer { m_Program };

    BNZ_GLUNIFORM(m_Program, GLuint, uNodeIndex);
    BNZ_GLUNIFORM(m_Program, GLSLSampler2DArrayf, uShadowTexture2D);
    BNZ_GLUNIFORM(m_Program, Vec3f, uIntensity);
    BNZ_GLUNIFORM(m_Program, Mat4f, uRcpProjMatrix);
    BNZ_GLUNIFORM(m_Program, Mat4f, uRcpViewMatrix);
    BNZ_GLUNIFORM(m_Program, bool, ubComputeShadow);

    /// Lights uniforms.
    BNZ_GLUNIFORM(m_Program, GLuint, uLightNumber);
    BNZ_GLUNIFORM(m_Program, GLuint, uLightOffset);
    BNZ_GLUNIFORM(m_Program, GLfloat, uGeometryTermBound);
    BNZ_GLUNIFORM(m_Program, GLuint, uInterleavedWindowSize);

    /// Our LightBuffer bridging with OpenGL to send world lights.
    GLBufferStorage<BufferedLightGPU> m_LightBuffer;
    GLBufferStorage<unsigned int> m_LightNearestNodesBuffer;
    GLBufferStorage<Vec2u> m_LightPerNodeANDCumulatedBuffer;
    GLBufferStorage<size_t> m_LightListOfTheCurrentNodeBuffer;

    // Light in x and the cumulated value of previous x in y.
    std::vector<Vec2u> m_LightPerNodeANDCumulated;
    std::vector<unsigned int> m_LightNearestNodes;
    std::vector<std::vector<size_t>> m_LightListForEachNodes;
    std::vector<size_t> m_NodeUsed;

    GLFramebuffer2D<1, false> m_AccumulationFramebuffer;
    GLFramebuffer2D<1, false> m_TransitionFramebuffer;
    GLFramebuffer2D<1, false> m_Transition2Framebuffer;
    GLFramebuffer2D<1, false> m_Transition3Framebuffer;
    GLFramebufferObject m_VisibilityFramebuffer;

    GLTexture2D m_VisibilityTexture2D;

    DirectIlluminationComputationPass m_DirectIlluminationComputationPass;
    ScalingComputationPass m_ScalingComputationPass;
    TextureFilteringComputationPass m_TextureFilteringComputationPass;
    GeometryAwareBlurFilteringComputationPass m_GeometryAwareBlurFilteringComputationPass;
    GammaPass m_GammaPass;

    GLQuery<GL_TIME_ELAPSED> m_DirectIlluminationComputationTimeQuery;
    GLQuery<GL_TIME_ELAPSED> m_GeometryAwareBlurComputationTimeQuery;
    GLQuery<GL_TIME_ELAPSED> m_CPUComputationTimeQuery;
    GLQuery<GL_TIME_ELAPSED> m_ScalingComputationTimeQuery;
    GLQuery<GL_TIME_ELAPSED> m_textureComputationTimeQuery;

    std::vector<GLQuery<GL_TIME_ELAPSED>> m_VisibilityQueries;
    std::vector<GLQuery<GL_TIME_ELAPSED>> m_IrradianceQueries;
};
}
