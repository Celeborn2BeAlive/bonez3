﻿#pragma once


#include "bonez/opengl/GLShaderManager.hpp"
#include "bonez/opengl/utils/GLTexture.hpp"
#include "bonez/scene/Scene.hpp"
#include "ShadowCubeMapContainer.hpp"

namespace BnZ {

///
/// \brief The ShadowPass class perfoms the shadow pass
/// using cube shadow mapping technique on a set of lights.
///
class ShadowCubeMapComputationPass {

public:

    ///
    /// \brief ShadowPass
    /// \param shaderManager
    ///
    ShadowCubeMapComputationPass(const GLShaderManager& shaderManager);

    ///
    /// \brief render fills shadow maps in the scene with lights registered.
    /// \param scene
    /// \param shadowContainer
    /// \param nShadowMapCount the number of shadow map to process.
    ///
    void render(const GLScene& scene, const ShadowCubeMapContainer& shadowContainer,
                size_t nShadowMapCount);

private:
    /// The shader program of this pass.
    GLProgram m_Program;
};
}
