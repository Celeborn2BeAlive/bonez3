#include "DirectIlluminationComputationPass.hpp"
#include "../SSMViewer.hpp"

#include <bonez/opengl/GLShaderManager.hpp>

namespace BnZ {

DirectIlluminationComputationPass::DirectIlluminationComputationPass(
        SSMViewer &viewer) :
    m_Program(viewer.getShaderManager().buildProgram({ "deferredShadingPass.vs",
                                                       "skeleton_shadow_mapping_refactored/DirectIlluminationComputationPass.fs"})),
    m_ShadowPass(viewer.getShaderManager()),
    m_ShadowContainer(viewer.getZNear(), viewer.getZFar(), 1024), m_DrawDepthPass(viewer.getShaderManager()) {
    std::cout << "DirectIlluminationComputationPass::Init shadowContainer address :"
              << &m_ShadowContainer << std::endl;
    m_ShadowContainer.init(1);
    m_DepthMapDisplayFramebuffer.init(512, 256, { GL_RGB32F });

}

void DirectIlluminationComputationPass::render(GLFramebuffer2D<1, false> &framebuffer,
                                               SSMViewer &viewer, float shadowBias) {
    const auto& gBuffer = viewer.getGBuffer();
    const auto& rcpViewMatrix = viewer.getRcpViewMatrix();
    const auto& rcpProjMatrix = viewer.getRcpProjMatrix();
    const auto& triangle = viewer.getScreenTriangle();

    /* Updating shadow cube maps. */
    /* Updating lights position. */
    std::vector<Vec3f> pos;
    pos.emplace_back(viewer.getLight().m_Position);
    m_ShadowContainer.buildShadowTransforms2(pos);
    m_ShadowPass.render(viewer.getGLScene(), m_ShadowContainer, 1);
    renderVPLShadowCubeMap(triangle, viewer.getGUI());

    framebuffer.bindForDrawing();

    // Accumulating direct lighting on the given FBO.
    glEnablei(GL_BLEND, 0);
    glBlendFunci(0, GL_ONE, GL_ONE);
    glBlendEquationi(0, GL_FUNC_ADD);
    glDisable(GL_DEPTH_TEST);
    glViewport(0, 0, framebuffer.getWidth(), framebuffer.getHeight());

    m_Program.use();

    // Sending light uniforms.
    auto& light = viewer.getLight();
    uLightPositionWs.set(Vec4f(light.m_Position, 1.f));
    uLightPositionVs.set(viewer.getViewMatrix() * Vec4f(light.m_Position, 1.f));
    uLightIntensity.set(light.m_Intensity);
    uLightKD.set(Vec3f(1));

    uGBuffer.set(gBuffer, 0, 1, 2);
    uRcpProjMatrix.set(rcpProjMatrix);
    uRcpViewMatrix.set(rcpViewMatrix);
    uDepthMapArray.set(m_Program, 3);
    ufBias.set(shadowBias);

    // Binding the current shadow cube map array.
    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_CUBE_MAP_ARRAY,
                  m_ShadowContainer.getDepthMapTextureForTheLayer(0));

    triangle.render();

    // Disabling the current SCMA texture.
    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_CUBE_MAP_ARRAY, 0);

    glDisablei(GL_BLEND, 0);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
}


void DirectIlluminationComputationPass::renderVPLShadowCubeMap(const GLScreenTriangle& triangle, GUI& gui) {
    auto window = gui.addWindow("DepthMap", false, m_DepthMapDisplayFramebuffer.getSize());
    if(!window) {
        return;
    }

    m_DepthMapDisplayFramebuffer.bindForDrawing();
    glClear(GL_COLOR_BUFFER_BIT);
    glViewport(0, 0, m_DepthMapDisplayFramebuffer.getWidth(), m_DepthMapDisplayFramebuffer.getHeight());

    // Retrieving the depthMap containing the layer of the last light processed.
    auto depthMap = m_ShadowContainer.getDepthMapTextureForTheLayer(0);
    auto layerLocalIndex = m_ShadowContainer.getDepthMapTextureLocalLayerIndex(0);

    glDisable(GL_DEPTH_TEST);

    m_DrawDepthPass.m_Program.use();

    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_CUBE_MAP_ARRAY, depthMap);

    m_DrawDepthPass.uCubeMapContainer.set(3);
    m_DrawDepthPass.uZNear.set(m_ShadowContainer.getShadowNear());
    m_DrawDepthPass.uZFar.set(m_ShadowContainer.getShadowFar());
    m_DrawDepthPass.uMapIndex.set(layerLocalIndex);
    m_DrawDepthPass.uDrawDepth.set(true);

    // Rendering the scene by rendering the triangle.
    triangle.render();

    // Disabling the current SCMA texture.
    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_CUBE_MAP_ARRAY, 0);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0u);

    gui.addImage(m_DepthMapDisplayFramebuffer.getColorBuffer(0));
}

}
