#include "RenderPass.hpp"

#include "SkeletonNodeVisibilityTextureComputationPass.hpp"

namespace BnZ {

class GLShaderManager;

class SkeletonNodeShadowsRenderPassOriginal: public RenderPass {
public:
    SkeletonNodeShadowsRenderPassOriginal(SSMViewer &viewer);

    virtual void render(SSMViewer &viewer) override;

private:
    void drawGUI(SSMViewer &viewer);
    size_t m_nNodeCount = 0;

    SkeletonNodeVisibilityTextureComputationPass m_SkeletonNodeVisibilityTextureComputationPass;
    SkeletonNodeVisibilityTextureContainer m_SkeletonNodeVisibilityTextureContainer;

    GLProgram m_Program;

    GLGBufferUniform uGBuffer { m_Program };

    BNZ_GLUNIFORM(m_Program, GLuint, uNodeIndex);
    BNZ_GLUNIFORM(m_Program, GLSLSampler2DArrayf, uShadowTexture2DArray);
    BNZ_GLUNIFORM(m_Program, Vec3f, uIntensity);

    GLBufferStorage<Vec4f> m_NodesPositionsBuffer;

    float m_fBias = 3.0f;
    Vec3f m_Intensity = Vec3f(1.f);
};

}
