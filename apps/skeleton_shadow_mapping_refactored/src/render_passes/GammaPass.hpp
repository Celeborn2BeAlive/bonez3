#pragma once

#include <bonez/opengl/utils/GLutils.hpp>
#include <bonez/opengl/GLShaderManager.hpp>

namespace BnZ {

class GammaPass {
public:
    GammaPass(const GLShaderManager& shaderManager);

    void compute(const GLTexture2D& input,
                GLTexture2D& output,
                float gamma);
private:
    GLProgram m_Program;

    BNZ_GLUNIFORM(m_Program, GLSLImage2Df, uInput);
    BNZ_GLUNIFORM(m_Program, GLSLImage2Df, uOutput);
    BNZ_GLUNIFORM(m_Program, GLfloat, uGamma);
};

}
