#include "ScalingComputationPass.hpp"


namespace BnZ {

ScalingComputationPass::
ScalingComputationPass(const GLShaderManager& shaderManager) :
    m_Program(shaderManager.buildProgram(
{ "skeleton_shadow_mapping/progressiveVPLRcpPathCountScalingPass.cs" })) {}


void ScalingComputationPass::render(const GLTexture2D& input,
                                    size_t pathCount,
                                    GLTexture2D& output) {
    m_Program.use();

    input.bind(0);

    uAccumulatedTexture.set(0);

    output.bindImage(0, 0, GL_WRITE_ONLY, GL_RGBA32F);
    uFinalRenderImage.set(0);

    uRcpPathCount.set(1.f / pathCount);

    const Vec2u groupSize(32, 32);
    glDispatchCompute(1 + input.getWidth() / groupSize.x,
                      1 + input.getHeight() / groupSize.y,
                      1);
    glMemoryBarrier(GL_ALL_BARRIER_BITS);
}
}
