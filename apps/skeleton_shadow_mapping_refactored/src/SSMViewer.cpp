﻿#include "SSMViewer.hpp"

#include <bonez/voxskel/GLVoxelizerTripiana2009.hpp>
#include <bonez/voxskel/ThinningProcessDGCI2013_2.hpp>
#include <bonez/voxskel/discrete_functions.hpp>

#include "render_passes/FlatRenderPass.hpp"
#include "render_passes/SkeletonNodeShadowsRenderPass.hpp"
#include "render_passes/CPUVPLRenderPass.hpp"

namespace BnZ {

SSMViewer::SSMViewer(const FilePath& applicationPath, const FilePath& settingsFilePath):
    Viewer("SkeletonShadowMappingSceneViewer", applicationPath, settingsFilePath),
    m_GBufferRenderPass(m_ShaderManager),
    m_SkeletonViewerModule(m_ShaderManager),
    m_GLDebugRenderer(m_ShaderManager),
    m_bInitFlag(initAll()),
    m_ShadowCubeMapComputationPass(m_ShaderManager),
    m_SkeletonNodeShadowCubeMapContainer(getZNear(), getZFar(), m_nSkeletonShadowMapResolution),
    m_LightViewerModule(m_ShaderManager, *this),
    m_ShadowCubeMapDrawPass(m_ShaderManager) {

    m_pCurrentRenderPass = createRenderPass("FlatRenderPass");

    m_GLDebugRenderer.setArrowSize(getArrowBase(), getArrowLength());
    m_OldCameraFlag = m_CameraUpdateFlag;

    for(auto i: range(getScene()->getCurvSkeleton()->size())) {
        m_NodePositionArray.emplace_back(getScene()->getCurvSkeleton()->getNode(i).P);
    }

    // TODO: Refactor this, no init plz, do it in constructor
    m_SkeletonNodeShadowCubeMapContainer.init(getScene()->getCurvSkeleton()->size());
    m_SkeletonNodeShadowCubeMapContainer.buildShadowTransforms(m_NodePositionArray);
    m_ShadowCubeMapComputationPass.render(*m_pGLScene, m_SkeletonNodeShadowCubeMapContainer,
                                          getScene()->getCurvSkeleton()->size());

    m_NodeShadowCubeMapDisplayFramebuffer.init(512, 256, { GL_RGB32F });

    auto nodePosition = m_NodePositionArray[m_nLightNodeIndex];
    m_Light = PointLight(nodePosition, Vec3f(m_fLightIntensity));

    if(!getScene()->getCurvSkeleton()->neighbours(m_nLightNodeIndex).empty()) {
        m_nNextNodeIndex = getScene()->getCurvSkeleton()->neighbours(m_nLightNodeIndex)[0];
    } else {
        m_nNextNodeIndex = m_nLightNodeIndex;
    }

	m_fGeometryFactorDistanceClamp = 0.01f * length(getScene()->getBBox().size()); // 1% of scene diagonal
}


void SSMViewer::setUp() {
    m_LastTime = getMicroseconds();
}

void SSMViewer::tearDown() {
    // Do nothing
}

bool SSMViewer::initAll() {
    initGLData();
    initViewports();
    m_SkeletonViewerModule.setUp(*this);
    applySceneThinning();

    return true;
}

void SSMViewer::initViewports() {
    m_FinalRenderViewport =
            Vec4f(0.5 * (m_Settings.m_WindowSize.x - m_Settings.m_FramebufferSize.x),
                  0.5 * (m_Settings.m_WindowSize.y - m_Settings.m_FramebufferSize.y),
                  m_Settings.m_FramebufferSize);
}

void SSMViewer::initGLData() {

    m_GBuffer.init(m_Settings.m_FramebufferSize);
    m_pGLScene = m_pScene->getGLScene();

    if(!m_pGLScene) {
        m_pGLScene = makeShared<GLScene>(m_pScene->getGeometry());
    }

    m_GLFramebuffer.init(m_Settings.m_FramebufferSize);
}


void SSMViewer::handleGlobalPreprocessParameters(const tinyxml2::XMLElement& input,
                                                 tinyxml2::XMLElement& output) {
    auto pThinning = input.FirstChildElement("Thinning");
    if(pThinning) {
        uint32_t gridResolution;
        getChildAttribute(*pThinning, "VoxelGridResolution", gridResolution);
        bool useSegmentedSkeleton = false;
        getChildAttribute(*pThinning, "UseSegmentedSkeleton", useSegmentedSkeleton);
    }
}


RenderPass* SSMViewer::createRenderPass(const std::string &name) {
    auto it = m_RenderPasses.find(name);
    if(it != end(m_RenderPasses)) {
        return (*it).second.get();
    }

    if(name == "FlatRenderPass") {
        return (m_RenderPasses[name] = makeUnique<FlatRenderPass>(m_ShaderManager)).get();
    }
    if(name == "SkeletonNodeShadowsRenderPass") {
        return (m_RenderPasses[name] = makeUnique<SkeletonNodeShadowsRenderPass>(*this)).get();
    }
	if (name == "CPUVPLRenderPass") {
		return (m_RenderPasses[name] = makeUnique<CPUVPLRenderPass>(*this)).get();
	}
}


void SSMViewer::setLightPosition(const Vec3f& position) {
    if (m_bLightCanMove) {
        return;
    }
    m_Light.m_Position = position;
    m_Light.m_Intensity = Vec3f(m_fLightIntensity);
}

void SSMViewer::updateLightPosition() {

    if (!m_bLightCanMove) {
        return;
    }
    auto& skeleton = *getScene()->getCurvSkeleton();

    Vec3f origin = skeleton.getNode(m_nLightNodeIndex).P;
    Vec3f dst = skeleton.getNode(m_nNextNodeIndex).P;

    m_Light.m_Position = origin + m_fLightInterpolationParam * (dst - origin);
    m_Light.m_Intensity = Vec3f(m_fLightIntensity);

    auto time = getMicroseconds();

    auto deltaTime = us2sec(time - m_LastTime);

    auto dist = distance(origin, dst);
    if(dist != 0) {
        auto deltaParam = m_fLightSpeed * deltaTime / dist;
        m_fLightInterpolationParam += deltaParam;
    }

    m_LastTime = time;

    if(m_fLightInterpolationParam >= 0.95) {
        auto& neighbours = skeleton.neighbours(m_nNextNodeIndex);
        if(neighbours.size() > 1) {
            std::vector<std::size_t> potentialNextNodes;
            for(auto& i: neighbours) {
                if(i != m_nLightNodeIndex) {
                    potentialNextNodes.emplace_back(i);
                }
            }
            auto next = clamp(int(m_LightMovementRng.getFloat() * potentialNextNodes.size()),
                              0, int(potentialNextNodes.size() - 1));
            m_nLightNodeIndex = m_nNextNodeIndex;
            m_nNextNodeIndex = potentialNextNodes[next];
        } else {
            std::swap(m_nLightNodeIndex, m_nNextNodeIndex);
        }
        m_fLightInterpolationParam = 0.f;
    }
}


void SSMViewer::drawFrame() {

    m_FpsCounter.tick();

    exposeIO();

    // Rendering the scene in the GBuffer.
    m_GBufferRenderPass.render(m_Camera.getProjMatrix(), m_Camera.getViewMatrix(),
                               m_ZNearFar.y, *m_pGLScene, m_GBuffer);

    updateLightPosition();
    m_Light.m_Intensity = Vec3f(m_fLightIntensity);

	m_IndirectVPLs.clear();
	m_EmissionVPLContainer.clear();
	VPLBuilder(*m_pScene).generateVPL(m_nPathCount, m_nRealPathCount, m_nMaxDepth, m_EmissionVPLContainer, m_IndirectVPLs, m_Light);
	auto lights = convertToGPULights(m_IndirectVPLs, m_EmissionVPLContainer);

    m_Lights = lights.first;
    m_DirectLights = lights.second;

    // Drawing in the framebuffer.
    if(m_pCurrentRenderPass) {
        m_pCurrentRenderPass->render(*this);
    }

    m_GLFramebuffer.blitFramebuffer(m_GBuffer, GL_DEPTH_BUFFER_BIT, GL_NEAREST);

    m_DisplayStream.clearObjects();

    if (m_PickedIntersection) {
        m_DisplayStream.addArrow(m_PickedIntersection.P, m_PickedIntersection.Ns, getArrowLength(), getArrowBase(), Vec3f(1.f));
    }

    // Rendering the skeleton.
    m_SkeletonViewerModule.draw();

    m_RenderModule.drawGLData(m_PickedIntersection, m_PickedIntersectionIncidentDirection, m_GLDebugRenderer);

    // Vpls debug draws.
    m_LightViewerModule.draw(m_Lights.begin(), m_Lights.end());
    m_LightViewerModule.drawLinksToSkeleton(m_Lights.begin(), m_Lights.end()/*m_Lights.begin() + nb*/,
                                            *(m_pScene->getCurvSkeleton()));
    m_LightViewerModule.drawLighToNode(m_Lights.begin(), m_Lights.end()/*m_Lights.begin() + nb*/,
                                       *(m_pScene->getCurvSkeleton()), m_Light.m_Position);

    if(m_bDisplayPointLight) {
        m_DisplayStream.addSphere(m_Light.m_Position, getArrowLength(), Vec3f(1, 0 ,0));
    }

    m_GLDebugRenderer.addStream(&m_DisplayStream);

    // Drawing in this final framebuffer.
    m_GLFramebuffer.bindForDrawing();
    glViewport(0, 0, m_GLFramebuffer.getWidth(), m_GLFramebuffer.getHeight());

    m_GLDebugRenderer.render(m_Camera);

    doPicking();

    // Drawing to the screen.
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    glClear(GL_COLOR_BUFFER_BIT);

    drawFinalRender();

    m_GLDebugRenderer.clearStreams();
}


void SSMViewer::exposeIO() {
    if(auto window = m_GUI.addWindow("SSMViewer")) {
		m_GUI.addButton("Screenshot", [&]() {
			doScreenshot();
		});

        m_GUI.addValue("FPS ", m_FpsCounter.getFPS());
        m_GUI.addValue("GPU Total Memory", getGPUMemoryInfoTotalAvailable());
        m_GUI.addValue("GPU Current Memory", getGPUMemoryInfoCurrentAvailable());

        m_GUI.addSeparator();

        m_GUI.addVarRW(BNZ_GUI_VAR(m_nThinningResolution));
        m_GUI.addVarRW(BNZ_GUI_VAR(m_bSegmentSkeleton));
        m_GUI.addButton("Thinning", [&]() {
            applySceneThinning();
        });

        m_GUI.addSeparator();

        m_GUI.addVarRW(BNZ_GUI_VAR(m_bDisplayPointLight));
        m_GUI.addVarRW(BNZ_GUI_VAR(m_bLightCanMove));
		m_GUI.addVarRW(BNZ_GUI_VAR(m_bSetLightPosAtSelectedNode));
        m_GUI.addButton("Move light here", [&] () {
            setLightPosition(Vec3f(getCamera().getOrigin()));
        });
		m_GUI.addVarRW(BNZ_GUI_VAR(m_Light.m_Position));
        m_GUI.addVarRW(BNZ_GUI_VAR(m_fLightIntensity));
        m_GUI.addVarRW(BNZ_GUI_VAR(m_fLightSpeed));
        m_GUI.addValue("VPL Count", m_Lights.size());
        m_GUI.addValue("Path count generated ", m_nRealPathCount);
        m_GUI.addVarRW(BNZ_GUI_VAR(m_nPathCount));
        m_GUI.addVarRW(BNZ_GUI_VAR(m_nMaxDepth));
		m_GUI.addVarRW(BNZ_GUI_VAR(m_fGamma));
		m_GUI.addVarRW(BNZ_GUI_VAR(m_bComputeShadows));
		m_GUI.addVarRW(BNZ_GUI_VAR(m_bComputeDirectLighting));
		m_GUI.addVarRW(BNZ_GUI_VAR(m_fGeometryFactorDistanceClamp));
		m_GUI.addValue("GeometryFactorBound", getGeometryFactorBound());

        m_GUI.addSeparator();

        if(m_GUI.addCombo("Current Render Pass", m_nCurrentRenderPassIndex, std::size(m_RenderPassNames),
                          [&](size_t index) {
                          return m_RenderPassNames[index].c_str();
    })) {
            m_pCurrentRenderPass = createRenderPass(m_RenderPassNames[m_nCurrentRenderPassIndex]);
        }

        // Drawing other UIs.
        m_LightViewerModule.drawGUI();
        m_SkeletonViewerModule.drawGUI();
    }

    drawSelectedNodeShadowCubeMap();
}

void SSMViewer::drawSelectedNodeShadowCubeMap() {
    if(auto window = m_GUI.addWindow("NodeShadowCubeMap", false,
                                     m_NodeShadowCubeMapDisplayFramebuffer.getSize())) {

        m_NodeShadowCubeMapDisplayFramebuffer.bindForDrawing();
        glClear(GL_COLOR_BUFFER_BIT);
        glViewport(0, 0, m_NodeShadowCubeMapDisplayFramebuffer.getWidth(), m_NodeShadowCubeMapDisplayFramebuffer.getHeight());

        // Retrieving the depthMap containing the layer of the last light processed.
        auto depthMap = m_SkeletonNodeShadowCubeMapContainer.getDepthMapTextureForTheLayer(m_SkeletonViewerModule.getSelectedNodeIndex());
        auto layerLocalIndex = m_SkeletonNodeShadowCubeMapContainer.getDepthMapTextureLocalLayerIndex(m_SkeletonViewerModule.getSelectedNodeIndex());

        glDisable(GL_DEPTH_TEST);

        m_ShadowCubeMapDrawPass.m_Program.use();

        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_CUBE_MAP_ARRAY, depthMap);

        m_ShadowCubeMapDrawPass.uCubeMapContainer.set(3);
        m_ShadowCubeMapDrawPass.uZNear.set(m_SkeletonNodeShadowCubeMapContainer.getShadowNear());
        m_ShadowCubeMapDrawPass.uZFar.set(m_SkeletonNodeShadowCubeMapContainer.getShadowFar());
        m_ShadowCubeMapDrawPass.uMapIndex.set(layerLocalIndex);
        m_ShadowCubeMapDrawPass.uDrawDepth.set(true);

        // Rendering the scene by rendering the triangle.
        m_ScreenTriangle.render();

        // Disabling the current SCMA texture.
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_CUBE_MAP_ARRAY, 0);
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0u);

        m_GUI.addImage(m_NodeShadowCubeMapDisplayFramebuffer.getColorBuffer(0));
    }
}


void SSMViewer::drawFinalRender() {
    m_GLFramebuffer.bindForReading();
    m_GLFramebuffer.setReadBuffer(0);

    glBlitFramebuffer(0, 0, m_GBuffer.getWidth(), m_GBuffer.getHeight(),
                      m_FinalRenderViewport.x, m_FinalRenderViewport.y,
                      m_FinalRenderViewport.x + m_FinalRenderViewport.z,
                      m_FinalRenderViewport.y + m_FinalRenderViewport.w,
                      GL_COLOR_BUFFER_BIT, GL_NEAREST);

    glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
}


void SSMViewer::doPicking() {

    if(!m_bGUIHasFocus && m_WindowManager.hasClicked()) {
        m_SelectedObjectID = m_GLFramebuffer.getObjectID(m_WindowManager.getCursorPosition(Vec2f(m_FinalRenderViewport)));

        if(m_SelectedObjectID == GLScreenFramebuffer::NULL_OBJECT_ID) {
            auto ndcPosition = m_WindowManager.getCursorNDC(m_FinalRenderViewport);
            if(viewportContains(ndcPosition, Vec4f(-1, -1, 2, 2))) {
                RaySample raySample;
                m_PickedIntersection =
                        tracePrimaryRay(getCamera(), *getScene(), Vec2f(0.5f), ndcToUV(ndcPosition), raySample);
                if(m_PickedIntersection) {
                    m_PickedIntersectionIncidentDirection = -raySample.value.dir;
                }
            }
        }

		if (m_bSetLightPosAtSelectedNode && m_SelectedObjectID.x == m_SkeletonViewerModule.getSkeletonNodeTypeID()) {
			auto nodeIdx = m_SelectedObjectID.y;
			Vec3f origin = getScene()->getCurvSkeleton()->getNode(nodeIdx).P;

			m_Light.m_Position = origin;
		}

        auto pixel = m_WindowManager.getCursorPosition(Vec2f(m_FinalRenderViewport));
        if(viewportContains(Vec2f(pixel), Vec4f(0, 0, m_FinalRenderViewport.z, m_FinalRenderViewport.w))) {
            setSelectedPixel(pixel);
        }
    }
}


void SSMViewer::setSelectedPixel(const Vec2u& pixel) {
    m_SelectedPixel = pixel;

    if(m_RenderModule.m_bCPURendering) {
        m_RenderModule.setSelectedPixel(pixel);
    }
}


float SSMViewer::getArrowLength() const {
    return length(size(getScene()->getBBox())) * 0.005f;
}


float SSMViewer::getArrowBase() const {
    return (1.f / 16) * getArrowLength();
}

void SSMViewer::storeScreenshotImage(const Image& image) {
	FilePath screenshotsPath = m_Path + "screenshots";
	createDirectory(screenshotsPath);
	FilePath baseName = m_ScreenshotPrefix + "screenshot." + toString(getMicroseconds());
	FilePath pngFile = screenshotsPath + baseName.addExt(".png");

	storeImage(pngFile, image);
}

void SSMViewer::doScreenshot() {
	Image image;
	fillImage(image, m_GLFramebuffer.getColorBuffer());
	image.flipY();

	storeScreenshotImage(image);
}

void SSMViewer::applySceneThinning() {
    GLVoxelFramebuffer voxelFramebuffer;
    Mat4f gridToWorldMatrix;
    CubicalComplex3D skeletonCubicalComplex, emptySpaceCubicalComplex;
    GLVoxelizerTripiana2009 voxelizer(m_ShaderManager);
    ThinningProcessDGCI2013_2 thinningProcess;

    Timer timer(true);
    std::clog << "Compute discrete scene dat at resolution " << m_nThinningResolution << std::endl;

    {
        Timer timer(true);
        std::clog << "Voxelizing the scene" << std::endl;
        voxelizer.initGLState(m_nThinningResolution, getScene()->getBBox(),
                              voxelFramebuffer, gridToWorldMatrix);
        m_pGLScene->render();
        voxelizer.restoreGLState();
    }

    {
        Timer timer(true);
        std::clog << "Convert empty space voxel grid to CC3D" << std::endl;
        auto voxelGrid = getVoxelGrid(voxelFramebuffer);
        voxelGrid = deleteEmptyBorder(voxelGrid, gridToWorldMatrix, gridToWorldMatrix);
        voxelGrid = inverse(voxelGrid);
        emptySpaceCubicalComplex = skeletonCubicalComplex = getCubicalComplex(voxelGrid);
    }

    auto distanceMap = [&]() {
        Timer timer(true);
        std::clog << "Compute distance map" << std::endl;
        return computeDistanceMap26(emptySpaceCubicalComplex, CubicalComplex3D::isInObject, true);
    }();

    auto openingMap = [&]() {
        Timer timer(true);
        std::clog << "Compute opening map" << std::endl;
        return parallelComputeOpeningMap26(distanceMap);
    }();

    {
        Timer timer(true);
        std::clog << "Initialize the thinning process" << std::endl;
        thinningProcess.init(skeletonCubicalComplex, &distanceMap, &openingMap);
    }

    {
        Timer timer(true);
        std::clog << "Run the thinning process" << std::endl;
        thinningProcess.directionalCollapse();
    }

    if (m_bSegmentSkeleton) {
        Timer timer(true);
        std::clog << "Convert skeleton CC3D to Curvilinear Skeleton (segmented)" << std::endl;
        auto skeleton = getSegmentedCurvilinearSkeleton(skeletonCubicalComplex, emptySpaceCubicalComplex,
                                                        distanceMap, openingMap,
                                                        gridToWorldMatrix);

        getScene()->setCurvSkeleton(skeleton);
    }
    else {
        Timer timer(true);
        std::clog << "Convert skeleton CC3D to Curvilinear Skeleton (NOT segmented)" << std::endl;
        auto skeleton = getCurvilinearSkeleton(skeletonCubicalComplex, emptySpaceCubicalComplex,
                                               distanceMap, openingMap,
                                               gridToWorldMatrix);
        getScene()->setCurvSkeleton(skeleton);
    }

    std::cerr << "Number of nodes = " << getScene()->getCurvSkeleton()->size() << std::endl;
}

}
