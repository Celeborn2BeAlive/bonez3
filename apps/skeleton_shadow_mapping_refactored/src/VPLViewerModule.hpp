#pragma once

#include <bonez/opengl/debug/GLDebugStream.hpp>
#include <bonez/opengl/GLShaderManager.hpp>

#include "gpu_vpl_utilities.hpp"

namespace BnZ {

class SSMViewer;
class Scene;

///
/// \brief The VPLViewerModule class Draws every debug data around BufferedLightGPU
/// - which represent our VPLs.
///
class VPLViewerModule {
public:
    VPLViewerModule(const GLShaderManager& shaderManager);

    ///
    /// \brief setUp sets up the current module.
    /// \param viewer The enclosing viewer.
    ///
    void setUp(SSMViewer& viewer);

    ///
    /// \brief drawGUI draws the current module's GUI.
    ///
    void drawGUI();

    ///
    /// \brief draw Draws vpls' normals according to the state of the current Module.
    /// \param begin
    /// \param end
    ///
    void draw(std::vector<BufferedLightGPU>::const_iterator begin,
              std::vector<BufferedLightGPU>::const_iterator end);

    ///
    /// \brief drawLinksToSkeleton draws links from the VPLs to each skeleton's node.
    /// If the internal state of the current module allows it.
    /// \param begin
    /// \param end
    /// \param GLBufferVPLToNodesIndexes
    /// \param skeleton
    ///
    void drawLinksToSkeleton(std::vector<BufferedLightGPU>::const_iterator begin,
                             const std::vector<BufferedLightGPU>::const_iterator& end,
                             const CurvilinearSkeleton& skeleton);
private:

    void drawVPLPosition(std::vector<BufferedLightGPU>::const_iterator end, std::vector<BufferedLightGPU>::const_iterator begin);

    void drawLighToNode(std::vector<BufferedLightGPU>::const_iterator begin,
                        const std::vector<BufferedLightGPU>::const_iterator& end,
                        const CurvilinearSkeleton& skeleton,
                        const Vec3f& lightPosition);

    /// If this module shows debug or not.
    bool m_bShowDebug = false;

    /// If this module shows links from VPLs to the skeleton or not.
    bool m_bShowLinksToSkeleton = false;

    /// Link from light to vpls.
    bool m_bShowLinksVPLsToLight = false;

    /// The enclosing viewer has to be a pointer since it is forward declared.
    SSMViewer* m_pViewer = nullptr;

    /// The scene also.
    Scene* m_pScene = nullptr;

    /// Stream used to draw debug datas. It will be added to the enclosing
    /// viewer's debug renderer.
    GLDebugStreamData m_DisplayStream;
};
}
