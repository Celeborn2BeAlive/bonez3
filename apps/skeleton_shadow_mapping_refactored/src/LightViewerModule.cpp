#include "LightViewerModule.hpp"
#include "SSMViewer.hpp"
#include <bonez/scene/Scene.hpp>


namespace BnZ {

LightViewerModule::LightViewerModule(const GLShaderManager &shaderManager,
                                     SSMViewer &viewer) :
    m_pViewer(&viewer),m_pScene(viewer.getScene()) {}


void LightViewerModule::draw(std::vector<BufferedLightGPU>::const_iterator begin,
                             std::vector<BufferedLightGPU>::const_iterator end) {

    if (!m_bShowDebug) {
        return;
    }

    m_DisplayStream.clearObjects();
    m_pViewer->getGLDebugRenderer().addStream(&m_DisplayStream);

    // Drawing the validColor normals.
    drawVPLPosition(end, begin);
}



void LightViewerModule::drawLighToNode(std::vector<BufferedLightGPU>::const_iterator begin,
                                       const std::vector<BufferedLightGPU>::const_iterator& end,
                                       const CurvilinearSkeleton& skeleton,
                                       const Vec3f& lightPosition) {
    if (!m_bShowDebug || !m_bShowLightToNode) {
        return;
    }

    // Colors : The last line is green (for debug purposes).
    auto greenColor = Vec3f(0, 1, 0);
    auto fWidth = 4.f;
    for (; begin != end; ++begin) {
        auto& vpl = *begin;

        // Determining the destination position. (The nearest node which is linked).
        size_t destID;
        if (0 == vpl.normal.w) {
            destID = skeleton.getNearestNode(Vec3f(vpl.positionWs));
        } else {
            destID = skeleton.getNearestNode(Vec3f(vpl.positionWs), Vec3f(vpl.normal));
        }

        if (UNDEFINED_NODE == destID) {
            continue;
        }

        m_DisplayStream.addLine(Vec3f(vpl.positionWs), lightPosition, greenColor, greenColor,
                               fWidth);
    }
}


void LightViewerModule::drawLinksToSkeleton(std::vector<BufferedLightGPU>::const_iterator begin,
                                            const std::vector<BufferedLightGPU>::const_iterator& end,
                                            const CurvilinearSkeleton& skeleton) {
    if (!m_bShowDebug || !m_bShowLinksToSkeleton) {
        return;
    }

    // Colors : The last line is green (for debug purposes).
    auto color = Vec3f(0, 1, 1), lastColor = Vec3f(0, 1, 0);
    auto fWidth = 2.f, fLastWidth = 4.f;
    for (; begin != end; ++begin) {
        auto& vpl = *begin;

        // Determining the destination position. (The nearest node which is linked).
        size_t destID;
        if (0 == vpl.normal.w) {
            destID = skeleton.getNearestNode(Vec3f(vpl.positionWs));
        } else {
            destID = skeleton.getNearestNode(Vec3f(vpl.positionWs), Vec3f(vpl.normal));
        }

        if (UNDEFINED_NODE == destID) {
            continue;
        }

        m_DisplayStream.addLine(Vec3f(vpl.positionWs), skeleton.getNode(destID).P, (begin +1 == end) ? lastColor : color,
                                (begin +1 == end) ? lastColor : color, (begin +1 == end) ? fLastWidth : fWidth);
    }
}

void LightViewerModule::drawVPLPosition(std::vector<BufferedLightGPU>::const_iterator end,
                                        std::vector<BufferedLightGPU>::const_iterator begin) {
    auto invalidColor = Vec3f(1, 0, 0);
    auto validColor = Vec3f(0.61, 0.19, 1);
    auto lastColor = Vec3f(0, 1, 0);
    auto i =  0;
    for (; begin != end; ++begin) {
        auto& vpl = *begin;
        // Choosing the right color.
        auto& color = invalidColor;
        // vpl's normal w is equal to 0 if not existing.
        if (vpl.normal.w != 0) {
            color = (begin + 1 == end) ? lastColor : validColor;
        }

        if (0 == vpl.normal.w) {
            m_DisplayStream.addArrow(Vec3f(vpl.positionWs), Vec3f(1, 0, 0),
                                     m_pViewer->getArrowLength(), m_pViewer->getArrowBase(),
                                     color);
        } else {
            m_DisplayStream.addArrow(Vec3f(vpl.positionWs), Vec3f(vpl.normal),
                                     m_pViewer->getArrowLength(), m_pViewer->getArrowBase(),
                                     color);
        }
    }
}


void LightViewerModule::drawGUI() {
    auto& gui = m_pViewer->getGUI();

    if(auto window = gui.addWindow("LightViewerModule")) {
        /* gui.addValue("last    : GREEN");
         gui.addValue("valid   : PURPLE");
         gui.addValue("invalid : BLACK");
         */
        gui.addVarRW(BNZ_GUI_VAR(m_bShowDebug));
        gui.addVarRW(BNZ_GUI_VAR(m_bShowLinksToSkeleton));
        gui.addVarRW(BNZ_GUI_VAR(m_bShowLightToNode));
        m_bShowDebug = m_bShowDebug || m_bShowLightToNode;
    }
}
}
