#version 430 core

layout(local_size_x = 32, local_size_y = 32) in;

layout(rgba32f) readonly uniform image2D uInput;
writeonly uniform image2D uOutput;

uniform float uGamma;

void main() {
    uvec2 size = uvec2(imageSize(uOutput));

    if(any(greaterThanEqual(gl_GlobalInvocationID.xy, size)) ) {
        return;
    }

    // Fetching pixel G-informations.
    ivec2 pixel = ivec2(gl_GlobalInvocationID.xy);
    vec3 value = imageLoad(uInput, pixel).xyz;
    imageStore(uOutput, pixel, vec4(pow(value, vec3(1.0 / uGamma)), 1));
}
