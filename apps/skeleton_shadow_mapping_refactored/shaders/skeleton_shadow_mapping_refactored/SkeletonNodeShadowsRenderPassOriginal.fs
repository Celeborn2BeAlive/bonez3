#version 430

struct GBuffer {
    sampler2D normalDepthSampler;
    sampler2D diffuseSampler;
    sampler2D glossyShininessSampler;
};

out vec3 fColor;

// Geometry.
uniform GBuffer uGBuffer;
uniform uint uNodeIndex;
uniform sampler2DArray uShadowTexture2DArray;
uniform vec3 uIntensity;

void main() {
    ivec2 pixelCoords = ivec2(gl_FragCoord.xy);
    vec3 kd = texelFetch(uGBuffer.diffuseSampler, pixelCoords, 0).rgb;
    vec4 ks = texelFetch(uGBuffer.glossyShininessSampler, pixelCoords, 0);

    fColor = vec3(0);

    //float visibility = texture(uShadowTexture2DArray, vec3(pixelCoords, uNodeIndex), 0).r;
    float color = texelFetch(uShadowTexture2DArray, ivec3(pixelCoords, uNodeIndex), 0).r;
    fColor = uIntensity * kd * color;
}
