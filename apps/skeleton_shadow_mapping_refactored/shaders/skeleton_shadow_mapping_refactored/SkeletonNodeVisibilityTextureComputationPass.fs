#version 430 core

const float PI = 3.14159265358979323846264;

// Attributs.
layout(location = 0) out vec3 fColor;

struct GBuffer {
    sampler2D normalDepthSampler;
    sampler2D diffuseSampler;
    sampler2D glossyShininessSampler;
};


// Textures. ===> should replace this with shadow sampler, but requires the bounded texture to be a depth texture, aka change the whole code...
uniform samplerCubeArray uDepthMap;
// Geometry.
uniform GBuffer uGBuffer;
// Matrices.
uniform mat4 uRcpViewMatrix;
// Shadow variables.
uniform float uBias;

uniform bool ubUsingPCF;
uniform bool ubPCFOpti;
uniform uint unPCFAlgo;
uniform float ufPCFRadius;
uniform float ufFarPlane;

uniform vec3 uNodePosition;
uniform uint uNodeLayer;

// Ins.
in vec3 vFarPoint_vs;
in vec2 vUVCoords;

// Computes if the points is in the shadow or not.
float shadowCalculation(vec3 position_ws, vec3 normal_ws, vec3 lightPos_ws, float depthIndex) {
    vec3 lightToFragDir = position_ws - lightPos_ws;
    float closestDepth = texture(uDepthMap, vec4(lightToFragDir, depthIndex)).r;
    float currentDepth = length(lightToFragDir);

    float bias = max(10 * uBias * (1.0 - abs(dot(normal_ws, -lightToFragDir))), uBias);

    return currentDepth + bias > closestDepth ? 1.0 : 0.0;
}

// With shadow texture:
//float shadowCalculation(vec3 position_ws, vec3 normal_ws, vec3 lightPos_ws, float depthIndex) {
//    vec3 lightToFragDir = position_ws - lightPos_ws;
//    float currentDepth = length(lightToFragDir);
//    float bias = max(10 * uBias * (1.0 - abs(dot(normal_ws, -lightToFragDir))), uBias);
//    return texture(uDepthMap, vec4(lightToFragDir, depthIndex), currentDepth + bias).r;
//    //float closestDepth = texture(uDepthMap, vec4(lightToFragDir, depthIndex)).r;
//    //return currentDepth + bias > closestDepth ? 1.0 : 0.0;
//}


// Computes if the points is in the shadow or not.
float shadowCalculationPCF(vec3 position_vs, vec3 position_ws, vec3 normal_ws, vec3 lightPos_ws, float depthIndex) {
    vec3 lightToFragDir = position_ws - lightPos_ws;
    float bias = max(10 * uBias * (1.0 - abs(dot(normal_ws, -lightToFragDir))), uBias);
    float currentDepth = length(lightToFragDir);
    float shadow = 0.0;
    float samples = 4.0;
    float offset = 15;
    float closestDepth = texture(uDepthMap, vec4(lightToFragDir, depthIndex)).r;
   if (ubPCFOpti) {
    uint nNeighbor = 0;
    nNeighbor += currentDepth + bias > texture(uDepthMap, vec4(lightToFragDir + vec3(offset, 0, 0), depthIndex)).r ? 1 : 0;
    nNeighbor += currentDepth + bias > texture(uDepthMap, vec4(lightToFragDir + vec3(-offset, 0, 0), depthIndex)).r ? 1 : 0;
    nNeighbor += currentDepth + bias > texture(uDepthMap, vec4(lightToFragDir + vec3(0, offset, 0), depthIndex)).r ? 1 : 0;
    nNeighbor += currentDepth + bias > texture(uDepthMap, vec4(lightToFragDir + vec3(0, -offset, 0), depthIndex)).r ? 1 : 0;
    if (4 == nNeighbor) {
        return 1;
    }
    }
    float diskRadius = ufPCFRadius * 0.32 * (1.0 + (1.0 - (currentDepth / ufFarPlane)) * 3) / textureSize(uDepthMap, 0).x;
    for(float x = -offset; x < offset; x += offset / (samples * 0.5)) {
        for(float y = -offset; y < offset; y += offset / (samples * 0.5)) {
            for(float z = -offset; z < offset; z += offset / (samples * 0.5)) {
                float closestDepth = texture(uDepthMap, vec4(lightToFragDir + diskRadius * vec3(x, y, z), depthIndex)).r;
                if(currentDepth + bias > closestDepth)
                    shadow += 1.0;
            }
        }
    }
    shadow /= (samples * samples * samples);
    return shadow;
}

vec3 gCubeSampleOffset[8] = vec3[] (
  vec3( 0.875f, -0.375f, -0.125f ),
  vec3( 0.625f, 0.625f, -0.625f ),
  vec3( 0.375f, 0.125f, -0.875f ),
  vec3( 0.125f, -0.875f, 0.125f ),
  vec3( 0.125f, 0.875f, 0.375f ),
  vec3( 0.375f, -0.625f, 0.625f ),
  vec3( 0.625f, -0.125f, 0.875f ),
  vec3( 0.875f, 0.375f, -0.375f )
);
float shadowCalculationPCF2(vec3 position_vs, vec3 position_ws, vec3 normal_ws, vec3 lightPos_ws, float depthIndex) {
    vec3 lightToFragDir = position_ws - lightPos_ws;
    float bias = max(10 * uBias * (1.0 - abs(dot(normal_ws, -lightToFragDir))), uBias);
    float currentDepth = length(lightToFragDir);
    float shadow = 0.0;
    //float bias = 0.15;
    int samples = 8;
    float diskRadius = ufPCFRadius * 0.32 * (1.0 + (1.0 - (currentDepth / ufFarPlane)) * 3) / textureSize(uDepthMap, 0).x;
    float viewDistance = length(position_vs);
    vec3 fragToLight = -lightToFragDir;

    for(int i = 0; i < samples; ++i) {
        float closestDepth = texture(uDepthMap, vec4(lightToFragDir + gCubeSampleOffset[i] * diskRadius, depthIndex)).r;
        if(currentDepth + bias > closestDepth)
            shadow += 1.0;
    }
    shadow /= float(samples);

    return shadow;
}

// Small variation of http://www.sunandblackcat.com/tipFullView.php?l=eng&topicid=36
vec3 gridSamplingDisk[20] = vec3[] (
   vec3( 1,  1,  1), vec3( 1, -1,  1), vec3(-1, -1,  1), vec3(-1,  1,  1),
   vec3( 1,  1, -1), vec3( 1, -1, -1), vec3(-1, -1, -1), vec3(-1,  1, -1),
   vec3( 1,  1,  0), vec3( 1, -1,  0), vec3(-1, -1,  0), vec3(-1,  1,  0),
   vec3( 1,  0,  1), vec3(-1,  0,  1), vec3( 1,  0, -1), vec3(-1,  0, -1),
   vec3( 0,  1,  1), vec3( 0, -1,  1), vec3( 0, -1, -1), vec3( 0,  1, -1)
);
float shadowCalculationPCF3(vec3 position_vs, vec3 position_ws, vec3 normal_ws, vec3 lightPos_ws, float depthIndex) {
    vec3 lightToFragDir = position_ws - lightPos_ws;
    float bias = max(10 * uBias * (1.0 - abs(dot(normal_ws, -lightToFragDir))), uBias);
    float currentDepth = length(lightToFragDir);
    float shadow = 0.0;
    //float bias = 0.15;
    int samples = 20;
    float viewDistance = length(position_vs);
    // 0.32 = magic constant to ensure maximal value to 0.01
    float diskRadius = ufPCFRadius * 0.32 * (1.0 + (1.0 - (currentDepth / ufFarPlane)) * 3) / textureSize(uDepthMap, 0).x;
    //float diskRadius = ufPCFRadius;

    vec3 fragToLight = lightToFragDir / currentDepth;
    for(int i = 0; i < samples; ++i) {
        float closestDepth = texture(uDepthMap, vec4(fragToLight + gridSamplingDisk[i] * diskRadius, depthIndex)).r;
        if(currentDepth + bias > closestDepth)
            shadow += 1.0;
    }
    shadow /= float(samples);

    return shadow;
}

// 9 samples
//vec2 gDiskSampleOffset[9] = vec2[] (
//    vec2( 0.181819, 0.405656),
//    vec2( 0.0571132, 0.78198),
//    vec2( 0.296235, 0.929997),
//    vec2( -0.529083, -0.0498827),
//    vec2( -0.726271, 0.117128),
//    vec2( -0.850216, -0.268191),
//    vec2( 0.12057, -0.290931),
//    vec2( 0.578315, -0.133379),
//    vec2( 0.867855, -0.0661755)
//);
// 16 samples
vec2 gDiskSampleOffset[16] = vec2[] (
    vec2( 0.250571, 0.292276),
    vec2( 0.293747, 0.612192),
    vec2( 0.493866, 0.68599),
    vec2( 0.6429, 0.740602),
    vec2( -0.243781, 0.310583),
    vec2( -0.499614, 0.310614),
    vec2( -0.480875, 0.585784),
    vec2( -0.861571, 0.147897),
    vec2( -0.0148941, -0.260657),
    vec2( -0.500842, -0.344345),
    vec2( -0.269446, -0.794003),
    vec2( -0.628939, -0.688789),
    vec2( 0.243955, -0.196678),
    vec2( 0.672888, -0.0790019),
    vec2( 0.0851204, -0.759673),
    vec2( 0.130298, -0.946079)
);
float shadowCalculationPCF4(vec3 position_vs, vec3 position_ws, vec3 normal_ws, vec3 lightPos_ws, float depthIndex) {
    vec3 lightToFragDir = position_ws - lightPos_ws;
    float bias = max(10 * uBias * (1.0 - abs(dot(normal_ws, -lightToFragDir))), uBias);
    float currentDepth = length(lightToFragDir);
    lightToFragDir /= currentDepth;
    float shadow = 0.0;
    // 0.32 = magic constant to ensure maximal value to 0.01
    float diskRadius = ufPCFRadius * 0.32 * (1.0 + (1.0 - (currentDepth / ufFarPlane)) * 3) / textureSize(uDepthMap, 0).x;
    int samples = 16;
    float viewDistance = length(position_vs);
    vec3 fragToLight = -lightToFragDir;

    vec3 vectorToComputeXAxis = vec3(1,0,0);
    if(lightToFragDir.y == 0 && lightToFragDir.z == 0) {
        vectorToComputeXAxis = vec3(0,1,0);
    }

    vec3 xAxis = cross(lightToFragDir, vectorToComputeXAxis);
    vec3 yAxis = cross(xAxis, lightToFragDir);

    for(int i = 0; i < samples; ++i) {
        float closestDepth = texture(uDepthMap, vec4(lightToFragDir + diskRadius * (gDiskSampleOffset[i].x * xAxis + gDiskSampleOffset[i].y * yAxis), depthIndex)).r;
        if(currentDepth + bias > closestDepth)
            shadow += 1.0;
    }
    shadow /= float(samples);

    return shadow;
}


void main() {

    // Getting material values.
    ivec2 pixelCoords = ivec2(gl_FragCoord.xy);

    // Normal in view space.
    //vec4 normalDepth = texelFetch(uGBuffer.normalDepthSampler, pixelCoords, 0);
    vec4 normalDepth = texture(uGBuffer.normalDepthSampler, vUVCoords);

    // Position in view space. Using Reconstructing Position from Depth (in w).
    vec4 position_vs = vec4(normalDepth.w * vFarPoint_vs, 1.0);
    vec4 position_ws = uRcpViewMatrix * position_vs;
    vec3 normal_vs = normalize(normalDepth.xyz);
    vec3 normal_ws = (uRcpViewMatrix * vec4(normal_vs, 0)).xyz;

    float shadow;
    if (ubUsingPCF) {
        switch(unPCFAlgo) {
            default :
                shadow = shadowCalculationPCF(position_vs.xyz, position_ws.xyz, normal_ws, uNodePosition, uNodeLayer);
                break;
            case 1 :
                shadow = shadowCalculationPCF2(position_vs.xyz, position_ws.xyz, normal_ws, uNodePosition, uNodeLayer);
                break;
            case 2 :
                shadow = shadowCalculationPCF3(position_vs.xyz, position_ws.xyz, normal_ws, uNodePosition, uNodeLayer);
                break;
            case 3:
                shadow = shadowCalculationPCF4(position_vs.xyz, position_ws.xyz, normal_ws, uNodePosition, uNodeLayer);
                break;
        }
    } else {
        shadow = shadowCalculation(position_ws.xyz, normal_ws, uNodePosition, uNodeLayer);
    }

    //fColor = vec3(1 - shadow);
    fColor = vec3(1 - shadow);
}
