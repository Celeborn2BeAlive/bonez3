#version 430

struct GBuffer {
    sampler2D normalDepthSampler;
    sampler2D diffuseSampler;
    sampler2D glossyShininessSampler;
};

out vec3 fColor;

// Geometry.
uniform GBuffer uGBuffer;
uniform mat4 uRcpViewMatrix;

uniform samplerCubeArray uDepthMapArray;
uniform uint uDepthMapMapSize;
uniform float ufBias;

uniform vec4 uLightPositionWs;
uniform vec4 uLightPositionVs;
uniform vec3 uLightIntensity;
uniform vec3 uLightKD;

in vec3 vFarPoint_vs;



// Lights the given point with the given light, using the
// current cube shadow map.
vec3 lightingPoint(vec3 dirToLight, float rcpDist, vec4 position_vs, vec3 normal_vs,
                   vec3 normal_ws, vec3 kd, vec3 ks, float shininess) {

    // NO SPECULAR.
    vec3 lightBRDF = uLightKD;
    vec3 pointBRDF = kd;

    float geometricFactor = max(dot(normal_vs, dirToLight), 0.f) * (rcpDist * rcpDist);

    // The result is 0 if the point is in the shadow, the right intensity otherwise.
    return max(vec3(0), geometricFactor * (uLightIntensity * lightBRDF * pointBRDF));
}


/* Everything has to be in view space. */
vec3 computeLighting(vec4 position_vs, vec3 normal_vs, vec3 normal_ws, vec3 kd, vec3 ks,
                     float shininess) {

    // Calculating the point to light direction.
    vec3 dirToVPL = vec3(uLightPositionVs - position_vs);
    float dist = length(dirToVPL);

    if (dist <= 0.f) {
        return vec3(0);
    }

    dist = 1.f / dist; // (now dist equals rcpDist)
    dirToVPL *= dist;

    return lightingPoint(dirToVPL, dist, position_vs, normal_vs, normal_ws, kd, ks,
                         shininess);
}


float shadowCalculation(vec3 position_ws, vec3 normal_ws, float depthIndex) {
    vec3 lightToFragDir = position_ws - uLightPositionWs.xyz;
    float closestDepth = texture(uDepthMapArray, vec4(lightToFragDir, depthIndex)).r;
    float currentDepth = length(lightToFragDir);

    float bias = max(10 * ufBias * (1.0 - abs(dot(normal_ws, -lightToFragDir))), ufBias);

    return currentDepth + bias > closestDepth ? 1.0 : 0.0;
}


void main() {
    ivec2 pixelCoords = ivec2(gl_FragCoord.xy);

    vec3 kd = texelFetch(uGBuffer.diffuseSampler, pixelCoords, 0).rgb;
    vec4 ks = texelFetch(uGBuffer.glossyShininessSampler, pixelCoords, 0);

    // Normal in view space.
    vec4 normalDepth = texelFetch(uGBuffer.normalDepthSampler, pixelCoords, 0);
    vec3 normal_vs = normalize(normalDepth.xyz);

    // Position in view space. Using Reconstructing Position from Depth (in w).
    vec4 position_vs = vec4(normalDepth.w * vFarPoint_vs, 1.0);
    vec4 position_ws = uRcpViewMatrix * position_vs;
    vec3 normal_ws = (uRcpViewMatrix * vec4(normal_vs, 0)).xyz;

    // Calcul of the pixelID in its block and its lightStep to sample lightsSSBO.
    vec3 totalColor = vec3(0);
    vec3 lighting = computeLighting(position_vs, normal_vs, normal_ws,
                                    kd, ks.rgb, ks.a);

  float shadowValue = shadowCalculation(position_ws.xyz, normal_ws, 1);
    fColor = (lighting * (1 - shadowValue));
}
