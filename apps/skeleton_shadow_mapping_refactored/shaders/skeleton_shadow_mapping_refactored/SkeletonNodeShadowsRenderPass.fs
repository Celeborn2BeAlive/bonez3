#version 430
// Everything is in the viewspace.
struct Light {
    vec4 normal;
    vec4 position_ws;
    vec4 position;
    vec4 intensity;
    vec4 incidentDir;
    vec4 kD;
    vec4 kS;
    vec4 shininess;
};

layout (std430, binding = 0) buffer LightsSSBO {
    Light lights[];
};

layout (std430, binding = 1) buffer LightNearestNodeSSBO {
    uint lightNearestNodeIndices[];
};

layout (std430, binding = 2) buffer lightPerNodeANDCumulatedSSBO {
    uvec2 lightPerNodeANDCumulated[];
};

layout (std430, binding = 3) buffer lightListOfTheCurrentNodeSSBO {
    uint lightListOfTheCurrentNode[];
};

struct GBuffer {
    sampler2D normalDepthSampler;
    sampler2D diffuseSampler;
    sampler2D glossyShininessSampler;
};

out vec3 fColor;

// Geometry.
uniform GBuffer uGBuffer;
uniform sampler2D uShadowTexture2D;
uniform vec3 uIntensity;

uniform bool ubComputeShadow;
uniform mat4 uRcpViewMatrix;
uniform float uGeometryTermBound;
uniform uint uInterleavedWindowSize;
uniform uint uLightOffset;
uniform uint uLightNumber;

// Ins.

in vec3 vFarPoint_vs;
in vec2 vUVCoords;

// Lights the given point with the given light, using the
// current cube shadow map.
vec3 lightingPoint(vec3 dirToLight, Light light, float rcpDist, vec4 position_vs, vec3 normal_vs,
                   vec3 normal_ws, vec3 kd, vec3 ks, float shininess) {

    // The light BSDF has to be used !
    vec3 lightBRDF = light.kD.rgb;
    vec3 pointBRDF = kd;

    vec3 lightNormal = light.normal.xyz;
    float lightNormalW = light.normal.w;

    // If lightNormal.w == 1, we can calculate the dot product. The result is 1 if lightNormal.w = 0.
    float lightDot = (1 - lightNormalW) + lightNormalW * max(dot(lightNormal.xyz, -dirToLight), 0.f);

    float geometricFactor = min(max(dot(normal_vs, dirToLight), 0.f) *
                                lightDot * (rcpDist * rcpDist), uGeometryTermBound);

    // The result is 0 if the point is in the shadow, the right intensity otherwise.
    return max(vec3(0), geometricFactor * (light.intensity.xyz * lightBRDF * pointBRDF));
}


/* Everything has to be in view space. */
vec3 computeLighting(vec4 position_vs, vec3 normal_vs, vec3 normal_ws,
                     Light light, vec3 kd, vec3 ks,
                     float shininess) {

    // Calculating the point to light direction.
    vec3 dirToVPL = vec3(light.position - position_vs);
    float dist = length(dirToVPL);

    if (dist <= 0.f) {
        return vec3(0);
    }

    dist = 1 / dist; // (now dist equals rcpDist)
    dirToVPL *= dist;

    return lightingPoint(dirToVPL, light, dist, position_vs,
                         normal_vs, normal_ws, kd, ks, shininess);
}


uint getNodeLayer(uint nodeID) {
    return nodeID; // TODO : nodeID % uDepthMapMaxSize;
}


void main() {
    ivec2 pixelCoords = ivec2(gl_FragCoord.xy);

    //float shadowValue = texelFetch(uShadowTexture2D, pixelCoords, 0).r;
    float shadowValue = ubComputeShadow ? texture(uShadowTexture2D, vUVCoords).r : 1f;
    if (shadowValue <= 0f) {
        fColor = vec3(0);
        return;
    }

    vec3 kd = vec3(1);//texelFetch(uGBuffer.diffuseSampler, pixelCoords, 0).rgb;
    vec4 ks = texelFetch(uGBuffer.glossyShininessSampler, pixelCoords, 0);

    // Normal in view space.
    vec4 normalDepth = texelFetch(uGBuffer.normalDepthSampler, pixelCoords, 0);
    vec3 normal_vs = normalize(normalDepth.xyz);

    // Position in view space. Using Reconstructing Position from Depth (in w).
    vec4 position_vs = vec4(normalDepth.w * vFarPoint_vs, 1.0);
    vec4 position_ws = uRcpViewMatrix * position_vs;
    vec3 normal_ws = (uRcpViewMatrix * vec4(normal_vs, 0)).xyz;

    // Calcul of the pixelID in its block and its lightStep to sample lightsSSBO.
    uint pixelID = int(gl_FragCoord.x) % uInterleavedWindowSize +
                  (int(gl_FragCoord.y) % uInterleavedWindowSize) * uInterleavedWindowSize;

    // TODO : Can be calculated outside the shader !!
    uint lightStep = uInterleavedWindowSize * uInterleavedWindowSize;

    vec3 totalColor = vec3(0);
    uint lightID, currentNodeID, oldCurrentNodeID = -1;
   // float shadowValue = texelFetch(uShadowTexture2D, pixelCoords, 0).r;
    for (uint i = pixelID; i < uLightNumber; i += lightStep) {
        lightID = lightListOfTheCurrentNode[i];
        vec3 lighting = computeLighting(position_vs, normal_vs, normal_ws, lights[lightID],
                                        kd, ks.rgb, ks.a);
        totalColor += lighting;
    }

    fColor = totalColor * shadowValue;

/*
    for (uint i = uLightOffset + pixelID; i < uLightOffset + uLightNumber; i += lightStep) {

        // Indirections needed.
        vec3 lighting = computeLighting(position_vs, normal_vs,
                                        normal_ws, lights[i], kd, ks.rgb, ks.a);
        totalColor = totalColor + lighting;
    }
    */

/* No shading for now.


    fColor = vec3(0);

    //float visibility = texture(uShadowTexture2DArray, vec3(pixelCoords, uNodeIndex), 0).r;
    float color = texelFetch(uShadowTexture2DArray, ivec3(pixelCoords, uNodeIndex), 0).r;
    fColor = uIntensity * kd * color;
    */
}
