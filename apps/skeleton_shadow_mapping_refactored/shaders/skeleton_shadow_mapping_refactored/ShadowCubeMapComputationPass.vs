#version 430 core

/// The goal here is to output the world position.
layout(location = 0) in vec3 aPosition;

flat out int vInstanceID;

void main() {
    // The scene has no transformation, we already are
    // in world space here.
    gl_Position = vec4(aPosition, 1.0);
    // The instance ID to choose the shadow map to compute in the geometry
    // shader.
    vInstanceID = gl_InstanceID;
}
