#version 430 core

/// The goal here is to output from 3 vertices of a triangle,
/// 1 triangle in each of the 6 light spaces. Thus 18 vertices.
layout (triangles) in;
layout (triangle_strip, max_vertices=18) out;

// View projection matrices.
layout (std430, binding = 0) buffer shadowMatSSBO {
    mat4 shadowMatrices[];
};

// light positions.
layout (std430, binding = 1) buffer lightPosSSBO {
    vec4 lightPositions[];
};

// Instance ID to choose the shadow map to compute (in fact, le light ID).
flat in int vInstanceID[];

// Fragment position will be used by the fragment shader.
out vec4 fragPos;
out vec3 lightPos;


void main() {

    lightPos = lightPositions[vInstanceID[0]].xyz;

    int startIndex = vInstanceID[0] * 6;
    // Iterating on each of the 6 cubeMap's faces.
    for (gl_Layer = startIndex; gl_Layer < startIndex + 6; ++gl_Layer) {
        // Writing in the right layer with the VP matrices of the current pointlight.
        // For each vertices, emiting the LightSpace position and the worldspace one.
        for (int i = 0; i < 3; ++i) {
            fragPos = gl_in[i].gl_Position;
            gl_Position = shadowMatrices[gl_Layer] * fragPos;
            EmitVertex();
        }

        // Finishing the current triangle.
        EndPrimitive();
    }
}
