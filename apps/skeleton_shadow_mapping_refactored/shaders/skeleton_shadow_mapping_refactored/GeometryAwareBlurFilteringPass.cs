#version 430 core

layout(local_size_x = 32, local_size_y = 32) in;

uniform sampler2D uAccumulatedTexture;

layout(rgba32f) uniform image2D uFinalRenderImage;

uniform sampler2D uNormalDepthSampler;

uniform float uTetaN;
uniform float uTetaP;

uniform uint uWindowSize;
uniform float uZFar;

// Gaussian blur according to page 346 of the book Real Time Shadow.
float gaussianBlur(float teta, float distance) {
    return exp(-(distance * distance) / (teta * teta));
}


void main() {
    uvec2 size = uvec2(imageSize(uFinalRenderImage));

    if(any(greaterThanEqual(gl_GlobalInvocationID.xy, size)) ) {
        return;
    }

    // Fetching pixel G-informations.
    ivec2 pixel = ivec2(gl_GlobalInvocationID.xy);
    vec4 normal_vs = texelFetch(uNormalDepthSampler, pixel, 0);

    // Accumulating neighbors values.
    ivec2 neighborPixel;
    float neighborWeight, neighborTotalWeight = 0;
    vec4 newCol = vec4(0), neighborIntensity, neighborNormal_vs;
    for (int x = 0; x < 2 * uWindowSize; ++x) {
        for (int y = 0; y < 2 * uWindowSize; ++y) {
            neighborPixel = pixel + ivec2(x, y) - ivec2(uWindowSize) / 2;
            // Fetching intensity of the neighbor.
            neighborIntensity = texelFetch(uAccumulatedTexture, neighborPixel, 0);
            // Getting its normal in vs.
            neighborNormal_vs = texelFetch(uNormalDepthSampler, neighborPixel, 0);
            // Calculating the current weight.
            float normalWeight = gaussianBlur(uTetaN / normal_vs.w, 1 - dot(normal_vs.xyz, neighborNormal_vs.xyz));
            if(normalWeight < 0.5) {
                normalWeight = 0.f;
            }
            float depthWeight = gaussianBlur(uTetaP, abs(neighborNormal_vs.w - normal_vs.w));
            if(depthWeight < 0.2) {
                depthWeight = 0.f;
            }
            neighborWeight = normalWeight * depthWeight;
            //float distNeighbourImagePlane = length(pixel - neighborPixel);
            //neighborWeight *= 1 / (0.01 + abs(distNeighbourImagePlane));
            // Adding it to the total weight.
            neighborTotalWeight += neighborWeight;
            // Mixing it with the pondarate intensity to the current color.
            newCol += neighborWeight * neighborIntensity;
        }
    }

    // Averaging the pixel color.
    float fAverage = neighborTotalWeight;//9.f;
    newCol = vec4(newCol.xyz / fAverage, 1);

    // Applying the diffuse coef of the pixel, then storing in the final image.
    imageStore(uFinalRenderImage, pixel, vec4(newCol.xyz, 1));
}
