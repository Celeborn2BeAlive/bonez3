#include "ResultsViewer.hpp"

namespace BnZ {

ResultsViewer::ResultsViewer(const FilePath& applicationPath,
                             const FilePath& resultsPath,
                             const Vec2u& windowSize):
    m_ApplicationPath(applicationPath),
    m_ResultsDirPath(resultsPath),
    m_WindowManager(windowSize.x, windowSize.y, "ResultsViewer"),
    m_GUI(applicationPath.file(), m_WindowManager),
    m_ShaderManager(applicationPath.directory() + "shaders"),
    m_GLImageRenderer(m_ShaderManager),
    m_SmallViewImage(5, 5) {
}

static bool isLoadable(const FilePath& path) {
    return path.ext() == "png" ||
            path.ext() == "raw" ||
            path.ext() == "exr" ||
            path.ext() == "rmseFloat";
}

void ResultsViewer::load(const FilePath& path) {
    if(path.ext() == "png") {
        auto pImg = BnZ::loadImage(path);
        pImg->flipY();

        m_CurrentImagePath = path;
        setCurrentImage(*pImg);
    } else if(path.ext() == "raw") {
        m_CurrentImagePath = path;
        setCurrentImage(*loadRawImage(path));
    } else if(path.ext() == "exr") {
        auto filename = path.file();
        filename = filename.substr(0, filename.size() - 4);
        if(stringEndsWith(filename, ".bnzframebuffer")) {
            std::clog << "Load EXR Framebuffer..." << std::endl;
            m_CurrentFramebufferPath = path;
            m_CurrentFramebuffer = loadEXRFramebuffer(path);
            m_nChannelIdx = 0u;
            setCurrentImage(m_CurrentFramebuffer.getChannel(m_nChannelIdx));
            m_CurrentImagePath = m_CurrentFramebufferPath;
        } else {
            m_CurrentImagePath = path;
            setCurrentImage(*loadEXRImage(path));
        }
        auto geometryFramebufferPath = path.directory() + "geometry.bnzframebuffer.exr";
        if(exists(geometryFramebufferPath)) {
            m_GeometryFramebuffer = loadEXRFramebuffer(geometryFramebufferPath);
            m_bHasGeometryFramebuffer = true;
        } else {
            m_bHasGeometryFramebuffer = false;
        }

    } else if(path.ext() == "rmseFloat") {
        m_RMSECurves.emplace_back();
        loadArray(path, m_RMSECurves.back());
    }
}

void ResultsViewer::checkCurrentImageValidity() {
    m_ListOfInvalidPixels.clear();
    m_ListOfInvalidPixelsStr.clear();
    m_nCurrentInvalidPixel = 0u;
    for(auto i: range(m_CurrentImage.getPixelCount())) {
        auto value = m_CurrentImage[i];
        if(isInvalidMeasurementEstimate(value)) {
            m_ListOfInvalidPixels.emplace_back(i);
            m_ListOfInvalidPixelsStr.emplace_back(toString(i));
        }
    }
}

void ResultsViewer::drawGUI(const Image& displayedImage) {
    auto average = [&](auto v) {
        return (v.x + v.y + v.z) / 3.f;
    };

    if(auto window = m_GUI.addWindow("ViewParameters")) {
        m_GUI.addVarRW(BNZ_GUI_VAR(m_fGamma));
        m_GUI.addVarRW(BNZ_GUI_VAR(m_fScale));
        m_GUI.addButton("Check validity", [&]() {
            checkCurrentImageValidity();
        });
        m_GUI.addSeparator();
        m_GUI.addButton("Set as reference", [&]() {
            m_CurrentReferenceImage = m_CurrentImage;
        });

        m_GUI.addSeparator();

        const char* labels[] = {
            "CURRENT_IMAGE",
            "CURRENT_REFERENCE",
            "ABS_DIFF_WITH_REF",
            "SQUARED_DIFF_WITH_REF",
            "SPECKLES",
            "LOCAL_VARIANCE",
            "CORRECTED_IMAGE"
        };

        m_GUI.addCombo("Display Mode", m_DisplayMode, DISPLAY_MODE_COUNT, labels);

        m_GUI.addSeparator();
        m_GUI.addVarRW(BNZ_GUI_VAR(m_bSumMode));
    }

    if(auto window = m_GUI.addWindow("ResultsDirectory")) {
        drawResultsDirGUI(m_ResultsDirPath, m_ResultsDirPath);
    }

    if(auto window = m_GUI.addWindow("Curves")) {
        m_GUI.addButton("Reset", [&]() {
            m_RMSECurves.clear();
        });
        for(const auto& curve: m_RMSECurves) {
            ImGui::PlotLines("NRMSE", curve.data(), curve.size(), 0, nullptr, 0, 1, ImVec2(512, 128));
        }
    }

    if(auto window = m_GUI.addWindow("InvalidPixels")) {
        if(!m_ListOfInvalidPixels.empty()) {
            m_GUI.addValue("Count", m_ListOfInvalidPixels.size());
            m_GUI.addCombo("pixel", m_nCurrentInvalidPixel, m_ListOfInvalidPixels.size(), [&](uint32_t i) {
               return m_ListOfInvalidPixelsStr[i].c_str();
            });
            auto pixelIndex = m_ListOfInvalidPixels[m_nCurrentInvalidPixel];
            m_GUI.addValue("PixelIndex", pixelIndex);
            auto pixel = getPixel(pixelIndex, m_CurrentImage.getSize());
            m_GUI.addValue("PixelCoords", pixel);
            m_GUI.addValue("PixelValue", m_CurrentImage[pixelIndex]);
        }
    }

    if(m_CurrentFramebuffer.getPixelCount() > 0u) {
        if(auto window = m_GUI.addWindow("CurrentFramebuffer")) {
            m_GUI.addValue("ChannelCount", m_CurrentFramebuffer.getChannelCount());
            m_GUI.addValue("Width", m_CurrentFramebuffer.getWidth());
            m_GUI.addValue("Height", m_CurrentFramebuffer.getHeight());
            m_GUI.addValue("FilePath", m_CurrentFramebufferPath.c_str());

            if(m_CurrentFramebuffer.getChannelCount()) {
                if(m_GUI.addCombo("Channel", m_nChannelIdx, m_CurrentFramebuffer.getChannelCount(), [&](uint32_t channelIdx) {
                    return m_CurrentFramebuffer.getChannelName(channelIdx).c_str();
                })) {
                    setCurrentImage(m_CurrentFramebuffer.getChannel(m_nChannelIdx));
                    m_CurrentImagePath = m_CurrentFramebufferPath;
                }
            }
        }
    }

    if(m_bHasGeometryFramebuffer) {
        if(auto window = m_GUI.addWindow("GeometryFramebuffer")) {
            m_GUI.addValue("ChannelCount", m_GeometryFramebuffer.getChannelCount());
            m_GUI.addValue("Width", m_GeometryFramebuffer.getWidth());
            m_GUI.addValue("Height", m_GeometryFramebuffer.getHeight());

            if(m_GeometryFramebuffer.getChannelCount()) {
                if(m_GUI.addCombo("Channel", m_nGeometryChannelIdx, m_GeometryFramebuffer.getChannelCount(), [&](uint32_t channelIdx) {
                    return m_GeometryFramebuffer.getChannelName(channelIdx).c_str();
                })) {
                    setCurrentImage(m_GeometryFramebuffer.getChannel(m_nGeometryChannelIdx));
                    m_CurrentImagePath = m_ResultsDirPath;
                }
            }
        }
    }

    if(m_CorrectedFramebuffer.getPixelCount() > 0) {
        if(auto window = m_GUI.addWindow("SpeckleCorrectedFramebuffer")) {
            m_GUI.addValue("ChannelCount", m_CorrectedFramebuffer.getChannelCount());
            m_GUI.addValue("Width", m_CorrectedFramebuffer.getWidth());
            m_GUI.addValue("Height", m_CorrectedFramebuffer.getHeight());

            if(m_CorrectedFramebuffer.getChannelCount()) {
                if(m_GUI.addCombo("Channel", m_nCorrectedFramebufferChannelIdx, m_CorrectedFramebuffer.getChannelCount(), [&](uint32_t channelIdx) {
                    return m_CorrectedFramebuffer.getChannelName(channelIdx).c_str();
                })) {
                    setCurrentImage(m_CorrectedFramebuffer.getChannel(m_nCorrectedFramebufferChannelIdx));
                    m_CurrentImagePath = m_ResultsDirPath;
                }
            }
        }
    }

    if(auto window = m_GUI.addWindow("DisplayedImage")) {
        ImGui::Text("Mouse Position: (%.1f,%.1f)", ImGui::GetIO().MousePos.x, ImGui::GetIO().MousePos.y);

        m_GUI.addValue(BNZ_GUI_VAR(m_HoveredPixel));
        if(!displayedImage.empty()) {
            auto value = displayedImage(m_HoveredPixel.x, m_HoveredPixel.y);
            m_GUI.addValue("Value", value);
            auto normalizedValue = value / value.w;
            m_GUI.addValue("Normalized Value", value / value.w);
            auto color = normalizedValue;
            ImGui::Color("Color", ImVec4(color.x, color.y, color.z, color.w));

            auto localVariance = computeLocalVariance(displayedImage, m_HoveredPixel, 2);
            m_GUI.addValue("Local variance", localVariance);

            m_GUI.addSeparator();
            Vec3f sum, mean, variance;
            computeImageStatistics(displayedImage, sum, mean, variance);
            m_GUI.addValue(BNZ_GUI_VAR(sum));
            m_GUI.addValue("sum (average)", average(sum));
            m_GUI.addValue(BNZ_GUI_VAR(mean));
            m_GUI.addValue("mean (average)", average(mean));
            m_GUI.addValue(BNZ_GUI_VAR(variance));
            m_GUI.addValue("variance (average)", average(variance));

            auto smallViewOrigin = Vec2i(m_HoveredPixel) - Vec2i(m_SmallViewImage.getSize()) / 2;
            m_GUI.addValue("Mean local variance",
                           computeMeanLocalVariance(displayedImage, Vec4u(smallViewOrigin, Vec2i(m_SmallViewImage.getSize())), 2));

            m_GUI.addSeparator();

            m_GUI.addVarRW(BNZ_GUI_VAR(m_fSpeckleDetectThreshold));
            m_GUI.addVarRW(BNZ_GUI_VAR(m_nSpeckleDetectWindow));
            m_GUI.addVarRW(BNZ_GUI_VAR(m_nSpeckleCorrectIterationCount));

            m_GUI.addButton("Compute speckle image", [&]() {
                std::vector<float> luminanceBuffer, localLuminanceBuffer;
                detectSpeckles(m_CurrentImage, m_nSpeckleDetectWindow, m_fSpeckleDetectThreshold,
                               luminanceBuffer, localLuminanceBuffer,
                               m_LocalVarianceImage, m_SpeckleImage);
                m_DisplayMode = SPECKLES;
            });

            m_GUI.addButton("Compute correction", [&]() {
                std::vector<float> luminanceBuffer, localLuminanceBuffer;

                m_CorrectedImage = m_CurrentImage;
                for(auto i: range(m_nSpeckleCorrectIterationCount)) {
                    detectSpeckles(m_CorrectedImage, m_nSpeckleDetectWindow, m_fSpeckleDetectThreshold,
                                   luminanceBuffer, localLuminanceBuffer,
                                   m_LocalVarianceImage, m_SpeckleImage);
                    Image tmp;
                    correctSpecklesLuminance(m_CorrectedImage, luminanceBuffer, localLuminanceBuffer, m_SpeckleImage,
                                             tmp);
                    m_CorrectedImage = tmp;
                }
                m_DisplayMode = CORRECTED_IMAGE;
            });

            m_GUI.addButton("Set correction as current image", [&]() {
                m_CurrentImage = m_CorrectedImage;
                m_DisplayMode = CURRENT_IMAGE;
            });

            if(m_CurrentFramebuffer.getPixelCount() > 0) {
                m_GUI.addButton("Compute correction on whole reference framebuffer", [&]() {
                    std::vector<float> luminanceBuffer, localLuminanceBuffer;
                    Image localVarianceImage, speckleImage, correctedImage;

                    m_CorrectedFramebuffer = Framebuffer(m_CurrentFramebuffer.getSize());
                    m_CorrectedFramebuffer.addChannel(m_CurrentFramebuffer.getChannelName(0));
                    size_t firstDepthImageIndex = 1u;
                    size_t depthImageCount = 0u;
                    for(auto i = firstDepthImageIndex; i < m_CurrentFramebuffer.getChannelCount(); ++i) {
                        auto name = m_CurrentFramebuffer.getChannelName(i);
                        if(name.substr(0, 5) == "depth") {
                            ++depthImageCount;
                        }
                        m_CorrectedFramebuffer.addChannel(name);
                        correctedImage = m_CurrentFramebuffer.getChannel(i);
                        for(auto i: range(m_nSpeckleCorrectIterationCount)) {
                            detectSpeckles(correctedImage, m_nSpeckleDetectWindow, m_fSpeckleDetectThreshold,
                                           luminanceBuffer, localLuminanceBuffer,
                                           localVarianceImage, speckleImage);
                            Image tmp;
                            correctSpecklesLuminance(correctedImage,
                                                     luminanceBuffer, localLuminanceBuffer, speckleImage,
                                                     tmp);
                            correctedImage = tmp;
                        }
                        m_CorrectedFramebuffer.setChannel(i, correctedImage);
                        LOG(INFO) << "Framebuffer " << i << " / " << m_CurrentFramebuffer.getChannelCount() << " done.";
                    }
                    LOG(INFO) << "Number of depth images = " << depthImageCount << std::endl;
                    // Reconstruct final render from depth images
                    processTasksDeterminist(m_CorrectedFramebuffer.getPixelCount(), [&](uint32_t pixelID, uint32_t threadID) {
                        auto L = Vec3f(0);
                        for(auto i = firstDepthImageIndex; i < firstDepthImageIndex + depthImageCount; ++i) {
                            auto C = m_CorrectedFramebuffer.getChannel(i)[pixelID];
                            if(C.w) {
                                C /= C.w;
                                L += Vec3f(C);
                            }
                        }
                        m_CorrectedFramebuffer.set(0, pixelID, Vec4f(L, 1));
                    }, getSystemThreadCount());
                });
            }

            if(m_CorrectedFramebuffer.getPixelCount() > 0) {
                m_GUI.addButton("Save corrected framebuffer", [&]() {
                    auto dir = m_CurrentFramebufferPath.directory();
                    auto filepath = dir + "despeckle.bnzframebuffer.exr";
                    storeEXRFramebuffer(filepath, m_CorrectedFramebuffer);
                    auto copy = m_CorrectedFramebuffer.getChannel(0);
                    copy.flipY();
                    copy.divideByAlpha();
                    copy.applyGamma(m_fGamma);
                    storeImage(dir + "reference_despeckle.png", copy);
                });
            }

        } else {
            m_GUI.addValue("Value", Vec4f(0.f));
            m_GUI.addValue("Normalized Value", Vec4f(0.f));
            ImGui::Color("Color", ImVec4(0, 0, 0, 0));
        }
    }

    if(m_CurrentReferenceImage.getSize() == m_CurrentImage.getSize()) {
        if(auto window = m_GUI.addWindow("Errors")) {
            auto mae = computeMeanAbsoluteError(m_CurrentReferenceImage, m_CurrentImage);
            auto rmse = computeRootMeanSquaredError(m_CurrentReferenceImage, m_CurrentImage);
            auto nrmse = computeNormalizedRootMeanSquaredError(m_CurrentReferenceImage, m_CurrentImage);
            m_GUI.addValue("Mean absolute error", mae);
            m_GUI.addValue("Mean absolute error (average)", average(mae));
            m_GUI.addValue("Root mean squared error", rmse);
            m_GUI.addValue("Root mean squared error (average)", average(rmse));
            m_GUI.addValue("Normalized root mean squared error", nrmse);
            m_GUI.addValue("Normalized root mean squared error (average)", average(nrmse));

            if(m_DisplayMode == ABS_DIFF_WITH_REF || m_DisplayMode == SQUARED_DIFF_WITH_REF) {
                m_GUI.addButton("Store error image", [&]() {
                    auto dirPath = m_CurrentImagePath.directory();
                    auto baseFileName = m_CurrentImagePath.file();
                    if(m_DisplayMode == ABS_DIFF_WITH_REF) {
                        baseFileName = FilePath(baseFileName + "-abs_diff-x" + toString(m_fScale) + ".png");
                    } else {
                        baseFileName = FilePath(baseFileName + "-squared_diff-x" + toString(m_fScale) + ".png");
                    }
                    auto copy = displayedImage;
                    copy.flipY();
                    copy.divideByAlpha();
                    storeImage(dirPath + baseFileName, copy);
                });
            }
        }
    }

    if(auto window = m_GUI.addWindow("CurrentImageStatistics")) {
        Vec3f sum, mean, variance;
        computeImageStatistics(m_CurrentImage, sum, mean, variance);
        m_GUI.addValue(BNZ_GUI_VAR(sum));
        m_GUI.addValue("sum (average)", average(sum));
        m_GUI.addValue(BNZ_GUI_VAR(mean));
        m_GUI.addValue("mean (average)", average(mean));
        m_GUI.addValue(BNZ_GUI_VAR(variance));
        m_GUI.addValue("variance (average)", average(variance));
    }
}

void ResultsViewer::detectSpeckles(const Image& image, int windowSize, float detectionThreshold,
                                   std::vector<float>& luminanceBuffer, std::vector<float>& localLuminanceBuffer,
                                   Image& localVarianceImage, Image& speckleImage) const {
    speckleImage = Image(image.getWidth(), image.getHeight());
    localVarianceImage = Image(image.getWidth(), image.getHeight());

    localLuminanceBuffer.resize(image.getPixelCount());
    luminanceBuffer.resize(image.getPixelCount());

    processTasksDeterminist(image.getPixelCount(), [&](uint32_t pixelID, uint32_t threadID) {
        auto color = image[pixelID];
        if(color.w) {
            color /= color.w;
        }
        luminanceBuffer[pixelID] = luminance(Vec3f(color));
    }, getSystemThreadCount());

    processTasksDeterminist(image.getPixelCount(), [&](uint32_t pixelID, uint32_t threadID) {
        Vec2u pixel = getPixel(pixelID, image.getSize());

        auto sumLuminance = 0.f;
        auto sumWeight = 0.f;
        for(auto j = -windowSize; j <= windowSize; ++j) {
            for(auto i = -windowSize; i <= windowSize; ++i) {
                if(i != 0 || j != 0) {
                    auto neighbor = Vec2i(int(pixel.x) + i, int(pixel.y) + j);
                    if(image.contains(neighbor.x, neighbor.y)) {
                        auto weight = computeGeometricWeight(pixel, neighbor);
                        sumLuminance += weight * luminanceBuffer[neighbor.x + neighbor.y * image.getWidth()];
                        sumWeight += weight;
                    }
                }
            }
        }
        if(sumWeight) {
            sumLuminance /= sumWeight;
        }

        localLuminanceBuffer[pixelID] = sumLuminance;
        auto localVariance = sqr(luminanceBuffer[pixelID] - localLuminanceBuffer[pixelID]) / sqr(localLuminanceBuffer[pixelID]);
        localVarianceImage[pixelID] = Vec4f(Vec3f(localVariance), 1);
    }, getSystemThreadCount());

    processTasksDeterminist(image.getPixelCount(), [&](uint32_t pixelID, uint32_t threadID) {
        auto localVariance = localVarianceImage[pixelID].x;

        if(localVariance > detectionThreshold) {
            speckleImage[pixelID] = Vec4f(Vec3f(1), 1);
        } else {
            speckleImage[pixelID] = Vec4f(Vec3f(0), 1);
        }
    }, getSystemThreadCount());
}

void ResultsViewer::correctSpecklesLuminance(const Image& image,
                                             const std::vector<float>& luminanceBuffer,
                                             const std::vector<float>& localLuminanceBuffer,
                                             const Image& speckleImage, Image& correctedImage) const {
    if(image.getSize() != speckleImage.getSize()) {
        LOG(INFO) << "image.getSize() != speckleImage.getSize()";
        return;
    }

    correctedImage = Image(image.getWidth(), image.getHeight());

    processTasksDeterminist(image.getPixelCount(), [&](uint32_t pixelID, uint32_t threadID) {
        if(speckleImage[pixelID].x) {
            Vec2u pixel = getPixel(pixelID, image.getSize());

            if(luminanceBuffer[pixelID]) {
                auto factor = localLuminanceBuffer[pixelID] / luminanceBuffer[pixelID];
                //auto factor = (sqrt(m_fSpeckleDetectThreshold * sqr(sumLuminance)) + sumLuminance) / luminanceBuffer[pixelID];
                auto globalWeight = image[pixelID].w;
                auto color = image[pixelID];
                if(globalWeight)
                    color /= globalWeight;
                correctedImage[pixelID] = Vec4f(factor * globalWeight * Vec3f(color), globalWeight);
            } else {
                correctedImage[pixelID] = image[pixelID];
            }
        } else {
            correctedImage[pixelID] = image[pixelID];
        }
    }, getSystemThreadCount());
}

float ResultsViewer::computeGeometricWeight(Vec2i pixel, Vec2i neighbor) const {
    if(!m_bHasGeometryFramebuffer) {
        return 1.f;
    }
    const auto& positionImage = m_GeometryFramebuffer.getChannel(GeometryFramebufferChannels::NORMALIZED_POSITION);
    const auto& normalImage = m_GeometryFramebuffer.getChannel(GeometryFramebufferChannels::SHADING_NORMAL);

    auto p1 = positionImage(pixel.x, pixel.y);
    if(p1.w)
        p1 /= p1.w;
    auto p2 = positionImage(neighbor.x, neighbor.y);
    if(p2.w)
        p2 /= p2.w;

    auto n1 = normalImage(pixel.x, pixel.y);
    if(n1.w)
        n1 /= n1.w;
    auto n2 = normalImage(neighbor.x, neighbor.y);
    if(n2.w)
        n2 /= n2.w;

    auto weight = 1.f / distanceSquared(Vec3f(p1), Vec3f(p2)) + sqr(abs(dot(Vec3f(n1), Vec3f(n2))));
    return weight;
}

void ResultsViewer::setCurrentImage(const Image& image) {
    if(m_bSumMode) {
        for(auto i: range(m_CurrentImage.getPixelCount())) {
            m_CurrentImage[i] += Vec4f(Vec3f(image[i]), 0.f);
        }
    } else {
        m_CurrentImage = image;
    }

    checkCurrentImageValidity();
}

void ResultsViewer::drawResultsDirGUI(const FilePath& relativePath, const FilePath& absolutePath) {
    Directory dir(absolutePath);

    for(const auto& child: dir.files()) {
        auto absoluteChildPath = absolutePath + child;

        if(isDirectory(absoluteChildPath)) {
            if (ImGui::TreeNode(child.c_str()))
            {
                drawResultsDirGUI(child, absoluteChildPath);
                ImGui::TreePop();
            }
        } else if(isLoadable(absoluteChildPath)) {
            if (ImGui::SmallButton(child.c_str())) {
                load(absoluteChildPath);
            }
        }
    }
}

Image ResultsViewer::getDisplayedImage() {
    switch(m_DisplayMode) {
    case CURRENT_IMAGE:
        return m_CurrentImage;
    case CURRENT_REFERENCE:
        return m_CurrentReferenceImage;
    case ABS_DIFF_WITH_REF:
        if(m_CurrentReferenceImage.getSize() == m_CurrentImage.getSize()) {
            return computeAbsoluteErrorImage(m_CurrentReferenceImage,
                                           m_CurrentImage);
        } else {
            return m_CurrentImage;
        }
    case SQUARED_DIFF_WITH_REF:
        if(m_CurrentReferenceImage.getSize() == m_CurrentImage.getSize()) {
            Vec3f nrmse;
            return computeSquareErrorImage(m_CurrentReferenceImage,
                                           m_CurrentImage,
                                           nrmse);
        } else {
            return m_CurrentImage;
        }
    case SPECKLES:
        return m_SpeckleImage;
    case LOCAL_VARIANCE:
        return m_LocalVarianceImage;
    case CORRECTED_IMAGE:
        return m_CorrectedImage;
    default:
        break;
    }
    return m_CurrentImage;
}

void ResultsViewer::run() {
    while (m_WindowManager.isOpen()) {
        Image displayImage = getDisplayedImage();

        if(!displayImage.empty()) {
            displayImage.scale(m_fScale);
        }

        glClear(GL_COLOR_BUFFER_BIT);

        m_WindowManager.handleEvents();
        m_GUI.startFrame();

        drawGUI(displayImage);

        if(!displayImage.empty()) {
            auto viewport = Vec4u((m_WindowManager.getSize() - displayImage.getSize()) / 2u,
                                  displayImage.getSize());
            glViewport(viewport.x, viewport.y, viewport.z, viewport.w);

            m_GLImageRenderer.drawImage(m_fGamma, displayImage, true, false);

            auto smallViewOrigin = Vec2i(m_HoveredPixel) - Vec2i(m_SmallViewImage.getSize()) / 2;
            for(auto y : range(m_SmallViewImage.getHeight())) {
                for(auto x : range(m_SmallViewImage.getWidth())) {
                    if(displayImage.contains(smallViewOrigin.x + x, smallViewOrigin.y + y)) {
                        m_SmallViewImage(x, y) = displayImage(smallViewOrigin.x + x, smallViewOrigin.y + y);
                    } else {
                        m_SmallViewImage(x, y) = Vec4f(0.f);
                    }
                }
            }

            auto smallImageViewport = Vec4u(0, 0, m_SmallViewImage.getWidth() * 32, m_SmallViewImage.getHeight() * 32);

            glViewport(smallImageViewport.x, smallImageViewport.y, smallImageViewport.z, smallImageViewport.w);
            m_GLImageRenderer.drawImage(m_fGamma, m_SmallViewImage, true, false, GL_NEAREST);

            if(m_WindowManager.hasClicked()) {
                auto pixel = m_WindowManager.getCursorPosition(Vec2u(viewport));
                if(pixel.x >= 0 && pixel.y >= 0 &&
                        pixel.x < viewport.z && pixel.y < viewport.w) {
                    m_HoveredPixel = Vec2u(pixel);
                } else {
                    pixel = m_WindowManager.getCursorPosition(Vec2u(smallImageViewport));
                    if(pixel.x >= 0 && pixel.y >= 0 &&
                            pixel.x < smallImageViewport.z && pixel.y < smallImageViewport.w) {
                        auto bigPixel = pixel / 32;
                        auto truePixel = smallViewOrigin + bigPixel;
                        if(displayImage.contains(truePixel.x, truePixel.y)) {
                            m_HoveredPixel = truePixel;
                        }
                    }
                }
            }
        }

        auto size = m_WindowManager.getSize();
        glViewport(0, 0, (int)size.x, (int)size.y);
        m_bGUIHasFocus = m_GUI.draw();

        m_WindowManager.swapBuffers();
    }
}

}
