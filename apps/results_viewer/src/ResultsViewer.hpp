#pragma once

#include <bonez/viewer/WindowManager.hpp>
#include <bonez/opengl/GLShaderManager.hpp>
#include <bonez/opengl/GLImageRenderer.hpp>
#include <bonez/parsing/parsing.hpp>

namespace BnZ {

class ResultsViewer {
    void load(const FilePath& path);

    void drawResultsDirGUI(const FilePath& relativePath, const FilePath& absolutePath);

    void drawGUI(const Image& image);

    void setCurrentImage(const Image& image);

    void checkCurrentImageValidity();

    Image getDisplayedImage();

    void detectSpeckles(const Image& image, int windowSize, float detectionThreshold,
                       std::vector<float>& luminanceBuffer, std::vector<float>& localLuminanceBuffer,
                       Image& localVarianceImage, Image& speckleImage) const;

    void correctSpecklesLuminance(const Image& image,
                                 const std::vector<float>& luminanceBuffer,
                                 const std::vector<float>& localLuminanceBuffer,
                                 const Image& speckleImage, Image& correctedImage) const;

    float computeGeometricWeight(Vec2i pixel, Vec2i neighbor) const;

protected:
    enum DisplayMode {
        CURRENT_IMAGE,
        CURRENT_REFERENCE,
        ABS_DIFF_WITH_REF,
        SQUARED_DIFF_WITH_REF,
        SPECKLES,
        LOCAL_VARIANCE,
        CORRECTED_IMAGE,
        DISPLAY_MODE_COUNT
    };

    DisplayMode m_DisplayMode = CURRENT_IMAGE;

    FilePath m_ApplicationPath;
    FilePath m_ResultsDirPath;

    WindowManager m_WindowManager;
    GUI m_GUI;
    GLShaderManager m_ShaderManager;

    GLImageRenderer m_GLImageRenderer;

    bool m_bGUIHasFocus = false;
    float m_fGamma = 1.f;
    float m_fScale = 1.f;
    bool m_bSumMode = false;

    Vec2u m_HoveredPixel = Vec2u(0);

    Image m_CurrentReferenceImage;
    Image m_CurrentImage;
    FilePath m_CurrentImagePath; // Can be the path to the framebuffer is one channel is loaded as current image

    Framebuffer m_CurrentFramebuffer;
    FilePath m_CurrentFramebufferPath;
    uint32_t m_nChannelIdx = 0u;

    bool m_bHasGeometryFramebuffer = false;
    Framebuffer m_GeometryFramebuffer;
    size_t m_nGeometryChannelIdx = 0;

    float m_fSpeckleDetectThreshold = 4.f;
    size_t m_nSpeckleDetectWindow = 2;
    size_t m_nSpeckleCorrectIterationCount = 1;
    Image m_SpeckleImage;
    Image m_LocalVarianceImage;

    Image m_CorrectedImage;
    Framebuffer m_CorrectedFramebuffer;
    size_t m_nCorrectedFramebufferChannelIdx = 0;

    std::vector<std::vector<float>> m_RMSECurves;

    std::vector<uint32_t> m_ListOfInvalidPixels;
    std::vector<std::string> m_ListOfInvalidPixelsStr;
    uint32_t m_nCurrentInvalidPixel = 0u;

    Image m_SmallViewImage;
public:
    ResultsViewer(const FilePath& applicationPath,
                  const FilePath& resultsPath,
                  const Vec2u& windowSize);

    void run();
};

}
