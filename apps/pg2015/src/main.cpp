#include <iostream>

#include "PG2015Viewer.hpp"

INITIALIZE_EASYLOGGINGPP

namespace BnZ {

void generateResultsForPG15(const FilePath& applicationPath);

void generateResults300secNewSettings(const FilePath& applicationPath);

void generateTeaserResults30sec(const FilePath& applicationPath);

void generateResultsLongTime(const FilePath& applicationPath);

int main(int argc, char** argv) {
    initEasyLoggingpp(argc, argv);

    generateResultsForPG15(argv[0]);
    //generateResultsLongTime(argv[0]);

	return 0;
}

struct SceneDescription {
    FilePath scenePath;
    std::string configName;
    FilePath referenceImagePath;
    FilePath resultPath;
};

void generateResultsForPG15(const FilePath& applicationPath) {
    std::vector<SceneDescription> sceneDescriptions = {
        { "scenes/corridor/viewer.bnz.xml", "far-from-light",
          "results/corridor/far-from-light/reference/reference.exr", "results/corridor/far-from-light/these_results" },
        { "scenes/door/viewer.bnz.xml", "corner",
          "results/door/corner/reference/reference.exr", "results/door/corner/these_results" },
        { "scenes/sponza/viewer.bnz.xml", "directional-light-from-power",
          "results/sponza/directional-light-from-power/reference/reference.exr", "results/sponza/directional-light-from-power/these_results" },
        { "scenes/sibenik/viewer.bnz.xml", "two-lights",
          "results/sibenik/two-lights/reference/reference.exr", "results/sibenik/two-lights/these_results" },
//        { "scenes/apartment/viewer.bnz.xml", "view3",
//          "results/apartment/view3/reference/reference.exr", "results/apartment/view3/these_results" },
//        { "scenes/plants/viewer.bnz.xml", "view1",
//          "results/plants/view1/reference/reference.exr", "results/plants/view1/these_results" }
    };

    std::vector<std::size_t> renderTimes = {
        10000, 30000, 120000, 300000, 900000
    };

    PG15ICBPTSettings icBPTSettings; // default settings
    std::vector<PG15SkelBPTSettings> skelBPTSettings;
    for(auto filteringFactor: { 0.1f, 0.5f, 1.f }) {
        skelBPTSettings.emplace_back(); // default settings
        skelBPTSettings.back().nodeFilteringFactor = filteringFactor; // Change filtering factor
    }

    // Compute lower bound on total render time
    std::size_t totalTime = 0;
    auto maxTime = *std::max_element(begin(renderTimes), end(renderTimes));
    totalTime += (2 + skelBPTSettings.size()) * maxTime;
    totalTime *= sceneDescriptions.size();
    LOG(INFO) << "Expected lower bound on render time = "
              << (ms2sec(totalTime) / 3600) << " hours" << std::endl;

    for(const auto& scene: sceneDescriptions) {
        PG2015Viewer viewer(
                    applicationPath,
                    scene.scenePath,
                    scene.configName,
                    scene.referenceImagePath,
                    scene.resultPath,
                    renderTimes, // render time
                    6, // max path depth
                    1024, // resampling path count
                    icBPTSettings,
                    skelBPTSettings,
                    128, // thinning resolution
                    true, // use or not segmented skel
                    true); // equalTime (true) or equalIterationCount (false)
        viewer.run();
    }
}

//void generateResults300secNewSettings(const FilePath& applicationPath) {
//    std::vector<FilePath> scenePaths = {
//        "scenes/corridor/viewer.bnz.xml",
//        "scenes/door/viewer.bnz.xml",
//        "scenes/sponza/viewer.bnz.xml",
//        "scenes/sibenik/viewer.bnz.xml"
//    };

//    std::vector<std::string> configNames = {
//        "far-from-light",
//        "corner",
//        "directional-light-from-power",
//        "two-lights"
//    };

//    std::vector<FilePath> referencePaths = {
//        "results/corridor/far-from-light/reference/reference.exr",
//        "results/door/corner/reference/reference.exr",
//        "results/sponza/directional-light-from-power/reference/reference.exr",
//        "results/sibenik/two-lights/reference/reference.exr"
//    };

//    std::vector<FilePath> resultPaths = {
//        "results/corridor/far-from-light/",
//        "results/door/corner/",
//        "results/sponza/directional-light-from-power",
//        "results/sibenik/two-lights/"
//    };

//    std::vector<std::size_t> renderTimes = {
//         300000
//    };

//    PG15ICBPTSettings icBPTSettings; // default settings
//    std::vector<PG15SkelBPTSettings> skelBPTSettings;
//    for(auto filteringFactor: { 1.0f, 1.5f, 2.f, 5.f }) {
//        skelBPTSettings.emplace_back(); // default settings
//        skelBPTSettings.back().nodeFilteringFactor = filteringFactor; // Change filtering factor
//    }

//    // Compute lower bound on total render time
//    std::size_t totalTime = 0;
//    for(auto time: renderTimes) {
//        totalTime += (2 + skelBPTSettings.size()) * time;
//    }
//    totalTime *= configNames.size();
//    LOG(INFO) << "Expected lower bound on render time = "
//              << (ms2sec(totalTime) / 3600) << " hours" << std::endl;

//    for(auto time: renderTimes) {
//        for(auto i = 0u; i < scenePaths.size(); ++i) {

//            PG2015Viewer viewer(
//                        applicationPath,
//                        scenePaths[i],
//                        configNames[i],
//                        referencePaths[i],
//                        resultPaths[i],
//                        time, // render time
//                        6, // max path depth
//                        1024, // resampling path count
//                        icBPTSettings,
//                        skelBPTSettings,
//                        128, // thinning resolution
//                        true // use or not segmented skel
//                        );
//            viewer.run();
//        }
//    }
//}

//void generateTeaserResults30sec(const FilePath& applicationPath) {
//    std::vector<FilePath> scenePaths = {
//        "scenes/sponza/viewer.bnz.xml"
//    };

//    std::vector<std::string> configNames = {
//        "directional-light-from-power"
//    };

//    std::vector<FilePath> referencePaths = {
//        "results/sponza/directional-light-from-power/reference/reference.exr"
//    };

//    std::vector<FilePath> resultPaths = {
//        "results/sponza/directional-light-from-power"
//    };

//    std::vector<std::size_t> renderTimes = {
//         60000
//    };

//    PG15ICBPTSettings icBPTSettings; // default settings
//    std::vector<PG15SkelBPTSettings> skelBPTSettings;
//    for(auto filteringFactor: { 1.0f }) {
//        skelBPTSettings.emplace_back(); // default settings
//        skelBPTSettings.back().nodeFilteringFactor = filteringFactor; // Change filtering factor
//    }

//    // Compute lower bound on total render time
//    std::size_t totalTime = 0;
//    for(auto time: renderTimes) {
//        totalTime += (2 + skelBPTSettings.size()) * time;
//    }
//    totalTime *= configNames.size();
//    LOG(INFO) << "Expected lower bound on render time = "
//              << (ms2sec(totalTime) / 3600) << " hours" << std::endl;

//    for(auto time: renderTimes) {
//        for(auto i = 0u; i < scenePaths.size(); ++i) {

//            PG2015Viewer viewer(
//                        applicationPath,
//                        scenePaths[i],
//                        configNames[i],
//                        referencePaths[i],
//                        resultPaths[i],
//                        time, // render time
//                        6, // max path depth
//                        1024, // resampling path count
//                        icBPTSettings,
//                        skelBPTSettings,
//                        128, // thinning resolution
//                        true // use or not segmented skel
//                        );
//            viewer.run();
//        }
//    }
//}

//void generateResultsLongTime(const FilePath& applicationPath) {
//    std::vector<FilePath> scenePaths = {
//        "scenes/corridor/viewer.bnz.xml",
//        "scenes/door/viewer.bnz.xml",
//        "scenes/sponza/viewer.bnz.xml",
//        "scenes/sibenik/viewer.bnz.xml"
//    };

//    std::vector<std::string> configNames = {
//        "far-from-light",
//        "corner",
//        "directional-light-from-power",
//        "two-lights"
//    };

//    std::vector<FilePath> referencePaths = {
//        "results/corridor/far-from-light/reference/reference.exr",
//        "results/door/corner/reference/reference.exr",
//        "results/sponza/directional-light-from-power/reference/reference.exr",
//        "results/sibenik/two-lights/reference/reference.exr"
//    };

//    std::vector<FilePath> resultPaths = {
//        "results/corridor/far-from-light/",
//        "results/door/corner/",
//        "results/sponza/directional-light-from-power",
//        "results/sibenik/two-lights/"
//    };

//    std::vector<std::size_t> renderTimes = {
//         3600000, 7200000
//    };

//    PG15ICBPTSettings icBPTSettings; // default settings
//    std::vector<PG15SkelBPTSettings> skelBPTSettings;
//    for(auto filteringFactor: { 1.0f }) {
//        skelBPTSettings.emplace_back(); // default settings
//        skelBPTSettings.back().nodeFilteringFactor = filteringFactor; // Change filtering factor
//    }

//    // Compute lower bound on total render time
//    std::size_t totalTime = 0;
//    for(auto time: renderTimes) {
//        totalTime += (2 + skelBPTSettings.size()) * time;
//    }
//    totalTime *= configNames.size();
//    LOG(INFO) << "Expected lower bound on render time = "
//              << (ms2sec(totalTime) / 3600) << " hours" << std::endl;

//    for(auto time: renderTimes) {
//        for(auto i = 0u; i < scenePaths.size(); ++i) {

//            PG2015Viewer viewer(
//                        applicationPath,
//                        scenePaths[i],
//                        configNames[i],
//                        referencePaths[i],
//                        resultPaths[i],
//                        time, // render time
//                        6, // max path depth
//                        1024, // resampling path count
//                        icBPTSettings,
//                        skelBPTSettings,
//                        128, // thinning resolution
//                        true // use or not segmented skel
//                        );
//            viewer.run();
//        }
//    }
//}

}

int main(int argc, char** argv) {
#ifdef _DEBUG
        std::cerr << "DEBUG MODE ACTIVATED" << std::endl;
#endif

#ifdef DEBUG
        std::cerr << "DEBUG MODE ACTIVATED" << std::endl;
#endif
    return BnZ::main(argc, argv);
}
