#pragma once

#include <bonez/common.hpp>
#include <bonez/rendering/renderers/recursive_mis_bdpt.hpp>
#include <bonez/rendering/renderers/DirectImportanceSampleTilePartionning.hpp>

#include "PG15BPTRenderer.hpp"
#include "PG15SkelBPTRenderer.hpp"
#include "PG15ICBPTRenderer.hpp"

namespace BnZ {

void storeFramebuffer(int resultIndex,
                      const FilePath& pngDir,
                      const FilePath& exrDir,
                      const FilePath& baseName,
                      float gamma,
                      const Framebuffer& framebuffer);

struct RenderStatistics {
    std::size_t currentIndexInTimesOrIterCountToStore = 0;
    Microseconds renderTime { 0 }; // Current render time

    // Render time at each frame
    std::vector<float> renderTimes;

    // Errors at each frame
    std::vector<float> nrmseMaxComponent;
    std::vector<float> rmseMaxComponent;
    std::vector<float> maeMaxComponent;
    std::vector<float> nrmseAverage;
    std::vector<float> rmseAverage;
    std::vector<float> maeAverage;

    std::size_t getIterationCount() const {
        return renderTimes.size();
    }
};

class PG15RendererManager {
public:
    PG15RendererManager(const Scene& scene, const Sensor& sensor,
                        Vec2u framebufferSize, const FilePath& referenceImagePath,
                        const FilePath& resultPath,
                        tinyxml2::XMLDocument& configDoc,
                        tinyxml2::XMLDocument& sceneDoc,
                        float gamma,
                        std::vector<std::size_t> renderTimeMsOrIterationCount,
                        std::size_t maxPathDepth,
                        std::size_t resamplingPathCount,
                        const PG15ICBPTSettings& icBPTSettings,
                        const std::vector<PG15SkelBPTSettings>& skelBPTSettings,
                        bool equalTime):
        m_Params(scene, sensor, framebufferSize, maxPathDepth, resamplingPathCount),
        m_SharedData(framebufferSize.x * framebufferSize.y, m_Params.m_nMaxDepth - 1u),
        m_pReferenceImage(loadEXRImage(referenceImagePath.str())),
        m_ICBPTRenderer(m_Params, m_SharedData, icBPTSettings),
        m_RenderStatistics(2 + skelBPTSettings.size()),
        m_fGamma(gamma),
        m_nRenderTimeMsOrIterationCount(renderTimeMsOrIterationCount),
        m_bEqualTime(equalTime) {

        // Store render times
        std::sort(begin(m_nRenderTimeMsOrIterationCount), end(m_nRenderTimeMsOrIterationCount));
        m_nMaxRenderTimeMsOrIterationCount = m_nRenderTimeMsOrIterationCount.back();

        for(const auto& settings: skelBPTSettings) {
            m_SkelBPTRenderers.emplace_back(m_Params, m_SharedData, settings);
        }

        m_Rng.init(getSystemThreadCount(), m_nSeed);

        m_SharedData.m_LightSampler.initFrame(scene);

        initResultDir(resultPath, configDoc, sceneDoc);
    }

    const std::vector<FilePath>& getResultPath() const {
        return m_ResultPath;
    }

    void initResultDir(const FilePath& globalResultPath,
                        tinyxml2::XMLDocument& configDoc,
                        tinyxml2::XMLDocument& sceneDoc) {
        createDirectory(globalResultPath); // Create global result path

        auto dateString = getDateString();

        for(auto i: range(m_nRenderTimeMsOrIterationCount.size())) {
            auto resultName = dateString + "_" + toString(m_nRenderTimeMsOrIterationCount[i]);

            auto resultPath = globalResultPath + resultName;
            m_ResultPath.emplace_back(resultPath);

            createDirectory(m_ResultPath.back());

            configDoc.SaveFile((m_ResultPath.back() + "config.bnz.xml").c_str());
            sceneDoc.SaveFile((m_ResultPath.back() + "scene.bnz.xml").c_str());
        }
    }

    const Image& getReferenceImage() const {
        return *m_pReferenceImage;
    }

    const Framebuffer& getCurrentFramebuffer() const {
        if(m_nDisplayRendererIndex == 0u) {
            return m_BPTRenderer.getFramebuffer();
        } else if(m_nDisplayRendererIndex == 1u) {
            return m_ICBPTRenderer.getFramebuffer();
        }
        return m_SkelBPTRenderers[m_nDisplayRendererIndex - 2].getFramebuffer();
    }

    template<typename RendererType>
    bool render(RendererType& renderer, std::size_t index) {
        auto& stats = m_RenderStatistics[index];

        if(m_bEqualTime && stats.currentIndexInTimesOrIterCountToStore >= m_nRenderTimeMsOrIterationCount.size()) {
            pLogger->info("Already done.");
            return true;
        }

        if(m_bEqualTime && us2ms(stats.renderTime) >= m_nRenderTimeMsOrIterationCount[stats.currentIndexInTimesOrIterCountToStore]) {
            pLogger->info("Renderer %v has ended step %v.", index, stats.currentIndexInTimesOrIterCountToStore);
            storeResults(renderer, index); // Store partial results
            ++stats.currentIndexInTimesOrIterCountToStore;
            if(stats.currentIndexInTimesOrIterCountToStore >= m_nRenderTimeMsOrIterationCount.size()) {
                return true;
            }
        }

        Timer timer;
        renderer.render();
        stats.renderTime += timer.getEllapsedTime<Microseconds>();

        auto& framebuffer = ((const RendererType&)renderer).getFramebuffer();

        auto nrmse = computeNormalizedRootMeanSquaredError(*m_pReferenceImage, framebuffer.getChannel(0));

        auto rmse = computeRootMeanSquaredError(*m_pReferenceImage, framebuffer.getChannel(0));

        auto mae = computeMeanAbsoluteError(*m_pReferenceImage, framebuffer.getChannel(0));

        stats.renderTimes.emplace_back(us2ms(stats.renderTime));
        stats.nrmseMaxComponent.emplace_back(reduceMax(nrmse));
        stats.rmseMaxComponent.emplace_back(reduceMax(rmse));
        stats.maeMaxComponent.emplace_back(reduceMax(mae));

        auto average = [&](auto v) {
            return (v.x + v.y + v.z) / 3.f;
        };

        stats.nrmseAverage.emplace_back(average(nrmse));
        stats.rmseAverage.emplace_back(average(rmse));
        stats.maeAverage.emplace_back(average(mae));

        pLogger->info("Time = %v | NRMSE = %v | RMSE = %v | MAE = %v",
                      us2ms(stats.renderTime), average(nrmse), average(rmse), average(mae));

        return false;
    }

    bool render() {
        bool allDone = true;

        pLogger->info("Start iteration %v", m_SharedData.m_nIterationCount);

        auto initFrameTime = [&](){
            Timer timer;

            {
                auto taskTimer = m_InitIterationTimer.start(0);
                sampleLightPaths();
            }

            {
                auto taskTimer = m_InitIterationTimer.start(1);
                buildDirectImportanceSampleTilePartitionning();
            }

            return timer.getEllapsedTime<Microseconds>();
        }();

        for(auto& stats: m_RenderStatistics) {
            stats.renderTime += initFrameTime;
        }

        pLogger->info("Render BPT");
        allDone = render(m_BPTRenderer, 0) && allDone;
        pLogger->info("Render ICBPT");
        allDone = render(m_ICBPTRenderer, 1) && allDone;

        for(auto i: range(m_SkelBPTRenderers.size())) {
            pLogger->info("Render SkelBPT %v", i);
            allDone = render(m_SkelBPTRenderers[i], 2 + i) && allDone;
        }

        ++m_SharedData.m_nIterationCount;

        if(!m_bEqualTime && m_SharedData.m_nIterationCount == m_nRenderTimeMsOrIterationCount[m_RenderStatistics[0].currentIndexInTimesOrIterCountToStore]) {
            storeResults();
            for(auto& stats: m_RenderStatistics) {
                ++stats.currentIndexInTimesOrIterCountToStore;
            }
            if(m_RenderStatistics[0].currentIndexInTimesOrIterCountToStore == m_nRenderTimeMsOrIterationCount.size()) {
                allDone = true;
            }
        }

        return allDone;
    }

    void drawGUI(GUI& gui) {
        if(auto window = gui.addWindow("RendererManager")) {
            std::vector<std::string> rendererNames = { "BPT", "ICBPT"};
            for(auto index: range(m_SkelBPTRenderers.size())) {
                rendererNames.emplace_back(std::string("SkelBPT ") + toString(index));
            }
            gui.addCombo("Display Framebuffer", m_nDisplayRendererIndex, rendererNames.size(), [&](auto index) {
                return rendererNames[index].c_str();
            });

            gui.addValue("Iteration", m_SharedData.m_nIterationCount);

            for(auto index: range(m_SkelBPTRenderers.size() + 2)) {
                gui.addSeparator();

                gui.addText(rendererNames[index]);

                auto& stats = m_RenderStatistics[index];
                gui.addValue("Time Ellapsed (ms)", us2ms(stats.renderTime));
                if(m_bEqualTime) {
                    gui.addValue("Remaining time", max(0.0, double(m_nMaxRenderTimeMsOrIterationCount) - us2ms(stats.renderTime)));
                }
                gui.addValue("Mean Time Per Frame", us2ms(stats.renderTime) / m_SharedData.m_nIterationCount);

                if(!stats.nrmseAverage.empty()) {
                    gui.addValue("NRMSE", stats.nrmseAverage.back());
                }
                if(!stats.rmseAverage.empty()) {
                    gui.addValue("RMSE", stats.rmseAverage.back());
                }
                if(!stats.maeAverage.empty()) {
                    gui.addValue("MAE", stats.maeAverage.back());
                }
            }
        }
    }

    template<typename RendererType>
    void storeResults(const RendererType& renderer, std::size_t index) {
        const auto& stats = m_RenderStatistics[index];

        FilePath baseName(toString3(index));

        const auto& resultPath = m_ResultPath[stats.currentIndexInTimesOrIterCountToStore];

        FilePath pngDir = resultPath + "png";
        FilePath exrDir = resultPath + "exr";
        FilePath statsDir = resultPath + "stats";

        storeFramebuffer(index, pngDir, exrDir, baseName, m_fGamma, renderer.getFramebuffer());

        createDirectory(statsDir);

        // For backward compatibility:
        storeArray(statsDir + baseName.addExt(".nrmseFloat"), stats.nrmseAverage);
        storeArray(statsDir + baseName.addExt(".rmseFloat"), stats.rmseAverage);
        storeArray(statsDir + baseName.addExt(".maeFloat"), stats.maeAverage);

        // New conventions:
        storeArray(statsDir + baseName.addExt(".nrmseAverage.floatarray"), stats.nrmseAverage);
        storeArray(statsDir + baseName.addExt(".rmseAverage.floatarray"), stats.rmseAverage);
        storeArray(statsDir + baseName.addExt(".maeAverage.floatarray"), stats.maeAverage);

        storeArray(statsDir + baseName.addExt(".nrmseMaxComponent.floatarray"), stats.nrmseMaxComponent);
        storeArray(statsDir + baseName.addExt(".rmseMaxComponent.floatarray"), stats.rmseMaxComponent);
        storeArray(statsDir + baseName.addExt(".maeMaxComponent.floatarray"), stats.maeMaxComponent);

        storeArray(statsDir + baseName.addExt(".processingTimes"), stats.renderTimes);

        auto reportsPath = resultPath + "reports";
        createDirectory(reportsPath);

        auto reportDocumentPath = reportsPath + (toString3(index) + ".report.bnz.xml");
        tinyxml2::XMLDocument reportDocument;

        auto pReport = reportDocument.NewElement("Result");
        auto pRendererStats = reportDocument.NewElement("RendererStats");
        auto pRendererSettings = reportDocument.NewElement("Renderer");

        renderer.storeSettings(*pRendererSettings);
        renderer.storeStatistics(*pRendererStats);

        reportDocument.InsertEndChild(pReport);

        setAttribute(*pReport, "Index", index);
        setChildAttribute(*pReport, "Gamma", m_fGamma);

        setChildAttribute(*pReport, "RenderTime", stats.renderTimes.back());
        setChildAttribute(*pReport, "NRMSEAverage", stats.nrmseAverage.back());
        setChildAttribute(*pReport, "RMSEAverage", stats.rmseAverage.back());
        setChildAttribute(*pReport, "MAEAverage", stats.maeAverage.back());
        setChildAttribute(*pReport, "NRMSEMaxComponent", stats.nrmseMaxComponent.back());
        setChildAttribute(*pReport, "RMSEMaxComponent", stats.rmseMaxComponent.back());
        setChildAttribute(*pReport, "MAEMaxComponent", stats.maeMaxComponent.back());
        setChildAttribute(*pReport, "IterationCount", stats.getIterationCount());

        pReport->InsertEndChild(pRendererStats);

        pReport->InsertEndChild(pRendererSettings);

        reportDocument.SaveFile(reportDocumentPath.c_str());
    }

    void storeResults() {
        pLogger->info("Store BPT results");
        storeResults(m_BPTRenderer, 0);
        pLogger->info("Store ICBPT results");
        storeResults(m_ICBPTRenderer, 1);
        for(auto i: range(m_SkelBPTRenderers.size())) {
            pLogger->info("Store SkelBPT %v results", i);
            storeResults(m_SkelBPTRenderers[i], 2 + i);
        }
    }

    void sampleLightPaths() {
        auto mis = [&](float v) {
            return Mis(v);
        };

        processTasksDeterminist(m_SharedData.m_nLightPathCount, [&](uint32_t pathID, uint32_t threadID) {
            ThreadRNG rng(m_Rng, threadID);
            auto pLightPath = m_SharedData.m_LightVertexBuffer.getSlicePtr(pathID);
            m_SharedData.m_EmissionVertexBuffer[pathID] =
                    sampleLightPath(pLightPath, m_SharedData.m_nLightPathMaxDepth, m_Params.m_Scene,
                            m_SharedData.m_LightSampler, m_Params.m_nResamplingPathCount, mis, rng);
        }, getSystemThreadCount());
    }

    void buildDirectImportanceSampleTilePartitionning() {
        ThreadRNG rng(m_Rng, 0);
        m_SharedData.m_DirectImportanceSampleTilePartionning.build(
                    m_SharedData.m_LightVertexBuffer.size(),
                    m_Params.m_Scene,
                    m_Params.m_Sensor,
                    m_Params.m_FramebufferSize,
                    m_Params.m_TileSize,
                    m_Params.m_TileCount,
                    [&](std::size_t i) {
                        return m_SharedData.m_LightVertexBuffer[i].m_Intersection;
                    },
                    [&](std::size_t i) {
                        return m_SharedData.m_LightVertexBuffer[i].m_Intersection && m_SharedData.m_LightVertexBuffer[i].m_fPathPdf > 0.f;
                    },
                    rng);
    }

private:
    el::Logger* pLogger = el::Loggers::getLogger("PG15RendererManager");

    std::vector<FilePath> m_ResultPath; // One result path per render time

    PG15RendererParams m_Params;
    PG15SharedData m_SharedData;
    Shared<Image> m_pReferenceImage;

    uint32_t m_nSeed = 1024u;
    mutable ThreadsRandomGenerator m_Rng;

    std::size_t m_nDisplayRendererIndex = 0;

    PG15BPTRenderer m_BPTRenderer { m_Params, m_SharedData };
    PG15ICBPTRenderer m_ICBPTRenderer;
    std::vector<PG15SkelBPTRenderer> m_SkelBPTRenderers;

    std::vector<RenderStatistics> m_RenderStatistics;

    float m_fGamma;
    std::vector<std::size_t> m_nRenderTimeMsOrIterationCount;
    std::size_t m_nMaxRenderTimeMsOrIterationCount;
    bool m_bEqualTime = true;

    TaskTimer m_InitIterationTimer = {
        {
            "SampleLightPaths",
            "BuildTilePartionning",
        }, 1u
    };
};

inline void storeFramebuffer(int resultIndex,
                             const FilePath& pngDir,
                             const FilePath& exrDir,
                             const FilePath& baseName,
                             float gamma,
                             const Framebuffer& framebuffer) {
    // Create output directories in case they don't exist
    createDirectory(pngDir.str());
    createDirectory(exrDir.str());

    // Path of primary output image files
    FilePath pngFile = pngDir + baseName.addExt(".png");
    FilePath exrFile = exrDir + baseName.addExt(".exr");

    // Make a copy of the image
    Image copy = framebuffer.getChannel(0);
    // Store the EXR image "as is"
    storeEXRImage(exrFile.str(), copy);

    // Post-process copy of image and store PNG file
    copy.flipY();
    copy.divideByAlpha();
    copy.applyGamma(gamma);
    storeImage(pngFile.str(), copy);

    // Store complete framebuffer as a single EXR multi layer image
    auto exrFramebufferFilePath = exrDir + baseName.addExt(".bnzframebuffer.exr");
    storeEXRFramebuffer(exrFramebufferFilePath.str(), framebuffer);

    // Store complete framebuffer as PNG indivual files in a dediacted subdirectory
    auto pngFramebufferDirPath = pngDir + "framebuffers/";
    createDirectory(pngFramebufferDirPath.str());
    if(resultIndex >= 0) {
        pngFramebufferDirPath = pngFramebufferDirPath + toString3(resultIndex); // Split different results of the same batch in different subdirectories
    }
    createDirectory(pngFramebufferDirPath.str());

    // Store each channel of the framebuffer
    for(auto i = 0u; i < framebuffer.getChannelCount(); ++i) {
        auto name = framebuffer.getChannelName(i);
        // Prefix by the index of the channel
        FilePath pngFile = pngFramebufferDirPath + FilePath(toString3(i)).addExt("_" + name + ".png");

        auto copy = framebuffer.getChannel(i);

        copy.flipY();
        copy.divideByAlpha();

        copy.applyGamma(gamma);
        storeImage(pngFile.str(), copy);
    }
}

}
