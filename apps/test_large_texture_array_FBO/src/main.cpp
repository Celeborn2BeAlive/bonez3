#include <iostream>
#include <bonez/common.hpp>

#include <bonez/viewer/WindowManager.hpp>
#include <bonez/opengl/opengl.hpp>
#include <bonez/opengl/utils/glGet.hpp>
#include <bonez/opengl/utils/GLFramebuffer.hpp>

INITIALIZE_EASYLOGGINGPP

namespace BnZ {

int main(int argc, char** argv) {
    initEasyLoggingpp(argc, argv);

    LOG(INFO) << sizeof(float);

	WindowManager wm(1024, 748, "test");

	GLint maxFBLayers;
	glGetIntegerv(GL_MAX_FRAMEBUFFER_LAYERS, &maxFBLayers);
	LOG(INFO) << "maxFBLayers = " << maxFBLayers;

	GLuint test, depth;

	glGenTextures(1, &test);
	glGenTextures(1, &depth);

	// Settings texture cube map.
	auto& distanceCubeMapArray = test;
	auto& depthCubeMapArray = depth;

	auto m_nTextureWidth = 1024;

    LOG(INFO) << getGPUMemoryInfoCurrentAvailable();
    LOG(INFO) << GLStates::COMPRESSED_TEXTURE_FORMATS::get().size();

    glTextureStorage3DEXT(distanceCubeMapArray, GL_TEXTURE_2D_ARRAY, 1, GL_R32F, 1024, 512, 1024);
	glTextureParameteriEXT(distanceCubeMapArray, GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTextureParameteriEXT(distanceCubeMapArray, GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glTextureStorage3DEXT(depthCubeMapArray, GL_TEXTURE_2D_ARRAY, 1, GL_DEPTH_COMPONENT32F, 1024, 512, 1024);
	glTextureParameteriEXT(depthCubeMapArray, GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTextureParameteriEXT(depthCubeMapArray, GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	GLuint depthMapFBO;
	glGenFramebuffers(1, &depthMapFBO);
	glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, distanceCubeMapArray, 0);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthCubeMapArray, 0);

	GLenum buff = GL_COLOR_ATTACHMENT0;
	glDrawBuffers(1, &buff);

    LOG(INFO) << getGPUMemoryInfoCurrentAvailable();

	GLenum status;
	if ((status = glCheckFramebufferStatus(GL_FRAMEBUFFER)) != GL_FRAMEBUFFER_COMPLETE) {
		std::cout << "Shadowpass Init : Framebuffer not complete!\n" <<
			GLFramebufferErrorString(status) << std::endl;
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	while (wm.isOpen()) {
		wm.handleEvents();
	}

	return 0;
}

}

int main(int argc, char** argv) {
#ifdef _DEBUG
        LOG(INFO) << "DEBUG MODE ACTIVATED";
#endif

#ifdef DEBUG
        LOG(INFO) << "DEBUG MODE ACTIVATED";
#endif
    return BnZ::main(argc, argv);
}
