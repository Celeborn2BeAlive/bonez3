#version 430 core

/// Registers the world distance between the fragment and the current light.
in vec3 lightPos;
in vec4 fragPos;

out float fFragColor;

void main() {
    // The distance in the WorldSpace between fragment and light source.
    fFragColor = distance(fragPos.xyz, lightPos);
}
