#version 430 core

// Calculates a PHONG illumination in the view space in deferred.

const float PI = 3.14159265358979323846264;

// Attributs.
layout(location = 0) out vec3 fColor;
layout(location = 1) out uvec4 fObjectID;

struct GBuffer {
    sampler2D normalDepthSampler;
    sampler2D diffuseSampler;
    sampler2D glossyShininessSampler;
};

// We have to pass a vec4 to align the struture.
// TODO Optimisation : passe this struct without all those vec4 :S///

// Everything is in the viewspace.
struct Light {
    vec4 normal;
    vec4 position_ws;
    vec4 position;
    vec4 intensity;
    vec4 incidentDir;
    vec4 kD;
    vec4 kS;
    vec4 shininess;
};

layout (std430, binding = 0) buffer LightsSSBO {
    Light lights[];
};

// Indexes of the light's SM in the textures.
layout (std430, binding = 1) buffer NodeIDsSSBO {
    uint lightNodeIDs[];
};

// Indexes of the light's SM in the textures.
layout (std430, binding = 2) buffer NodePositionsSSBO {
    vec4 nodePositions[];
};

// Indexes of the light's SM in the textures.
layout (std430, binding = 3) buffer lightIndirectIDSSBO {
    uint lightIndirectID[];
};

// Indexes of the light's SM in the textures.
layout (std430, binding = 4) buffer LightPerNodeSSBO {
    uint lightPerNode[];
};

// Textures.
uniform samplerCubeArray uDepthMap;

// Geometry.
uniform GBuffer uGBuffer;

// Matrices.
uniform mat4 uRcpViewMatrix;

// Lights variables.
uniform uint uLightNumber;

// Used to pool correctyl in the lights SSBO.
uniform uint uLightOffset;

// Used to prevent artefacts on sVPLs.
uniform float uGeometryTermBound;

// Camera variables.
uniform float uFarPlane;

// Shadow variables.
uniform float uBias;
uniform bool uShowShadowMap;
uniform bool ubCastShadow;
uniform bool ubUsingPCF;
uniform uint uNPCFAlgo;
uniform float ufPCFRadius;

// Used to determine the layer of a node.
uniform int uDepthMapMaxSize;

// Ins.
in vec3 vFarPoint_vs;

vec3 gridSamplingDisk[20] = vec3[] (
   vec3( 1,  1,  1), vec3( 1, -1,  1), vec3(-1, -1,  1), vec3(-1,  1,  1),
   vec3( 1,  1, -1), vec3( 1, -1, -1), vec3(-1, -1, -1), vec3(-1,  1, -1),
   vec3( 1,  1,  0), vec3( 1, -1,  0), vec3(-1, -1,  0), vec3(-1,  1,  0),
   vec3( 1,  0,  1), vec3(-1,  0,  1), vec3( 1,  0, -1), vec3(-1,  0, -1),
   vec3( 0,  1,  1), vec3( 0, -1,  1), vec3( 0, -1, -1), vec3( 0,  1, -1)
);

vec3 gCubeSampleOffset[8] = vec3[] (
  vec3( 0.875f, -0.375f, -0.125f ),
  vec3( 0.625f, 0.625f, -0.625f ),
  vec3( 0.375f, 0.125f, -0.875f ),
  vec3( 0.125f, -0.875f, 0.125f ),
  vec3( 0.125f, 0.875f, 0.375f ),
  vec3( 0.375f, -0.625f, 0.625f ),
  vec3( 0.625f, -0.125f, 0.875f ),
  vec3( 0.875f, 0.375f, -0.375f )
);

const uint nbDiskSample = 16;
vec2 gDiskSampleOffset[nbDiskSample] = vec2[] (
vec2( 0.250571, 0.292276),
vec2( 0.293747, 0.612192),
vec2( 0.493866, 0.68599),
vec2( 0.6429, 0.740602),
vec2( -0.243781, 0.310583),
vec2( -0.499614, 0.310614),
vec2( -0.480875, 0.585784),
vec2( -0.861571, 0.147897),
vec2( -0.0148941, -0.260657),
vec2( -0.500842, -0.344345),
vec2( -0.269446, -0.794003),
vec2( -0.628939, -0.688789),
vec2( 0.243955, -0.196678),
vec2( 0.672888, -0.0790019),
vec2( 0.0851204, -0.759673),
vec2( 0.130298, -0.946079)
);


//const uint nbDiskSample = 4;
//vec2 gDiskSampleOffset[nbDiskSample] = vec2[] (
//vec2( -0.0831653, 0.538057),
//vec2( -0.600852, 0.749072),
//vec2( 0.207794, -0.621124),
//vec2( 0.135065, -0.951516)
//);

mat3 frameY(vec3 yAxis) {
    mat3 matrix;

    matrix[1] = yAxis;
    if(abs(yAxis.y) > abs(yAxis.x)) {
        float rcpLength = 1.f / length(vec2(yAxis.x, yAxis.y));
        matrix[0] = rcpLength * vec3(yAxis.y, -yAxis.x, 0.f);
    } else {
        float rcpLength = 1.f / length(vec2(yAxis.x, yAxis.z));
        matrix[0] = rcpLength * vec3(yAxis.z, 0.f, -yAxis.x);
    }
    matrix[2] = cross(matrix[0], yAxis);
    return matrix;
}

// zAxis should be normalized
mat3 frameZ(vec3 zAxis) {
    mat3 matrix;

    matrix[2] = zAxis;
    if(abs(zAxis.y) > abs(zAxis.x)) {
        float rcpLength = 1.f / length(vec2(zAxis.x, zAxis.y));
        matrix[0] = rcpLength * vec3(zAxis.y, -zAxis.x, 0.f);
    } else {
        float rcpLength = 1.f / length(vec2(zAxis.x, zAxis.z));
        matrix[0] = rcpLength * vec3(zAxis.z, 0.f, -zAxis.x);
    }
    matrix[1] = cross(zAxis, matrix[0]);
    return matrix;
}


float shadowCalculationPCFWithMatrix(vec3 position_ws, vec3 normal_ws, vec3 lightPos_ws, float depthIndex) {
    vec3 lightToFragDir = position_ws - lightPos_ws;

    float bias = max(10 * uBias * (1.0 - abs(dot(normal_ws, -lightToFragDir))), uBias);
    float currentDepth = length(lightToFragDir);

    mat3 localMatrix = frameZ(lightToFragDir / currentDepth);

    float shadow = 0.0;
    float diskRadius = 30;
    float sumWeights = 0.f;

    for(uint i = 0; i < nbDiskSample; ++i) {
        vec3 dir = localMatrix * vec3(diskRadius * gDiskSampleOffset[i].x,  diskRadius * gDiskSampleOffset[i].y, currentDepth);
        float closestDepth = texture(uDepthMap, vec4(lightToFragDir + dir, depthIndex)).r;
        float weight = 1/* - length(gDiskSampleOffset[i])*/;
        sumWeights += weight;

        if(currentDepth + bias > closestDepth) {
            shadow += weight;
        }
    }

    shadow /= sumWeights;
    return shadow;
}

/* Computes if the points is in the shadow or not. */
float shadowCalculationPCF(vec3 position_ws, vec3 normal_ws, vec3 lightPos_ws, float depthIndex) {
    vec3 lightToFragDir = position_ws - lightPos_ws;
    float bias = max(10 * uBias * (1.0 - abs(dot(normal_ws, -lightToFragDir))), uBias);
    float currentDepth = length(lightToFragDir);
    float shadow = 0.0;
    float samples = 4.0;
    float offset = 15;
    for(float x = -offset; x < offset; x += offset / (samples * 0.5))
    {
        for(float y = -offset; y < offset; y += offset / (samples * 0.5))
        {
            for(float z = -offset; z < offset; z += offset / (samples * 0.5))
            {
                float closestDepth = texture(uDepthMap, vec4(lightToFragDir + vec3(x, y, z), depthIndex)).r;
                if(currentDepth + bias > closestDepth)
                    shadow += 1.0;
            }
        }
    }
    shadow /= (samples * samples * samples);
    return shadow;
}

/* Computes if the points is in the shadow or not. */

float shadowCalculationPCF2(vec3 position_vs, vec3 position_ws, vec3 normal_ws, vec3 lightPos_ws, float depthIndex) {
    vec3 lightToFragDir = position_ws - lightPos_ws;
    float bias = max(10 * uBias * (1.0 - abs(dot(normal_ws, -lightToFragDir))), uBias);
    float currentDepth = length(lightToFragDir);
    float shadow = 0.0;
    //float bias = 0.15;
    int samples = 20;
    float viewDistance = length(position_vs);
    float diskRadius = (1.0 + viewDistance / uFarPlane) / (25*2);
    vec3 fragToLight = lightToFragDir/currentDepth;
    for(int i = 0; i < samples; ++i) {
        float closestDepth = texture(uDepthMap, vec4(fragToLight + gridSamplingDisk[i] * diskRadius/** ufPCFRadius*/, depthIndex)).r;
        if(currentDepth + bias > closestDepth)
            shadow += 1.0;
    }
    shadow /= float(samples);

    return shadow;
}

float shadowCalculationPCF3(vec3 position_vs, vec3 position_ws, vec3 normal_ws, vec3 lightPos_ws, float depthIndex) {
    vec3 lightToFragDir = position_ws - lightPos_ws;
    float bias = max(10 * uBias * (1.0 - abs(dot(normal_ws, -lightToFragDir))), uBias);
    float currentDepth = length(lightToFragDir);
    float shadow = 0.0;
    //float bias = 0.15;
    int samples = 8;
    float viewDistance = length(position_vs);
    vec3 fragToLight = -lightToFragDir;
    for(int i = 0; i < samples; ++i) {
        float closestDepth = texture(uDepthMap, vec4(lightToFragDir + gCubeSampleOffset[i] * 10, depthIndex)).r;
        if(currentDepth + bias > closestDepth)
            shadow += 1.0;
    }
    shadow /= float(samples);

    return shadow;
}

/* Computes if the points is in the shadow or not. */
float shadowCalculation(vec3 position_ws, vec3 normal_ws, vec3 lightPos_ws, float depthIndex) {
    vec3 lightToFragDir = position_ws - lightPos_ws;
    float closestDepth = texture(uDepthMap, vec4(lightToFragDir, depthIndex)).r;
    float currentDepth = length(lightToFragDir);

    float bias = max(10 * uBias * (1.0 - abs(dot(normal_ws, -lightToFragDir))), uBias);

    return currentDepth + bias > closestDepth ? 1.0 : 0.0;
}


/* Computes the given light's BRDF. */
vec3 computeLightBRDF(Light light, vec3 outgoingDir) {
    vec3 R = normalize(reflect(light.incidentDir.xyz, light.normal.xyz));
    float reflectCos = max(0.0, dot(outgoingDir, R));
    return light.kS.rgb * pow(reflectCos, light.shininess.r) + light.kD.rgb;
}


/* Computes the current point's BRDF. */
vec3 computePointBRDF(vec3 position_vs, vec3 normal_vs, vec3 outgoingDir,
                      vec3 kd, vec3 ks, float shininess) {
    // incidentDir = position_vs;
    vec3 R = normalize(reflect(position_vs, normal_vs));
    float reflectCos = max(0, dot(outgoingDir, R));
    return ks * pow(reflectCos, shininess) + kd;
}


// Lights the given point with the given light, using the
// current cube shadow map.
vec3 lightingPoint(vec3 dirToLight, Light light, float rcpDist, vec4 position_vs, vec3 normal_vs,
                   vec3 normal_ws, vec3 kd, vec3 ks, float shininess) {

    // Getting BRDF of the light and the point.
    vec3 lightBRDF = light.kD.rgb;
    vec3 lightKs = light.kS.rgb;

    // No branching thanks to conditional execution(just a flag).
    if (lightKs[0] + lightKs[1] + lightKs[2] > 0) {
        lightBRDF = computeLightBRDF(light, -dirToLight);
    }
    vec3 pointBRDF = kd;
    if (ks[0] + ks[1] + ks[2] > 0) {
        pointBRDF = computePointBRDF(position_vs.xyz, normal_vs, dirToLight,
                                     kd, ks, shininess);
    }
    // The geometric factor is bounded to avoid point aterfacts..
    // Because we only have pointLight for the moment.
    // Just make the normal to be equal to the incidentDirection for a point
    // light.

    vec3 lightNormal = light.normal.xyz;
    float lightNormalW = light.normal.w;

    // If lightNormal.w == 1, we can calculate the dot product. The result is 1 if lightNormal.w = 0.
    float lightDot = (1 - lightNormalW) + lightNormalW * max(dot(lightNormal.xyz, -dirToLight), 0.f);

    float geometricFactor = min(max(dot(normal_vs, dirToLight), 0.f) *
                                lightDot * (rcpDist * rcpDist), uGeometryTermBound);

    // The result is 0 if the point is in the shadow, the right intensity otherwise.
    return max(vec3(0), geometricFactor * (light.intensity.xyz * lightBRDF * pointBRDF));
}


/* Everything has to be in view space. */
vec3 computeLighting(vec4 position_vs, vec3 normal_vs, vec3 normal_ws,
                     Light light, vec3 kd, vec3 ks,
                     float shininess) {

    // Calculating the point to light direction.
    vec3 dirToVPL = vec3(light.position - position_vs);
    float dist = length(dirToVPL);

    if (dist <= 0.f) {
        return vec3(0);
    }

    dist = 1 / dist; // (now dist equals rcpDist)
    dirToVPL *= dist;

    return lightingPoint(dirToVPL, light, dist, position_vs,
                         normal_vs, normal_ws, kd, ks, shininess);
}


/// Calculates the local node layer in the ShadowMapArray in function of the
/// given nodeID.
uint getNodeLayer(uint nodeID) {
    return nodeID % uDepthMapMaxSize;
}


void main() {

    fObjectID = uvec4(0);

    // Getting material values.
    ivec2 pixelCoords = ivec2(gl_FragCoord.xy);
    vec3 kd = texelFetch(uGBuffer.diffuseSampler, pixelCoords, 0).rgb;
    vec4 ks = texelFetch(uGBuffer.glossyShininessSampler, pixelCoords, 0);

    // Normal in view space.
    vec4 normalDepth = texelFetch(uGBuffer.normalDepthSampler, pixelCoords, 0);
    vec3 normal_vs = normalize(normalDepth.xyz);

    // Position in view space. Using Reconstructing Position from Depth (in w).
    vec4 position_vs = vec4(normalDepth.w * vFarPoint_vs, 1.0);
    vec4 position_ws = uRcpViewMatrix * position_vs;
    vec3 normal_ws = (uRcpViewMatrix * vec4(normal_vs, 0)).xyz;

    // Final color computed. Note : We pool lights using i + uLightOffset !
    // i -> 0 - uLightNumber allows us to pool in the depthMap array.
    vec3 totalColor = vec3(0);

    if (ubCastShadow) {
        uint lightID;
        uint currentNodeID;
        uint lightNb;
        for (uint i = uLightOffset; i < uLightOffset + uLightNumber; ) {
            lightID = lightIndirectID[i];
            currentNodeID = lightNodeIDs[lightID];
            lightNb = lightPerNode[currentNodeID];

            float shadow = 0.0;
            if (ubUsingPCF) {
                switch(uNPCFAlgo) {
                    default : shadow = shadowCalculationPCF(position_ws.xyz, normal_ws, nodePositions[currentNodeID].xyz, getNodeLayer(currentNodeID));
                        break;
                    case 1 : shadow = shadowCalculationPCF2(position_vs.xyz, position_ws.xyz, normal_ws, nodePositions[currentNodeID].xyz, getNodeLayer(currentNodeID));
                        break;
                    case 2 : shadow = shadowCalculationPCFWithMatrix(position_ws.xyz, normal_ws, nodePositions[currentNodeID].xyz, getNodeLayer(currentNodeID));
                        break;
               }
            } else {
                shadow = shadowCalculation(position_ws.xyz, normal_ws, nodePositions[currentNodeID].xyz, getNodeLayer(currentNodeID));
            }

            if(shadow < 1) {
               for (; lightNb > 0 && i < uLightOffset + uLightNumber;
                     ++i, --lightNb, lightID = lightIndirectID[i]) {
                    vec3 lighting = computeLighting(position_vs, normal_vs,
                                                    normal_ws, lights[lightID], kd, ks.rgb, ks.a);
                    totalColor = (totalColor + lighting) - shadow * lighting ;
                }
            } else {
                i += lightNb;
            }
        }
    }

    fColor = totalColor;
}
