#version 430 core

// Calculates a PHONG illumination in the view space in deferred.

const float PI = 3.14159265358979323846264;

struct GBuffer {
    sampler2D normalDepthSampler;
    sampler2D diffuseSampler;
    sampler2D glossyShininessSampler;
};

// Everything is in the viewspace.
// We have to pass a vec4 intensity to align the struture.
struct Light {
    vec4 normal;
    vec4 position;
    vec4 intensity;
};

layout (std430, binding = 0) buffer LightsSSBO {
    Light lights[];
};


// Uniforms.
uniform GBuffer uGBuffer;
uniform uint uLightNumber;

// Attributs.
layout(location = 0) out vec3 fColor;
layout(location = 1) out uvec4 fObjectID;

// Ins/Outs.
in vec3 vFarPoint_vs;


/* Everything has to be in view space. */
vec3 computeLighting(vec4 position_vs, vec3 normal_vs, vec4 lightPosition,
                     vec3 lightIntensity, vec3 kd, vec3 ks, float shininess) {

    vec3 pointToLight = vec3(lightPosition - position_vs);
    float dist = length(pointToLight);

    // Calculating diffuse composant.
    vec3 incidentDir = pointToLight / dist;
    float sDotN = max(dot(incidentDir, normal_vs) , 0.0);
    vec3 diffuse = kd;

    // Calculating specular composant.
    vec3 exitantDir = -incidentDir;
    vec3 reflectionVector = reflect(exitantDir, normal_vs);
    float cosAngle = max(0.0, dot(-normalize(position_vs.xyz), reflectionVector));
    vec3 specular = ks * pow(cosAngle, shininess);

    return lightIntensity * (diffuse + specular) * sDotN / (dist * dist);
}


void main() {

    // Getting material values.
    ivec2 pixelCoords = ivec2(gl_FragCoord.xy);
    vec3 kd = PI * texelFetch(uGBuffer.diffuseSampler, pixelCoords, 0).rgb;
    vec4 ks = texelFetch(uGBuffer.glossyShininessSampler, pixelCoords, 0);

    // Normal in view space.
    vec4 normalDepth = texelFetch(uGBuffer.normalDepthSampler, pixelCoords, 0);
    vec3 normal_vs = normalDepth.xyz;

    // Position in view space. Using Reconstructing Position from Depth (in w).
    vec4 position_vs = vec4(normalDepth.w * vFarPoint_vs, 1.0);

    vec3 totalColor = vec3(0);

    // Final color computed.
    for (int i = 0; i < uLightNumber; ++i) {
        totalColor = totalColor + computeLighting(position_vs, normal_vs,
                                                  lights[i].position,
                                                  vec3(lights[i].intensity), kd,
                                                  ks.rgb, ks.a);
    }

    fColor = totalColor;
    fObjectID = uvec4(0);
}
