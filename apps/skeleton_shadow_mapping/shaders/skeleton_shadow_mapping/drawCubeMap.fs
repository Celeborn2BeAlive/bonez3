#version 430 core

uniform uint uMapIndex;
uniform float uZNear;
uniform float uZFar;
uniform bool uDrawDepth;

in vec2 vTexCoords;

out vec3 fColor;

uniform samplerCubeArray uCubeMapContainer;

vec4 textureSphere(uint texIdx, vec3 wi) {
    return texture(uCubeMapContainer, vec4(wi, texIdx));
}

vec3 sphericalMapping(vec2 texCoords) {
    float PI = 3.14;
    float HALF_PI = 0.5 * PI;
    float QUARTER_PI = 0.25 * PI;

    float phi = texCoords.x * 2 * PI;
    float theta = texCoords.y * PI;

    return vec3(cos(phi) * sin(theta), sin(phi) * sin(theta), cos(theta));
}


void main() {
    vec3 wi = sphericalMapping(vTexCoords);

    if(wi == vec3(0, 0, 0)) {
        fColor = wi;
    } else {
        vec4 value = textureSphere(uMapIndex, wi);
        if(uDrawDepth) {
            float depth = value.r;
            fColor = vec3(/*pow(depth / uZFar, 1/2.2)*/depth / uZFar);
        } else {
            fColor = value.rgb;
        }
    }
}
