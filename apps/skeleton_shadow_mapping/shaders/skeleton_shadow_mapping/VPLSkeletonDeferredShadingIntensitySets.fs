#version 430 core

// Calculates a PHONG illumination in the view space in deferred.

const float PI = 3.14159265358979323846264;

// Attributs.
layout(location = 0) out vec3 fColor;
layout(location = 1) out uvec4 fObjectID;

struct GBuffer {
    sampler2D normalDepthSampler;
    sampler2D diffuseSampler;
    sampler2D glossyShininessSampler;
};

// We have to pass a vec4 to align the struture.
// TODO Optimisation : passe this struct without all those vec4 :S///

// Everything is in the viewspace.
struct Light {
    vec4 normal;
    vec4 position_ws;
    vec4 position;
    vec4 intensity;
    vec4 incidentDir;
    vec4 kD;
    vec4 kS;
    vec4 shininess;
};

layout (std430, binding = 0) buffer LightsSSBO {
    Light lights[];
};

// Indexes of the light's SM in the textures.
layout (std430, binding = 1) buffer NodeIDsSSBO {
    uint lightNodeIDs[];
};

// Indexes of the light's SM in the textures.
layout (std430, binding = 2) buffer NodePositionsSSBO {
    vec4 nodePositions[];
};

// Indexes of the light's SM in the textures.
layout (std430, binding = 3) buffer lightIndirectIDSSBO {
    uint lightIndirectID[];
};

// Indexes of the light's SM in the textures.
layout (std430, binding = 4) buffer LightPerNodeSSBO {
    uvec2 lightCountOffsetPerNode[];
};

uniform sampler2DArray uShadowTexture2DArray;

// Geometry.
uniform GBuffer uGBuffer;

// Matrices.
uniform mat4 uRcpViewMatrix;

// Lights variables.
uniform uint uLightNumber;

// Used to pool correctyl in the lights SSBO.
uniform uint uLightOffset;

// Used to prevent artefacts on sVPLs.
uniform float uGeometryTermBound;

// Camera variables.
uniform float uFarPlane;

// Shadow variables.
uniform float uBias;
uniform bool uShowShadowMap;
uniform bool ubCastShadow;
uniform bool ubUsingPCF;
uniform uint uNPCFAlgo;
uniform float ufPCFRadius;

// Interleaving variables.
uniform uint uInterleavedWindowSize;

// Used to determine the layer of a node.
uniform int uDepthMapMaxSize;

// Ins.
in vec3 vFarPoint_vs;



/* Computes the given light's BRDF. */
vec3 computeLightBRDF(Light light, vec3 outgoingDir) {
    vec3 R = normalize(reflect(light.incidentDir.xyz, light.normal.xyz));
    float reflectCos = max(0.0, dot(outgoingDir, R));
    return light.kS.rgb * pow(reflectCos, light.shininess.r) + light.kD.rgb;
}


/* Computes the current point's BRDF. */
vec3 computePointBRDF(vec3 position_vs, vec3 normal_vs, vec3 outgoingDir,
                      vec3 kd, vec3 ks, float shininess) {
    // incidentDir = position_vs;
    vec3 R = normalize(reflect(position_vs, normal_vs));
    float reflectCos = max(0, dot(outgoingDir, R));
    return ks * pow(reflectCos, shininess) + kd;
}


// Lights the given point with the given light, using the
// current cube shadow map.
vec3 lightingPoint(vec3 dirToLight, Light light, float rcpDist, vec4 position_vs, vec3 normal_vs,
                   vec3 normal_ws, vec3 kd, vec3 ks, float shininess) {

    // Getting BRDF of the light and the point.

    // The light BSDF has to be used !
    vec3 lightBRDF = light.kD.rgb;
    vec3 pointBRDF = vec3(1);//kd;
    vec3 lightKs = light.kS.rgb;

    // No branching thanks to conditional execution(just a flag).
    /*if (lightKs[0] + lightKs[1] + lightKs[2] > 0) {
        lightBRDF = computeLightBRDF(light, -dirToLight);
    }
    if (ks[0] + ks[1] + ks[2] > 0) {
        pointBRDF = computePointBRDF(position_vs.xyz, normal_vs, dirToLight,
                                     kd, ks, shininess);
    }*/
    // The geometric factor is bounded to avoid point aterfacts..
    // Because we only have pointLight for the moment.
    // Just make the normal to be equal to the incidentDirection for a point
    // light.

    vec3 lightNormal = light.normal.xyz;
    float lightNormalW = light.normal.w;

    // If lightNormal.w == 1, we can calculate the dot product. The result is 1 if lightNormal.w = 0.
    float lightDot = (1 - lightNormalW) + lightNormalW * max(dot(lightNormal.xyz, -dirToLight), 0.f);

    float geometricFactor = min(max(dot(normal_vs, dirToLight), 0.f) *
                                lightDot * (rcpDist * rcpDist), uGeometryTermBound);

    // The result is 0 if the point is in the shadow, the right intensity otherwise.
    return max(vec3(0), geometricFactor * (light.intensity.xyz * lightBRDF * pointBRDF));
}


/* Everything has to be in view space. */
vec3 computeLighting(vec4 position_vs, vec3 normal_vs, vec3 normal_ws,
                     Light light, vec3 kd, vec3 ks,
                     float shininess) {

    // Calculating the point to light direction.
    vec3 dirToVPL = vec3(light.position - position_vs);
    float dist = length(dirToVPL);

    if (dist <= 0.f) {
        return vec3(0);
    }

    dist = 1 / dist; // (now dist equals rcpDist)
    dirToVPL *= dist;

    return lightingPoint(dirToVPL, light, dist, position_vs,
                         normal_vs, normal_ws, kd, ks, shininess);
}


/// Returns the next multiple of multiple after start.
/// Can perform the same adding an offset to the global operation.
uint getNextMultiple(uint start, uint multiple, uint offset) {
    return (0 == (int(start) - offset) % multiple) ? start : (start / multiple)
            * multiple + multiple + offset;
}

/// Calculates the local node layer in the ShadowMapArray in function of the
/// given nodeID.
uint getNodeLayer(uint nodeID) {
    return nodeID % uDepthMapMaxSize;
}


void main() {

    fObjectID = uvec4(0);

    // Getting material values.
    ivec2 pixelCoords = ivec2(gl_FragCoord.xy);
    vec3 kd = texelFetch(uGBuffer.diffuseSampler, pixelCoords, 0).rgb;
    vec4 ks = texelFetch(uGBuffer.glossyShininessSampler, pixelCoords, 0);

    // Normal in view space.
    vec4 normalDepth = texelFetch(uGBuffer.normalDepthSampler, pixelCoords, 0);
    vec3 normal_vs = normalize(normalDepth.xyz);

    // Position in view space. Using Reconstructing Position from Depth (in w).
    vec4 position_vs = vec4(normalDepth.w * vFarPoint_vs, 1.0);
    vec4 position_ws = uRcpViewMatrix * position_vs;
    vec3 normal_ws = (uRcpViewMatrix * vec4(normal_vs, 0)).xyz;


    // Calcul of the pixelID in its block and its lightStep to sample lightsSSBO.
    uint pixelID = int(gl_FragCoord.x) % uInterleavedWindowSize +
                  (int(gl_FragCoord.y) % uInterleavedWindowSize) * uInterleavedWindowSize;

    // TODO : Can be calculated outside the shader !!
    uint lightStep = uInterleavedWindowSize * uInterleavedWindowSize;

    // Final color computed. Note : We pool lights using i + uLightOffset !
    // i -> 0 - uLightNumber allows us to pool in the depthMap array.
    vec3 totalColor = vec3(0);

    if (ubCastShadow) {
        uint lightID;
        uint currentNodeID;
        int lightNb;

        float shadow = 0.0;
        uint oldCurrentNodeID = -1;
        // Laurent's version !!! supported by Jefferson mega swag
        for (uint i = uLightOffset + pixelID; i < uLightOffset + uLightNumber;) {

            // Indirections needed.
            lightID = lightIndirectID[i];
            currentNodeID = lightNodeIDs[lightID];
            //lightNb = int(lightPerNode[currentNodeID]);

            if(oldCurrentNodeID != currentNodeID) {
                shadow = texture(uShadowTexture2DArray,
                                 vec3(pixelCoords, getNodeLayer(currentNodeID)), 0).r;
                oldCurrentNodeID = currentNodeID;
            }
            // shadow == (1 - shadow) already.
            if (shadow > 0) {
                vec3 lighting = computeLighting(position_vs, normal_vs,
                                                normal_ws, lights[lightID], kd, ks.rgb, ks.a);
                totalColor = totalColor + shadow * lighting;

                i += lightStep;
            } else {
                uvec2 countOffset = lightCountOffsetPerNode[currentNodeID];
                uint numerator = countOffset.x + countOffset.y - i;
                uint nextMultiple = uint(ceil(numerator / float(lightStep)));

                i += nextMultiple * lightStep;
            }
        }
    } else {
        for (uint i = uLightOffset + pixelID; i < uLightOffset + uLightNumber; i += lightStep) {
            // Indirections needed.
            uint lightID = lightIndirectID[i];
            vec3 lighting = computeLighting(position_vs, normal_vs,
                                            normal_ws, lights[lightID], kd, ks.rgb, ks.a);
            totalColor = totalColor + lighting;
        }
    }

    fColor = totalColor;
}
