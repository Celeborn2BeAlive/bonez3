#version 430 core

// Calculates a PHONG illumination in the view space in deferred.

const float PI = 3.14159265358979323846264;

// Attributs.
layout(location = 0) out vec3 fColor;

struct GBuffer {
    sampler2D normalDepthSampler;
    sampler2D diffuseSampler;
    sampler2D glossyShininessSampler;
};


// Textures.
uniform samplerCubeArray uDepthMap;
// Geometry.
uniform GBuffer uGBuffer;
// Matrices.
uniform mat4 uRcpViewMatrix;
// Shadow variables.
uniform float uBias;

uniform vec3 uNodePosition;
uniform uint uNodeLayer;

// Ins.
in vec3 vFarPoint_vs;

/* Computes if the points is in the shadow or not. */
float shadowCalculation(vec3 position_ws, vec3 normal_ws, vec3 lightPos_ws, float depthIndex) {
    vec3 lightToFragDir = position_ws - lightPos_ws;
    float closestDepth = texture(uDepthMap, vec4(lightToFragDir, depthIndex)).r;
    float currentDepth = length(lightToFragDir);

    float bias = max(10 * uBias * (1.0 - abs(dot(normal_ws, -lightToFragDir))), uBias);

    return currentDepth + bias > closestDepth ? 1.0 : 0.0;
}

void main() {
    // Getting material values.
    ivec2 pixelCoords = ivec2(gl_FragCoord.xy);

    // Normal in view space.
    vec4 normalDepth = texelFetch(uGBuffer.normalDepthSampler, pixelCoords, 0);

    // Position in view space. Using Reconstructing Position from Depth (in w).
    vec4 position_vs = vec4(normalDepth.w * vFarPoint_vs, 1.0);
    vec4 position_ws = uRcpViewMatrix * position_vs;
    vec3 normal_vs = normalize(normalDepth.xyz);
    vec3 normal_ws = (uRcpViewMatrix * vec4(normal_vs, 0)).xyz;

    float shadow = shadowCalculation(position_ws.xyz, normal_ws, uNodePosition, uNodeLayer);

    //fColor = vec3(1 - shadow);
    fColor = abs(normal_ws) * vec3(1 - shadow);
}
