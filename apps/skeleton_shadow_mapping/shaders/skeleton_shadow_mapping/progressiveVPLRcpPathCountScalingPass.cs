#version 430 core

layout(local_size_x = 32, local_size_y = 32) in;

uniform sampler2D uAccumulatedTexture;
uniform float uRcpPathCount;

layout(rgba32f) uniform image2D uFinalRenderImage;

void main() {
    uvec2 size = uvec2(imageSize(uFinalRenderImage));

    if(any(greaterThanEqual(gl_GlobalInvocationID.xy, size))) {
        return;
    }

    ivec2 pixel = ivec2(gl_GlobalInvocationID.xy);
    vec4 value = texelFetch(uAccumulatedTexture, pixel, 0);
    imageStore(uFinalRenderImage, pixel, value * uRcpPathCount);
}
