#version 430

out vec3 fColor;

uniform uint uNodeIndex;
uniform sampler2DArray uShadowTexture2DArray;
uniform vec3 uIntensity;

void main() {
    ivec2 pixelCoords = ivec2(gl_FragCoord.xy);

    fColor = vec3(0);

    //float visibility = texture(uShadowTexture2DArray, vec3(pixelCoords, uNodeIndex), 0).r;
    vec3 color = texelFetch(uShadowTexture2DArray, ivec3(pixelCoords, uNodeIndex), 0).xyz;
    fColor += uIntensity * color;
}
