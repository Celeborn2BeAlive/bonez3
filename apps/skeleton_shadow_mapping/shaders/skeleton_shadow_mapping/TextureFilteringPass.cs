#version 430 core

layout(local_size_x = 32, local_size_y = 32) in;

layout(rgba32f) readonly uniform image2D uInitialRenderImage;
writeonly uniform image2D uFinalRenderImage;

uniform sampler2D uDiffuseSampler;

void main() {
    uvec2 size = uvec2(imageSize(uFinalRenderImage));

    if(any(greaterThanEqual(gl_GlobalInvocationID.xy, size)) ) {
        return;
    }

    // Fetching pixel G-informations.
    ivec2 pixel = ivec2(gl_GlobalInvocationID.xy);
    vec3 kd = texelFetch(uDiffuseSampler, pixel, 0).rgb;
    vec3 oldValue = imageLoad(uInitialRenderImage, pixel).xyz;
    imageStore(uFinalRenderImage, pixel, vec4(oldValue * kd, 1));
}
