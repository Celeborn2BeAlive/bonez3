#version 430 core

const float PI = 3.14159265358979323846264;

// Attributs.
layout(location = 0) out vec3 fColor;
layout(location = 1) out uvec4 fObjectID;

struct GBuffer {
    sampler2D normalDepthSampler;
    sampler2D diffuseSampler;
    sampler2D glossyShininessSampler;
};

// We have to pass a vec4 to align the struture.
// TODO Optimisation : passe this struct without all those vec4 :S///

// Everything is in the viewspace.
struct Light {
    vec4 normal;
    vec4 position_ws;
    vec4 position;
    vec4 intensity;
    vec4 incidentDir;
    vec4 kD;
    vec4 kS;
    vec4 shininess;
};

layout (std430, binding = 0) buffer LightsSSBO {
    Light lights[];
};

// Indexes of the light's SM in the textures.
layout (std430, binding = 1) buffer NodeIDsSSBO {
    uint lightNodeIDs[];
};

// Indexes of the light's SM in the textures.
layout (std430, binding = 2) buffer NodePositionsSSBO {
    vec4 nodePositions[];
};

// Textures.
uniform samplerCubeArray uDepthMap;

// Geometry.
uniform GBuffer uGBuffer;

// Matrices.
uniform mat4 uRcpViewMatrix;

// Lights variables.
uniform uint uLightNumber;
// Used to pool correctyl in the lights SSBO.
uniform uint uLightOffset;
// Used to prevent artefacts on sVPLs.
uniform float uGeometryTermBound;

// Camera variables.
uniform float uFarPlane;

// Shadow variables.
uniform float uBias;
uniform bool uShowShadowMap;
uniform int uCastShadow;
uniform bool ubUsingPCF;

// Used to determine the layer of a node.
uniform int uDepthMapMaxSize;

// Node variables.
uniform uint uFirstNodeID;
uniform uint uLastNodeID;

// Ins.
in vec3 vFarPoint_vs;



/* Computes the given light's BRDF. */
vec3 computeLightBRDF(Light light, vec3 outgoingDir) {
    vec3 R = normalize(reflect(light.incidentDir.xyz, light.normal.xyz));
    float reflectCos = max(0.0, dot(outgoingDir, R));
    return light.kD.rgb + light.kS.rgb * pow(reflectCos, light.shininess.r);
}


/* Computes the current point's BRDF. */
vec3 computePointBRDF(vec4 position_vs, vec3 normal_vs, vec3 outgoingDir,
                      vec3 kd, vec3 ks, float shininess) {
    vec3 incidentDir = normalize(position_vs.xyz);
    vec3 R = normalize(reflect(incidentDir, normal_vs));
    float reflectCos = max(0.0, dot(outgoingDir, R));
    return kd + ks * pow(reflectCos, shininess);
}


/* Computes if the points is in the shadow or not. */
float shadowCalculationPCF(vec3 position_ws, vec3 normal_ws, vec3 lightPos_ws, float depthIndex) {
    vec3 lightToFragDir = position_ws - lightPos_ws;
    float bias = max(10 * uBias * (1.0 - dot(normal_ws, -lightToFragDir)), uBias);
    float currentDepth = length(lightToFragDir);
    float shadow = 0.0;
    float samples = 4.0;
    float offset = 15;
    for(float x = -offset; x < offset; x += offset / (samples * 0.5))
    {
        for(float y = -offset; y < offset; y += offset / (samples * 0.5))
        {
            for(float z = -offset; z < offset; z += offset / (samples * 0.5))
            {
                float closestDepth = texture(uDepthMap, vec4(lightToFragDir + vec3(x, y, z), depthIndex)).r;
                if(currentDepth + bias > closestDepth)
                    shadow += 1.0;
            }
        }
    }
    shadow /= (samples * samples * samples);
    return shadow;
}


/* Computes if the points is in the shadow or not. */
float shadowCalculation(vec3 position_ws, vec3 normal_ws, vec3 lightPos_ws, float depthIndex) {
    vec3 lightToFragDir = position_ws - lightPos_ws;
    float closestDepth = texture(uDepthMap, vec4(lightToFragDir, depthIndex)).r;
    float currentDepth = length(lightToFragDir);

    float bias = max(10 * uBias * (1.0 - dot(normal_ws, -lightToFragDir)), uBias);

    return currentDepth + bias > closestDepth ? 1.0 : 0.0;
}

// Lights the given point with the given light, using the
// current cube shadow map.
vec3 lightingPoint(vec3 dirToLight, Light light, float dist, float depthIndex,
                   uint nodeID, vec4 position_vs, vec3 normal_vs, vec4 position_ws,
                   vec3 normal_ws, vec3 kd, vec3 ks, float shininess) {

    // Getting BRDF of the light and the point.
    vec3 lightBRDF = computeLightBRDF(light, -dirToLight);
    vec3 pointBRDF = computePointBRDF(position_vs, normal_vs, dirToLight,
                                      kd, ks, shininess);

    // The geometric factor is bounded to avoid point aterfacts..
    // Because we only have pointLight for the moment.
    // Just make the normal to be equal to the incidentDirection for a point
    // light.
    vec4 lightNormal = light.normal;
    // If lightNormal.w == 1, we can calculate the dot product. The result is 1 if lightNormal.w = 0.
    float lightDot = (1 - lightNormal.w) + lightNormal.w * max(dot(lightNormal.xyz, -dirToLight), 0.f);

    float geometricFactor = min(max(dot(normal_vs, dirToLight), 0.f) *
                                lightDot / (dist * dist), uGeometryTermBound);

    float shadow = 0.0;
    if (uCastShadow != 0.0) {
        if (ubUsingPCF) {
            // Calculating the shadow state (the point is in => 1.0, 0.0 otherwise).
            shadow = shadowCalculationPCF(position_ws.xyz, normal_ws, nodePositions[nodeID].xyz, depthIndex);
        } else {
            shadow = shadowCalculation(position_ws.xyz, normal_ws, nodePositions[nodeID].xyz, depthIndex);
        }
    }

    // The result is 0 if the point is in the shadow, the right intensity otherwise.
    return (1.0 - shadow) *
            max(vec3(0), light.intensity.xyz * lightBRDF * pointBRDF * geometricFactor);
}


/* Everything has to be in view space. */
vec3 computeLighting(vec4 position_vs, vec4 position_ws, vec3 normal_vs, vec3 normal_ws,
                     Light light, float depthIndex, uint nodeID, vec3 kd, vec3 ks,
                     float shininess) {

    // Calculating the point to light direction.
    vec3 dirToVPL = vec3(light.position - position_vs);
    float dist = length(dirToVPL);

    if (dist <= 0.f) {
        return vec3(0);
    }

    dirToVPL /= dist;
    return lightingPoint(dirToVPL, light, dist, depthIndex, nodeID, position_vs,
                         normal_vs, position_ws, normal_ws, kd, ks, shininess);
}


/* Returns the closestDepth grey Level of the first light. */
/*float closestDepth(vec3 position_ws, vec3 lightPos_ws, float depthIndex) {
    vec3 lightToFragDir = position_ws - lightPos_ws;
    float d = texture(uDepthMap, vec4(lightToFragDir, depthIndex)).r;
    return d;
}
*/

/// Calculates the local node layer in the ShadowMapArray in function of the
/// given nodeID.
uint getNodeLayer(uint nodeID) {
    return nodeID % uDepthMapMaxSize;
}


void main() {

    fObjectID = uvec4(0);

    // Getting material values.
    ivec2 pixelCoords = ivec2(gl_FragCoord.xy);
    vec3 kd = texelFetch(uGBuffer.diffuseSampler, pixelCoords, 0).rgb;
    vec4 ks = texelFetch(uGBuffer.glossyShininessSampler, pixelCoords, 0);

    // Normal in view space.
    vec4 normalDepth = texelFetch(uGBuffer.normalDepthSampler, pixelCoords, 0);
    vec3 normal_vs = normalize(normalDepth.xyz);

    // Position in view space. Using Reconstructing Position from Depth (in w).
    vec4 position_vs = vec4(normalDepth.w * vFarPoint_vs, 1.0);
    vec4 position_ws = uRcpViewMatrix * position_vs;
    vec3 normal_ws = (uRcpViewMatrix * vec4(normal_vs, 0)).xyz;


    // Final color computed. Note : We pool lights using i + uLightOffset !
    // i -> 0 - uLightNumber allows us to pool in the depthMap array.
    vec3 totalColor = vec3(0);
    int nodeID;
    // Parsing all the lights.
    for (int i = 0; i < uLightNumber; ++i) {
        uint nodeID = lightNodeIDs[i];
        if (nodeID < uFirstNodeID || nodeID > uLastNodeID) {
            continue;
        }
        totalColor = totalColor + computeLighting(position_vs, position_ws, normal_vs,
                                                  normal_ws, lights[i], getNodeLayer(nodeID),
                                                  nodeID, kd, ks.rgb, ks.a);
    }

    fColor = totalColor;
}
