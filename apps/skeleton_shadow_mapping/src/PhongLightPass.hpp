#pragma once

#include "bonez/viewer/GUI.hpp"
#include "bonez/opengl/utils/GLutils.hpp"
#include "bonez/opengl/GLGBuffer.hpp"
#include "bonez/opengl/GLShaderManager.hpp"
#include "bonez/maths/BBox.hpp"
#include "bonez/scene/Scene.hpp"
#include "bonez/opengl/GLScreenFramebuffer.hpp"

namespace BnZ {

/// Performs a test pass by rendering a GLGBuffer
/// in a GLScreenTriangle.
class PhongLightPass {

public:
    PhongLightPass(const GLShaderManager& shaderManager,
                   const Scene& scene);

    ///
    /// \brief render renders the triangle in finalDrawBuffer
    /// by using gBuffer, viewMatrix and rcpProjMatrix as uniform values.
    /// \param gBuffer the gBuffer containing deferred values.
    /// \param viewMatrix the viewMatrix of the camera.
    /// \param rcpProjMatrix the reciproque projection matrix used after DefPass.
    /// \param triangle the triangle to draw.
    ///
    void render(const GLGBuffer& gBuffer,
                const Mat4f& viewMatrix,
                const Mat4f& rcpProjMatrix,
                const GLScreenTriangle& triangle);

    /// Exposes the current statue into gui.
    void exposeIO(GUI& gui);

    /// Updates the position in ViewSpace of light buffered.
    void updateGPULightPositions(const Mat4f& viewMatrix);

    /// Structure representing lights passed to the shader using a SSBO.
    /// Everything is in the world space.
    /// We have to pass a Vec4f intensity to align the struture.
    struct Light {
        Vec4f normal;
        Vec4f position;
        Vec4f intensity;

    Light(const Vec4f &normal, const Vec4f &position, Vec3f intensity) :
        normal(normal), position(position), intensity(intensity, 0) {}
    };
private:

    /// Lights in world space.
    std::vector<Light> m_WorldLights;

    /// Our LightBuffer bridging with OpenGL to send world lights.
    GLBuffer<Light> m_LightBuffer;

    /// The shader program of this pass.
    GLProgram m_Program;

    /// The uniform buffer of this pass.
    GLGBufferUniform uGBuffer { m_Program };

    /// The id of the light buffer.
    GLuint m_LightBufferID;

    /// Lights.
    std::vector<Light> m_Lights;

    /// Uniform variables used during this pass.
    BNZ_GLUNIFORM(m_Program, GLuint, uLightNumber);
    BNZ_GLUNIFORM(m_Program, Mat4f, uRcpProjMatrix);
};
}
