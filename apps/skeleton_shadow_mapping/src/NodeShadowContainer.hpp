#pragma once

#include "ShadowContainer.hpp"

#include <vector>
#include <bonez/opengl/utils/GLTexture.hpp>
#include <bonez/opengl/GLGBuffer.hpp>
#include <bonez/opengl/utils/GLutils.hpp>
#include <bonez/opengl/GLShaderManager.hpp>

namespace BnZ {
///
/// \brief The NodeShadowContainer class Allows to contain shadow
/// FBO for each node of the Skeleton.
///
class NodeShadowContainer {
public:

    NodeShadowContainer(const GLShaderManager& shaderManager,
                        size_t nTextureWidth, size_t nTextureHeight);

    void init(size_t nMaxLayerRequested);

    void render(const GLScreenTriangle& triangle,
                const GLGBuffer& gBuffer,
                const std::vector<Vec3f>& positions,
                const ShadowContainer& shadowContainer,
                float fBias,
                size_t nCount, // Number of nodes to process in array "positions"
                const Mat4f& rcpProjMatrix,
                const Mat4f& rcpViewMatrix);

    void renderOneLayer(const GLScreenTriangle& triangle,
                        const GLGBuffer& gBuffer,
                        const std::vector<Vec3f>& positions,
                        const ShadowContainer& shadowContainer,
                        float fBias,
                        const Mat4f& rcpProjMatrix,
                        const Mat4f& rcpViewMatrix,
                        size_t layerIndex);


    ///
    /// \brief getNumberOfTextureArrayToPool Calculates the number of depth map Array to
    /// pool, to request the exact number of layer requested.
    /// \param nLayerCountRequested
    /// \return the number of texture array necessary to retrieve nLayerCountRequested CSM.
    ///
    inline size_t getNumberOfTextureArrayToPool(size_t nLayerCountRequested) const {
        return getSetCount(nLayerCountRequested, m_nLimitArraySizePerFBOs);
    }

    ///
    /// \brief getTextureArrayId
    /// \return texture array lastly computed.
    ///
    inline GLuint getTextureArrayId(size_t index) const {
        return m_ShadowTexture2DArrays[index].glId();
    }

    inline size_t getLimitArraySizePerFBO() const {
        return m_nLimitArraySizePerFBOs;
    }

    inline const GLTexture2DArray& getTextureArray(size_t index) const {
        return m_ShadowTexture2DArrays[index];
    }

private:

    ///
    /// \brief getTextureArrayForTheLayer
    /// \param nLayerRequested is from 0 to the max -1 global size allocated at the initialization.
    /// --> eg : Starting from 0.
    ///
    inline size_t getTextureArrayForTheLayer(size_t nLayerRequested) const {
        // We get the FBO count needed to contain nLayerRequested. Then we return
        // the id of the last one (which contains the nLayerRequested_th layer).
        return getSetCount(nLayerRequested + 1, m_nLimitArraySizePerFBOs) - 1;
    }

    ///
    /// \brief getTextureArrayLocalLayerIndex
    /// \param nLayerRequested is from 1 to the max global size allocated at the initialization.
    /// --> eg : if you want the first light, just pass one.
    /// \return the local index of a layer inside its depth map texture array.
    ///
    inline size_t getTextureArrayLocalLayerIndex(size_t nLayerRequested) const  {
        return (nLayerRequested) % m_nLimitArraySizePerFBOs;
    }

    ///
    /// \brief getSetCount Calculates the number of set needed to contain nTotal elements.
    /// \param nTotal
    /// \param nSetSize
    /// \return the number of set to contain nTotal elements.
    ///
    size_t getSetCount(size_t nTotal, size_t nSetSize) const;

    ///
    /// \brief generateTextureArray
    /// \param nLayerCount
    /// \param index
    ///
    void generateTextureArray(size_t index, size_t nLayerCount);

    /// The shader program of this pass.
    GLProgram m_Program;

    /// The uniform buffer of this pass.
    GLGBufferUniform uGBuffer { m_Program };
    BNZ_GLUNIFORM(m_Program, Vec3f, uNodePosition);
    BNZ_GLUNIFORM(m_Program, GLuint, uNodeLayer);
    BNZ_GLUNIFORM(m_Program, int, uDepthMap); // TODO try GLint instead.
    BNZ_GLUNIFORM(m_Program, GLfloat, uBias);
    BNZ_GLUNIFORM(m_Program, Mat4f, uRcpViewMatrix);
    BNZ_GLUNIFORM(m_Program, Mat4f, uRcpProjMatrix);

    ///
    size_t m_nTextureWidth;
    size_t m_nTextureHeight;

    /// The maximum array size per fbo.
    size_t m_nLimitArraySizePerFBOs;

    /// The max global size. Can not be exceeded.
    size_t m_nMaxGlobalSizeAllocated;

    /// Fbos to handle more textures than the limit.
    GLFramebufferObject m_FBO;

    /// The shadow textures.
    std::vector<GLTexture2DArray> m_ShadowTexture2DArrays;
};
}
