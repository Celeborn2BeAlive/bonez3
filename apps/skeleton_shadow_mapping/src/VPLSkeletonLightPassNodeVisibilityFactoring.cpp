#include "VPLSkeletonLightPassNodeVisibilityFactoring.hpp"
#include "bonez/sampling/Random.hpp"
#include "VPLBuilder.hpp"
#include "bonez/viewer/GUI.hpp"

namespace BnZ {


VPLSkeletonLightPassNodeVisibilityFactoring::VPLSkeletonLightPassNodeVisibilityFactoring(const GLShaderManager& shaderManager,
                                           const Vec2f& nearFar,
                                           size_t width, size_t height) :
    m_Program(shaderManager.buildProgram({ "skeleton_shadow_mapping/viewSpaceDeferredShadingPass.vs",
                                           "skeleton_shadow_mapping/VPLSkeletonDeferredShadingPassNodeVisibilityFactoring.fs" })),
    m_VPLRcpPathCountScalingPass(shaderManager), m_ShadowPass(shaderManager),
    m_ShadowContainer(nearFar.x, nearFar.y, 128), m_DrawDepthPass(shaderManager) {

    // Used for the accumulation pass & the display of the depth map in the debug window.
    m_AccumulationFramebuffer.init(width, height, { GL_RGB32F });
    m_DepthMapDisplayFramebuffer.init(512, 256, { GL_RGB32F });
}

void VPLSkeletonLightPassNodeVisibilityFactoring::init(const Scene& scene,
                                const CurvilinearSkeleton& skeleton) {

    Timer timer;

    // Intializing the shadow pass maximum size from our light count.
    m_nSkeletonSize = skeleton.size();
    m_ShadowContainer.init(m_nSkeletonSize);

    // Initializing skeleton nodes positions.
    m_NodesPositions.clear();
    m_NodesPositions.reserve(m_nSkeletonSize);

    // Creating Skeleton Nodes Position GPU Buffer.
    std::vector<Vec4f> nodesPositions;
    nodesPositions.reserve(m_nSkeletonSize);
    for (auto i : range(m_nSkeletonSize)) {
        auto pos = skeleton.getNode(i).P;
        m_NodesPositions.emplace_back(pos);
        nodesPositions.emplace_back(pos, 1);
    }
    m_NodesPositionsBuffer = GLBufferStorage<Vec4f>(m_nSkeletonSize, nodesPositions.data());


    // Initializing lights world positions.
    auto lights = generateVPLLights(m_nPathCount, m_nMaxDepth, VPLBuilder(scene));
    if (0 == lights.size()) {
        std::cerr << "Warning : VPLSkeletonLightPassNodeVisibilityFactoring::init : lights generated == 0 from generateVPLLights "
                     "(VPLBuilder)." << std::endl;
        exit(EXIT_FAILURE);
    }

    // Setting lights nearest node index.
    // Setting the LightShadowMapIndexes buffer.
    std::vector<unsigned int> nearestNodeID;
    nearestNodeID.reserve(lights.size());

    // Histogram of the number of light per skeleton node.
    m_LightPerNodeNumber = std::vector<unsigned int>(m_nSkeletonSize, 0);

    auto i = 0;
    for (auto& light : lights) {
        // The normal.w equals 0 if not existing, 1 otherwise.
        size_t id;
        if (0 == light.normal.w) {
            id = skeleton.getNearestNode(Vec3f(light.positionWs));
        } else {
            id = skeleton.getNearestNode(Vec3f(light.positionWs), Vec3f(light.normal));
        }

        if(UNDEFINED_NODE == id) {
            std::cerr << "Warning : VPLSkeletonLightPassNodeVisibilityFactoring::init : id is equal to UNDEFINED_NODE for : "
                      << "Position " << light.positionWs << " index : " << i << std::endl;
            ++i;
            continue;
        }

        m_Lights.emplace_back(light);
        //std::cout << "light : "  << light.positionWs << i++ << " index : " << id << std::endl;

        // Incrementing the number of light for this skeleton node.
        m_LightPerNodeNumber[id]++;
        nearestNodeID.emplace_back(id);
    }

    std::vector<unsigned int> tabIndices;
    for (auto i : range(m_Lights.size())) {
        tabIndices.emplace_back(i);
    }

    // Sorting the indices vector by nodeID to process them in the GPU without tests.
    std::sort(tabIndices.begin(), tabIndices.end(),
              [&](size_t lightID1, size_t lightID2) -> bool {
        // Is lightID1's nearestID lower than lightID2's one.
        if (nearestNodeID[lightID1] == nearestNodeID[lightID2])
            // Here sorting by the lightID.
            return lightID1 < lightID2;
        return nearestNodeID[lightID1] < nearestNodeID[lightID2];
    });

    // Storing the current number of light processed.
    m_nLightProcessed = m_Lights.size();

    //
    m_LightToNodesIndexesBuffer = GLBufferStorage<unsigned int>(m_nLightProcessed, nearestNodeID.data());

    // Filling nodes linked to VPLs GPU buffer.
    m_IndirectIndicesBuffer = GLBufferStorage<unsigned int>(m_nLightProcessed, tabIndices.data());

    // Filling the GPU light buffer.
    // !!! Directly done at the update in the render loop with the calcul of the VS position.
    // m_LightBuffer = GLBufferStorage<BufferedLightGPU>(m_nLightProcessed, m_Lights.data());

    // Filling the light per node buffer.
    m_LightPerNodeBuffer = GLBufferStorage<unsigned int>(m_nSkeletonSize, m_LightPerNodeNumber.data());

    // Precalculing the numbers of lights linked to each layer in each DepthMap.
    auto nDepthMapCount = m_ShadowContainer.getNumberOfDepthMapTextureToPool(m_nSkeletonSize);


    // A layer corresponds to a skeleton node's distance map.
    m_nLightPerDepthMapTexture.clear();
    m_nLightPerDepthMapTexture.reserve(nDepthMapCount);
    auto nFirstNode = 0, nLastNode = 0;
    auto nb = 0;
    for (int i : range(nDepthMapCount)) {

        auto nLayerCount = m_ShadowContainer.getLayerCount(i);
        nLastNode = nFirstNode + nLayerCount;

        // Accumulating light count for this depthMap.
        m_nLightPerDepthMapTexture[i] = std::accumulate(m_LightPerNodeNumber.begin() + nFirstNode,
                                                        m_LightPerNodeNumber.begin() + nLastNode, 0);
        // Going forward.
        nFirstNode = nLastNode;
        nb += m_nLightPerDepthMapTexture[i];
    }

    std::cerr << "Info : End of VPLSkeletonLightPassNodeVisibilityFactoring::init. Duration : "
              << timer.getMicroEllapsedTime().count() / 1000 << " ms. Nb : "
              << nb << std::endl;
}


void VPLSkeletonLightPassNodeVisibilityFactoring::updateGPULightInViewSpace(const Mat4f& viewMatrix) {

    // Mapping the GPU buffer.
    std::vector<BufferedLightGPU> tmpLights;
    tmpLights.reserve(m_Lights.size());
    //    BufferedLightGPU *lights = m_LightBuffer.map(GL_WRITE_ONLY);

    // Calculating ViewSpace positions.
    auto i = 0;
    Vec4f tmp;
    for(auto &worldLight : m_Lights) {
        tmpLights.emplace_back(worldLight);
        auto& light = tmpLights.back();
        light = worldLight;
        light.positionWs = worldLight.positionWs;
        light.position = viewMatrix * worldLight.position;

        // The normal fourth coordinate can carry interferring informations.
        tmp = worldLight.normal; tmp.w = 0;
        light.normal = normalize(viewMatrix * tmp);
        light.normal.w = worldLight.normal.w;
        light.incidentDir = normalize(viewMatrix * worldLight.incidentDir);
    }

    m_LightBuffer = GLBufferStorage<BufferedLightGPU>(m_Lights.size(), tmpLights.data());
    // Unmapping the GPU buffer.
//    m_LightBuffer.unmap();
}


// Creating a vector of light positions.
std::vector<Vec3f> VPLSkeletonLightPassNodeVisibilityFactoring::createPositionsFromLight() {
    std::vector<Vec3f> positions;
    positions.reserve(m_nLightProcessed);

    for (auto i : range(m_nLightProcessed)) {
        auto& light = m_Lights[i];
        positions.emplace_back(light.positionWs);
    }

    return positions;
}

void VPLSkeletonLightPassNodeVisibilityFactoring::buildShadowCubeFromPositions(const GLScene& scene,
                                                        const std::vector<Vec3f>& positions) {
    m_ShadowContainer.buildShadowTransforms(positions);
    m_ShadowPass.render(scene, m_ShadowContainer, positions.size());
}


void VPLSkeletonLightPassNodeVisibilityFactoring::render(GLScreenFramebuffer& finalDrawBuffer,
                                  const GLScene& scene,
                                  const GLGBuffer& gBuffer,
                                  const Mat4f& viewMatrix,
                                  const Mat4f& rcpProjMatrix,
                                  const Mat4f& rcpViewMatrix,
                                  const Vec3f& camPosition,
                                  const GLScreenTriangle& triangle,
                                  GUI& gui) {

    // Updating values.
    exposeIO(gui);

    // Problem : Can not be called in init for some reason...
    // The shadowmaps are built but not usable !
    static bool done = false;
    if (!done) {
        buildShadowCubeFromPositions(scene, m_NodesPositions);
        done = true;
    }

    // Updating GPU lights positions.
    updateGPULightInViewSpace(viewMatrix);

    // No accumulative rendering here.
    if (m_bShowShadowMap) {
        RenderLastVPLShadowCubeMap(triangle, gui);
    }

    // Using our deffered program.
    m_Program.use();

    // Sending our "global" uniforms which never change during the iteration.
    sendUniforms(gBuffer, viewMatrix, rcpViewMatrix, rcpProjMatrix,
                 m_ShadowContainer.getShadowFar(), camPosition);

    //Binding the current light buffer.
    m_LightBuffer.bindBase(GL_SHADER_STORAGE_BUFFER, 0);
    m_LightToNodesIndexesBuffer.bindBase(GL_SHADER_STORAGE_BUFFER, 1);
    m_NodesPositionsBuffer.bindBase(GL_SHADER_STORAGE_BUFFER, 2);
    m_IndirectIndicesBuffer.bindBase(GL_SHADER_STORAGE_BUFFER, 3);
    m_LightPerNodeBuffer.bindBase(GL_SHADER_STORAGE_BUFFER, 4);

    // Drawing in this accumulationg framebuffer.
    m_AccumulationFramebuffer.bindForDrawing();
    glClear(GL_COLOR_BUFFER_BIT);

    // The depth test is already done by the deferred pass.
    glDisable(GL_DEPTH_TEST);
    glViewport(0, 0, m_AccumulationFramebuffer.getWidth(), m_AccumulationFramebuffer.getHeight());

    // Getting the number of render to do. It is equal to the number of depth map containing
    // m_nLightProcessed layers (previously initialized).
    auto nRenderCount = m_ShadowContainer.getNumberOfDepthMapTextureToPool(m_nSkeletonSize);

    // Enabling the  progressive pass values.
    glEnablei(GL_BLEND, 0);
    glBlendFunci(0, GL_ONE, GL_ONE);
    glBlendEquationi(0, GL_FUNC_ADD);

    // Rendering for each valid fbo and setting right specific uniforms like the offset to retrieve
    // lights and the depthMap Array used.
    uShowShadowMap.set(false);
    size_t nFirstNodeID, nLastNodeID, nNodeProcessed = 0, nLightProcessed = 0;

    for (auto i : range(nRenderCount)) {
        // Nodes.
        /*auto nCurrentLayerNumber = m_ShadowContainer.getLayerCount(i);
        nFirstNodeID = nNodeProcessed;
        nLastNodeID = nFirstNodeID + nCurrentLayerNumber - 1; // Strict.
        nNodeProcessed += nCurrentLayerNumber;

        // Getting the light number to process.
        auto nLightNumber = std::accumulate(m_LightPerNodeNumber.begin() + nFirstNodeID,
                                            m_LightPerNodeNumber.begin() + nLastNodeID, 0);
        */
        // Getting the light number to process.
        auto nLightNumber = m_nLightPerDepthMapTexture[i];

        // Updating the count of lightProcessed.
        // Setting the start offset.
        uLightOffset.set(nLightProcessed);
        uLightNumber.set(nLightProcessed + nLightNumber);
        nLightProcessed += nLightNumber;

        // Binding the current shadow cube map array.
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_CUBE_MAP_ARRAY, m_ShadowContainer.getDepthMapId(i));

        // Rendering the scene by rendering the triangle.
        triangle.render();

        // Disabling the current SCMA texture.
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_CUBE_MAP_ARRAY, 0);
    }

    // Disabling the progressive pass values.
    glDisablei(GL_BLEND, 0);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);


    // Final pass rendering to the finalFrameBuffer and normalizing by nPathCount.
    m_VPLRcpPathCountScalingPass.render(m_AccumulationFramebuffer.getColorBuffer(0),
                                        m_nPathCount,
                                        finalDrawBuffer.getColorBuffer());

}


void VPLSkeletonLightPassNodeVisibilityFactoring::sendUniforms(const GLGBuffer& gBuffer, const Mat4f& viewMatrix,
                                        const Mat4f& rcpViewMatrix, const Mat4f& rcpProjMatrix,
                                        float shadowFar, const Vec3f& camPosition) {
    uDepthMap.set(3);
    uGBuffer.set(gBuffer, 0, 1, 2);
    // Sending uniforms.
    uRcpProjMatrix.set(rcpProjMatrix);
    uRcpViewMatrix.set(rcpViewMatrix);
    uGeometryTermBound.set(m_fGeometryTermBound);
    uFarPlane.set(shadowFar);
    uBias.set(m_fBias);
    ubCastShadow.set(m_bCastShadow);
    uShowShadowMap.set(m_bShowShadowMap);
    uLightNumber.set(m_nLightProcessed);
    ubUsingPCF.set(m_bUsingPCF);
    uNPCFAlgo.set(m_nPCFAlgo);
    ufPCFRadius.set(m_fPCFRadius);
    uDepthMapMaxSize.set(m_ShadowContainer.getDepthMapMaxLayerCount());
}


void VPLSkeletonLightPassNodeVisibilityFactoring::RenderLastVPLShadowCubeMap(const GLScreenTriangle& triangle, GUI& gui) {
    auto window = gui.addWindow("DepthMap", false, m_DepthMapDisplayFramebuffer.getSize());
    if(!window) {
        return;
    }

    m_DepthMapDisplayFramebuffer.bindForDrawing();
    glClear(GL_COLOR_BUFFER_BIT);
    glViewport(0, 0, m_DepthMapDisplayFramebuffer.getWidth(), m_DepthMapDisplayFramebuffer.getHeight());

    // Retrieving the depthMap containing the layer of the last light processed.
    auto depthMap = m_ShadowContainer.getDepthMapTextureForTheLayer(m_nLightProcessed - 1);
    auto layerLocalIndex = m_ShadowContainer.getDepthMapTextureLocalLayerIndex(m_nLightProcessed - 1);

    glDisable(GL_DEPTH_TEST);

    m_DrawDepthPass.m_Program.use();

    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_CUBE_MAP_ARRAY, depthMap);

    m_DrawDepthPass.uCubeMapContainer.set(3);
    m_DrawDepthPass.uZNear.set(m_ShadowContainer.getShadowNear());
    m_DrawDepthPass.uZFar.set(m_ShadowContainer.getShadowFar());
    m_DrawDepthPass.uMapIndex.set(layerLocalIndex);
    m_DrawDepthPass.uDrawDepth.set(true);

    // Rendering the scene by rendering the triangle.
    triangle.render();

    // Disabling the current SCMA texture.
    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_CUBE_MAP_ARRAY, 0);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0u);

    gui.addImage(m_DepthMapDisplayFramebuffer.getColorBuffer(0));
}


void VPLSkeletonLightPassNodeVisibilityFactoring::computeNodeStats(float& fPercentBiggerThafAverage, float& fVariance,
                                            float& fNotZero, float& fLightPerNodeAverage) {
    for (auto& nb : m_LightPerNodeNumber) {
        fNotZero += (nb > 0) ? 1 : 0;
        fLightPerNodeAverage += nb;
    }

    fLightPerNodeAverage /= m_nSkeletonSize;

    auto fValue = fLightPerNodeAverage * m_nPercentOnAverageToCmp / 100.0f;
    for (auto& nb : m_LightPerNodeNumber) {
        fVariance += (nb - fLightPerNodeAverage) * (nb - fLightPerNodeAverage);
        fPercentBiggerThafAverage += (nb > fValue) ? 1 : 0;
    }

    fVariance /= m_nSkeletonSize;
    fPercentBiggerThafAverage *= 100.0f / m_nSkeletonSize;
}


void VPLSkeletonLightPassNodeVisibilityFactoring::exposeIO(GUI &gui) {

    // Updates the number of light processed.
    if(auto window = gui.addWindow("VPLSkeletonLightPassNodeVisibilityFactoring")) {

        gui.addValue("Path count", m_nPathCount);
        gui.addValue("Max depth ", m_nMaxDepth - 2);
        gui.addSeparator();

        gui.addVarRW(BNZ_GUI_VAR(m_nLightProcessed));

        // Clamping the number of light processed.
        if (0 == m_nLightProcessed) m_nLightProcessed = m_Lights.size();
        else m_nLightProcessed = m_nLightProcessed > m_Lights.size() ? 1 :
                                                                       m_nLightProcessed;

        // Geometry term limit.
        gui.addVarRW(BNZ_GUI_VAR(m_fGeometryTermBound));

        // Position info.
        gui.addSeparator();
        gui.addValue("Last position ",
                     Vec3f(m_Lights[m_nLightProcessed-1].position));

        gui.addVarRW(BNZ_GUI_VAR(m_bCastShadow));
        gui.addVarRW(BNZ_GUI_VAR(m_bUsingPCF));
        gui.addValue("Basic PCF ", 0);
        gui.addValue("Disk PCF ", 1);
        gui.addValue("Matrix PCF ", 2);
        gui.addVarRW(BNZ_GUI_VAR(m_nPCFAlgo));
        gui.addVarRW(BNZ_GUI_VAR(m_fPCFRadius));
        gui.addVarRW(BNZ_GUI_VAR(m_bShowShadowMap));

        gui.addVarRW(BNZ_GUI_VAR(m_fBias));


        auto fNotZero = 0.f;
        auto fVariance = 0.f;
        auto fLightPerNodeAverage = 0.f;
        auto fPercentBiggerThafAverage = 0.f;

        computeNodeStats(fPercentBiggerThafAverage, fVariance,
                         fNotZero, fLightPerNodeAverage);

        gui.addSeparator();
        auto nbNode = m_nSkeletonSize;
        gui.addValue(BNZ_GUI_VAR(nbNode));
        gui.addValue(BNZ_GUI_VAR(fNotZero));
        gui.addValue(BNZ_GUI_VAR(fLightPerNodeAverage));
        gui.addValue(BNZ_GUI_VAR(fVariance));
        gui.addVarRW(BNZ_GUI_VAR(m_nPercentOnAverageToCmp));
        gui.addValue(BNZ_GUI_VAR(fPercentBiggerThafAverage));
    }
}
}
