#include "VPLLightPass.hpp"
#include "bonez/sampling/Random.hpp"
#include "VPLBuilder.hpp"
#include "bonez/viewer/GUI.hpp"

namespace BnZ {

VPLLightPass::VPLLightPass(const GLShaderManager& shaderManager,
                           const Vec2f& nearFar,
                           const Scene& scene,
                           size_t width, size_t height) :
    m_Program(shaderManager.buildProgram({ "skeleton_shadow_mapping/viewSpaceDeferredShadingPass.vs",
                                           "skeleton_shadow_mapping/VPLDeferredShadingPass.fs" })),
    m_VPLRcpPathCountScalingPass(shaderManager), m_ShadowPass(shaderManager),
    m_ShadowContainer(nearFar.x, nearFar.y, 128), m_DrawDepthPass(shaderManager) {
    // Initializing lights world positions.
    m_Lights = generateVPLLights(m_nPathCount, m_nMaxDepth, VPLBuilder(scene));
    // Storing the current number of light seen.
    m_nLightProcessed = m_Lights.size();
    // Filling the GPU light  buffer.
    m_LightBuffer.setData(m_nLightProcessed, m_Lights.data(), GL_DYNAMIC_DRAW);
    // Intializing the shadow pass maximum size from our light count.
    m_ShadowContainer.init(m_nLightProcessed);
    m_AccumulationFramebuffer.init(width, height, { GL_RGB32F });
    m_DepthMapDisplayFramebuffer.init(512, 256, { GL_RGB32F });
}


void VPLLightPass::updateGPULightInViewSpace(const Mat4f& viewMatrix) {

    // Mapping the GPU buffer.
    BufferedLightGPU *lights= m_LightBuffer.map(GL_WRITE_ONLY);

    // Calculating ViewSpace positions.
    Vec4f tmp;
    for(auto &worldLight : m_Lights) {
        auto& light = (*lights++);
        light.positionWs = worldLight.positionWs;
        light.position = viewMatrix * worldLight.position;
        // The normal fourth coordinate can carry interferring informations.
        tmp = worldLight.normal; tmp.w = 0;
        light.normal = normalize(viewMatrix * tmp);
        light.normal.w = worldLight.normal.w;
        light.incidentDir = normalize(viewMatrix * worldLight.incidentDir);
    }

    // Unmapping the GPU buffer.
    m_LightBuffer.unmap();
}


void VPLLightPass::buildShadows(const GLScene& scene) {

    // Creating a vector of light positions.
    std::vector<Vec3f> positions;
    positions.reserve(m_nLightProcessed);

    for (auto i : range(m_nLightProcessed)) {
        auto& light = m_Lights[i];
        positions.emplace_back(light.positionWs);
    }
    // Building the shadows.
    m_ShadowContainer.buildShadowTransforms(positions);
    m_ShadowPass.render(scene, m_ShadowContainer, m_nLightProcessed);
}


void VPLLightPass::render(GLScreenFramebuffer& finalDrawBuffer,
                          const GLScene& scene,
                          const GLGBuffer& gBuffer,
                          const Mat4f& viewMatrix,
                          const Mat4f& rcpProjMatrix,
                          const Mat4f& rcpViewMatrix,
                          const Vec3f& camPosition,
                          const GLScreenTriangle& triangle,
                          GUI& gui) {

    // Updating values.
    exposeIO(gui);

    // Problem : Can not be called in init for some reason...
    // The shadowmaps are built but not usable !
    static bool done = false;
    if (!done) {
        buildShadows(scene);
        done = true;
    }

    // Updating GPU lights positions.
    updateGPULightInViewSpace(viewMatrix);

    // No accumulative rendering here.
    if (m_bShowShadowMap) {

        RenderLastVPLShadowCubeMap(triangle, gui);

    }

    // Using our deffered program.
    m_Program.use();

    // Sending our "global" uniforms which never change during the iteration.
    sendUniforms(gBuffer, viewMatrix, rcpViewMatrix, rcpProjMatrix,
                 m_ShadowContainer.getShadowFar(), camPosition);

    //Binding the current light buffer.
    m_LightBuffer.bindBase(GL_SHADER_STORAGE_BUFFER, 0);

    // Drawing in this accumulationg framebuffer.
    m_AccumulationFramebuffer.bindForDrawing();
    glClear(GL_COLOR_BUFFER_BIT);

    // The depth test is already done by the deferred pass.
    glDisable(GL_DEPTH_TEST);
    glViewport(0, 0, m_AccumulationFramebuffer.getWidth(), m_AccumulationFramebuffer.getHeight());

    // Getting the number of render to do. It is equal to the number of depth map containing
    // m_nLightProcessed layers (previously initialized).
    auto nRenderCount = m_ShadowContainer.getNumberOfDepthMapTextureToPool(m_nLightProcessed);

    // Enabling the  progressive pass values.
    glEnablei(GL_BLEND, 0);
    glBlendFunci(0, GL_ONE, GL_ONE);
    glBlendEquationi(0, GL_FUNC_ADD);

    // Rendering for each valid fbo and setting right specific uniforms like the offset to retrieve
    // lights and the depthMap Array used.
    auto nLightOffset = 0;
    uShowShadowMap.set(false);
    for (auto i : range(nRenderCount)) {

        if (nLightOffset >= m_nLightProcessed) {
            break;
        }

        auto nCurrentLightNumber = m_ShadowContainer.getLayerCount(i);

        // Setting specific uniforms.
        uLightNumber.set(m_nLightProcessed > nCurrentLightNumber ? nCurrentLightNumber : m_nLightProcessed);
        uLightOffset.set(nLightOffset);

        // Starting at the right light index, the next iteration.
        nLightOffset += nCurrentLightNumber;

        // Binding the current shadow cube map array.
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_CUBE_MAP_ARRAY, m_ShadowContainer.getDepthMapId(i));

        // Rendering the scene by rendering the triangle.
        triangle.render();

        // Disabling the current SCMA texture.
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_CUBE_MAP_ARRAY, 0);
    }

    // Disabling the progressive pass values.
    glDisablei(GL_BLEND, 0);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

    // Final pass rendering to the finalFrameBuffer and normalizing by nPathCount.
    m_VPLRcpPathCountScalingPass.render(m_AccumulationFramebuffer.getColorBuffer(0),
                                        m_nPathCount,
                                        finalDrawBuffer.getColorBuffer());
}


void VPLLightPass::sendUniforms(const GLGBuffer& gBuffer, const Mat4f& viewMatrix,
                                const Mat4f& rcpViewMatrix, const Mat4f& rcpProjMatrix,
                                float shadowFar, const Vec3f& camPosition) {
    uDepthMap.set(3);
    uGBuffer.set(gBuffer, 0, 1, 2);
    // Sending uniforms.
    uRcpProjMatrix.set(rcpProjMatrix);
    uRcpViewMatrix.set(rcpViewMatrix);
    uGeometryTermBound.set(m_fGeometryTermBound);
    uFarPlane.set(shadowFar);
    uBias.set(m_fBias);
    uCastShadow.set(m_bCastShadow ? 1.0f : 0.0f);
    uShowShadowMap.set(m_bShowShadowMap);
    ubUsingPCF.set(m_bUsingPCF);
}


void VPLLightPass::RenderLastVPLShadowCubeMap(const GLScreenTriangle& triangle, GUI& gui) {
    auto window = gui.addWindow("DepthMap", false, m_DepthMapDisplayFramebuffer.getSize());
    if(!window) {
        return;
    }

    m_DepthMapDisplayFramebuffer.bindForDrawing();
    glClear(GL_COLOR_BUFFER_BIT);
    glViewport(0, 0, m_DepthMapDisplayFramebuffer.getWidth(), m_DepthMapDisplayFramebuffer.getHeight());

    // Retrieving the depthMap containing the layer of the last light processed.
    auto depthMap = m_ShadowContainer.getDepthMapTextureForTheLayer(m_nLightProcessed - 1);
    auto layerLocalIndex = m_ShadowContainer.getDepthMapTextureLocalLayerIndex(m_nLightProcessed - 1);

    glDisable(GL_DEPTH_TEST);

    m_DrawDepthPass.m_Program.use();

    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_CUBE_MAP_ARRAY, depthMap);

    m_DrawDepthPass.uCubeMapContainer.set(3);
    m_DrawDepthPass.uZNear.set(m_ShadowContainer.getShadowNear());
    m_DrawDepthPass.uZFar.set(m_ShadowContainer.getShadowFar());
    m_DrawDepthPass.uMapIndex.set(layerLocalIndex);
    m_DrawDepthPass.uDrawDepth.set(true);

    // Rendering the scene by rendering the triangle.
    triangle.render();

    // Disabling the current SCMA texture.
    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_CUBE_MAP_ARRAY, 0);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0u);

    gui.addImage(m_DepthMapDisplayFramebuffer.getColorBuffer(0));
}


void VPLLightPass::exposeIO(GUI &gui) {

    // Updates the number of light processed.
    if(auto window = gui.addWindow("VPLLightPass")) {

        gui.addValue("Path count", m_nPathCount);
        gui.addValue("Max depth ", m_nMaxDepth - 2);
        gui.addSeparator();

        gui.addVarRW(BNZ_GUI_VAR(m_nLightProcessed));

        // Clamping the number of light processed.
        if (m_nLightProcessed < 0) m_nLightProcessed = m_Lights.size() - 1;
        else m_nLightProcessed = m_nLightProcessed % (m_Lights.size() + 1);

        // Geometry term limit.
        gui.addVarRW(BNZ_GUI_VAR(m_fGeometryTermBound));

        // Position info.
        gui.addSeparator();
        gui.addValue("Last position ",
                     Vec3f(m_Lights[m_nLightProcessed-1].position));

        gui.addSeparator();
        gui.addVarRW(BNZ_GUI_VAR(m_bCastShadow));
        gui.addVarRW(BNZ_GUI_VAR(m_bUsingPCF));
        gui.addVarRW(BNZ_GUI_VAR(m_bShowShadowMap));

        gui.addVarRW(BNZ_GUI_VAR(m_fBias));
    }
}
}
