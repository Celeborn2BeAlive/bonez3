#pragma once

#include <bonez/opengl/GLShaderManager.hpp>
#include <bonez/opengl/utils/GLutils.hpp> // Warning if you change these two lines.
#include <bonez/opengl/utils/GLUniform.hpp> // There will be a compilation error.

namespace BnZ {

struct VPLRcpPathCountScalingPass {
    GLProgram m_Program;

    BNZ_GLUNIFORM(m_Program, GLSLSampler2Df, uAccumulatedTexture);
    BNZ_GLUNIFORM(m_Program, float, uRcpPathCount);
    BNZ_GLUNIFORM(m_Program, GLSLImage2Df, uFinalRenderImage);

    VPLRcpPathCountScalingPass(const GLShaderManager& shaderManager);

    void render(const GLTexture2D& input,
                size_t pathCount,
                GLTexture2D& output);
};
}
