#pragma once

#include "gpu_vpl_utilities.hpp"
#include "ShadowContainer.hpp"
#include "ShadowPass.hpp"
#include "VPLRcpPathCountScalingPass.hpp"
#include "GeometryAwareBlurFilteringPass.hpp"
#include "TextureFilteringPass.hpp"

#include <bonez/viewer/GUI.hpp>
#include <bonez/opengl/utils/GLutils.hpp>
#include <bonez/opengl/GLGBuffer.hpp>
#include <bonez/opengl/GLShaderManager.hpp>
#include <bonez/scene/Scene.hpp>
#include <bonez/scene/shading/BSDF.hpp>
#include <bonez/opengl/GLScreenFramebuffer.hpp>
#include <bonez/opengl/cube_mapping/GLCubeMapContainer.hpp>


namespace BnZ {

/// Same as VPLSkeletonLightPassOPtimizeShader but
/// with the interleaved sampling algorithme.
class VPLSkeletonLightPassInterleavedSampling {

public:
    VPLSkeletonLightPassInterleavedSampling(
            const GLShaderManager& shaderManager,
            const Vec2f& nearFar,
            size_t width, size_t height);

    ///
    /// \brief init initializes the pass with the skeleton used to approximate
    /// VPLs' shadow map.
    /// \param skeleton
    ///
    void init(const Scene& scene,
              const CurvilinearSkeleton& skeleton);

    ///
    /// \brief render renders the triangle in finalDrawBuffer
    /// by using gBuffer, viewMatrix and rcpProjMatrix as uniform values.
    /// \param finalDrawBuffer the Framebuffer used to retrieve colors of the pass.
    /// \param scene the scene to retrieve world vertices (TMP).
    /// \param gBuffer the gBuffer containing deferred values.
    /// \param viewMatrix the viewMatrix of the camera.
    /// \param rcpProjMatrix the reciproque projection matrix used after DefPass.
    /// \param triangle the triangle to draw.
    /// \param gui the GUI to draw the pass' prompt.
    ///
    void render(GLScreenFramebuffer& finalDrawBuffer,
                const GLScene& scene,
                const GLGBuffer& gBuffer,
                const Mat4f& viewMatrix,
                const Mat4f& rcpProjMatrix,
                const Mat4f& rcpViewMatrix,
                const Vec3f& camPosition,
                const GLScreenTriangle& triangle,
                GUI& gui);

    ///
    /// \brief getLights
    /// \return the last set of lights processed.
    ///
    inline const std::vector<BufferedLightGPU>& getLights() const {
        return m_Lights;
    }

    inline void printDaShit() const {
        auto index = glGetProgramResourceIndex(m_Program.glId(), GL_UNIFORM, "uDepthMapMaxSize");
        auto location = glGetProgramResourceLocation(m_Program.glId(), GL_UNIFORM, "uDepthMapMaxSize");
        char buffer[2048];
        GLsizei length;
        glGetActiveUniformName(m_Program.glId(), index,
                               2048, &length, buffer);
        assert(length);
        LOG(INFO) << "Index of the uniform = " << index;
        LOG(INFO) << "Name of the uniform = " << buffer;
        LOG(INFO) << "Location of the uniform = " << location << " vs " << uDepthMapMaxSize.location();
    }

    ///
    /// \brief getProcessedLighNumber
    /// \return the last number of light processed.
    ///
    inline int getProcessedLighNumber() const {
        return m_nLightProcessed;
    }


private:


    ///
    /// \brief computeNodeStats computes stats.
    /// \param fPercentBiggerThafAverage
    /// \param fVariance
    /// \param fNotZero
    /// \param fLightPerNodeAverage
    ///
    void computeNodeStats(float& fPercentBiggerThafAverage, float& fVariance,
                          float& fNotZero, float& fLightPerNodeAverage);

    ///
    /// \brief createPositionsFromLight
    /// \return A positions vector built from our VPLs positions.
    ///
    std::vector<Vec3f> createPositionsFromLight();

    ///
    /// \brief buildShadows builds the shadow maps of the current lights.
    /// \param scene rendered to generate shadows.
    /// \param positions used for the origine of each shadow map.
    ///
    void buildShadowCubeFromPositions(const GLScene& scene,
                                      const std::vector<Vec3f>& positions);

    ///
    /// \brief sendUniforms sends pass'uniforms, except the texture index and the light offset
    ///  which are handled within the accumulative loop.
    /// \param gBuffer
    /// \param viewMatrix
    /// \param rcpViewMatrix
    /// \param rcpProjMatrix
    /// \param shadowFar
    /// \param camPosition
    ///
    void sendUniforms(const GLGBuffer& gBuffer, const Mat4f& viewMatrix,
                      const Mat4f& rcpViewMatrix, const Mat4f& rcpProjMatrix,
                      float shadowFar, const Vec3f& camPosition);

    void RenderLastVPLShadowCubeMap(const GLScreenTriangle& triangle, GUI& gui);

    /// Bounds the geometry term.
    float m_fGeometryTermBound = .00005;

    /// The bias used for the shadow acne in the light shader.
    float m_fBias = 3.0f;

    /// Path constants.
    size_t m_nPathCount = 100;
    size_t m_nMaxDepth = 10;

    /// The shader program of this pass.
    GLProgram m_Program;

    /// Lights in world space.
    std::vector<BufferedLightGPU> m_Lights;

    /// Vector of all skeleton nodes' position.
    std::vector<Vec3f> m_NodesPositions;

    /// Number of light per skeleton node.
    std::vector<Vec2u> m_LightCountOffsetPerNode;

    /// Number of light per "depth map texture" where a depth map texture
    /// contains multiple ones, each corresponding to a skeleton's node.
    std::vector<unsigned int> m_nLightPerDepthMapTextureArray;

    /// Our LightBuffer bridging with OpenGL to send world lights.
    GLBufferStorage<BufferedLightGPU> m_LightBuffer;

    /// Our VPLToNodesIndexesBuffer gives the index of the corresponding SM.
    GLBufferStorage<unsigned int> m_LightToNodesIndexesBuffer;

    /// Sorted light indices in function of their nearest VPL.
    GLBufferStorage<unsigned int> m_IndirectIndicesBuffer;

    /// Number of light for each node.
    GLBufferStorage<Vec2u> m_LightCountOffsetPerNodeBuffer;

    /// Skeleton nodes positions.
    GLBufferStorage<Vec4f> m_NodesPositionsBuffer;

    /// The uniform buffer of this pass.
    GLGBufferUniform uGBuffer { m_Program };

    /// Number of light processed.
    size_t m_nLightProcessed;

    /// Size of the skeleton (the number of vertices).
    size_t m_nSkeletonSize;

    /// Casting shadow or not.
    bool m_bCastShadow = true;

    bool m_bShowShadowMap = false;

    /// Using the Percentage closest filtering on shadows or not.
    bool m_bUsingPCF = false;

    /// Using the blur after the interleaved sampling or not.
    bool m_bBlur = true;
    bool m_bTexture = true;

    /// Gaussian blur teta parameters used after the interleaved sampling.
    float m_fGaussianTetaNormal = 0.1f;
    float m_fGaussianTetaPosition = 0.05f;

    /// Using the second version of PCF when m_bUsingPCF is enabled.
    size_t m_nPCFAlgo = 0;

    /// Radius used for the PCF2 Algo.
    float m_fPCFRadius= 5.f;

    /// Percentage.
    size_t m_nPercentOnAverageToCmp = 100;

    /// Block size used in the interleaved sampling.
    size_t m_nInterleavedWindowSize = 3;
    size_t m_nBlurrWindowSize = 3;

    /// The pass normalizing all pixels and writing the frame into
    /// the "final" framebuffer.
    VPLRcpPathCountScalingPass m_VPLRcpPathCountScalingPass;

    /// The pass bluring all pixels by respecting the geometry
    /// and writing the frame into the "final" framebuffer.
    GeometryAwareBlurFilteringPass m_GeometryAwareBlurFilteringPass;
    TextureFilteringPass m_TextureFilteringPass;

    /// The inner shadow pass.
    ShadowPass m_ShadowPass;

    /// The shadow container.
    ShadowContainer m_ShadowContainer;

    /// FBO used to accumulate or frame (using multiple SCMA & multiple passes).
    GLFramebuffer2D<1, false> m_TransitionFrameBuffer;
    GLFramebuffer2D<1, false> m_Transition2FrameBuffer;
    GLFramebuffer2D<1, false> m_DepthMapDisplayFramebuffer;
    GLFramebuffer2D<1, false> m_AccumulationFramebuffer;
    /// FBO used to cross treatment during filtering passes.

    /// Uniform variables used during this pass.
    /// Matrices uniforms.
    BNZ_GLUNIFORM(m_Program, Mat4f, uRcpViewMatrix);
    BNZ_GLUNIFORM(m_Program, Mat4f, uRcpProjMatrix);
    /// Lights uniforms.
    BNZ_GLUNIFORM(m_Program, GLuint, uLightNumber);
    BNZ_GLUNIFORM(m_Program, GLuint, uLightOffset);
    BNZ_GLUNIFORM(m_Program, GLfloat, uGeometryTermBound);
    /// Camera uniforms.
    BNZ_GLUNIFORM(m_Program, GLfloat, uFarPlane);
    /// Shadow uniforms.
    BNZ_GLUNIFORM(m_Program, int, uDepthMap);
    BNZ_GLUNIFORM(m_Program, bool, ubCastShadow);
    BNZ_GLUNIFORM(m_Program, GLfloat, uBias);
    BNZ_GLUNIFORM(m_Program, GLfloat, ufPCFRadius);
    BNZ_GLUNIFORM(m_Program, bool, ubUsingPCF);
    BNZ_GLUNIFORM(m_Program, GLuint, uNPCFAlgo);
    BNZ_GLUNIFORM(m_Program, GLint, uDepthMapMaxSize);
    /// Interleaving uniforms.
    BNZ_GLUNIFORM(m_Program, GLuint, uInterleavedWindowSize);

    struct DrawPass {
        GLProgram m_Program;

        BNZ_GLUNIFORM(m_Program, float, uZNear);
        BNZ_GLUNIFORM(m_Program, float, uZFar);
        BNZ_GLUNIFORM(m_Program, GLuint, uMapIndex);
        BNZ_GLUNIFORM(m_Program, bool, uDrawDepth);
        BNZ_GLUNIFORM(m_Program, int, uCubeMapContainer);

        // GLCubeMapContainerUniform uCubeMapContainer { m_Program };

        DrawPass(const GLShaderManager& shaderManager):
            m_Program(shaderManager.buildProgram({
                                                     "skeleton_shadow_mapping/image.vs",
                                                     "skeleton_shadow_mapping/drawCubeMap.fs"
                                                 })) {}
    };

    DrawPass m_DrawDepthPass;
    size_t m_nSelectedNodeIndex = 0;

    /// Exposes the current statue into gui.
    void exposeIO(GUI& gui);

    /// Updates the position in ViewSpace of light buffered.
    void updateGPULightInViewSpace(const Mat4f& viewMatrix);

    GLQuery<GL_TIME_ELAPSED> m_IrradianceComputationTimeQuery;
    GLQuery<GL_TIME_ELAPSED> m_GeometryBlurComputationTimeQuery;
    GLQuery<GL_TIME_ELAPSED> m_TextureComputationTimeQuery;
    GLQuery<GL_TIME_ELAPSED> m_RcpPathScalingComputationTimeQuery;
};
}
