#pragma once


#include "FpsCounter.hpp"
#include "PhongLightPass.hpp"
#include "SkeletonViewerModule.hpp"
#include "VPLViewerModule.hpp"

#include "VPLLightPass.hpp"
#include "VPLSkeletonLightPass.hpp"
#include "VPLSkeletonNonOptiLightPass.hpp"
#include "VPLLightProgressivePass.hpp"
#include "VPLSkeletonLightPassNodeVisibilityFactoring.hpp"
#include "VPLSkeletonLightPassOptimizeShader.hpp"
#include "VPLSkeletonLightPassInterleavedSampling.hpp"
#include "VPLSkeletonLightPassIntensitySets.hpp"
#include "SkeletonShadowsRenderPass.hpp"

#include <bonez/opengl/GLGBuffer.hpp>
#include <bonez/opengl/GLScreenFramebuffer.hpp>
#include <bonez/opengl/GLScene.hpp>

#include <bonez/opengl/GLGBufferRenderPass.hpp>
#include <bonez/opengl/debug/GLDebugRenderer.hpp>
#include <bonez/opengl/debug/GLDebugStream.hpp>
#include <bonez/viewer/Viewer.hpp>
#include <bonez/parsing/parsing.hpp>




namespace BnZ {

class VPLViewerModule;

class SSMViewer: public Viewer {

public:
    SSMViewer(const FilePath& applicationPath, const FilePath& settingsFilePath);

    void setUp() override;

    void tearDown() override;

    float getArrowLength() const;

    float getArrowBase() const;

    inline GLDebugRenderer& getGLDebugRenderer() {
        assert(m_pGLDebugRenderer);
        return *m_pGLDebugRenderer;
    }

    inline const GLScene& getGLScene() const {
        assert(m_pGLScene);
        return *m_pGLScene;
    }

    inline const GLScreenTriangle& getScreenTriangle() const {
        return m_ScreenTriangle;
    }

    inline const Vec4u& getSelectedObjectID() const {
        return m_SelectedObjectID;
    }

    inline GLScreenFramebuffer& getScreenFramebuffer() {
        return m_GLFramebuffer;
    }

    inline const Intersection& getPickedIntersection() const {
        return m_PickedIntersection;
    }

    inline const Vec3f& getPickedIntersectionIncidentDirection() const {
        return m_PickedIntersectionIncidentDirection;
    }

private:
    void initViewports();

    void initGLData();

    void handleGlobalPreprocessParameters(const tinyxml2::XMLElement& input,
                                          tinyxml2::XMLElement& output) override;
    void drawFrame() override;

    void drawFinalRender();

    void doPicking();

    void setSelectedPixel(const Vec2u& pixel);

    void applySceneThinning();

    void exposeIO();

    // The FPS counter.
    FpsCounter m_FpsCounter;

    // The Gbuffer.
    GLGBuffer m_GBuffer;

    // The framebuffer.
    GLScreenFramebuffer m_GLFramebuffer;

    // Size of the screen.
    Vec2f m_GBufferTexScreenSize;

    // Renders the scene.
    GLGBufferRenderPass m_GBufferRenderPass;

    enum RenderPassType {
        VPL_SKELETON_SHADOWS_RENDER_PASS,
        VPL_SKELETON_LIGHT_PASS_INTENSITY_SETS,
        VPL_SKELETON_LIGHT_PASS_INTERLEAVED_SAMPLING,
        VPL_SKELETON_LIGHT_PASS_NODE_VISIBILITY_FACTORING,
        VPL_SKELETON_LIGHT_PASS_OPTIMIZE_SHADER,
        PHONG_LIGHT_PASS,
        VPL_SKELETON_LIGHT_PASS,
        VPL_SKELETON_NON_OPTI_LIGHT_PASS,
        VPL_LIGHT_PASS,
        PROGRESSIVE_VPL_LIGHT_PASS,
    };

    RenderPassType m_nRenderPassType = VPL_SKELETON_LIGHT_PASS_INTERLEAVED_SAMPLING;
//Backup !
    // Renders a phong pass.
    // PhongLightPass m_PhongLightPass;
    // Renders a VPL pass.
    //VPLLightPass m_VPLLightPass;
    //VPLLightProgressivePass m_ProgressiveVPLLightPass;
    //VPLSkeletonLightPass m_VPLSkeletonLightPass;
    //VPLSkeletonNonOptiLightPass m_VPLSkeletonNonOptiLightPass;
    // VPLSkeletonLightPassNodeVisibilityFactoring m_VPLSkeletonLightPassNodeVisibilityFactoring;
//    VPLSkeletonLightPassOptimizeShader m_VPLSkeletonLightPassOptimizeShader;
    VPLSkeletonLightPassInterleavedSampling m_VPLSkeletonLightPassInterleavedSampling;
    //VPLSkeletonLightPassIntensitySets m_VPLSkeletonLightPassIntensitySets;
    //SkeletonShadowsRenderPass m_SkeletonShadowsRenderPass;

    // Triangle used to draw during the FlashShadingPass.
    GLScreenTriangle m_ScreenTriangle;

    // Viewport of the final render.
    Vec4f m_FinalRenderViewport;

    // Used to draw some debug informations.
    Shared<GLDebugRenderer> m_pGLDebugRenderer;

    // Shared pointer on the scene.
    Shared<const GLScene> m_pGLScene;

    VPLViewerModule m_VPLViewerModule;

    GLDebugStreamData m_DisplayStream;

    SkeletonViewerModule m_SkeletonViewerModule;

    Vec4u m_SelectedObjectID = GLScreenFramebuffer::NULL_OBJECT_ID;

    // Intersections.
    Intersection m_PickedIntersection;
    Vec3f m_PickedIntersectionIncidentDirection;
    Vec2u m_SelectedPixel;


    size_t m_nThinningResolution = 128;
    bool m_bSegmentSkeleton = true;

    UpdateFlag m_OldCameraFlag;
};
}
