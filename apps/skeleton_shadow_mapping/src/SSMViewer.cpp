﻿#include "SSMViewer.hpp"

#include <bonez/voxskel/GLVoxelizerTripiana2009.hpp>
#include <bonez/voxskel/ThinningProcessDGCI2013_2.hpp>
#include <bonez/voxskel/discrete_functions.hpp>

namespace BnZ {

// Backup !
SSMViewer::SSMViewer(const FilePath& applicationPath, const FilePath& settingsFilePath):
    Viewer("SkeletonShadowMappingSceneViewer", applicationPath, settingsFilePath),
    m_GBufferRenderPass(m_ShaderManager),
    /*m_PhongLightPass(m_ShaderManager, *m_pScene),
            m_VPLLightPass(m_ShaderManager, m_ZNearFar, *m_pScene,
                           m_Settings.m_FramebufferSize.x, m_Settings.m_FramebufferSize.y),
            m_VPLSkeletonLightPass(m_ShaderManager, m_ZNearFar, m_Settings.m_FramebufferSize.x,
                                   m_Settings.m_FramebufferSize.y),
            m_VPLSkeletonNonOptiLightPass(m_ShaderManager, m_ZNearFar, m_Settings.m_FramebufferSize.x,
                                          m_Settings.m_FramebufferSize.y),
            m_ProgressiveVPLLightPass(m_ShaderManager, m_ZNearFar, *m_pScene,
                                      m_Settings.m_FramebufferSize.x, m_Settings.m_FramebufferSize.y),
    m_VPLSkeletonLightPassOptimizeShader(m_ShaderManager, m_ZNearFar, m_Settings.m_FramebufferSize.x,
                                         m_Settings.m_FramebufferSize.y),
    m_VPLSkeletonLightPassNodeVisibilityFactoring(m_ShaderManager, m_ZNearFar, m_Settings.m_FramebufferSize.x,
                                                  m_Settings.m_FramebufferSize.y),*/
    m_VPLViewerModule(m_ShaderManager),
    m_SkeletonViewerModule(m_ShaderManager),
    m_VPLSkeletonLightPassInterleavedSampling(m_ShaderManager, m_ZNearFar, m_Settings.m_FramebufferSize.x,
                                              m_Settings.m_FramebufferSize.y),
//    m_VPLSkeletonLightPassIntensitySets(m_ShaderManager, m_ZNearFar, m_Settings.m_FramebufferSize.x,
//                                        m_Settings.m_FramebufferSize.y),
//    m_SkeletonShadowsRenderPass(m_ShaderManager, m_ZNearFar, m_Settings.m_FramebufferSize.x,
//                                m_Settings.m_FramebufferSize.y),
    m_pGLDebugRenderer(makeShared<GLDebugRenderer>(m_ShaderManager)) {

    m_VPLSkeletonLightPassInterleavedSampling.printDaShit();

    m_pGLDebugRenderer->setArrowSize(getArrowBase(), getArrowLength());
    m_OldCameraFlag = m_CameraUpdateFlag;

    m_VPLSkeletonLightPassInterleavedSampling.printDaShit();
}


void SSMViewer::setUp() {
    m_VPLSkeletonLightPassInterleavedSampling.printDaShit();

    initGLData();

    m_VPLSkeletonLightPassInterleavedSampling.printDaShit();

    initViewports();

    m_VPLSkeletonLightPassInterleavedSampling.printDaShit();

    m_VPLViewerModule.setUp(*this);

    m_VPLSkeletonLightPassInterleavedSampling.printDaShit();

    m_SkeletonViewerModule.setUp(*this);

    m_VPLSkeletonLightPassInterleavedSampling.printDaShit();

    applySceneThinning();

    m_VPLSkeletonLightPassInterleavedSampling.printDaShit();

    //m_VPLSkeletonLightPass.init(*m_pScene, *(m_pScene->getCurvSkeleton()));
   // m_VPLSkeletonNonOptiLightPass.init(*m_pScene, *(m_pScene->getCurvSkeleton()));
    //m_VPLSkeletonLightPassOptimizeShader.init(*m_pScene, *(m_pScene->getCurvSkeleton()));

    m_VPLSkeletonLightPassInterleavedSampling.printDaShit();

    //m_VPLSkeletonLightPassNodeVisibilityFactoring.init(*m_pScene, *(m_pScene->getCurvSkeleton()));

    m_VPLSkeletonLightPassInterleavedSampling.printDaShit();

    m_VPLSkeletonLightPassInterleavedSampling.init(*m_pScene, *(m_pScene->getCurvSkeleton()));

    m_VPLSkeletonLightPassInterleavedSampling.printDaShit();

//    m_VPLSkeletonLightPassIntensitySets.init(*m_pScene, *(m_pScene->getCurvSkeleton()));

//    m_SkeletonShadowsRenderPass.init(*m_pGLScene, *(m_pScene->getCurvSkeleton()));

    std::cerr << "End of SSMViewer::setUp : "<< getGPUMemoryInfoTotalAvailable() << "ko"<< std::endl;
}


void SSMViewer::tearDown() { /* Nothing to do*/ }


void SSMViewer::initViewports() {
    // Setup the viewports depending on the window size and the framebuffer size
    float W = 0.75 * m_Settings.m_WindowSize.x;

    m_GBufferTexScreenSize.x = W / 5;
    m_GBufferTexScreenSize.y = m_GBufferTexScreenSize.x / m_Settings.m_fFramebufferRatio;
    m_FinalRenderViewport =
            Vec4f(0.5 * (m_Settings.m_WindowSize.x - m_Settings.m_FramebufferSize.x),
                  0.5 * (m_Settings.m_WindowSize.y - m_Settings.m_FramebufferSize.y),
                  m_Settings.m_FramebufferSize);
}

void SSMViewer::initGLData() {

    m_GBuffer.init(m_Settings.m_FramebufferSize);
    m_pGLScene = m_pScene->getGLScene();

    if(!m_pGLScene) {
        m_pGLScene = makeShared<GLScene>(m_pScene->getGeometry());
    }

    m_GLFramebuffer.init(m_Settings.m_FramebufferSize);
}


void SSMViewer::handleGlobalPreprocessParameters(const tinyxml2::XMLElement& input,
                                                 tinyxml2::XMLElement& output) {
    auto pThinning = input.FirstChildElement("Thinning");
    if(pThinning) {
        uint32_t gridResolution;
        getChildAttribute(*pThinning, "VoxelGridResolution", gridResolution);
        bool useSegmentedSkeleton = false;
        getChildAttribute(*pThinning, "UseSegmentedSkeleton", useSegmentedSkeleton);
    }
}


void SSMViewer::drawFrame() {

    m_FpsCounter.tick();

    exposeIO();

    // Rendering the scene in the GBuffer.
    m_GBufferRenderPass.render(m_Camera.getProjMatrix(), m_Camera.getViewMatrix(),
                               m_ZNearFar.y, *m_pGLScene, m_GBuffer);


    // Drawing in this final framebuffer.
    m_GLFramebuffer.bindForDrawing();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glViewport(0, 0, m_GLFramebuffer.getWidth(), m_GLFramebuffer.getHeight());

    // Drawing in the framebuffer.
    size_t nb = 0;
    std::vector<BufferedLightGPU> lights;

    switch(m_nRenderPassType) {
    default:
        break;/*
    case PHONG_LIGHT_PASS:
        m_PhongLightPass.render(m_GBuffer, m_Camera.getViewMatrix(),
                                m_Camera.getRcpProjMatrix(), m_ScreenTriangle);
        break;
    case VPL_SKELETON_LIGHT_PASS:
        m_VPLSkeletonLightPass.render(m_GLFramebuffer, *m_pGLScene, m_GBuffer, m_Camera.getViewMatrix(),
                                      m_Camera.getRcpProjMatrix(), m_Camera.getRcpViewMatrix(),
                                      m_Camera.getOrigin(), m_ScreenTriangle, m_GUI);
        lights = m_VPLSkeletonLightPass.getLights();
        nb = m_VPLSkeletonLightPass.getProcessedLighNumber();
        break;
    case VPL_SKELETON_NON_OPTI_LIGHT_PASS:
        m_VPLSkeletonNonOptiLightPass.render(m_GLFramebuffer, *m_pGLScene, m_GBuffer, m_Camera.getViewMatrix(),
                                             m_Camera.getRcpProjMatrix(), m_Camera.getRcpViewMatrix(),
                                             m_Camera.getOrigin(), m_ScreenTriangle, m_GUI);
        lights = m_VPLSkeletonNonOptiLightPass.getLights();
        nb = m_VPLSkeletonNonOptiLightPass.getProcessedLighNumber();
        break;
    case VPL_LIGHT_PASS:
        m_VPLLightPass.render(m_GLFramebuffer, *m_pGLScene, m_GBuffer, m_Camera.getViewMatrix(),
                              m_Camera.getRcpProjMatrix(), m_Camera.getRcpViewMatrix(),
                              m_Camera.getOrigin(), m_ScreenTriangle, m_GUI);
        lights = m_VPLLightPass.getLights();
        nb = m_VPLLightPass.getProcessedLighNumber();
        break;
    case PROGRESSIVE_VPL_LIGHT_PASS:
        m_ProgressiveVPLLightPass.render(m_GLFramebuffer, *m_pGLScene, m_GBuffer, m_Camera.getViewMatrix(),
                                         m_Camera.getRcpProjMatrix(), m_Camera.getRcpViewMatrix(),
                                         m_Camera.getOrigin(), m_ScreenTriangle, m_GUI,
                                         m_CameraUpdateFlag.hasChangedAndUpdate(m_OldCameraFlag));
        // The current FBO has been unbound.
        m_GLFramebuffer.bindForDrawing();
        break;
    case VPL_SKELETON_LIGHT_PASS_OPTIMIZE_SHADER:
        m_VPLSkeletonLightPassOptimizeShader.render(m_GLFramebuffer, *m_pGLScene, m_GBuffer, m_Camera.getViewMatrix(),
                                                    m_Camera.getRcpProjMatrix(), m_Camera.getRcpViewMatrix(),
                                                    m_Camera.getOrigin(), m_ScreenTriangle, m_GUI);
        lights = m_VPLSkeletonLightPassOptimizeShader.getLights();
        nb = m_VPLSkeletonLightPassOptimizeShader.getProcessedLighNumber();
        break;
    case VPL_SKELETON_LIGHT_PASS_NODE_VISIBILITY_FACTORING:
        m_VPLSkeletonLightPassNodeVisibilityFactoring.render(m_GLFramebuffer, *m_pGLScene, m_GBuffer, m_Camera.getViewMatrix(),
                                                             m_Camera.getRcpProjMatrix(), m_Camera.getRcpViewMatrix(),
                                                             m_Camera.getOrigin(), m_ScreenTriangle, m_GUI);
        lights = m_VPLSkeletonLightPassNodeVisibilityFactoring.getLights();
        nb = m_VPLSkeletonLightPassNodeVisibilityFactoring.getProcessedLighNumber();
        break;*/
    case VPL_SKELETON_LIGHT_PASS_INTERLEAVED_SAMPLING:
        m_VPLSkeletonLightPassInterleavedSampling.render(m_GLFramebuffer, *m_pGLScene, m_GBuffer, m_Camera.getViewMatrix(),
                                                         m_Camera.getRcpProjMatrix(), m_Camera.getRcpViewMatrix(),
                                                         m_Camera.getOrigin(), m_ScreenTriangle, m_GUI);
        lights = m_VPLSkeletonLightPassInterleavedSampling.getLights();
        nb = m_VPLSkeletonLightPassInterleavedSampling.getProcessedLighNumber();
        break;
//    case VPL_SKELETON_LIGHT_PASS_INTENSITY_SETS:
//        m_VPLSkeletonLightPassIntensitySets.render(m_GLFramebuffer, *m_pGLScene, m_GBuffer, m_Camera.getViewMatrix(),
//                                                         m_Camera.getRcpProjMatrix(), m_Camera.getRcpViewMatrix(),
//                                                         m_Camera.getOrigin(), m_ScreenTriangle, m_GUI);
//        lights = m_VPLSkeletonLightPassIntensitySets.getLights();
//        nb = m_VPLSkeletonLightPassIntensitySets.getProcessedLighNumber();
//        break;
//    case VPL_SKELETON_SHADOWS_RENDER_PASS:
//        m_SkeletonShadowsRenderPass.render(m_GLFramebuffer, *m_pGLScene, m_GBuffer, m_Camera.getViewMatrix(),
//                                           m_Camera.getRcpProjMatrix(), m_Camera.getRcpViewMatrix(),
//                                           m_Camera.getOrigin(), m_ScreenTriangle, m_GUI);
//        break;
    }

    m_GLFramebuffer.blitFramebuffer(m_GBuffer, GL_DEPTH_BUFFER_BIT, GL_NEAREST);

    m_DisplayStream.clearObjects();

    if (m_PickedIntersection) {
        m_DisplayStream.addArrow(m_PickedIntersection.P, m_PickedIntersection.Ns, getArrowLength(), getArrowBase(), Vec3f(1.f));
    }

    // Rendering the skeleton.
    m_SkeletonViewerModule.draw();

    m_RenderModule.drawGLData(m_PickedIntersection, m_PickedIntersectionIncidentDirection, *m_pGLDebugRenderer);

    if (VPL_SKELETON_LIGHT_PASS == m_nRenderPassType
            || VPL_SKELETON_NON_OPTI_LIGHT_PASS == m_nRenderPassType
            || VPL_LIGHT_PASS == m_nRenderPassType
            || VPL_SKELETON_LIGHT_PASS_INTERLEAVED_SAMPLING == m_nRenderPassType
            || VPL_SKELETON_LIGHT_PASS_OPTIMIZE_SHADER == m_nRenderPassType
            || VPL_SKELETON_LIGHT_PASS_NODE_VISIBILITY_FACTORING == m_nRenderPassType
            || VPL_SKELETON_LIGHT_PASS_INTENSITY_SETS == m_nRenderPassType) {

        // Rendering vpl debug stuffs in the stream.
        m_VPLViewerModule.draw(lights.begin(), lights.begin() + nb);

        // In skeleton based light pass we draw the links from VPLs to each nodes.
        if (VPL_SKELETON_LIGHT_PASS == m_nRenderPassType
                || VPL_SKELETON_NON_OPTI_LIGHT_PASS == m_nRenderPassType
                || VPL_SKELETON_LIGHT_PASS_INTERLEAVED_SAMPLING == m_nRenderPassType
                || VPL_SKELETON_LIGHT_PASS_OPTIMIZE_SHADER == m_nRenderPassType
                || VPL_SKELETON_LIGHT_PASS_NODE_VISIBILITY_FACTORING == m_nRenderPassType
                || VPL_SKELETON_LIGHT_PASS_INTENSITY_SETS == m_nRenderPassType) {
            m_VPLViewerModule.drawLinksToSkeleton(lights.begin(), lights.begin() + nb,
                                                  *(m_pScene->getCurvSkeleton()));
        }
    }
    m_pGLDebugRenderer->addStream(&m_DisplayStream);
    m_pGLDebugRenderer->render(m_Camera);

    doPicking();

    // Drawing to the screen.
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    glClear(GL_COLOR_BUFFER_BIT);

    drawFinalRender();

    m_pGLDebugRenderer->clearStreams();
}


void SSMViewer::exposeIO() {
    if(auto window = m_GUI.addWindow("SSMViewer")) {
        m_GUI.addValue("FPS ", m_FpsCounter.getFPS());
        m_GUI.addVarRW(BNZ_GUI_VAR(m_nThinningResolution));
        m_GUI.addVarRW(BNZ_GUI_VAR(m_bSegmentSkeleton));
        m_GUI.addButton("Thinning", [&]() {
            applySceneThinning();
        });

        m_GUI.addSeparator();

        static const char* renderPassTypeStrings[] = {
            /*"PHONG_LIGHT_PASS",
                    "VPL_SKELETON_LIGHT_PASS",
                    "VPL_SKELETON_NON_OPTI_LIGHT_PASS",
                    "VPL_LIGHT_PASS",
                    "PROGRESSIVE_VPL_LIGHT_PASS",*/
//            "VPL_SKELETON_SHADOWS_RENDER_PASS",
//            "VPL_SKELETON_LIGHT_PASS_INTENSITY_SETS",
           // "VPL_SKELETON_LIGHT_PASS_NODE_VISIBILITY_FACTORING",
            "VPL_SKELETON_LIGHT_PASS_INTERLEAVED_SAMPLING"
            /*"VPL_SKELETON_LIGHT_PASS_OPTIMIZE_SHADER",*/
        };
        m_GUI.addCombo(BNZ_GUI_VAR(m_nRenderPassType), std::size(renderPassTypeStrings),
                       renderPassTypeStrings);

        // Drawing other UIs.
        m_VPLViewerModule.drawGUI();
        m_SkeletonViewerModule.drawGUI();
    }
}


void SSMViewer::drawFinalRender() {
    m_GLFramebuffer.bindForReading();
    m_GLFramebuffer.setReadBuffer(0);

    glBlitFramebuffer(0, 0, m_GBuffer.getWidth(), m_GBuffer.getHeight(),
                      m_FinalRenderViewport.x, m_FinalRenderViewport.y,
                      m_FinalRenderViewport.x + m_FinalRenderViewport.z,
                      m_FinalRenderViewport.y + m_FinalRenderViewport.w,
                      GL_COLOR_BUFFER_BIT, GL_NEAREST);

    glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
}


void SSMViewer::doPicking() {
    if(!m_bGUIHasFocus && m_WindowManager.hasClicked()) {
        m_SelectedObjectID = m_GLFramebuffer.getObjectID(m_WindowManager.getCursorPosition(Vec2f(m_FinalRenderViewport)));

        if(m_SelectedObjectID == GLScreenFramebuffer::NULL_OBJECT_ID) {
            auto ndcPosition = m_WindowManager.getCursorNDC(m_FinalRenderViewport);
            if(viewportContains(ndcPosition, Vec4f(-1, -1, 2, 2))) {
                RaySample raySample;
                m_PickedIntersection =
                        tracePrimaryRay(getCamera(), *getScene(), Vec2f(0.5f), ndcToUV(ndcPosition), raySample);
                if(m_PickedIntersection) {
                    m_PickedIntersectionIncidentDirection = -raySample.value.dir;
                }
            }
        }

        auto pixel = m_WindowManager.getCursorPosition(Vec2f(m_FinalRenderViewport));
        if(viewportContains(Vec2f(pixel), Vec4f(0, 0, m_FinalRenderViewport.z, m_FinalRenderViewport.w))) {
            setSelectedPixel(pixel);
        }
    }
}


void SSMViewer::setSelectedPixel(const Vec2u& pixel) {
    m_SelectedPixel = pixel;

    if(m_RenderModule.m_bCPURendering) {
        m_RenderModule.setSelectedPixel(pixel);
    }
}


float SSMViewer::getArrowLength() const {
    return length(size(getScene()->getBBox())) * 0.005f;
}


float SSMViewer::getArrowBase() const {
    return (1.f / 16) * getArrowLength();
}


void SSMViewer::applySceneThinning() {
    GLVoxelFramebuffer voxelFramebuffer;
    Mat4f gridToWorldMatrix;
    CubicalComplex3D skeletonCubicalComplex, emptySpaceCubicalComplex;
    GLVoxelizerTripiana2009 voxelizer(m_ShaderManager);
    ThinningProcessDGCI2013_2 thinningProcess;

    Timer timer(true);
    std::clog << "Compute discrete scene dat at resolution " << m_nThinningResolution << std::endl;

    {
        Timer timer(true);
        std::clog << "Voxelizing the scene" << std::endl;
        voxelizer.initGLState(m_nThinningResolution, getScene()->getBBox(),
                              voxelFramebuffer, gridToWorldMatrix);
        m_pGLScene->render();
        voxelizer.restoreGLState();
    }

    {
        Timer timer(true);
        std::clog << "Convert empty space voxel grid to CC3D" << std::endl;
        auto voxelGrid = getVoxelGrid(voxelFramebuffer);
        voxelGrid = deleteEmptyBorder(voxelGrid, gridToWorldMatrix, gridToWorldMatrix);
        voxelGrid = inverse(voxelGrid);
        emptySpaceCubicalComplex = skeletonCubicalComplex = getCubicalComplex(voxelGrid);
    }

    auto distanceMap = [&]() {
        Timer timer(true);
        std::clog << "Compute distance map" << std::endl;
        return computeDistanceMap26(emptySpaceCubicalComplex, CubicalComplex3D::isInObject, true);
    }();

    auto openingMap = [&]() {
        Timer timer(true);
        std::clog << "Compute opening map" << std::endl;
        return parallelComputeOpeningMap26(distanceMap);
    }();

    {
        Timer timer(true);
        std::clog << "Initialize the thinning process" << std::endl;
        thinningProcess.init(skeletonCubicalComplex, &distanceMap, &openingMap);
    }

    {
        Timer timer(true);
        std::clog << "Run the thinning process" << std::endl;
        thinningProcess.directionalCollapse();
    }

    if (m_bSegmentSkeleton) {
        Timer timer(true);
        std::clog << "Convert skeleton CC3D to Curvilinear Skeleton (segmented)" << std::endl;
        auto skeleton = getSegmentedCurvilinearSkeleton(skeletonCubicalComplex, emptySpaceCubicalComplex,
                                                        distanceMap, openingMap,
                                                        gridToWorldMatrix);

        getScene()->setCurvSkeleton(skeleton);
    }
    else {
        Timer timer(true);
        std::clog << "Convert skeleton CC3D to Curvilinear Skeleton (NOT segmented)" << std::endl;
        auto skeleton = getCurvilinearSkeleton(skeletonCubicalComplex, emptySpaceCubicalComplex,
                                               distanceMap, openingMap,
                                               gridToWorldMatrix);
        getScene()->setCurvSkeleton(skeleton);
    }

    std::cerr << "Number of nodes = " << getScene()->getCurvSkeleton()->size() << std::endl;
}

}
