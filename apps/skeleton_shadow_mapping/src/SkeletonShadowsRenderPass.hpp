#pragma once

#include <bonez/viewer/GUI.hpp>
#include <bonez/opengl/utils/GLutils.hpp>
#include <bonez/opengl/GLGBuffer.hpp>
#include <bonez/opengl/GLShaderManager.hpp>
#include <bonez/scene/Scene.hpp>
#include <bonez/scene/shading/BSDF.hpp>
#include <bonez/opengl/GLScreenFramebuffer.hpp>
#include <bonez/opengl/cube_mapping/GLCubeMapContainer.hpp>

#include "ShadowPass.hpp"
#include "ShadowContainer.hpp"
#include "NodeShadowContainer.hpp"

namespace BnZ {

class SkeletonShadowsRenderPass {
public:
    SkeletonShadowsRenderPass(const GLShaderManager& shaderManager,
                              const Vec2f& zNearFar,
                              size_t framebufferWidth, size_t framebufferHeight);

    void init(const GLScene& scene,
              const CurvilinearSkeleton& skeleton);

    void drawGUI(GUI& gui);

    void render(GLScreenFramebuffer& finalDrawBuffer,
                const GLScene& scene,
                const GLGBuffer& gBuffer,
                const Mat4f& viewMatrix,
                const Mat4f& rcpProjMatrix,
                const Mat4f& rcpViewMatrix,
                const Vec3f& camPosition,
                const GLScreenTriangle& triangle,
                GUI& gui);

private:
    NodeShadowContainer m_NodeShadowContainer;

    /// The inner shadow pass.
    ShadowPass m_ShadowPass;

    /// The shadow container.
    ShadowContainer m_ShadowContainer;

    GLProgram m_Program;

    BNZ_GLUNIFORM(m_Program, GLuint, uNodeIndex);
    BNZ_GLUNIFORM(m_Program, GLSLSampler2DArrayf, uShadowTexture2DArray);
    BNZ_GLUNIFORM(m_Program, Vec3f, uIntensity);

    size_t m_nNodeCount = 0;

    std::vector<Vec3f> m_NodesPositions;
    GLBufferStorage<Vec4f> m_NodesPositionsBuffer;

    float m_fBias = 3.0f;
    Vec3f m_Intensity = Vec3f(1.f);
    size_t m_nNodeIndex = 0;
};

}
