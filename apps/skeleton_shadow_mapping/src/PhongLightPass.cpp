#include "PhongLightPass.hpp"
#include "bonez/sampling/Random.hpp"
#include "VPLBuilder.hpp"

namespace BnZ {


/// Returns a random color according to value.
/// It can be R, G or B.
static Vec3f getRandomColor(float value) {

    float intensity = 10000;

    if (value <= 0.333) {
        return Vec3f(intensity, 0, 0);
    }
    if (value <= 0.666) {
        return Vec3f(0, intensity, 0);
    }

    return Vec3f(0, 0, intensity);
}


/// Returns a random position within a box of size size,
///  centered in x and z.
static Vec3f getRandomPositionXZ(Vec3f &size, RandomGenerator &rnd) {

    auto rndVec3 = size * rnd.getFloat3();
    return Vec3f(rndVec3.x - size.x /2, rndVec3.y, rndVec3.z - size.z /2);
}


/// Generates random lights centered in x and z of sceneSize.
static std::vector<PhongLightPass::Light>
generateRandomLights(int nLightNb, Vec3f sceneSize, RandomGenerator rnd) {

    std::vector<PhongLightPass::Light> lights;
    lights.reserve(nLightNb);
    for (auto i = 0; i < nLightNb; ++i) {
        auto position = Vec4f(getRandomPositionXZ(sceneSize, rnd), 1.0);
        lights.emplace_back(Vec4f(0), position, getRandomColor(rnd.getFloat()));
    }

    return lights;
}



PhongLightPass::PhongLightPass(const GLShaderManager& shaderManager,
                                 const Scene& scene) :
    m_Program(shaderManager.buildProgram({ "skeleton_shadow_mapping/viewSpaceDeferredShadingPass.vs",
                                           "skeleton_shadow_mapping/phongDeferredShadingPass.fs"})){
    // Random generator used to sample lights.
    RandomGenerator rnd;

    // Initializing lights world positions.
    auto nLightNb = 100;
    auto size = scene.getBBox().size();

    // Generating the lights.
    m_Lights = generateRandomLights(nLightNb, size, rnd);

    // Filling the GPU light  buffer.
    m_LightBuffer.setData(m_Lights.size(), m_Lights.data(), GL_DYNAMIC_DRAW);
}


void PhongLightPass::updateGPULightPositions(const Mat4f& viewMatrix) {
    // Mapping the GPU buffer.
    Light *lights= m_LightBuffer.map(GL_WRITE_ONLY);

    // Calculating ViewSpace positions.
    for(auto &worldLight : m_Lights) {
        (*lights++).position = viewMatrix * worldLight.position;
    }

    // Unmapping the GPU buffer.
    m_LightBuffer.unmap();
}


void PhongLightPass::render(const GLGBuffer& gBuffer,
                            const Mat4f& viewMatrix,
                            const Mat4f& rcpProjMatrix,
                            const GLScreenTriangle& triangle)  {

    // Using our deffered program.
    m_Program.use();

    // Copying the three textures from gBuffer to our uniform buffer.
    uGBuffer.set(gBuffer, 0, 1, 2);

    // Sending uniforms.
    uLightNumber.set(m_Lights.size());
    uRcpProjMatrix.set(rcpProjMatrix);

    // Updating GPU lights positions.
    updateGPULightPositions(viewMatrix);

    //Bind the buffer to the correct interface block number
    m_LightBuffer.bindBase(GL_SHADER_STORAGE_BUFFER, 0);

    // Rendering the scene by rendering the triangle.
    triangle.render();
}

void PhongLightPass::exposeIO(GUI &gui) {
    // Notifying gui of variable's state. Nothing for us.
}
}
