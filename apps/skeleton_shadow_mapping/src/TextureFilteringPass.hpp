#pragma once

#include <bonez/opengl/GLShaderManager.hpp>
#include <bonez/opengl/utils/GLutils.hpp> // Warning if you change these two lines.
#include <bonez/opengl/utils/GLUniform.hpp> // There will be a compilation error.

namespace BnZ {

struct TextureFilteringPass {
    GLProgram m_Program;

    BNZ_GLUNIFORM(m_Program, GLSLSampler2Df, uAccumulatedTexture);
    BNZ_GLUNIFORM(m_Program, GLSLImage2Df, uInitialRenderImage);
    BNZ_GLUNIFORM(m_Program, GLSLImage2Df, uFinalRenderImage);
    BNZ_GLUNIFORM(m_Program, GLSLSampler2Df, uDiffuseSampler);

    TextureFilteringPass(const GLShaderManager& shaderManager);

    void render(GLTexture2D& input, GLTexture2D& output,
                const GLTexture2D& diffuseSampler);
};
}
