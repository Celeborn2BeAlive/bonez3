#include "SkeletonShadowsRenderPass.hpp"

namespace BnZ {

SkeletonShadowsRenderPass::SkeletonShadowsRenderPass(
        const GLShaderManager& shaderManager,
        const Vec2f& zNearFar,
        size_t framebufferWidth, size_t framebufferHeight):
    m_Program(shaderManager.buildProgram({ "skeleton_shadow_mapping/viewSpaceDeferredShadingPass.vs",
                                           "skeleton_shadow_mapping/skeletonShadowsRenderPass.fs" })),
    m_NodeShadowContainer(shaderManager, framebufferWidth, framebufferHeight),
    m_ShadowPass(shaderManager),
    m_ShadowContainer(zNearFar.x, zNearFar.y, 128) {
}

void SkeletonShadowsRenderPass::init(
        const GLScene& scene,
        const CurvilinearSkeleton& skeleton) {
    m_nNodeCount = skeleton.size();
    m_ShadowContainer.init(m_nNodeCount);
    m_NodeShadowContainer.init(m_nNodeCount);

    m_NodesPositions.clear();
    m_NodesPositions.reserve(m_nNodeCount);

    for (auto i : range(m_nNodeCount)) {
        m_NodesPositions.emplace_back(skeleton.getNode(i).P);
    }

    m_NodesPositionsBuffer = GLBufferStorage<Vec4f>(m_nNodeCount, nullptr, GL_MAP_WRITE_BIT);
    auto pNodePosition = m_NodesPositionsBuffer.mapRange(0, m_nNodeCount, GL_MAP_WRITE_BIT);
    for(auto position: m_NodesPositions) {
        *pNodePosition = Vec4f(position, 1.f);
        ++pNodePosition;
    }
    m_NodesPositionsBuffer.unmap();

    m_ShadowContainer.buildShadowTransforms(m_NodesPositions);
    m_ShadowPass.render(scene, m_ShadowContainer, m_nNodeCount);
}

void SkeletonShadowsRenderPass::drawGUI(GUI& gui) {
    if(auto window = gui.addWindow("SkeletonShadowsRenderPass")) {
        gui.addVarRW(BNZ_GUI_VAR(m_fBias));
        gui.addVarRW(BNZ_GUI_VAR(m_Intensity));
        gui.addVarRW(BNZ_GUI_VAR(m_nNodeIndex));
    }
}

void SkeletonShadowsRenderPass::render(
        GLScreenFramebuffer& finalDrawBuffer,
        const GLScene& scene,
        const GLGBuffer& gBuffer,
        const Mat4f& viewMatrix,
        const Mat4f& rcpProjMatrix,
        const Mat4f& rcpViewMatrix,
        const Vec3f& camPosition,
        const GLScreenTriangle& triangle,
        GUI& gui) {
    drawGUI(gui);

    m_NodeShadowContainer.renderOneLayer(triangle, gBuffer, m_NodesPositions,
                                 m_ShadowContainer, m_fBias,
                                 rcpProjMatrix, rcpViewMatrix, m_nNodeIndex);

    glDisable(GL_DEPTH_TEST);
    finalDrawBuffer.bindForDrawing();

    glClear(GL_COLOR_BUFFER_BIT);

    m_Program.use();

    //auto nodeCountToProcess = min(m_NodeShadowContainer.getLimitArraySizePerFBO(), nodeCount);
    uNodeIndex.set(m_nNodeIndex);

    uShadowTexture2DArray.set(0);
    m_NodeShadowContainer.getTextureArray(0).bind(0);

    uIntensity.set(m_Intensity);

    triangle.render();
}

}
