#include "VPLLightProgressivePass.hpp"
#include "bonez/sampling/Random.hpp"
#include "VPLBuilder.hpp"
#include "bonez/viewer/GUI.hpp"
#include "gpu_vpl_utilities.hpp"

namespace BnZ {

VPLLightProgressivePass::VPLLightProgressivePass(const GLShaderManager& shaderManager,
                                                 const Vec2f& nearFar,
                                                 const Scene& scene,
                                                 size_t width, size_t height) :
    m_Program(shaderManager.buildProgram({ "skeleton_shadow_mapping/viewSpaceDeferredShadingPass.vs",
                                           "skeleton_shadow_mapping/VPLDeferredShadingPass.fs" })),
    m_Scene(scene),
    m_VPLBuilder(scene),
    m_ShadowPass(shaderManager),
    m_ShadowContainer(nearFar.x, nearFar.y, 2048),
    m_VPLRcpPathCountScalingPass(shaderManager) {

    // Intializing the shadow pass maximum size from our light count.
    m_ShadowContainer.init(m_nMaxDepth);

    m_AccumulationFramebuffer.init(width, height, { GL_RGB32F });
}

void VPLLightProgressivePass::updateGPULightInViewSpace(const Mat4f& viewMatrix) {
    // Mapping the GPU buffer.
    BufferedLightGPU* lights = m_LightBuffer.map(GL_WRITE_ONLY);

    // Calculating ViewSpace positions.
    for(auto &worldLight : m_Lights) {
        auto& light = (*lights++);

        light = worldLight;
        light.position = viewMatrix * worldLight.position;
        light.normal = normalize(viewMatrix * Vec4f(Vec3f(worldLight.normal), 0.f));
        light.incidentDir = normalize(viewMatrix * worldLight.incidentDir);
    }

    // Unmapping the GPU buffer.
    m_LightBuffer.unmap();
}


void VPLLightProgressivePass::render(GLScreenFramebuffer& finalDrawBuffer,
                                     const GLScene& scene,
                                     const GLGBuffer& gBuffer,
                                     const Mat4f& viewMatrix,
                                     const Mat4f& rcpProjMatrix,
                                     const Mat4f& rcpViewMatrix,
                                     const Vec3f& camPosition,
                                     const GLScreenTriangle& triangle,
                                     GUI& gui,
                                     bool framebufferHasToBeCleared) {

    // Reseting the pathCount if the frameBuffer has to be cleared.
    if(framebufferHasToBeCleared) {
        m_nPathCount = 0u;
    }

    // Updating values.
    exposeIO(gui);

    // Initializing lights world positions.
    m_Lights = generateVPLLights(1, m_nMaxDepth, m_VPLBuilder);
    ++m_nPathCount;

    // Updating GPU lights positions.
    updateGPULightInViewSpace(viewMatrix);

    // Storing the current number of light seen.
    m_nLightProcessed = m_Lights.size();

    // Creating a vector of light positions.
    std::vector<Vec3f> positions;
    positions.reserve(m_nLightProcessed);
    for (int i = 0; i < m_nLightProcessed; ++i) {
        auto& light = m_Lights[i];
        positions.emplace_back(light.positionWs);
    }

    m_ShadowContainer.buildShadowTransforms(positions);
    // TODO Shadow pass, build shadow maps, using m_nLightProcessed lights.
    m_ShadowPass.render(scene, m_ShadowContainer, m_nLightProcessed);

    // Retrieves shadow maps and use them
    GLuint depthMap = m_ShadowContainer.getDepthMapId(0);
    // shadowFar used in shadow calculations.
    float shadowFar = m_ShadowContainer.getShadowFar();

    // Drawing in this final framebuffer.
    m_AccumulationFramebuffer.bindForDrawing();

    // Clearing the FBO if needed.
    if(framebufferHasToBeCleared) {
        glClear(GL_COLOR_BUFFER_BIT);
    }
    glDisable(GL_DEPTH_TEST);
    glViewport(0, 0, m_AccumulationFramebuffer.getWidth(), m_AccumulationFramebuffer.getHeight());

    // Using our deffered program.
    m_Program.use();

    // Copying the three textures from gBuffer to our uniform buffer.
    uGBuffer.set(gBuffer, 0, 1, 2);

    // Sending uniforms.
    uLightNumber.set(m_nLightProcessed);
    uRcpProjMatrix.set(rcpProjMatrix);
    uRcpViewMatrix.set(rcpViewMatrix);
    uCamPos.set(camPosition);
    uViewMatrix.set(viewMatrix);
    uGeometryTermBound.set(m_fGeometryTermBound);
    uFarPlane.set(shadowFar);
    uBias.set(m_fBias);
    uCastShadow.set(m_bCastShadow ? 1.0f : 0.0f);
    uShowShadowMap.set(m_bShowShadowMap);
    ubUsingPCF.set(m_bUsingPCF);

    //Bind the buffer to the correct interface block number
    m_LightBuffer.bindBase(GL_SHADER_STORAGE_BUFFER, 0);

    // Shadow map bindings.
    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_CUBE_MAP_ARRAY, depthMap);
    uDepthMap.set(3);

    glEnablei(GL_BLEND, 0);
    glBlendFunci(0, GL_ONE, GL_ONE);
    glBlendEquationi(0, GL_FUNC_ADD);

    // Rendering the scene by rendering the triangle.
    triangle.render();

    glDisablei(GL_BLEND, 0);

    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_CUBE_MAP_ARRAY, 0);
    uDepthMap.set(3);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

    m_VPLRcpPathCountScalingPass.render(m_AccumulationFramebuffer.getColorBuffer(0),
                                        m_nPathCount,
                                        finalDrawBuffer.getColorBuffer());
}

void VPLLightProgressivePass::exposeIO(GUI &gui) {

    // Updates the number of light processed.
    if(auto window = gui.addWindow("ProgressiveVPLLightPass")) {

        gui.addValue("Path count", m_nPathCount);
        gui.addValue("Max depth ", m_nMaxDepth - 2);
        gui.addSeparator();

        gui.addVarRW(BNZ_GUI_VAR(m_nLightProcessed));

        // Clamping the number of light processed.
        if (m_nLightProcessed < 0) m_nLightProcessed = m_Lights.size() - 1;
        else m_nLightProcessed = m_nLightProcessed % (m_Lights.size() + 1);

        // Geometry term limit.
        gui.addVarRW(BNZ_GUI_VAR(m_fGeometryTermBound));

        // Shadow info.
        gui.addSeparator();
        gui.addVarRW(BNZ_GUI_VAR(m_bCastShadow));
        gui.addVarRW(BNZ_GUI_VAR(m_bUsingPCF));
        gui.addVarRW(BNZ_GUI_VAR(m_bShowShadowMap));

        gui.addVarRW(BNZ_GUI_VAR(m_fBias));
    }
}
}
