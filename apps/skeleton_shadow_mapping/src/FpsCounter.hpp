#pragma once
#include <bonez/sys/time.hpp>

//#include "GLFW/glfw3.h"



// TODO :  No glfw because of inclusion problems, No std::chrono becaus of cast everywhere
// each frame for a simple TEST.

// Clock seems to be platform dependant : See what Laurent wants to use.


namespace BnZ {
///
/// \brief The FpsCounter class allows to calculates the number
/// of frame per seconds.
///
class FpsCounter {

public:
    FpsCounter() : m_LastTickTime(m_Timer.getMicroEllapsedTime()) {}

    ///
    /// \brief tick updates the current FpsCounter of one tick.
    ///
    void tick() {
        ++m_TickCount;
        auto now = m_Timer.getMicroEllapsedTime();
        auto delta = now - m_LastTickTime;
        m_LastTickTime = now;
        m_CurrentTime += delta.count();

        if (1000000.0 <= m_CurrentTime) {
            m_CurrentTime -= 1000000.0;
            m_LastFPS = m_TickCount;
            m_TickCount = 0;
        }
    }

    ///
    /// \brief getFPS
    /// \return the last FPS count calculated.
    ///
    inline size_t getFPS() {
        return m_LastFPS;
    }

private:
    Timer m_Timer;
    size_t m_LastFPS = 0;
    size_t m_TickCount = 0;
    std::chrono::microseconds m_LastTickTime;
    double m_CurrentTime = 0;
};

}
