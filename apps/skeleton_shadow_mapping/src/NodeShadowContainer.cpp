#include "NodeShadowContainer.hpp"

namespace BnZ {

NodeShadowContainer::NodeShadowContainer(const GLShaderManager& shaderManager,
                                         size_t nTextureWidth, size_t nTextureHeight) :
    m_Program(shaderManager.buildProgram({ "deferredShadingPass.vs",
                                           "skeleton_shadow_mapping/nodeShadowFramebufferPass.fs"})),
    m_nTextureWidth(nTextureWidth), m_nTextureHeight(nTextureHeight) {

    // Calculating the max count of texture per FBOs.
    GLint nMaxSize;
    glGetIntegerv(GL_MAX_ARRAY_TEXTURE_LAYERS, &nMaxSize);
    m_nLimitArraySizePerFBOs = nMaxSize;
}

void NodeShadowContainer::init(size_t nMaxLayerRequested) {

    m_nMaxGlobalSizeAllocated = nMaxLayerRequested;

    // Getting the number of FBOs needed to handle every layer.
    auto nArrayNeeded = getSetCount(nMaxLayerRequested, m_nLimitArraySizePerFBOs);

    // Clearing.
    m_ShadowTexture2DArrays.clear();

    // Reserving sizes.
    m_ShadowTexture2DArrays.resize(nArrayNeeded);

    for (size_t i = 0; i < nArrayNeeded; ++i) {
        auto nLayerCount = m_nLimitArraySizePerFBOs <= nMaxLayerRequested ?
                    m_nLimitArraySizePerFBOs : nMaxLayerRequested;
        nMaxLayerRequested -= nLayerCount;
        generateTextureArray(i, nLayerCount);
    }
}

void NodeShadowContainer::generateTextureArray(size_t index, size_t nLayerCount) {

    m_ShadowTexture2DArrays[index].setImage(0, GL_RGB32F, m_nTextureWidth, m_nTextureHeight, nLayerCount,
                                            0, GL_RGB, GL_FLOAT, nullptr);

    // Setting the texture array parameters.
    m_ShadowTexture2DArrays[index].setFilters(GL_NEAREST, GL_NEAREST);
    m_ShadowTexture2DArrays[index].setWrapS(GL_CLAMP_TO_EDGE);
    m_ShadowTexture2DArrays[index].setWrapR(GL_CLAMP_TO_EDGE);
    m_ShadowTexture2DArrays[index].setWrapT(GL_CLAMP_TO_EDGE);
}

void NodeShadowContainer::render(const GLScreenTriangle& triangle,
                                 const GLGBuffer& gBuffer,
                                 const std::vector<Vec3f>& positions,
                                 const ShadowContainer& shadowContainer,
                                 float fBias,
                                 size_t nCount,
                                 const Mat4f& rcpProjMatrix,
                                 const Mat4f& rcpViewMatrix) {

    std::cout << "Start of the NodeShadowContainer::render loop." << std::endl;
    m_Program.use();

    glBindFramebuffer(GL_FRAMEBUFFER, m_FBO.glId());
    glDisable(GL_DEPTH_TEST);

    glViewport(0, 0, m_nTextureWidth, m_nTextureHeight);

    uGBuffer.set(gBuffer, 0, 1, 2);
    uDepthMap.set(3);
    uBias.set(fBias);
    uRcpProjMatrix.set(rcpProjMatrix);
    uRcpViewMatrix.set(rcpViewMatrix);

    //nCount = 1;

    for (auto i : range(nCount)) {
        // Getting the array containing the layer of the current node.
        // auto& textureArray = m_ShadowTexture2DArrays[0];
        // Binding the right layer of the right array.
        glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                                  m_ShadowTexture2DArrays[getTextureArrayForTheLayer(i)].glId(), 0,
                                  getTextureArrayLocalLayerIndex(i));

        glNamedFramebufferDrawBuffer(m_FBO.glId(), GL_COLOR_ATTACHMENT0);

        // Checking the framebuffer status.
        GLenum status;
        if ((status = glCheckFramebufferStatus(GL_FRAMEBUFFER)) != GL_FRAMEBUFFER_COMPLETE) {
            std::cout << "NodeShadowContainer::generateFBOs : Framebuffer not complete !\n"
                      << GLFramebufferErrorString(status) << std::endl;
        }

        std::cout << "Clearing the texture" << std::endl;
        glClear(GL_COLOR_BUFFER_BIT);

        // Sending the position of the current node.
        uNodePosition.set(positions[i]);
        uNodeLayer.set(shadowContainer.
                       getDepthMapTextureLocalLayerIndex(i));


        // Binding the current shadow cube map array.
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_CUBE_MAP_ARRAY,
                      shadowContainer.getDepthMapTextureForTheLayer(i));

        // Drawing the whole scene.
        triangle.render();

        // Disabling the current SCMA texture.
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_CUBE_MAP_ARRAY, 0);
    }

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    std::cout << "NodeShadowContainer::render end of the render loop" << std::endl;
}

void NodeShadowContainer::renderOneLayer(const GLScreenTriangle& triangle,
                    const GLGBuffer& gBuffer,
                    const std::vector<Vec3f>& positions,
                    const ShadowContainer& shadowContainer,
                    float fBias,
                    const Mat4f& rcpProjMatrix,
                    const Mat4f& rcpViewMatrix,
                    size_t layerIndex) {
    std::cout << "Start of the NodeShadowContainer::render loop." << std::endl;
    m_Program.use();

    glBindFramebuffer(GL_FRAMEBUFFER, m_FBO.glId());
    glDisable(GL_DEPTH_TEST);

    glViewport(0, 0, m_nTextureWidth, m_nTextureHeight);

    uGBuffer.set(gBuffer, 0, 1, 2);
    uDepthMap.set(3);
    uBias.set(fBias);
    uRcpProjMatrix.set(rcpProjMatrix);
    uRcpViewMatrix.set(rcpViewMatrix);

    //nCount = 1;

    // Getting the array containing the layer of the current node.
    // auto& textureArray = m_ShadowTexture2DArrays[0];
    // Binding the right layer of the right array.
    glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                              m_ShadowTexture2DArrays[getTextureArrayForTheLayer(layerIndex)].glId(), 0,
                              getTextureArrayLocalLayerIndex(layerIndex));

    glNamedFramebufferDrawBuffer(m_FBO.glId(), GL_COLOR_ATTACHMENT0);

    // Checking the framebuffer status.
    GLenum status;
    if ((status = glCheckFramebufferStatus(GL_FRAMEBUFFER)) != GL_FRAMEBUFFER_COMPLETE) {
        std::cout << "NodeShadowContainer::generateFBOs : Framebuffer not complete !\n"
                  << GLFramebufferErrorString(status) << std::endl;
    }

    std::cout << "Clearing the texture" << std::endl;
    glClear(GL_COLOR_BUFFER_BIT);

    // Sending the position of the current node.
    uNodePosition.set(positions[layerIndex]);
    uNodeLayer.set(shadowContainer.
                   getDepthMapTextureLocalLayerIndex(layerIndex)); // From 1 to max +1.


    // Binding the current shadow cube map array.
    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_CUBE_MAP_ARRAY,
                  shadowContainer.getDepthMapTextureForTheLayer(layerIndex));

    // Drawing the whole scene.
    triangle.render();

    // Disabling the current SCMA texture.
    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_CUBE_MAP_ARRAY, 0);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    std::cout << "NodeShadowContainer::render end of the render loop" << std::endl;
}

size_t NodeShadowContainer::getSetCount(size_t nTotal, size_t nSetSize) const {
    return (nTotal / nSetSize) + (nTotal % nSetSize == 0 ? 0 : 1);
}
}
