#pragma once

#include "gpu_vpl_utilities.hpp"
#include "ShadowContainer.hpp"
#include "ShadowPass.hpp"
#include "VPLRcpPathCountScalingPass.hpp"

#include <bonez/viewer/GUI.hpp>
#include <bonez/opengl/utils/GLutils.hpp>
#include <bonez/opengl/GLGBuffer.hpp>
#include <bonez/opengl/GLShaderManager.hpp>
#include <bonez/scene/Scene.hpp>
#include <bonez/scene/shading/BSDF.hpp>
#include <bonez/opengl/GLScreenFramebuffer.hpp>
#include <bonez/opengl/cube_mapping/GLCubeMapContainer.hpp>


namespace BnZ {

/// Performs a test pass by rendering a GLGBuffer
/// in a GLScreenTriangle.
class VPLLightPass {

public:
    VPLLightPass(const GLShaderManager& shaderManager,
                 const Vec2f& nearFar,
                 const Scene& scene,
                 size_t width, size_t height);

    ///
    /// \brief render renders the triangle in finalDrawBuffer
    /// by using gBuffer, viewMatrix and rcpProjMatrix as uniform values.
    /// \param finalDrawBuffer the Framebuffer used to retrieve colors of the pass.
    /// \param scene the scene to retrieve world vertices (TMP).
    /// \param gBuffer the gBuffer containing deferred values.
    /// \param viewMatrix the viewMatrix of the camera.
    /// \param rcpProjMatrix the reciproque projection matrix used after DefPass.
    /// \param triangle the triangle to draw.
    /// \param gui the GUI to draw the pass' prompt.
    ///
    void render(GLScreenFramebuffer& finalDrawBuffer,
                const GLScene& scene,
                const GLGBuffer& gBuffer,
                const Mat4f& viewMatrix,
                const Mat4f& rcpProjMatrix,
                const Mat4f& rcpViewMatrix,
                const Vec3f& camPosition,
                const GLScreenTriangle& triangle,
                GUI& gui);

    ///
    /// \brief getLights
    /// \return the last set of lights processed.
    ///
    inline const std::vector<BufferedLightGPU>& getLights() const {
        return m_Lights;
    }

    ///
    /// \brief getProcessedLighNumber
    /// \return the last number of light processed.
    ///
    inline int getProcessedLighNumber() const {
        return m_nLightProcessed;
    }

private:

    ///
    /// \brief buildShadows builds the shadow maps of the current lights.
    /// \param scene rendered to generate shadows.
    ///
    void buildShadows(const GLScene& scene);

    ///
    /// \brief sendUniforms sends pass'uniforms, except the texture index and the light offset
    ///  which are handled within the accumulative loop.
    /// \param gBuffer
    /// \param viewMatrix
    /// \param rcpViewMatrix
    /// \param rcpProjMatrix
    /// \param shadowFar
    /// \param camPosition
    ///
    void sendUniforms(const GLGBuffer& gBuffer, const Mat4f& viewMatrix,
                      const Mat4f& rcpViewMatrix, const Mat4f& rcpProjMatrix,
                      float shadowFar, const Vec3f& camPosition);

    void RenderLastVPLShadowCubeMap(const GLScreenTriangle& triangle, GUI& gui);

    /// Bounds the geometry term.
    float m_fGeometryTermBound = .00005;

    /// The bias used for the shadow acne in the light shader.
    float m_fBias = 3.0f;

    /// Path constants.
    size_t m_nPathCount = 100;
    size_t m_nMaxDepth = 10;

    /// The shader program of this pass.
    GLProgram m_Program;

    /// Lights in world space.
    std::vector<BufferedLightGPU> m_Lights;

    /// Our LightBuffer bridging with OpenGL to send world lights.
    GLBuffer<BufferedLightGPU> m_LightBuffer;

    /// Our LightShadowMapIndexBuffer giving the index of the corresponding
    /// shadow map.
    GLBuffer<unsigned int> m_LightShadowMapIndexBuffer;

    /// The uniform buffer of this pass.
    GLGBufferUniform uGBuffer { m_Program };

    /// Number of light processed.
    int m_nLightProcessed;

    /// Casting shadow or not.
    bool m_bCastShadow = true;

    /// Showing the shadow map or the lighed scene.
    bool m_bShowShadowMap = false;

    /// Using the Percentage closest filtering on shadows or not.
    bool m_bUsingPCF = false;

    /// The pass normalizing all pixels and writing the frame into
    /// the "final" framebuffer.
    VPLRcpPathCountScalingPass m_VPLRcpPathCountScalingPass;

    /// The inner shadow pass.
    ShadowPass m_ShadowPass;

    /// The shadow container.
    ShadowContainer m_ShadowContainer;

    /// FBO used to accumulate or frame (using multiple SCMA & multiple passes).
    GLFramebuffer2D<1, false> m_AccumulationFramebuffer;
    GLFramebuffer2D<1, false> m_DepthMapDisplayFramebuffer;

    /// Uniform variables used during this pass.
    /// Matrices uniforms.
    BNZ_GLUNIFORM(m_Program, Mat4f, uRcpProjMatrix);
    BNZ_GLUNIFORM(m_Program, Mat4f, uRcpViewMatrix);
    /// Lights uniforms.
    BNZ_GLUNIFORM(m_Program, GLuint, uLightNumber);
    BNZ_GLUNIFORM(m_Program, GLuint, uLightOffset);
    BNZ_GLUNIFORM(m_Program, GLfloat, uGeometryTermBound);
    /// Camera uniforms.
    BNZ_GLUNIFORM(m_Program, GLfloat, uFarPlane);
    /// Shadow uniforms.
    BNZ_GLUNIFORM(m_Program, int, uDepthMap);
    BNZ_GLUNIFORM(m_Program, int, uCastShadow);
    BNZ_GLUNIFORM(m_Program, int, uShowShadowMap);
    BNZ_GLUNIFORM(m_Program, GLfloat, uBias);
    BNZ_GLUNIFORM(m_Program, bool, ubUsingPCF);


    struct DrawPass {
        GLProgram m_Program;

        BNZ_GLUNIFORM(m_Program, float, uZNear);
        BNZ_GLUNIFORM(m_Program, float, uZFar);
        BNZ_GLUNIFORM(m_Program, GLuint, uMapIndex);
        BNZ_GLUNIFORM(m_Program, bool, uDrawDepth);
        BNZ_GLUNIFORM(m_Program, int, uCubeMapContainer);

        // GLCubeMapContainerUniform uCubeMapContainer { m_Program };

        DrawPass(const GLShaderManager& shaderManager):
            m_Program(shaderManager.buildProgram({
                "skeleton_shadow_mapping/image.vs",
                "skeleton_shadow_mapping/drawCubeMap.fs"
            })) {}
    };

    DrawPass m_DrawDepthPass;

    /// Exposes the current statue into gui.
    void exposeIO(GUI& gui);

    /// Updates the position in ViewSpace of light buffered.
    void updateGPULightInViewSpace(const Mat4f& viewMatrix);
};
}
