#include "ShadowPass.hpp"


namespace BnZ {

/// TODO get the light dynamic nor multiple lights.
ShadowPass::ShadowPass(const GLShaderManager& shaderManager) :
    m_Program(shaderManager.
              buildProgram({ "skeleton_shadow_mapping/shadowPass.vs",
                             "skeleton_shadow_mapping/shadowPass.gs",
                             "skeleton_shadow_mapping/shadowPass.fs" })){}

ShadowPass::~ShadowPass() {
}

void ShadowPass::render(const GLScene& scene, const ShadowContainer& shadowContainer, size_t nShadowMapCount) {

    m_Program.use();

    auto nIteration = shadowContainer.getNumberOfDepthMapTextureToPool(nShadowMapCount);
    std::cout << "Iteration : " << nIteration << std::endl;
    for (auto i : range(nIteration)) {

        glBindFramebuffer(GL_FRAMEBUFFER, shadowContainer.getFBO(i));
        glViewport(0, 0, shadowContainer.getShadowMapWidth(), shadowContainer.getShadowMapWidth());
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        const auto& lightMatricesBuffer = shadowContainer.getLightMatricesBuffer(i);
        lightMatricesBuffer.bindBase(GL_SHADER_STORAGE_BUFFER, 0);
        const auto& lightPositionBuffer = shadowContainer.getlightPositionsBuffer(i);
        lightPositionBuffer.bindBase(GL_SHADER_STORAGE_BUFFER, 1);

        // Setting uniforms.
        int processNumber = lightPositionBuffer.size();

        glEnable(GL_CULL_FACE);
        glCullFace(GL_FRONT);

        // Filling every shadow map.
        scene.render(processNumber);

        glDisable(GL_CULL_FACE);

        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }
}
}
