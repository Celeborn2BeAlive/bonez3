#pragma once

#include <bonez/scene/lights/VPLs.hpp>
#include <bonez/scene/shading/BSDF.hpp>
#include "VPLBuilder.hpp"
#include <bonez/scene/lights/VPLs.hpp>

namespace BnZ {

///
/// \brief The BufferedLightGPU struct
/// Structure representing lights passed to the shader using a SSBO.
/// Everything is in the world space.
/// We have to pass a Vec4f intensity to align the struture;
struct BufferedLightGPU {
    /// The normal of the light;
    Vec4f normal;
    /// The position of the position;
    Vec4f positionWs;
    /// The position of the position;
    Vec4f position;
    /// The intensity of the light;
    Vec4f intensity;
    /// The incident direction;
    Vec4f incidentDir;
    /// The diffuse coefficient;
    Vec4f kD;
    /// The specular coefficient;
    Vec4f kS;
    /// The shininess of the light;
    Vec4f shininess;

    BufferedLightGPU(const Vec3f &normal, const Vec3f &position, const Vec3f &intensity,
                     const BSDF &bsdf) :
        // We put normal.w = 1 to indicate that it exists.
        normal(normalize(normal), 1), positionWs(position, 1.0),
        position(position, 1.0), intensity(intensity, 0),
        incidentDir(normalize(bsdf.getIncidentDirection()), 0),
        kD(bsdf.getDiffuseCoefficient(), 0), kS(bsdf.getGlossyCoefficient(), 0),
        shininess(Vec4f(bsdf.getShininess(), 0, 0, 0)){}

    /// Usualy used to create from a surface or a point light.
    BufferedLightGPU(const Vec4f &normal, const Vec4f &position, const Vec3f &intensity,
                     const Vec3f &incidentDir) :
        normal(normal), intensity(intensity, 0), positionWs(position),
        position(position), incidentDir(normalize(incidentDir), 0),
        kD(1), kS(0), shininess(1) {}
};

///
/// \brief convertToLightGPU converts an instance of SVPL to a BufferedLightGPU.
/// \param svpl SVPL to convert.
/// \return the BufferedLightGPU created.
///
inline BufferedLightGPU convertToLightGPU(const SVPL& svpl) {
    // Offset the position by the normal to prevent artefacts in the shadow map with the surface.
    return BufferedLightGPU(svpl.normal, svpl.intersection.P + normalize(svpl.normal) *15.5f,
                            svpl.power, svpl.bsdf);
}

///
/// \brief convertToLightGPU
/// \param areaLightVPL
/// \return
///
inline BufferedLightGPU convertToLightGPU(const AreaLightVPL& areaLightVPL) {
    return BufferedLightGPU(Vec4f(normalize(areaLightVPL.m_Normal), 1), Vec4f(areaLightVPL.m_Position, 1.0f),
                            areaLightVPL.m_Intensity, Vec3f(0));
}

///
/// \brief convertToLightGPU
/// \param areaLightVPL
/// \return
///
inline BufferedLightGPU convertToLightGPU(const DirectionalLightVPL& directionalVPL) {
    return BufferedLightGPU(Vec4f(0), Vec4f(directionalVPL.m_IncidentDirection, 0), directionalVPL.m_Intensity,
                            directionalVPL.m_IncidentDirection);
}

///
/// \brief convertToLightGPU
/// \param pointLightVPL
/// \return
///
inline BufferedLightGPU convertToLightGPU(const PointLightVPL& pointLightVPL) {
    return BufferedLightGPU(Vec4f(0), Vec4f(pointLightVPL.m_Position, 1.0f),
                            pointLightVPL.m_Intensity, Vec3f(0));
}

static std::vector<BufferedLightGPU> generateVPLs(size_t nbPath, size_t maxDepth,
                                                  const VPLBuilder& builder) {
    std::vector<SVPL> svpls;
    EmissionVPLContainer emissionVPLContainer;
    builder.generateVPL(nbPath, maxDepth, emissionVPLContainer, svpls);
    std::vector<BufferedLightGPU> lights;
    lights.reserve(emissionVPLContainer.size() + svpls.size());

    auto convertAll = [&](const auto& array) {
        for(const auto& light: array) {
            lights.emplace_back(convertToLightGPU(light));
        }
    };

    // There is maxDepth - 2 svpl and nbPath evpls.
    /* convertAll(emissionVPLContainer.getAreaLightVPLArray());
    convertAll(emissionVPLContainer.getDirectionalLightVPLArray());
    convertAll(emissionVPLContainer.getPointLightVPLArray());*/
    convertAll(svpls);

    return lights;
}

///
/// \brief generateVPLLights generates a vector of BufferedLightGPU from some VPL.
/// \param nbPath number of VPL paths.
/// \param maxDepth maximum depth of these VPLs paths.
/// \param scene the scene where lights come from.
/// \return the BufferedLightGPU vector.
///
inline std::vector<BufferedLightGPU>
generateVPLLights(size_t nbPath, size_t maxDepth, const VPLBuilder& builder) {
    return generateVPLs(nbPath, maxDepth, builder);
}

inline std::vector<std::vector<BufferedLightGPU>>
generateLayeredVPLLights(size_t nbPath, size_t maxDepth, const VPLBuilder& builder) {

    std::vector<std::vector<SVPL>> svpls;
    EmissionVPLContainer emissionVPLContainer;
    builder.generateVPLWithDepthLayer(nbPath, maxDepth, emissionVPLContainer, svpls);
    std::vector<std::vector<BufferedLightGPU>> lights;
    lights.resize(svpls.size() +1); // +1 for emission vpl.

    auto convertAll = [&](size_t layer, const auto& array) {
        for(const auto& light: array) {
            lights[layer].emplace_back(convertToLightGPU(light));
        }
    };

    // There is maxDepth - 2 svpl and nbPath evpls.
    /* convertAll(0, emissionVPLContainer.getAreaLightVPLArray());
    convertAll(0, emissionVPLContainer.getDirectionalLightVPLArray());
    convertAll(0, emissionVPLContainer.getPointLightVPLArray());*/

    for (auto i : range(svpls.size())) {
        convertAll(i + 1, svpls[i]); // layer = 0 has to be Emission VPLS.
    }

    return lights;
}
}
