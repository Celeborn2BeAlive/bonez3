#include <iostream>

// TODO make my own.
#include "SSMViewer.hpp"
#include "bonez/sys/easyloggingpp.hpp"
#include "bonez/parsing/tinyxml/tinyxml2.h"

#include "bonez/sampling/shapes.hpp"
#include "bonez/sampling/patterns.hpp"

INITIALIZE_EASYLOGGINGPP

void usage(char** argv)
{
    std::cerr << "Usage: " << argv[0] << " < XML configuration filepath > [flat]" << std::endl;
}

using namespace BnZ;

inline std::vector<Vec2f> makeUVGridTest(size_t width, size_t height) {
    std::vector<Vec2f> uvGrid;
    uvGrid.reserve(width * height);

    Vec2f delta(1.f / width, 1.f / height);

    for(auto j = size_t(0); j < height; ++j) {
        for(auto i = size_t(0); i < width; ++i) {
            uvGrid.emplace_back(Vec2f(i + 0.5f, j + 0.5f) * delta);
        }
    }

    return uvGrid;
}

void generateDiskSamples() {
    auto w = 3, h = 3;
    //auto grid = makeUVGridTest(w, h);
    RandomGenerator rng;
    JitteredDistribution2D distrib(w, h);
    std::cout << "vec2 gDiskSampleOffset[" << (w * h) << "] = vec2[] (" << std::endl;
    for(auto y: range(h)) {
        for(auto x: range(w)) {
            auto diskSample = uniformSampleDisk(distrib.getSample(x, y, rng.getFloat2()), 1.f);
            std::cout << "vec2( " << diskSample.x << ", " << diskSample.y << ")," << std::endl;
        }
    }
    std::cout << ");";
}

int main(int argc, char** argv) {

    // Checking the configuration filepath
    if(argc < 2) {
        usage(argv);
        return -1;
    }

    generateDiskSamples();
    return 0;

    std::cout << "Max : " << GL_MAX_FRAMEBUFFER_LAYERS<< std::endl;
    //
    BnZ::initEasyLoggingpp(argc, argv);

    // Handles the XLMDocument
    tinyxml2::XMLDocument inputDocument;

    // Loading the XLM document
    if(tinyxml2::XML_NO_ERROR != inputDocument.LoadFile(argv[1])) {
        std::cerr << "Unable to open input file " << argv[1] << std::endl;
        return -1;
    }

    // Checking if the root can be retrieved
    auto pRootElement = inputDocument.RootElement();
    if(!pRootElement) {
        std::cerr << "No root element in input file " << argv[1] << std::endl;
        return -1;
    }

    // Retrieving the root name and launching the good viewer
    auto rootName = std::string(pRootElement->Name());
    if (rootName != "Viewer") {
        std::cerr << "Name of the root is not Viewer " << rootName << std::endl;
        return -1;
    }

    // Root's name is Viewer, creating it
    BnZ::SSMViewer viewer(argv[0], argv[1]);
    viewer.run();

    return 0;
}







