#include "VPLBuilder.hpp"
#include "bonez/sampling/Random.hpp"

namespace BnZ {


VPLBuilder::VPLBuilder(const Scene& scene): m_Scene(scene) {
    m_Sampler.initFrame(scene);
}

void VPLBuilder::generateVPL(std::size_t nLightPathCount, std::size_t maxDepth,
                             EmissionVPLContainer& emissionVPLBuffer,
                             std::vector<SVPL>& surfaceVPLBuffers) const {
    auto count = 0, zeror = 0, outgoing = 0;


    //m_Sampler.initFrame(m_Scene);
    // The Light depth (substracting the subCameraPath depth (shadowRay + cameraRay);
    auto lightPathDepth = maxDepth - 2;

    // Reserving size to store our light VPL.
    // They will be put with other sVPL at the end.
    emissionVPLBuffer.clear();

    // Reserving a size for the number of path nLightPathCount of depth lightPathDepth;
    surfaceVPLBuffers.clear();
    surfaceVPLBuffers.reserve(lightPathDepth * nLightPathCount);

    std::cout << "VPLBuiledr : Surface reserve : " << lightPathDepth * nLightPathCount << std::endl;
    // Generatingg paths from lights.
    for (auto i: range(nLightPathCount)) {
        // Not dividing by nLightPathCount. It is done in the light pass.
        // Because for progressive rendering there is no division at this step of
        // the algorithm (only at the end in a compute shader).
        auto power = Vec3f(1.0f);
        // Creating the light VPL for this path.
        // Selecting a light to be the root of the path.
        float lightPdf;
        auto pLight = m_Sampler.sample(m_Scene, rnd.getFloat(), lightPdf);
        // If everything went ok, creating it.
        if (pLight && lightPdf) {
            // Light attributs.-
            float emissionVertexPdf;
            RaySample exitantRaySample;
            auto emissionVertexSample = rnd.getFloat2();

            pLight->sampleVPL(m_Scene, lightPdf, emissionVertexSample, emissionVPLBuffer);

            // Light emission.
            auto Le = pLight->sampleExitantRay(m_Scene, emissionVertexSample,
                                               rnd.getFloat2(), exitantRaySample,
                                               emissionVertexPdf);

            // Creating the sub path of SurfaceVPL.
            if (lightPathDepth > 0u && Le != zero<Vec3f>() && exitantRaySample.pdf) {
                // Getting the intersection.
                auto intersection = m_Scene.intersect(exitantRaySample.value);
                // If it intersects.
                if (intersection) {
                    // Updating the power.
                    power *= Le / (lightPdf * exitantRaySample.pdf);
                    // Creating the first SurfaceVPL.
                    surfaceVPLBuffers.emplace_back(intersection, -exitantRaySample.value.dir,
                                                   m_Scene, power);

                    // Creating the rest of the Path.
                    for (auto depth = 1u; depth < lightPathDepth; ++depth) {
                        Sample3f outgoingDir;
                        float cosThetaOutDir;
                        // Getting the fs to calculate the new power & filling values.
                        auto fs = surfaceVPLBuffers.back().bsdf.sample(rnd.getFloat3(),
                                                                       outgoingDir, cosThetaOutDir,
                                                                       nullptr, true);

                        if (fs == zero<Vec3f>() || !outgoingDir.pdf) {
                            ++zeror;
                            // No more light for this path.
                            break;
                        }
                        // If everything went good, and there is light, generating the next VPL.
                        Ray ray(surfaceVPLBuffers.back().intersection, outgoingDir.value);
                        auto intersection = m_Scene.intersect(ray);
                        // If there is an intersection, transmitting
                        // the power & creating a new SVPL.
                        if (intersection) {
                            power *= fs * abs(cosThetaOutDir) / outgoingDir.pdf;
                            surfaceVPLBuffers.emplace_back(intersection, -ray.dir, m_Scene, power);
                        }
                    }
                }
            }
        }
    }
    std::cerr << "VPLBuilder : Count : " << surfaceVPLBuffers.size() << " Zero : " << zeror << std::endl;
}

void VPLBuilder::generateVPLWithDepthLayer(std::size_t nLightPathCount, std::size_t maxDepth,
                                           EmissionVPLContainer& emissionVPLBuffer,
                                           std::vector<std::vector<SVPL>>& surfaceVPLBuffers) const{
    Timer timer;
    auto count = 0, zeror = 0, outgoing = 0;

    //m_Sampler.initFrame(m_Scene);
    // The Light depth (substracting the subCameraPath depth (shadowRay + cameraRay);
    auto lightPathDepth = maxDepth - 2;

    // Reserving size to store our light VPL.
    // They will be put with other sVPL at the end.
    emissionVPLBuffer.clear();

    // Reserving a size for the number of path nLightPathCount of depth lightPathDepth;
    surfaceVPLBuffers.clear();
    surfaceVPLBuffers.resize(lightPathDepth);
    for (auto& buff : surfaceVPLBuffers) {
        buff.reserve(lightPathDepth);
    }

    std::cout << "VPLBuiledr layer : Surface reserve : " << lightPathDepth * nLightPathCount << std::endl;
    // Generatingg paths from lights.
    for (auto i: range(nLightPathCount)) {
        // Not dividing by nLightPathCount. It is done in the light pass.
        // Because for progressive rendering there is no division at this step of
        // the algorithm (only at the end in a compute shader).
        auto power = Vec3f(1.0f);
        // Creating the light VPL for this path.
        // Selecting a light to be the root of the path.
        float lightPdf;
        auto pLight = m_Sampler.sample(m_Scene, rnd.getFloat(), lightPdf);

        // If everything went ok, creating it.
        if (pLight && lightPdf) {
            // Light attributs.-
            float emissionVertexPdf;
            RaySample exitantRaySample;
            auto emissionVertexSample = rnd.getFloat2();

            pLight->sampleVPL(m_Scene, lightPdf, emissionVertexSample, emissionVPLBuffer);

            // Light emission.
            auto Le = pLight->sampleExitantRay(m_Scene, emissionVertexSample,
                                               rnd.getFloat2(), exitantRaySample,
                                               emissionVertexPdf);

            // Creating the sub path of SurfaceVPL.
            if (lightPathDepth > 0u && Le != zero<Vec3f>() && exitantRaySample.pdf) {
                // Getting the intersection.
                auto intersection = m_Scene.intersect(exitantRaySample.value);
                // If it intersects.
                if (intersection) {
                    // Updating the power.
                    power *= Le / (lightPdf * exitantRaySample.pdf);
                    // Creating the first SurfaceVPL.
                    surfaceVPLBuffers[0].emplace_back(intersection, -exitantRaySample.value.dir,
                                                      m_Scene, power);

                    // Creating the rest of the Path.
                    for (auto depth = 1u; depth < lightPathDepth; ++depth) {
                        Sample3f outgoingDir;
                        float cosThetaOutDir;
                        // Getting the fs to calculate the new power & filling values.
                        auto fs = surfaceVPLBuffers[depth-1].back().bsdf.sample(rnd.getFloat3(),
                                                                                outgoingDir, cosThetaOutDir,
                                                                                nullptr, true);

                        if (fs == zero<Vec3f>() || !outgoingDir.pdf) {
                            ++zeror;
                            // No more light for this path.
                            break;
                        }

                        // If everything went good, and there is light, generating the next VPL.
                        Ray ray(surfaceVPLBuffers[depth-1].back().intersection, outgoingDir.value);
                        auto intersection = m_Scene.intersect(ray);
                        // If there is an intersection, transmitting
                        // the power & creating a new SVPL.
                        if (intersection) {
                            power *= fs * abs(cosThetaOutDir) / outgoingDir.pdf;
                            surfaceVPLBuffers[depth].emplace_back(intersection, -ray.dir, m_Scene, power);
                        }
                    }
                }
            }
        }
    }
    auto nCount = 0;
    for (auto& buff : surfaceVPLBuffers) {
        nCount += buff.size();
    }
    auto time = (timer.getMicroEllapsedTime().count() / 1000.f);
    std::cerr << "VPLBuilder LAYER TIME :"  << time << "ms Count : " << nCount << " Zero : " << zeror << std::endl;
}

}
