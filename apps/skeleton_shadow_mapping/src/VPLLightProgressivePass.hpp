#pragma once

#include "gpu_vpl_utilities.hpp"
#include "ShadowContainer.hpp"
#include "ShadowPass.hpp"
#include "VPLBuilder.hpp"
#include "VPLRcpPathCountScalingPass.hpp"

#include "bonez/viewer/GUI.hpp"
#include "bonez/opengl/utils/GLutils.hpp"
#include "bonez/opengl/GLGBuffer.hpp"
#include "bonez/opengl/GLShaderManager.hpp"
#include "bonez/scene/Scene.hpp"
#include "bonez/scene/shading/BSDF.hpp"
#include "bonez/opengl/GLScreenFramebuffer.hpp"


namespace BnZ {

/// Performs a test pass by rendering a GLGBuffer
/// in a GLScreenTriangle.
class VPLLightProgressivePass {

public:
    VPLLightProgressivePass(const GLShaderManager& shaderManager,
                 const Vec2f& nearFar,
                 const Scene& scene,
                 size_t framebufferWidth, size_t framebufferHeight);

    ///
    /// \brief render renders the triangle in finalDrawBuffer
    /// by using gBuffer, viewMatrix and rcpProjMatrix as uniform values.
    /// \param finalDrawBuffer the Framebuffer used to retrieve colors.
    /// \param scene the scene to retrieve world vertices (TMP).
    /// \param gBuffer the gBuffer containing deferred values.
    /// \param viewMatrix the viewMatrix of the camera.
    /// \param rcpProjMatrix the reciproque projection matrix used after DefPass.
    /// \param triangle the triangle to draw.
    /// \param gui the GUI to draw the pass' prompt.
    ///
    void render(GLScreenFramebuffer& finalDrawBuffer,
                const GLScene& scene,
                const GLGBuffer& gBuffer,
                const Mat4f& viewMatrix,
                const Mat4f& rcpProjMatrix,
                const Mat4f& rcpViewMatrix,
                const Vec3f& camPosition,
                const GLScreenTriangle& triangle,
                GUI& gui,
                bool framebufferHasToBeCleared);

private:
    /// Path constants.
    unsigned int m_nMaxDepth = 4;

    /// Bounds the geometry term.
    float m_fGeometryTermBound = .00005;

    /// The bias used for the shadow acne in the light shader.
    float m_fBias = 3.0f;

    /// The shader program of this pass.
    GLProgram m_Program;

    /// Lights in world space.
    std::vector<BufferedLightGPU> m_Lights;

    /// Our LightBuffer bridging with OpenGL to send world lights.
    GLBufferStorage<BufferedLightGPU> m_LightBuffer { m_nMaxDepth, nullptr,
                GL_MAP_WRITE_BIT };

    /// The uniform buffer of this pass.
    GLGBufferUniform uGBuffer { m_Program };

    /// Number of light processed.
    int m_nLightProcessed;
    size_t m_nPathCount = 0u;

    /// Casting shadow or not.
    bool m_bCastShadow = true;

    /// Showing the shadow map or the lighed scene.
    bool m_bShowShadowMap = false;

    /// Using the Percentage closest filtering on shadows or not.
    bool m_bUsingPCF = false;

    const Scene& m_Scene;
    VPLBuilder m_VPLBuilder;

    /// The inner shadow pass.
    ShadowPass m_ShadowPass;

    /// The shadow container.
    ShadowContainer m_ShadowContainer;

    GLFramebuffer2D<1, false> m_AccumulationFramebuffer;

    /// Uniform variables used during this pass.
    BNZ_GLUNIFORM(m_Program, GLuint, uLightNumber);
    BNZ_GLUNIFORM(m_Program, Mat4f, uRcpProjMatrix);
    BNZ_GLUNIFORM(m_Program, Mat4f, uRcpViewMatrix);
    BNZ_GLUNIFORM(m_Program, Mat4f, uViewMatrix);
    BNZ_GLUNIFORM(m_Program, Vec3f, uCamPos);
    BNZ_GLUNIFORM(m_Program, GLfloat, uGeometryTermBound);
    BNZ_GLUNIFORM(m_Program, GLfloat, uFarPlane);
    BNZ_GLUNIFORM(m_Program, GLfloat, uBias);
    BNZ_GLUNIFORM(m_Program, int, uCastShadow);
    BNZ_GLUNIFORM(m_Program, int, uShowShadowMap);
    BNZ_GLUNIFORM(m_Program, bool, ubUsingPCF);
    BNZ_GLUNIFORM(m_Program, int, uDepthMap);

    VPLRcpPathCountScalingPass m_VPLRcpPathCountScalingPass;

    /// Exposes the current statue into gui.
    void exposeIO(GUI& gui);

    /// Updates the position in ViewSpace of light buffered.
    void updateGPULightInViewSpace(const Mat4f& viewMatrix);
};
}
