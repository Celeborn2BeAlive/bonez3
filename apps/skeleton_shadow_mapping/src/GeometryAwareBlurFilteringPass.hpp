#pragma once

#include <bonez/opengl/GLShaderManager.hpp>
#include <bonez/opengl/utils/GLutils.hpp> // Warning if you change these two lines.
#include <bonez/opengl/utils/GLUniform.hpp> // There will be a compilation error.

namespace BnZ {

struct GeometryAwareBlurFilteringPass {
    GLProgram m_Program;

    BNZ_GLUNIFORM(m_Program, GLSLSampler2Df, uAccumulatedTexture);
    BNZ_GLUNIFORM(m_Program, GLSLImage2Df, uFinalRenderImage);
    BNZ_GLUNIFORM(m_Program, GLSLSampler2Df, uNormalDepthSampler);
    BNZ_GLUNIFORM(m_Program, GLfloat, uTetaN);
    BNZ_GLUNIFORM(m_Program, GLfloat, uTetaP);
    BNZ_GLUNIFORM(m_Program, GLuint, uWindowSize);

    GeometryAwareBlurFilteringPass(const GLShaderManager& shaderManager);

    void render(const GLTexture2D& input,
                float gaussianTetaN,
                float gaussianTetaP,
                size_t windowSize,
                const GLTexture2D& normalDepthSampler,
                GLTexture2D& output);
};
}
