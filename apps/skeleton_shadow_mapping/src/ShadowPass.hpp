﻿#pragma once


#include "bonez/opengl/GLShaderManager.hpp"
#include "bonez/opengl/utils/GLTexture.hpp"
#include "bonez/scene/Scene.hpp"
#include "ShadowContainer.hpp"

namespace BnZ {

///
/// \brief The ShadowPass class perfoms the shadow pass
/// using cube shadow mapping technique on a set of lights.
///
class ShadowPass {

public:

    ///
    /// \brief ShadowPass
    /// \param shaderManager
    ///
    ShadowPass(const GLShaderManager& shaderManager);

    ~ShadowPass();

    ///
    /// \brief render fills shadow maps in the scene with lights registered.
    /// \param scene
    /// \param shadowContainer
    /// \param nShadowMapCount the number of shadow map to process.
    ///
    void render(const GLScene& scene, const ShadowContainer& shadowContainer,
                size_t nShadowMapCount);

private:
    /// The shader program of this pass.
    GLProgram m_Program;
};
}
