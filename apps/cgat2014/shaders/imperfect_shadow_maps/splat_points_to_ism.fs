#version 430 core

in vec3 vProjectedPosition;
in float vDepth;
flat in uvec2 vViewportIndex;

struct ISMData {
    uint smResolution;
    uint smCountPerTexture;
    uint smCountPerDimension;
};

uniform ISMData uISMData;

out float fDepth;

void main() {
    if(vProjectedPosition.z >= 0.f) {
        uvec2 pixel = uvec2(gl_FragCoord.xy);
        uvec2 viewportOrigin = vViewportIndex.xy * uISMData.smResolution;

        // Do not write outside the viewport
        if(pixel.x < viewportOrigin.x || pixel.x >= viewportOrigin.x + uISMData.smResolution
                || pixel.y < viewportOrigin.y || pixel.y >= viewportOrigin.y + uISMData.smResolution) {
            discard;
        } else {
            fDepth = vDepth;
        }
    } else {
        discard;
    }
}
