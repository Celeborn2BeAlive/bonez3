#version 430

uniform sampler2D uTexture;

uniform float uRes;

struct ISMData {
    uint smResolution;
    uint smCountPerTexture;
    uint smCountPerDimension;
};

uniform ISMData uISMData;
uniform uint uTexIndex;

vec3 randomColor(uint value) {
    return fract(
                sin(
                    vec3(float(value) * 12.9898,
                         float(value) * 78.233,
                         float(value) * 56.128)
                    )
                * 43758.5453
                );
}

in vec2 vTexCoords;

out vec3 fColor;

void main() {
    uint x = uint(vTexCoords.x * uISMData.smCountPerDimension);
    uint y = uint(vTexCoords.y * uISMData.smCountPerDimension);
    uint viewIdx = uTexIndex * uISMData.smCountPerTexture + y * uISMData.smCountPerDimension + x;

    vec3 color = vec3(1);

    float depth = texture(uTexture, vTexCoords).r;
    fColor = color * depth;
}
