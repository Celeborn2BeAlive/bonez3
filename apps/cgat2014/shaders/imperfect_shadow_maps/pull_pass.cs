#version 430 core
#extension GL_NV_gpu_shader5: enable

layout(local_size_x = 32, local_size_y = 32) in;

layout(r32f) coherent uniform image2D uInputImage;
layout(r32f) coherent uniform image2D uOutputImage;

uniform float uTreshold;

void main() {
    ivec2 pixel = ivec2(gl_GlobalInvocationID.xy);

    ivec2 outputSize = imageSize(uOutputImage);

    if(pixel.x >= outputSize.x || pixel.y >= outputSize.y) {
        return;
    }

    float tl = imageLoad(uInputImage, pixel * 2).r;
    float tr = imageLoad(uInputImage, pixel * 2 + ivec2(1, 0)).r;
    float bl = imageLoad(uInputImage, pixel * 2 + ivec2(0, 1)).r;
    float br = imageLoad(uInputImage, pixel * 2 + ivec2(1, 1)).r;

    if(tl + tr + bl + br >= 4.f) {
        imageStore(uOutputImage, pixel, vec4(1.f));
    } else {
        float minimum = min(tl, min(tr, min(bl, br)));

        float sum = 0.f;
        uint count = 0;
        if(tl < 1.f && abs(tl - minimum) <= uTreshold) {
            sum += tl;
            ++count;
        }
        if(tr < 1.f && abs(tr - minimum) <= uTreshold) {
            sum += tr;
            ++count;
        }
        if(bl < 1.f && abs(bl - minimum) <= uTreshold) {
            sum += bl;
            ++count;
        }
        if(br < 1.f && abs(br - minimum) <= uTreshold) {
            sum += br;
            ++count;
        }
        sum /= float(count);
        imageStore(uOutputImage, pixel, vec4(sum));
    }

    memoryBarrier();
}
