#version 430

layout(location = 0) in vec2 aPosition;

// Origin of the shadow map in texture space
uniform float uSMX;
uniform float uSMY;

// Resolution of the shadow map in texture space
uniform float uRes;

out vec2 vTexCoords;

void main() {
    vTexCoords = 0.5f * (aPosition + vec2(1, 1)); // between 0 and 1
    vTexCoords = vec2(uSMX, uSMY) + vTexCoords * uRes; // In the correct shadow map

    gl_Position = vec4(aPosition, 0, 1);
}
