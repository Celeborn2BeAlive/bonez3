#version 430 core
#extension GL_NV_gpu_shader5: enable

layout(location = 0) in vec3 aPosition;
layout(location = 1) in vec3 aNormal;

uniform bool uDoPacked;
uniform uint uPackSize;

uniform float uZFar;
uniform float uZNear;

out vec3 vProjectedPosition; // Projected position in clip space
out float vDepth;
flat out uvec2 vViewportIndex; // Position (i, j) of the viewport in image space

layout (std430, binding = 0) buffer ViewMatrixData {
    mat4 bViewMatrixBuffer[];
};

uniform uint uViewMatrixCount;

struct ISMData {
    uint smResolution;
    uint smCountPerTexture;
    uint smCountPerDimension;
};

uniform ISMData uISMData;

uniform float uPointMaxSize;

void main() {
    // Get the index of the view associated with the input vertex
    uint viewIdx;

    if(uDoPacked) {
        viewIdx = gl_VertexID / uPackSize;
    } else {
        viewIdx = gl_VertexID % uViewMatrixCount;
    }

    mat4 viewMatrix = bViewMatrixBuffer[viewIdx];

    vec4 Pvs = viewMatrix * vec4(aPosition, 1); // Position in view space

    /*
    if(dot(aNormal, -Pvs.xyz) <= 0.f) {
        gl_Position = vec4(0);
        return;
    }*/

    Pvs.z = -Pvs.z; // View the negative hemisphere

    // Paraboloid projection
    Pvs /= Pvs.w;

    vec3 v = Pvs.xyz;
    vProjectedPosition.z = Pvs.z;

    float l = length(v);
    v /= l;

    vec3 h = v + vec3(0, 0, 1); // h = k * (x, y, 1)  => x = h.x / h.z   y = h.y / h.z

    vDepth = (l - uZNear) / (uZFar - uZNear);

    // vProjectedPosition is in [-1, 1] x [-1, 1]
    vProjectedPosition.xy = h.xy / h.z;

    //float b = 1.f / (uPointMaxSize - 1);
    //float a = uPointMaxSize * b;

    //gl_PointSize = a / (b + vDepth * vDepth);

    gl_PointSize = max(1, uPointMaxSize * exp(-vDepth));

    // Dimension of a view in clip space
    float size = 2.f / uISMData.smCountPerDimension;

    // 2D coordinates of the viewport attached to the view
    uint i = viewIdx % uISMData.smCountPerDimension;
    uint j = (viewIdx - i) / uISMData.smCountPerDimension;
    vViewportIndex = uvec2(i, j);

    // Coordinates between 0 and 1 in the viewport of the view
    vec2 xy = 0.5 * (vProjectedPosition.xy + vec2(1, 1));

    // Origin of the viewport of the view in clip space
    vec2 origin = vec2(-1, -1) + vec2(i, j) * size;

    gl_Position = vec4(origin + xy * size, vDepth, 1);
}
