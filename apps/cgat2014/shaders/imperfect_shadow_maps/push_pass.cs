#version 430 core
#extension GL_NV_gpu_shader5: enable

layout(local_size_x = 32, local_size_y = 32) in;

layout(r32f) coherent uniform image2D uInputImage;
layout(r32f) coherent uniform image2D uOutputImage;

uniform uint uInputSMResolution;

uniform float uTreshold;

void main() {
    ivec2 pixel = ivec2(gl_GlobalInvocationID.xy);

    ivec2 outputSize = imageSize(uOutputImage);

    if(pixel.x >= outputSize.x || pixel.y >= outputSize.y) {
        return;
    }

    int k = (pixel.x % 2 != 0) ? 1 : -1;
    int l = (pixel.y % 2 != 0) ? 1 : -1;

    ivec2 coarsePixel = pixel / 2;

    vec4 depths = vec4(imageLoad(uInputImage, coarsePixel).r,
                       imageLoad(uInputImage, coarsePixel + ivec2(k, 0)).r,
                       imageLoad(uInputImage, coarsePixel + ivec2(0, l)).r,
                       imageLoad(uInputImage, coarsePixel + ivec2(k, l)).r);
    vec4 weights = vec4(9, 3, 3, 1) / 16.f;

    float diff = depths.x;

    float sum = 0.f;
    float wsum = 0.f;
    uint count = 0;
    if(depths.x < 1.f) {
        sum += weights.x * depths.x;
        wsum += weights.x;
        ++count;
    }
    if(depths.y < 1.f && uInputSMResolution != 1) {
        bool badCondition = (coarsePixel.x % uInputSMResolution == 0 && k == -1)
                || (coarsePixel.x % uInputSMResolution == (uInputSMResolution - 1) && k == 1);

        if(!badCondition) {
            sum += weights.y * depths.y;
            wsum += weights.y;
            ++count;
        }
    }
    if(depths.z < 1.f && uInputSMResolution != 1) {
        bool badCondition = (coarsePixel.y % uInputSMResolution == 0 && l == -1)
                || (coarsePixel.y % uInputSMResolution == (uInputSMResolution - 1) && l == 1);

        if(!badCondition) {
            sum += weights.z * depths.z;
            wsum += weights.z;
            ++count;
        }
    }
    if(depths.w < 1.f && uInputSMResolution != 1) {
        bool badCondition = (coarsePixel.x % uInputSMResolution == 0 && k == -1)
                || (coarsePixel.x % uInputSMResolution == (uInputSMResolution - 1) && k == 1)
                || (coarsePixel.y % uInputSMResolution == 0 && l == -1)
                || (coarsePixel.y % uInputSMResolution == (uInputSMResolution - 1) && l == 1);

        if(!badCondition) {
            sum += weights.w * depths.w;
            wsum += weights.w;
            ++count;
        }
    }

    if(count == 0) {
        imageStore(uOutputImage, pixel, vec4(1.f));
    } else {
        float depth = sum / wsum;

//            weights /= wsum;
//            float depth = dot(depths, weights);
//            depth /= wsum;

        float v = imageLoad(uOutputImage, pixel).r;
        //diff = abs(v - diff);

        if(v < 1.0f && (v < diff || abs(v - diff) < uTreshold)) {
            return;
        }
        imageStore(uOutputImage, pixel, vec4(depth));
    }

    memoryBarrier();
}
