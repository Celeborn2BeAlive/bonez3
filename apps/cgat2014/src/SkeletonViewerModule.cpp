#include "SkeletonViewerModule.hpp"
#include "CGAT14Viewer.hpp"

#include <bonez/sampling/patterns.hpp>
#include <bonez/scene/Scene.hpp>

namespace BnZ {

SkeletonViewerModule::SkeletonViewerModule(const GLShaderManager& shaderManager):
    m_CubeMapContainer(256, 1),
    m_CubeMapRenderPass(shaderManager) {
    GLenum internalFormat = GL_RGB32F;
    m_DepthMapDisplayFramebuffer.init(512, 256, &internalFormat);
}

void SkeletonViewerModule::setUp(CGAT14Viewer& viewer) {
    m_pViewer = &viewer;
    m_pScene = viewer.getScene();

    m_nNodeObjectTypeID = m_pViewer->getScreenFramebuffer().addObjectType();
}

void SkeletonViewerModule::draw() {
    const auto& pSkel = m_pScene->getCurvSkeleton();
    if(!pSkel) {
        return;
    }

    updateSelectedNode();

    m_DisplayStream.clearObjects();
    m_pViewer->getGLDebugRenderer().addStream(&m_DisplayStream);

    drawCurvSkel();

    // Draw maxball
    if(m_bDisplaySelectedNodeMaxball && m_nSelectedNodeIndex != UNDEFINED_NODE) {
        auto selectedNode = pSkel->getNode(m_nSelectedNodeIndex);
        m_DisplayStream.addSphere(selectedNode.P, selectedNode.maxball, getColor(m_nSelectedNodeIndex));
    }

    // Draw link to skeleton node for picked point
    if (m_pScene->getCurvSkeleton() && m_bDisplaySkeleton && m_pViewer->getPickedIntersection()) {
        auto nodeIdx = m_pScene->getCurvSkeleton()->getNearestNode(m_pViewer->getPickedIntersection(), m_pViewer->getPickedIntersectionIncidentDirection());
        if (nodeIdx != UNDEFINED_NODE) {
            m_DisplayStream.addLine(m_pViewer->getPickedIntersection().P, m_pScene->getCurvSkeleton()->getNode(nodeIdx).P, Vec3f(1), Vec3f(1), 3.f);
        }
    }

    // Draw surface point mapping
    if(m_pScene->getCurvSkeleton() && m_bDisplaySkeleton && m_bDisplaySurfacePointNodeMapping) {
        auto processPoint = [&](const SurfacePoint& P) {
            auto wi = normalize(m_pViewer->getCamera().getOrigin() - P.P);
            auto nodeIdx = m_pScene->getCurvSkeleton()->getNearestNode(P, wi);
            if(nodeIdx != UNDEFINED_NODE) {
                if(!m_bDisplayOnlyUnmapped) {
                    if(!m_bDisplayOnlyForSelectedNode || nodeIdx == m_nSelectedNodeIndex) {
                        m_DisplayStream.addArrow(P.P, P.Ns, m_pViewer->getArrowLength(), m_pViewer->getArrowBase(), getColor(nodeIdx));
                        if(m_bDisplayNodeMappingLines) {
                            m_DisplayStream.addLine(P.P, m_pScene->getCurvSkeleton()->getNode(nodeIdx).P, getColor(nodeIdx), getColor(nodeIdx), 3.f);
                        }
                    }
                }
            } else {
                m_DisplayStream.addArrow(P.P, P.Ns, m_pViewer->getArrowLength(), m_pViewer->getArrowBase(), zero<Vec3f>());
            }
        };

        RandomGenerator rng;
        if(m_bUniformPointMapping) {
            for(auto i: range(m_nPointCount)) {
                auto s1DMesh = rng.getFloat();
                auto s1DTriangle = rng.getFloat();
                auto s1DSide = rng.getFloat();
                auto s2D = rng.getFloat2();

                SurfacePointSample pointSample;
                m_pScene->uniformSampleSurfacePoints(1u, &s1DMesh, &s1DTriangle, &s1DSide, &s2D, &pointSample);
                processPoint(pointSample.value);
            }
        } else {
            Vec2u gridSize = max(Vec2u(1), Vec2u(Vec2f(m_pViewer->getRenderModule().m_CPUFramebuffer.getSize()) * m_fSurfacePointDensity));

            JitteredDistribution2D sampler2D(gridSize.x, gridSize.y);

            for(auto j = 0u; j < gridSize.y; ++j) {
                for(auto i = 0u; i < gridSize.x; ++i) {
                    auto s2D = sampler2D.getSample(i + j * gridSize.x, rng.getFloat2());

                    RaySample raySample;
                    Intersection I;
                    float positionPdf, directionPdf, sampledPointToIncidentDirectionJacobian,
                            intersectionPdfWrtArea;

                    m_pViewer->getCamera().sampleExitantRay(*m_pViewer->getScene(), Vec2f(0.5f), s2D, raySample, positionPdf, directionPdf, I,
                                                 sampledPointToIncidentDirectionJacobian, intersectionPdfWrtArea);

                    if(I) {
                        processPoint(I);
                    }
                }
            }
        }
    }
}

void SkeletonViewerModule::drawCurvSkel() {
    if(!m_bDisplaySkeleton) {
        return;
    }

    const auto& pSkel = m_pScene->getCurvSkeleton();

    if(pSkel) {
        float lineWidth = 2.f;
        std::function<Vec3f(uint32_t)> getNodeColor = [](uint32_t i) {
            return getColor(i);
        };
        float scale = 0.0005f;

        if(m_bHighlightSkeleton) {
            lineWidth = 5.f;
            getNodeColor = [](uint32_t i) { return Vec3f(1, 0, 0); };
            scale = 0.001f;
        }

        for(auto i = 0u; i < pSkel->size(); ++i) {
            auto node = pSkel->getNode(i);
            m_DisplayStream.addSphere(node.P, m_pViewer->getZFar() * scale,
                             getNodeColor(i), Vec4u(m_nNodeObjectTypeID, i, 0, 0));

            for(auto j: pSkel->neighbours(i)) {
                auto neighbour = pSkel->getNode(j);
                m_DisplayStream.addLine(node.P, neighbour.P,
                               getNodeColor(i), getNodeColor(j), lineWidth);
            }
        }
    }
}

void SkeletonViewerModule::drawGUI() {
    auto& gui = m_pViewer->getGUI();

    if(auto window = gui.addWindow("SkeletonViewerModule")) {
        // Skeleton informations
        gui.addValue("NodeCount", m_pScene->getCurvSkeleton()->size());

        gui.addSeparator();

        // Skeleton display
        gui.addVarRW(BNZ_GUI_VAR(m_bDisplaySkeleton));
        gui.addVarRW(BNZ_GUI_VAR(m_bHighlightSkeleton));

        gui.addButton("Set segmented skeleton", [&]() {
            m_pScene->setCurvSkeleton(computeMaxballBasedSegmentedSkeleton(*m_pScene->getCurvSkeleton()));
        });

        gui.addSeparator();

        const auto& pSkel = m_pScene->getCurvSkeleton();

        if(pSkel) {
            // Selected node
            gui.addVarRW(BNZ_GUI_VAR(m_nSelectedNodeIndex));
            if(m_nSelectedNodeIndex >= pSkel->size()) {
                m_nSelectedNodeIndex = pSkel->size() - 1;
            }

            auto selectedNode = pSkel->getNode(m_nSelectedNodeIndex);

            gui.addValue(BNZ_GUI_VAR(selectedNode.P));
            gui.addValue(BNZ_GUI_VAR(selectedNode.maxball));
            auto selectedNodeColor = getColor(m_nSelectedNodeIndex);
            gui.addValue(BNZ_GUI_VAR(selectedNodeColor));
            gui.addVarRW(BNZ_GUI_VAR(m_bDisplaySelectedNodeMaxball));

            gui.addSeparator();

            gui.addButton("Add Node Point Light", [&]() {
                m_pScene->addLight(makeShared<PointLight>(selectedNode.P, zero<Vec3f>()));
            });

            gui.addSeparator();
        }

        gui.addVarRW(BNZ_GUI_VAR(m_bDisplaySurfacePointNodeMapping));
        gui.addVarRW(BNZ_GUI_VAR(m_bDisplayNodeMappingLines));
        gui.addVarRW(BNZ_GUI_VAR(m_bUniformPointMapping));
        if(m_bUniformPointMapping) {
            gui.addVarRW(BNZ_GUI_VAR(m_nPointCount));
        } else {
            gui.addVarRW(BNZ_GUI_VAR(m_fSurfacePointDensity));
        }
        gui.addVarRW(BNZ_GUI_VAR(m_bDisplayOnlyUnmapped));
        gui.addVarRW(BNZ_GUI_VAR(m_bDisplayOnlyForSelectedNode));
    }

    drawSelectedNodeDepthMapWindow();
}

void SkeletonViewerModule::drawSelectedNodeDepthMapWindow() {
    auto& gui = m_pViewer->getGUI();
    const auto& pSkel = m_pScene->getCurvSkeleton();
    if(pSkel && m_nSelectedNodeIndex != UNDEFINED_NODE) {
        if(auto window = gui.addWindow("NodeViewMap", false, m_DepthMapDisplayFramebuffer.getSize())) {
            auto selectedNode = pSkel->getNode(m_nSelectedNodeIndex);

            Mat4f viewMatrix = getViewMatrix(selectedNode);
            auto viewMatrixBuffer = genBufferStorage(1, &viewMatrix);

            m_CubeMapRenderPass.render(m_pViewer->getGLScene(), m_CubeMapContainer,
                                       viewMatrixBuffer, 1, m_pViewer->getZNear(), m_pViewer->getZFar());

            m_DepthMapDisplayFramebuffer.bindForDrawing();

            glViewport(0, 0, m_DepthMapDisplayFramebuffer.getWidth(), m_DepthMapDisplayFramebuffer.getHeight());
            m_CubeMapRenderPass.drawMap(m_CubeMapContainer, 0, 0, m_pViewer->getZNear(), m_pViewer->getZFar(),
                                        m_pViewer->getScreenTriangle());

            glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0u);

            gui.addImage(m_DepthMapDisplayFramebuffer.getColorBuffer(0));
        }
    }
}

void SkeletonViewerModule::updateSelectedNode() {
    const auto& pSkel = m_pScene->getCurvSkeleton();
    auto objectID = m_pViewer->getSelectedObjectID();

    if(!objectID.x || objectID.x != m_nNodeObjectTypeID || !pSkel) {
        return;
    }

    m_nSelectedNodeIndex = objectID.y;
}

}
