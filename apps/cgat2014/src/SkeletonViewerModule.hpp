#pragma once

#include <bonez/opengl/debug/GLDebugStream.hpp>
#include <bonez/opengl/cube_mapping/GLCubeMapRenderPass.hpp>
#include <bonez/scene/topology/CurvilinearSkeleton.hpp>


namespace BnZ {

class CGAT14Viewer;
class Scene;

class SkeletonViewerModule {
public:
    SkeletonViewerModule(const GLShaderManager& shaderManager);

    void setUp(CGAT14Viewer& viewer);

    void drawGUI();

    void draw();

    uint32_t getSelectedNodeIndex() const {
        return m_nSelectedNodeIndex;
    }

    bool isSkeletonDisplayed() const {
        return m_bDisplaySkeleton;
    }

private:
    void drawCurvSkel();

    void drawSelectedNodeDepthMapWindow();

    void updateSelectedNode();

    CGAT14Viewer* m_pViewer = nullptr;
    Scene* m_pScene = nullptr;

    bool m_bDisplaySkeleton = true;
    bool m_bHighlightSkeleton = false;

    GLDebugStreamData m_DisplayStream;

    // Selected node
    uint32_t m_nNodeObjectTypeID = 0u;
    uint32_t m_nSelectedNodeIndex = 0u;
    //CurvilinearSkeleton::Node m_SelectedNode;
    //Col3f m_SelectedNodeColor;
    bool m_bDisplaySelectedNodeMaxball = false;
    bool m_bDisplaySurfacePointNodeMapping = false;
    bool m_bDisplayNodeMappingLines = false;
    float m_fSurfacePointDensity = 0.05;
    bool m_bUniformPointMapping = true;
    uint32_t m_nPointCount = 16000u;
    bool m_bDisplayOnlyUnmapped = false;
    bool m_bDisplayOnlyForSelectedNode = false;

    // GL components to display selected node depth map
    GLCubeMapContainer<1> m_CubeMapContainer;
    GLCubeMapRenderPass m_CubeMapRenderPass;

    GLFramebuffer2D<1, false> m_DepthMapDisplayFramebuffer;
};

}
