#include "CGAT14Viewer.hpp"

#include <bonez/voxskel/GLVoxelizerTripiana2009.hpp>
#include <bonez/voxskel/ThinningProcessDGCI2013_2.hpp>
#include <bonez/voxskel/discrete_functions.hpp>

namespace BnZ {

CGAT14Viewer::CGAT14Viewer(const FilePath& applicationPath, const FilePath& settingsFilePath):
    Viewer(applicationPath.c_str(), applicationPath, settingsFilePath),
    m_GBufferRenderPass(m_ShaderManager),
    m_FlatShadingPass(m_ShaderManager),
    m_pGLDebugRenderer(makeShared<GLDebugRenderer>(m_ShaderManager)),
    m_SkeletonViewerModule(m_ShaderManager),
    m_ISMRenderer(m_ShaderManager),
    m_ISMDrawer(m_ShaderManager) {
    m_pGLDebugRenderer->setArrowSize(getArrowBase(), getArrowLength());
}

void CGAT14Viewer::initViewports() {
    // Setup the viewports depending on the window size and the framebuffer size
    float W = 0.75 * m_Settings.m_WindowSize.x;

    m_GBufferTexScreenSize.x = W / 5;
    m_GBufferTexScreenSize.y = m_GBufferTexScreenSize.x / m_Settings.m_fFramebufferRatio;

    Vec4f mainViewport(0, m_GBufferTexScreenSize.y, W, m_Settings.m_WindowSize.y - m_GBufferTexScreenSize.y);

    m_FinalRenderViewport =
            Vec4f(0.5 * (m_Settings.m_WindowSize.x - m_Settings.m_FramebufferSize.x),
                  0.5 * (m_Settings.m_WindowSize.y - m_Settings.m_FramebufferSize.y),
                  m_Settings.m_FramebufferSize);
}

void CGAT14Viewer::initGLData() {
    m_GBuffer.init(m_Settings.m_FramebufferSize);

    m_pGLScene = m_pScene->getGLScene();
    if(!m_pGLScene) {
        m_pGLScene = makeShared<GLScene>(m_pScene->getGeometry());
    }

    m_GLFramebuffer.init(m_Settings.m_FramebufferSize);
}

void CGAT14Viewer::setUp() {
    initGLData();
    initViewports();

    if(!getScene()->getCurvSkeleton()) {
        applySceneThinning();
    }

    GLenum format = GL_RGB32F;
    m_GBufferDisplayFramebuffer.init(5 * m_GBufferTexScreenSize.x, m_GBufferTexScreenSize.y, &format, GL_NEAREST);

    m_SkeletonViewerModule.setUp(*this);

    computeISMs();
    m_ISMDisplayFramebuffer.init(256, 256, { GL_RGB32F }, GL_NEAREST);
}

void CGAT14Viewer::computeISMs() {
    if(m_nOldISMPointSampleCount != m_nISMPointSampleCount) {
        std::vector<SurfacePointSample> surfacePointSamples;
        DefaultSurfacePointSampleGenerator rng;

        getScene()->sampleSurfacePointsWrtArea(m_nISMPointSampleCount,
                                               std::back_inserter(surfacePointSamples),
                                               rng);
        std::vector<GLPoint> glPoints;
        for(const auto& sample: surfacePointSamples) {
            glPoints.emplace_back(sample.value.P, sample.value.Ns);
        }
        m_SampledScene.fill(glPoints.size(), glPoints.data());

        m_nOldISMPointSampleCount = m_nISMPointSampleCount;
    }

    for(auto i: range(getScene()->getCurvSkeleton()->size())) {
        m_ISMViewMatrixBuffer.emplace_back(getViewMatrix(getScene()->getCurvSkeleton()->getNode(i)));
    }

    m_ISMRenderer.render(m_SampledScene, m_ISMContainer,
                         m_ISMViewMatrixBuffer.data(), m_ISMViewMatrixBuffer.size(),
                         m_fISMMaxPointSize, m_bISMDoPull, m_bISMDoPullPush, m_fISMPullThreshold, m_fISMPushThreshold,
                         m_nISMMaxPushPullLevels, getZNear(), getZFar());
}

void CGAT14Viewer::tearDown() {
}

void CGAT14Viewer::drawFinalRender() {
    m_GLFramebuffer.bindForReading();
    m_GLFramebuffer.setReadBuffer(0);

    glBlitFramebuffer(0, 0, m_GBuffer.getWidth(), m_GBuffer.getHeight(),
                      m_FinalRenderViewport.x, m_FinalRenderViewport.y, m_FinalRenderViewport.x + m_FinalRenderViewport.z, m_FinalRenderViewport.y + m_FinalRenderViewport.w,
                      GL_COLOR_BUFFER_BIT, GL_NEAREST);

    glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
}

void CGAT14Viewer::setSelectedPixel(const Vec2u& pixel) {
    m_SelectedPixel = pixel;

    if(m_RenderModule.m_bCPURendering) {
        m_RenderModule.setSelectedPixel(pixel);
    }
}

float CGAT14Viewer::getArrowLength() const {
    return length(size(getScene()->getBBox())) * 0.005f;
}

float CGAT14Viewer::getArrowBase() const {
    return (1.f / 8) * getArrowLength();
}

void CGAT14Viewer::doPicking() {
    if(!m_bGUIHasFocus && m_WindowManager.hasClicked()) {
        m_SelectedObjectID = m_GLFramebuffer.getObjectID(m_WindowManager.getCursorPosition(Vec2f(m_FinalRenderViewport)));

        if(m_SelectedObjectID == GLScreenFramebuffer::NULL_OBJECT_ID) {
            auto ndcPosition = m_WindowManager.getCursorNDC(m_FinalRenderViewport);
            if(viewportContains(ndcPosition, Vec4f(-1, -1, 2, 2))) {
                RaySample raySample;
                m_PickedIntersection =
                        tracePrimaryRay(getCamera(), *getScene(), Vec2f(0.5f), ndcToUV(ndcPosition), raySample);
                if(m_PickedIntersection) {
                    m_PickedIntersectionIncidentDirection = -raySample.value.dir;
                }
            }
        }

        auto pixel = m_WindowManager.getCursorPosition(Vec2f(m_FinalRenderViewport));
        if(viewportContains(Vec2f(pixel), Vec4f(0, 0, m_FinalRenderViewport.z, m_FinalRenderViewport.w))) {
            setSelectedPixel(pixel);
        }
    }
}

void CGAT14Viewer::storeScreenshotImage(const Image& image) {
    FilePath screenshotsPath = m_Path + "screenshots";
    createDirectory(screenshotsPath);
    FilePath baseName = m_ScreenshotPrefix + "screenshot." + toString(getMicroseconds());
    FilePath pngFile = screenshotsPath + baseName.addExt(".png");

    storeImage(pngFile, image);
}

void CGAT14Viewer::doScreenshot() {
    Image image;
    fillImage(image, m_GLFramebuffer.getColorBuffer());
    image.flipY();

    storeScreenshotImage(image);
}

void CGAT14Viewer::drawFrame() {
    // Draw GUI
    exposeIO();
    m_RenderModule.exposeIO(m_GUI, m_GLImageRenderer);
    m_SkeletonViewerModule.drawGUI();

    m_GBufferRenderPass.render(m_Camera.getProjMatrix(), m_Camera.getViewMatrix(),
                               m_ZNearFar.y, *m_pGLScene, m_GBuffer);
    m_GLFramebuffer.bindForDrawing();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glViewport(0, 0, m_GBuffer.getWidth(), m_GBuffer.getHeight());

    if(m_RenderModule.m_bCPURendering) {
        m_RenderModule.doCPURendering(m_CameraUpdateFlag, m_Camera, *m_pScene, m_GLImageRenderer);
    } else {
        m_FlatShadingPass.render(m_GBuffer, m_Camera.getRcpProjMatrix(),
                                 m_Camera.getViewMatrix(), m_ScreenTriangle);
    }

    m_GLFramebuffer.blitFramebuffer(m_GBuffer, GL_DEPTH_BUFFER_BIT, GL_NEAREST);

    m_DisplayStream.clearObjects();

    if (m_PickedIntersection) {
        m_DisplayStream.addArrow(m_PickedIntersection.P, m_PickedIntersection.Ns, getArrowLength(), getArrowBase(), Vec3f(1.f));
    }

    m_SkeletonViewerModule.draw();

    m_RenderModule.drawGLData(m_PickedIntersection, m_PickedIntersectionIncidentDirection, *m_pGLDebugRenderer);

    m_pGLDebugRenderer->addStream(&m_DisplayStream);
    m_pGLDebugRenderer->render(m_Camera);

    doPicking();

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    glClear(GL_COLOR_BUFFER_BIT);

    drawFinalRender();

    if(m_bDoScreenshot) {
        doScreenshot();
        m_bDoScreenshot = false;
    }

    m_pGLDebugRenderer->clearStreams();
}

void CGAT14Viewer::handleGlobalPreprocessParameters(const tinyxml2::XMLElement& input, tinyxml2::XMLElement& output) {
    auto pThinning = input.FirstChildElement("Thinning");
    if(pThinning) {
        uint32_t gridResolution;
        getChildAttribute(*pThinning, "VoxelGridResolution", gridResolution);
        bool useSegmentedSkeleton = false;
        getChildAttribute(*pThinning, "UseSegmentedSkeleton", useSegmentedSkeleton);

		// Must thinn the scene
    }
}

void CGAT14Viewer::exposeIO() {
    if(auto window = m_GUI.addWindow("CGAT14Viewer")) {
        m_GUI.addButton("Screenshot", [this]() {
            m_bDoScreenshot = true;
        });

        m_GUI.addSeparator();

        // Scene rendering
        m_FlatShadingPass.exposeIO(m_GUI);

		m_GUI.addSeparator();

		m_GUI.addVarRW(BNZ_GUI_VAR(m_nThinningResolution));
		m_GUI.addVarRW(BNZ_GUI_VAR(m_bSegmentSkeleton));
		m_GUI.addButton("Thinning", [&]() {
			applySceneThinning();
		});
    }

    if(auto window = m_GUI.addWindow("GBuffer", false, m_GBufferDisplayFramebuffer.getSize())) {
        m_GBufferDisplayFramebuffer.bindForDrawing();

        glViewport(0, 0,
                   m_GBufferTexScreenSize.x, m_GBufferTexScreenSize.y);
        m_GLImageRenderer.drawNormalTexture(m_GBuffer);

        glViewport(0 + m_GBufferTexScreenSize.x, 0,
                   m_GBufferTexScreenSize.x, m_GBufferTexScreenSize.y);
        m_GLImageRenderer.drawDepthTexture(m_GBuffer);

        glViewport(0 + 2 * m_GBufferTexScreenSize.x, 0,
                   m_GBufferTexScreenSize.x, m_GBufferTexScreenSize.y);;
        m_GLImageRenderer.drawDiffuseTexture(m_GBuffer);

        glViewport(0 + 3 * m_GBufferTexScreenSize.x, 0,
                   m_GBufferTexScreenSize.x, m_GBufferTexScreenSize.y);
        m_GLImageRenderer.drawGlossyTexture(m_GBuffer);

        glViewport(0 + 4 * m_GBufferTexScreenSize.x, 0,
                   m_GBufferTexScreenSize.x, m_GBufferTexScreenSize.y);
        m_GLImageRenderer.drawShininessTexture(m_GBuffer);

        m_GUI.addImage(m_GBufferDisplayFramebuffer.getColorBuffer(0));
    }

    if(auto window = m_GUI.addWindow("ISMRenderer")) {
        m_ISMDisplayFramebuffer.bindForDrawing();
        glViewport(0, 0, m_ISMDisplayFramebuffer.getWidth(), m_ISMDisplayFramebuffer.getHeight());
        m_ISMDrawer.drawISM(m_ISMContainer, m_SkeletonViewerModule.getSelectedNodeIndex());
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

        m_GUI.addImage(m_ISMDisplayFramebuffer.getColorBuffer(0));
//        m_GUI.addImage(m_ISMContainer.getTexture(0));

        m_GUI.addSeparator();
        m_GUI.addButton("Render ISMs", [&]() {
            computeISMs();
        });
        m_GUI.addVarRW(BNZ_GUI_VAR(m_nISMPointSampleCount));
        m_GUI.addVarRW(BNZ_GUI_VAR(m_fISMMaxPointSize));
        m_GUI.addVarRW(BNZ_GUI_VAR(m_bISMDoPull));
        m_GUI.addVarRW(BNZ_GUI_VAR(m_bISMDoPullPush));
        m_GUI.addVarRW(BNZ_GUI_VAR(m_fISMPullThreshold));
        m_GUI.addVarRW(BNZ_GUI_VAR(m_fISMPushThreshold));
        m_GUI.addVarRW(BNZ_GUI_VAR(m_nISMMaxPushPullLevels));
    }
}

void CGAT14Viewer::applySceneThinning() {
	GLVoxelFramebuffer voxelFramebuffer;
	Mat4f gridToWorldMatrix;
	CubicalComplex3D skeletonCubicalComplex, emptySpaceCubicalComplex;
	GLVoxelizerTripiana2009 voxelizer(m_ShaderManager);
	ThinningProcessDGCI2013_2 thinningProcess;

	Timer timer(true);
	std::clog << "Compute discrete scene dat at resolution " << m_nThinningResolution << std::endl;

	{
		Timer timer(true);
		std::clog << "Voxelizing the scene" << std::endl;
		voxelizer.initGLState(m_nThinningResolution, getScene()->getBBox(),
			voxelFramebuffer, gridToWorldMatrix);
		m_pGLScene->render();
		voxelizer.restoreGLState();
	}

	{
		Timer timer(true);
		std::clog << "Convert empty space voxel grid to CC3D" << std::endl;
		auto voxelGrid = getVoxelGrid(voxelFramebuffer);
		voxelGrid = deleteEmptyBorder(voxelGrid, gridToWorldMatrix, gridToWorldMatrix);
		voxelGrid = inverse(voxelGrid);
		emptySpaceCubicalComplex = skeletonCubicalComplex = getCubicalComplex(voxelGrid);
	}

	auto distanceMap = [&]() {
		Timer timer(true);
		std::clog << "Compute distance map" << std::endl;
		return computeDistanceMap26(emptySpaceCubicalComplex, CubicalComplex3D::isInObject, true);
	}();

	auto openingMap = [&]() {
		Timer timer(true);
		std::clog << "Compute opening map" << std::endl;
		return parallelComputeOpeningMap26(distanceMap);
	}();

	{
		Timer timer(true);
		std::clog << "Initialize the thinning process" << std::endl;
		thinningProcess.init(skeletonCubicalComplex, &distanceMap, &openingMap);
	}

	{
		Timer timer(true);
		std::clog << "Run the thinning process" << std::endl;
		thinningProcess.directionalCollapse();
	}

	if (m_bSegmentSkeleton) {
		Timer timer(true);
		std::clog << "Convert skeleton CC3D to Curvilinear Skeleton (segmented)" << std::endl;
		auto skeleton = getSegmentedCurvilinearSkeleton(skeletonCubicalComplex, emptySpaceCubicalComplex,
			distanceMap, openingMap,
			gridToWorldMatrix);

		getScene()->setCurvSkeleton(skeleton);
	}
	else {
		Timer timer(true);
		std::clog << "Convert skeleton CC3D to Curvilinear Skeleton (NOT segmented)" << std::endl;
		auto skeleton = getCurvilinearSkeleton(skeletonCubicalComplex, emptySpaceCubicalComplex,
			distanceMap, openingMap,
			gridToWorldMatrix);
		getScene()->setCurvSkeleton(skeleton);
	}

	std::cerr << "Number of nodes = " << getScene()->getCurvSkeleton()->size() << std::endl;
}

}
