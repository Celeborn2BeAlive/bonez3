#pragma once

#include <bonez/common.hpp>
#include <bonez/opengl/utils/GLutils.hpp>
#include <bonez/opengl/GLShaderManager.hpp>
#include "GLPoints.hpp"

namespace BnZ {

// Imperfect Shadow Maps container
class GLISMContainer {
    // Size of a texture
    uint32_t m_nTextureRes;

    // Resolution of single shadow map inside a texture (the size of the viewport)
    uint32_t m_nSMRes;

    // Number of shadow maps per texture in each dimension ( = m_nTextureSize / m_nRes)
    uint32_t m_nSMCountPerDimension;

    // Number of shadow maps per texture (m_nSMCount * m_nSMCount)
    uint32_t m_nSMCountPerTexture;

    // All the textures
    std::vector<GLTexture2D> m_Textures;

//    // Texture handles for bindless texture access
//    std::vector<GLuint64> m_TextureHandles;

    // Textures used to do the pull-push phase of ISM rendering
    std::vector<GLTexture2D> m_PullPushTextures;
public:
    GLISMContainer(uint32_t smRes);

    // Reserve enough textures to render smCount shadow maps
    void resize(uint32_t smCount);

    uint32_t getTextureResolution() const {
        return m_nTextureRes;
    }

    uint32_t getShadowMapResolution() const {
        return m_nSMRes;
    }

    uint32_t getShadowMapCountPerDimension() const {
        return m_nSMCountPerDimension;
    }

    uint32_t getShadowMapCountPerTexture() const {
        return m_nSMCountPerTexture;
    }

    const GLTexture2D& getTexture(uint32_t i) const {
        return m_Textures[i];
    }

    const GLTexture2D& getPullPushTexture(uint32_t i) const {
        return m_PullPushTextures[i];
    }

    uint32_t getPullPushTextureCount() const {
        return m_PullPushTextures.size();
    }

    uint32_t getTextureCount() const {
        return m_Textures.size();
    }

//    const GLuint64* getTextureHandleBufferPtr() const {
//        return m_TextureHandles.data();
//    }

//    GLuint64 getImageHandle(uint32_t i) const {
//        return glGetImageHandleNV(m_Textures[i].glId(), 0, GL_FALSE, 0, GL_R32F);
//    }

//    GLuint64 getPullPushImageHandle(uint32_t i) const {
//        return glGetImageHandleNV(m_PullPushTextures[i].glId(), 0, GL_FALSE, 0, GL_R32F);
//    }
};

// Imperfect Shadow Maps renderer
class GLISMRenderer {
public:
    GLISMRenderer(const GLShaderManager& shaderManager);

    struct ISMUniforms {
        GLUniform<GLuint> smResolution;
        GLUniform<GLuint> smCountPerTexture;
        GLUniform<GLuint> smCountPerDimension;

        ISMUniforms(const GLProgram& program):
            smResolution(program, "uISMData.smResolution"),
            smCountPerTexture(program, "uISMData.smCountPerTexture"),
            smCountPerDimension(program, "uISMData.smCountPerDimension") {
        }

        void set(const GLISMContainer& container) {
            smCountPerDimension.set(container.getShadowMapCountPerDimension());
            smCountPerTexture.set(container.getShadowMapCountPerTexture());
            smResolution.set(container.getShadowMapResolution());
        }
    };

    // Render "viewMatrixCount" ISMs in the provided container
    // The method call resize on the container to reserve enough space
    void render(const GLPoints& sampledScene, GLISMContainer& container,
                const Mat4f* viewMatrices, uint32_t viewMatrixCount,
                float pointMaxSize,
                bool doPull, bool doPullPush,
                float pullTreshold, float pushTreshold,
                int nbLevels,
                float zNear, float zFar);

    void renderCoherent(const GLPoints& sampledScene, GLISMContainer& container,
                        const Mat4f* viewMatrices, uint32_t viewMatrixCount,
                        float pointMaxSize,
                        bool doPull, bool doPullPush,
                        float pullTreshold, float pushTreshold,
                        int nbLevels,
                        float zNear, float zFar);

    void renderPacks(const GLPoints& sampledScene, uint32_t pointCountPerISM, GLISMContainer& container,
                      const Mat4f* viewMatrices, uint32_t viewMatrixCount,
                      float pointMaxSize,
                      bool doPull, bool doPullPush,
                      float pullTreshold, float pushTreshold,
                      int nbLevels,
                      float zNear, float zFar);

    void pullPush(GLISMContainer& container, bool doPull, bool doPullPush,
                  float pullTreshold, float pushTreshold,
                  int nbLevels);

private:
    GLFramebufferObject m_FBO;
    GLTextureObject<GL_TEXTURE_2D> m_DepthBuffer;

    struct SplatPass {
        GLProgram m_Program;

        BNZ_GLUNIFORM(m_Program, GLfloat, uZFar);
        BNZ_GLUNIFORM(m_Program, GLfloat, uZNear);
        BNZ_GLUNIFORM(m_Program, GLfloat, uPointMaxSize);
        BNZ_GLUNIFORM(m_Program, bool, uDoPacked);
        BNZ_GLUNIFORM(m_Program, GLuint, uPackSize);
        BNZ_GLUNIFORM(m_Program, GLuint, uViewMatrixCount);

        ISMUniforms uISMData { m_Program };

        SplatPass(const GLShaderManager& shaderManager);
    };

    SplatPass m_SplatPass;

    struct SplatPassCoherent {
        GLProgram m_Program;

        BNZ_GLUNIFORM(m_Program, GLfloat, uZFar);
        BNZ_GLUNIFORM(m_Program, GLfloat, uZNear);
        BNZ_GLUNIFORM(m_Program, GLfloat, uPointMaxSize);
        BNZ_GLUNIFORM(m_Program, GLuint, uViewMatrixCount);

        ISMUniforms uISMData { m_Program };

        SplatPassCoherent(const GLShaderManager& shaderManager);
    };

    SplatPassCoherent m_SplatPassCoherent;

    GLBuffer<Mat4f> m_ViewMatrixBuffer;

    struct PullPass {
        GLProgram m_Program;

        BNZ_GLUNIFORM(m_Program, GLSLImage2Df, uInputImage);
        BNZ_GLUNIFORM(m_Program, GLSLImage2Df, uOutputImage);
        BNZ_GLUNIFORM(m_Program, GLfloat, uTreshold);

        PullPass(const GLShaderManager& shaderManager);
    };

    PullPass m_PullPass;

    void pullPass(uint32_t texIdx, GLISMContainer& container, float treshold, int nbLevels);

    struct PushPass {
        GLProgram m_Program;

        BNZ_GLUNIFORM(m_Program, GLSLImage2Df, uInputImage);
        BNZ_GLUNIFORM(m_Program, GLSLImage2Df, uOutputImage);
        BNZ_GLUNIFORM(m_Program, GLfloat, uTreshold);
        BNZ_GLUNIFORM(m_Program, GLuint, uInputSMResolution);

        PushPass(const GLShaderManager& shaderManager);
    };

    PushPass m_PushPass;

    void pushPass(uint32_t texIdx, GLISMContainer& container, float treshold, int nbLevels);
};

class GLISMDrawer {
public:
    GLISMDrawer(const GLShaderManager& shaderManager);

    // Draw all ISMs from a texture
    void drawISMTexture(const GLISMContainer& container,
                        uint32_t texIdx, uint32_t texLvl, int textureUnit = 0);

    // Draw one ISM
    void drawISM(const GLISMContainer& container,
                 uint32_t ismIdx, int textureUnit = 0);

    // Draw one ISM
    void drawISM(const GLISMContainer& container,
                 uint32_t ismIdx, uint32_t texLvl, int textureUnit = 0);

private:
    GLProgram m_Program;

    BNZ_GLUNIFORM(m_Program, GLfloat, uSMX);
    BNZ_GLUNIFORM(m_Program, GLfloat, uSMY);
    BNZ_GLUNIFORM(m_Program, GLfloat, uRes);
    BNZ_GLUNIFORM(m_Program, GLint, uTexture);
    BNZ_GLUNIFORM(m_Program, GLuint, uTexIndex);

    GLISMRenderer::ISMUniforms m_uISMData { m_Program };

    GLScreenTriangle m_ScreenTriangle;
};

}
