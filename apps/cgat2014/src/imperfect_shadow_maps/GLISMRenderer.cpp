#include "GLISMRenderer.hpp"

namespace BnZ {

GLISMContainer::GLISMContainer(uint32_t smRes):
    m_nTextureRes(4096),
    m_nSMRes(smRes),
    m_nSMCountPerDimension(m_nTextureRes / m_nSMRes),
    m_nSMCountPerTexture(m_nSMCountPerDimension * m_nSMCountPerDimension) {


    // Create pull push textures
    uint32_t res = m_nTextureRes / 2;
    for(int s = smRes / 2; s >= 1; s /= 2) {
        m_PullPushTextures.emplace_back();
        GLTexture2D& texture = m_PullPushTextures.back();

        texture.setFilters(GL_NEAREST, GL_NEAREST);
        texture.setStorage(1, GL_R32F, res, res);

        res /= 2;
    }
}

void GLISMContainer::resize(uint32_t smCount) {
    uint32_t offset = m_Textures.size();
    size_t texCount = smCount / m_nSMCountPerTexture + (smCount % m_nSMCountPerTexture != 0);
    if(texCount > offset) {
        m_Textures.resize(texCount);
        for(auto i = offset; i < m_Textures.size(); ++i) {
            GLTexture2D& texture = m_Textures[i];

            texture.setFilters(GL_NEAREST, GL_NEAREST);
            texture.setStorage(1, GL_R32F, m_nTextureRes, m_nTextureRes);

        }
    }
}

GLISMRenderer::GLISMRenderer(const GLShaderManager& shaderManager):
    m_SplatPass(shaderManager),
    m_SplatPassCoherent(shaderManager),
    m_PullPass(shaderManager),
    m_PushPass(shaderManager) {
}

GLISMRenderer::SplatPass::SplatPass(const GLShaderManager &shaderManager):
    m_Program(shaderManager.buildProgram({ "imperfect_shadow_maps/splat_points_to_ism.vs", "imperfect_shadow_maps/splat_points_to_ism.fs" })) {
}

GLISMRenderer::SplatPassCoherent::SplatPassCoherent(const GLShaderManager &shaderManager):
    m_Program(shaderManager.buildProgram({ "imperfect_shadow_maps/splat_points_to_ism_coherent.vs", "imperfect_shadow_maps/splat_points_to_ism.fs" })) {
}

GLISMRenderer::PullPass::PullPass(const GLShaderManager &shaderManager):
    m_Program(shaderManager.buildProgram({ "imperfect_shadow_maps/pull_pass.cs" })) {
}

GLISMRenderer::PushPass::PushPass(const GLShaderManager &shaderManager):
    m_Program(shaderManager.buildProgram({ "imperfect_shadow_maps/push_pass.cs" })) {
}

void GLISMRenderer::render(const GLPoints& sampledScene, GLISMContainer& container,
            const glm::mat4* viewMatrices, uint32_t viewMatrixCount,
            float pointMaxSize,
            bool doPull, bool doPullPush,
            float pullTreshold, float pushTreshold,
            int nbLevels,
            float zNear, float zFar) {
    glEnable(GL_DEPTH_TEST);
    container.resize(viewMatrixCount);

    m_SplatPass.m_Program.use();

    // Send constant uniforms accros all passes
    m_SplatPass.uZFar.set(zFar);
    m_SplatPass.uZNear.set(zNear);

    m_SplatPass.uDoPacked.set(false);

    m_SplatPass.uISMData.set(container);

    m_SplatPass.uISMData.smCountPerDimension.set(container.getShadowMapCountPerDimension());
    m_SplatPass.uISMData.smCountPerTexture.set(container.getShadowMapCountPerTexture());
    m_SplatPass.uISMData.smResolution.set(container.getShadowMapResolution());

    m_SplatPass.uPointMaxSize.set(pointMaxSize);

    // Bind the framebuffer
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_FBO.glId());

    glTextureParameteriEXT(m_DepthBuffer.glId(),
                           GL_TEXTURE_2D,
                           GL_TEXTURE_MIN_FILTER,
                           GL_NEAREST);
    glTextureParameteriEXT(m_DepthBuffer.glId(),
                           GL_TEXTURE_2D,
                           GL_TEXTURE_MAG_FILTER,
                           GL_NEAREST);

    glTextureImage2DEXT(m_DepthBuffer.glId(),
                        GL_TEXTURE_2D,
                        0,
                        GL_DEPTH_COMPONENT32F,
                        container.getTextureResolution(),
                        container.getTextureResolution(),
                        0,
                        GL_DEPTH_COMPONENT,
                        GL_FLOAT,
                        nullptr);

    glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                           GL_TEXTURE_2D, m_DepthBuffer.glId(), 0);

    GLenum drawBuffer = GL_COLOR_ATTACHMENT0;
    glDrawBuffers(1, &drawBuffer);

    glEnable(GL_PROGRAM_POINT_SIZE);

    glClearColor(1, 1, 1, 1);
    uint32_t viewportResolution = container.getShadowMapCountPerDimension() * container.getShadowMapResolution();
    glViewport(0, 0, viewportResolution, viewportResolution);

    uint32_t viewMatrixCountPerPass = container.getShadowMapCountPerTexture();

    m_ViewMatrixBuffer.bindBase(GL_SHADER_STORAGE_BUFFER, 0);

    // Rendering of ISMs
    for(auto i = 0u; i < container.getTextureCount(); ++i) {
        glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                               GL_TEXTURE_2D, container.getTexture(i).glId(), 0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Send view matrices to the GPU
        auto viewMatrixOffset = i * viewMatrixCountPerPass;

        uint32_t viewCount = glm::min(viewMatrixCount, viewMatrixCountPerPass);
        m_ViewMatrixBuffer.setData(viewCount, viewMatrices + viewMatrixOffset, GL_DYNAMIC_DRAW);

        m_SplatPass.uViewMatrixCount.set(viewCount);

        sampledScene.render();
        viewMatrixCount -= viewMatrixCountPerPass;
    }

    glClearColor(0, 0, 0, 0);

    glDisable(GL_PROGRAM_POINT_SIZE);

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

    pullPush(container, doPull, doPullPush, pullTreshold, pushTreshold, nbLevels);
}

void GLISMRenderer::renderPacks(const GLPoints& sampledScene, uint32_t pointCountPerISM, GLISMContainer& container,
            const glm::mat4* viewMatrices, uint32_t viewMatrixCount,
            float pointMaxSize,
            bool doPull, bool doPullPush,
            float pullTreshold, float pushTreshold,
            int nbLevels,
            float zNear, float zFar) {
    glEnable(GL_DEPTH_TEST);

    container.resize(viewMatrixCount);

    m_SplatPass.m_Program.use();

    // Send constant uniforms accros all passes
    m_SplatPass.uZFar.set(zFar);
    m_SplatPass.uZNear.set(zNear);

    m_SplatPass.uDoPacked.set(true);
    m_SplatPass.uPackSize.set(pointCountPerISM);

    m_SplatPass.uISMData.set(container);

    m_SplatPass.uISMData.smCountPerDimension.set(container.getShadowMapCountPerDimension());
    m_SplatPass.uISMData.smCountPerTexture.set(container.getShadowMapCountPerTexture());
    m_SplatPass.uISMData.smResolution.set(container.getShadowMapResolution());

    m_SplatPass.uPointMaxSize.set(pointMaxSize);

    // Bind the framebuffer
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_FBO.glId());

    glTextureParameteriEXT(m_DepthBuffer.glId(),
                           GL_TEXTURE_2D,
                           GL_TEXTURE_MIN_FILTER,
                           GL_NEAREST);
    glTextureParameteriEXT(m_DepthBuffer.glId(),
                           GL_TEXTURE_2D,
                           GL_TEXTURE_MAG_FILTER,
                           GL_NEAREST);

    glTextureImage2DEXT(m_DepthBuffer.glId(),
                        GL_TEXTURE_2D,
                        0,
                        GL_DEPTH_COMPONENT32F,
                        container.getTextureResolution(),
                        container.getTextureResolution(),
                        0,
                        GL_DEPTH_COMPONENT,
                        GL_FLOAT,
                        nullptr);

    glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                           GL_TEXTURE_2D, m_DepthBuffer.glId(), 0);

    GLenum drawBuffer = GL_COLOR_ATTACHMENT0;
    glDrawBuffers(1, &drawBuffer);

    glEnable(GL_PROGRAM_POINT_SIZE);

    glClearColor(1, 1, 1, 1);
    uint32_t viewportResolution = container.getShadowMapCountPerDimension() * container.getShadowMapResolution();
    glViewport(0, 0, viewportResolution, viewportResolution);

    uint32_t viewMatrixCountPerPass = container.getShadowMapCountPerTexture();

    m_ViewMatrixBuffer.bindBase(GL_SHADER_STORAGE_BUFFER, 0);

    // Rendering of ISMs
    for(auto i = 0u; i < container.getTextureCount(); ++i) {
        glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                               GL_TEXTURE_2D, container.getTexture(i).glId(), 0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Send view matrices to the GPU
        auto viewMatrixOffset = i * viewMatrixCountPerPass;
        uint32_t viewCount = glm::min(viewMatrixCount, viewMatrixCountPerPass);
        m_ViewMatrixBuffer.setData(viewCount, viewMatrices + viewMatrixOffset, GL_DYNAMIC_DRAW);

        m_SplatPass.uViewMatrixCount.set(viewCount);

        sampledScene.render();
        viewMatrixCount -= viewMatrixCountPerPass;
    }

    glClearColor(0, 0, 0, 0);

    glDisable(GL_PROGRAM_POINT_SIZE);

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

    pullPush(container, doPull, doPullPush, pullTreshold, pushTreshold, nbLevels);
}

void GLISMRenderer::renderCoherent(const GLPoints& sampledScene, GLISMContainer& container,
            const glm::mat4* viewMatrices, uint32_t viewMatrixCount,
            float pointMaxSize,
            bool doPull, bool doPullPush,
            float pullTreshold, float pushTreshold,
            int nbLevels,
            float zNear, float zFar) {
    glEnable(GL_DEPTH_TEST);

    container.resize(viewMatrixCount);

    m_SplatPassCoherent.m_Program.use();

    // Send constant uniforms accros all passes
    m_SplatPassCoherent.uZFar.set(zFar);
    m_SplatPassCoherent.uZNear.set(zNear);

    m_SplatPassCoherent.uISMData.set(container);

    m_SplatPassCoherent.uISMData.smCountPerDimension.set(container.getShadowMapCountPerDimension());
    m_SplatPassCoherent.uISMData.smCountPerTexture.set(container.getShadowMapCountPerTexture());
    m_SplatPassCoherent.uISMData.smResolution.set(container.getShadowMapResolution());

    m_SplatPassCoherent.uPointMaxSize.set(pointMaxSize);

    // Bind the framebuffer
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_FBO.glId());

    glTextureParameteriEXT(m_DepthBuffer.glId(),
                           GL_TEXTURE_2D,
                           GL_TEXTURE_MIN_FILTER,
                           GL_NEAREST);
    glTextureParameteriEXT(m_DepthBuffer.glId(),
                           GL_TEXTURE_2D,
                           GL_TEXTURE_MAG_FILTER,
                           GL_NEAREST);

    glTextureImage2DEXT(m_DepthBuffer.glId(),
                        GL_TEXTURE_2D,
                        0,
                        GL_DEPTH_COMPONENT32F,
                        container.getTextureResolution(),
                        container.getTextureResolution(),
                        0,
                        GL_DEPTH_COMPONENT,
                        GL_FLOAT,
                        nullptr);

    glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                           GL_TEXTURE_2D, m_DepthBuffer.glId(), 0);

    GLenum drawBuffer = GL_COLOR_ATTACHMENT0;
    glDrawBuffers(1, &drawBuffer);

    glEnable(GL_PROGRAM_POINT_SIZE);

    glClearColor(1, 1, 1, 1);
    uint32_t viewportResolution = container.getShadowMapCountPerDimension() * container.getShadowMapResolution();
    glViewport(0, 0, viewportResolution, viewportResolution);

    uint32_t viewMatrixCountPerPass = container.getShadowMapCountPerTexture();

    m_ViewMatrixBuffer.bindBase(GL_SHADER_STORAGE_BUFFER, 0);

    // Rendering of ISMs
    for(auto i = 0u; i < container.getTextureCount(); ++i) {
        glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                               GL_TEXTURE_2D, container.getTexture(i).glId(), 0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Send view matrices to the GPU
        auto viewMatrixOffset = i * viewMatrixCountPerPass;
        uint32_t viewCount = glm::min(viewMatrixCount, viewMatrixCountPerPass);
        m_ViewMatrixBuffer.setData(viewCount, viewMatrices + viewMatrixOffset, GL_DYNAMIC_DRAW);

        m_SplatPass.uViewMatrixCount.set(viewCount);

        sampledScene.render(viewCount);
        viewMatrixCount -= viewMatrixCountPerPass;
    }

    glClearColor(0, 0, 0, 0);

    glDisable(GL_PROGRAM_POINT_SIZE);

    pullPush(container, doPull, doPullPush, pullTreshold, pushTreshold, nbLevels);
}

void GLISMRenderer::pullPush(GLISMContainer& container, bool doPull, bool doPullPush,
              float pullTreshold, float pushTreshold,
              int nbLevels) {
    if(doPull) {
        for(auto i = 0u; i < container.getTextureCount(); ++i) {
            pullPass(i, container, pullTreshold, nbLevels);
            if(doPullPush) {
                pushPass(i, container, pushTreshold, nbLevels);
            }
        }
    }
}

void GLISMRenderer::pullPass(uint32_t texIdx, GLISMContainer& container, float treshold, int nbLevels) {
    m_PullPass.m_Program.use();

    uint32_t pullPushIdx = 0;

    static const auto inputImageUnit = 0;
    static const auto outputImageUnit = 1;

    container.getTexture(texIdx).bindImage(inputImageUnit, 0, GL_READ_WRITE, GL_R32F);
    container.getPullPushTexture(0).bindImage(outputImageUnit, 0, GL_READ_WRITE, GL_R32F);

    m_PullPass.uInputImage.set(inputImageUnit);
    m_PullPass.uOutputImage.set(outputImageUnit);

    /*
    glBindImageTexture(0, container.getTexture(texIdx).glId(), 0, GL_FALSE, 0, GL_READ_WRITE, GL_R32F);
    glBindImageTexture(1, container.getPullPushTexture(0).glId(), 0, GL_FALSE, 0, GL_READ_WRITE, GL_R32F);
*/

    float scale = container.getShadowMapResolution();
    uint32_t outputTexSize = container.getTextureResolution() / 2;

    for(uint32_t s = container.getShadowMapResolution() / 2; s >= 1; s /= 2) {
        if(nbLevels >= 0 && pullPushIdx == nbLevels) {
            break;
        }

        uint32_t blockSize = 32;
        uint32_t blockCount = outputTexSize / blockSize
                + (outputTexSize % blockSize != 0);

        m_PullPass.uTreshold.set(treshold / scale);

        glDispatchCompute(blockCount, blockCount, 1);

        glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

        //m_PullPass.uInputImage.set(container.getPullPushImageHandle(pullPushIdx));
        container.getPullPushTexture(pullPushIdx).bindImage(inputImageUnit, 0, GL_READ_WRITE, GL_R32F); // Result of the previous pass becomes input of the new pass

        if(s > 1) {
            container.getPullPushTexture(pullPushIdx + 1).bindImage(outputImageUnit, 0, GL_READ_WRITE, GL_R32F);
//            m_PullPass.uOutputImage.set(container.getPullPushImageHandle(pullPushIdx + 1));
        }
        pullPushIdx++;
        outputTexSize /= 2;
        scale /= 2.f;
    }
}

void GLISMRenderer::pushPass(uint32_t texIdx, GLISMContainer& container, float treshold, int nbLevels) {
    m_PushPass.m_Program.use();

    uint32_t pullPushIdx = container.getPullPushTextureCount() - 1;

    // First input is 1x1 for each SM, so the output is 2x2 for each SM
    // Giving (2*nbSM)x(2*nbSM) for the complete texture
    uint32_t outputTexSize = container.getShadowMapCountPerDimension() * 2;

    uint32_t startRes = 2;
    float scale = 1;//container.getShadowMapResolution() / 2;

    if(nbLevels >= 0) {
        pullPushIdx = nbLevels - 1;
        startRes = 2 * container.getShadowMapResolution() / (1 << nbLevels);
        scale = startRes / 2;
        outputTexSize = container.getShadowMapCountPerDimension() * startRes;
    }

    static const auto inputImageUnit = 0;
    static const auto outputImageUnit = 1;

    m_PushPass.uInputImage.set(inputImageUnit);
    m_PushPass.uOutputImage.set(outputImageUnit);

    for(uint32_t s = startRes; s <= container.getShadowMapResolution(); s *= 2) {
        //m_PushPass.uInputImage.set(container.getPullPushImageHandle(pullPushIdx));
        container.getPullPushTexture(pullPushIdx).bindImage(inputImageUnit, 0, GL_READ_WRITE, GL_R32F);

        if(pullPushIdx > 0) {
            container.getPullPushTexture(pullPushIdx - 1).bindImage(outputImageUnit, 0, GL_READ_WRITE, GL_R32F);
//            m_PushPass.uOutputImage.set(container.getPullPushImageHandle(pullPushIdx - 1));
        } else {
            container.getTexture(texIdx).bindImage(outputImageUnit, 0, GL_READ_WRITE, GL_R32F);
//            m_PushPass.uOutputImage.set(container.getImageHandle(texIdx));
        }

        m_PushPass.uTreshold.set(treshold / scale);
        m_PushPass.uInputSMResolution.set(s / 2);

        uint32_t blockSize = 32;
        uint32_t blockCount = outputTexSize / blockSize
                + (outputTexSize % blockSize != 0);

        glDispatchCompute(blockCount, blockCount, 1);

        glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

        pullPushIdx--;
        outputTexSize *= 2;
        scale *= 2.f;
    }

    glMemoryBarrier(GL_ALL_BARRIER_BITS);
}

GLISMDrawer::GLISMDrawer(const GLShaderManager& shaderManager):
    m_Program(shaderManager.buildProgram({ "imperfect_shadow_maps/draw_ism.vs", "imperfect_shadow_maps/draw_ism.fs" })) {
}

void GLISMDrawer::drawISMTexture(const GLISMContainer& container,
                    uint32_t texIdx, uint32_t texLvl, int textureUnit) {
    glDisable(GL_DEPTH_TEST);
    glActiveTexture(GL_TEXTURE0 + textureUnit);

    if(texLvl == 0) {
        glBindTexture(GL_TEXTURE_2D, container.getTexture(texIdx).glId());
    } else {
        glBindTexture(GL_TEXTURE_2D, container.getPullPushTexture(texLvl - 1).glId());
    }

    m_Program.use();

    uSMX.set(0.f);
    uSMY.set(0.f);
    uRes.set(1.f);
    uTexture.set(textureUnit);
    uTexIndex.set(texIdx);
    m_uISMData.smCountPerDimension.set(container.getShadowMapCountPerDimension());
    m_uISMData.smCountPerTexture.set(container.getShadowMapCountPerTexture());
    m_uISMData.smResolution.set(container.getShadowMapResolution());

    m_ScreenTriangle.render();
}

void GLISMDrawer::drawISM(const GLISMContainer& container,
                          uint32_t ismIdx, uint32_t texLvl, int textureUnit) {
    glDisable(GL_DEPTH_TEST);
    glActiveTexture(GL_TEXTURE0 + textureUnit);

    uint32_t smLocalIdx = ismIdx % container.getShadowMapCountPerTexture();
    uint32_t texIdx = (ismIdx - smLocalIdx) / container.getShadowMapCountPerTexture();

    uint32_t smX = smLocalIdx % container.getShadowMapCountPerDimension();
    uint32_t smY = (smLocalIdx - smX) / container.getShadowMapCountPerDimension();

    if(texLvl == 0) {
        glBindTexture(GL_TEXTURE_2D, container.getTexture(texIdx).glId());
    } else {
        glBindTexture(GL_TEXTURE_2D, container.getPullPushTexture(texLvl - 1).glId());
    }

    m_Program.use();

    float rcp = 1.f / container.getShadowMapCountPerDimension();

    uSMX.set((float) smX * rcp);
    uSMY.set((float) smY * rcp);
    uRes.set(rcp);
    uTexture.set(textureUnit);
    uTexIndex.set(texIdx);
    m_uISMData.smCountPerDimension.set(container.getShadowMapCountPerDimension());
    m_uISMData.smCountPerTexture.set(container.getShadowMapCountPerTexture());
    m_uISMData.smResolution.set(container.getShadowMapResolution());

    m_ScreenTriangle.render();
}

void GLISMDrawer::drawISM(const GLISMContainer& container,
                          uint32_t ismIdx, int textureUnit) {
    glDisable(GL_DEPTH_TEST);
    glActiveTexture(GL_TEXTURE0 + textureUnit);

    uint32_t smLocalIdx = ismIdx % container.getShadowMapCountPerTexture();
    uint32_t texIdx = (ismIdx - smLocalIdx) / container.getShadowMapCountPerTexture();

    uint32_t smX = smLocalIdx % container.getShadowMapCountPerDimension();
    uint32_t smY = (smLocalIdx - smX) / container.getShadowMapCountPerDimension();

    glBindTexture(GL_TEXTURE_2D, container.getTexture(texIdx).glId());

    m_Program.use();

    float rcp = 1.f / container.getShadowMapCountPerDimension();

    uSMX.set((float) smX * rcp);
    uSMY.set((float) smY * rcp);
    uRes.set(rcp);
    uTexture.set(textureUnit);
    uTexIndex.set(texIdx);
    m_uISMData.smCountPerDimension.set(container.getShadowMapCountPerDimension());
    m_uISMData.smCountPerTexture.set(container.getShadowMapCountPerTexture());
    m_uISMData.smResolution.set(container.getShadowMapResolution());

    m_ScreenTriangle.render();
}

}
