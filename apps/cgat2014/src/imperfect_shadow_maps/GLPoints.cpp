#include "GLPoints.hpp"

namespace BnZ {

GLPoints::GLPoints() {

}

void GLPoints::fill(uint32_t count, const GLPoint* points) {
    m_nCount = count;
    glBindBuffer(GL_ARRAY_BUFFER, m_PositionNormalBO.glId());
    glBufferData(GL_ARRAY_BUFFER, count * sizeof(points[0]), points, GL_STATIC_DRAW);

    glBindVertexArray(m_VAO.glId());

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(GLPoint), (const GLvoid*) BNZ_OFFSETOF(GLPoint, position));
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(GLPoint), (const GLvoid*) BNZ_OFFSETOF(GLPoint, normal));

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

void GLPoints::setColors(const glm::vec3* colors) {
    glBindBuffer(GL_ARRAY_BUFFER, m_ColorBO.glId());
    glBufferData(GL_ARRAY_BUFFER, m_nCount * sizeof(colors[0]), colors, GL_STATIC_DRAW);

    glBindVertexArray(m_VAO.glId());

    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), 0);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

void GLPoints::render() const {
    glBindVertexArray(m_VAO.glId());
    glDrawArrays(GL_POINTS, 0, m_nCount);
}

void GLPoints::render(uint32_t numInstances) const {
    glBindVertexArray(m_VAO.glId());
    glDrawArraysInstanced(GL_POINTS, 0, m_nCount, numInstances);
}

}
