#pragma once

#include <bonez/opengl/utils/GLutils.hpp>

namespace BnZ {

struct GLPoint {
    Vec3f position;
    Vec3f normal;
    float sqrRadius;

    GLPoint() {}
    GLPoint(Vec3f P, Vec3f N): position(P), normal(N), sqrRadius(0.f) {}
};

class GLPoints {
public:
    GLPoints();

    void fill(uint32_t count, const GLPoint* points);

    void setColors(const Vec3f* colors);

    void render() const;

    void render(uint32_t numInstances) const;

    uint32_t getSize() const {
        return m_nCount;
    }
private:
    uint32_t m_nCount;
    GLBufferObject m_PositionNormalBO;
    GLBufferObject m_ColorBO;
    GLVertexArrayObject m_VAO;
};

}
