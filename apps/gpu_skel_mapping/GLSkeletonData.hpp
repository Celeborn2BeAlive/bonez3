#pragma once

#include <bonez/scene/topology/CurvilinearSkeleton.hpp>
#include <bonez/types.hpp>
#include <bonez/opengl/utils/GLutils.hpp>
#include <bonez/opengl/cube_mapping/GLCubeMapContainer.hpp>
#include <bonez/opengl/cube_mapping/GLCubeMapRenderPass.hpp>

namespace BnZ {

template<uint32_t ChannelCount>
struct GLSkeletonData {
    GLCubeMapContainer<ChannelCount> m_NodeCubeMapContainer;
    GLImmutableBuffer<Mat4f> m_NodeViewMatrixBuffer;
    GLImmutableBuffer<Vec4f> m_NodePositionBuffer;
    GLImmutableBuffer<GLfloat> m_NodeRadiusBuffer;
    GLImmutableBuffer<Vec4f> m_NodeColorBuffer;
    uint32_t m_nNodeCount;

    size_t size() const {
        return m_nNodeCount;
    }

    void init(const CurvilinearSkeleton& skel, uint32_t cubeMapRes) {
        m_nNodeCount = skel.size();

        m_NodeCubeMapContainer = GLCubeMapContainer<ChannelCount>(cubeMapRes, m_nNodeCount);

//        m_NodeViewMatrixBuffer = genBufferStorage<Mat4f>(m_nNodeCount, nullptr, GL_MAP_WRITE_BIT, GL_READ_ONLY);
//        m_NodePositionBuffer = genBufferStorage<Vec4f>(m_nNodeCount, nullptr, GL_MAP_WRITE_BIT, GL_READ_ONLY);
//        m_NodeRadiusBuffer = genBufferStorage<GLfloat>(m_nNodeCount, nullptr, GL_MAP_WRITE_BIT, GL_READ_ONLY);
//        m_NodeColorBuffer = genBufferStorage<Vec4f>(m_nNodeCount, nullptr, GL_MAP_WRITE_BIT, GL_READ_ONLY);

//        auto pNodeViewMatrix = m_NodeViewMatrixBuffer.map(GL_MAP_WRITE_BIT);
//        auto pNodePosition = m_NodePositionBuffer.map(GL_MAP_WRITE_BIT);
//        auto pNodeRadius = m_NodeRadiusBuffer.map(GL_MAP_WRITE_BIT);
//        auto pNodeColor = m_NodeColorBuffer.map(GL_MAP_WRITE_BIT);

//        for(auto i = 0u; i < m_nNodeCount; ++i) {
//            auto n = skel.getNode(i);
//            *pNodePosition++ = Vec4f(n.P, 1);
//            *pNodeViewMatrix++ = getViewMatrix(n);
//            *pNodeRadius++ = n.maxball;
//            *pNodeColor++ = Vec4f(getColor(i), 1.f);
//        }

//        m_NodePositionBuffer.unmap();
//        m_NodeViewMatrixBuffer.unmap();
//        m_NodeRadiusBuffer.unmap();
//        m_NodeColorBuffer.unmap();
    }
};

struct GLSkelDataUniforms {
    GLUniform<GLuint> nodeCount;
    GLUniform<GLBufferAddress<Mat4f>> nodeViewMatrixBuffer;
    GLUniform<GLBufferAddress<GLfloat>> nodeRadiusBuffer;
    GLUniform<Mat4f[]> faceProjMatrices;
    GLUniform<GLfloat> depthBias;
    GLUniform<GLTextureCubeMapArrayHandle> nodeDepthBuffer;

    GLSkelDataUniforms(const GLProgram& program):
        nodeCount(program, "uSkelData.nodeCount"),
        nodeViewMatrixBuffer(program, "uSkelData.nodeViewMatrixBuffer"),
        nodeRadiusBuffer(program, "uSkelData.nodeRadiusBuffer"),
        faceProjMatrices(program, "uSkelData.faceProjMatrices"),
        depthBias(program, "uSkelData.depthBias"),
        nodeDepthBuffer(program, "uSkelData.nodeDepthBuffer") {
    }

    template<uint32_t ChannelCount>
    void set(const GLSkeletonData<ChannelCount>& skelData, float fDepthBias) {
        nodeCount.set(skelData.size());
        nodeViewMatrixBuffer.set(skelData.m_NodeViewMatrixBuffer.getGPUAddress());
        nodeRadiusBuffer.set(skelData.m_NodeRadiusBuffer.getGPUAddress());
        faceProjMatrices.set(6, skelData.m_NodeCubeMapContainer.getFaceProjMatrices());
        depthBias.set(fDepthBias);
        nodeDepthBuffer.set(skelData.m_NodeCubeMapContainer.getDepthMapArray().getTextureHandle());
    }
};

}
