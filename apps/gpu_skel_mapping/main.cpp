#include <iostream>

#include "SceneViewer.hpp"

namespace BnZ {

int main(int argc, char** argv) {
    if(argc != 2) {
        std::cerr << "Usage: " << argv[0] << " < XML configuration filepath >" << std::endl;
        return -1;
    }

    SceneViewer viewer(argv[0], argv[1]);
    viewer.run();

	return 0;
}

}

int main(int argc, char** argv) {
#ifdef _DEBUG
        std::cerr << "DEBUG MODE ACTIVATED" << std::endl;
#endif

#ifdef DEBUG
        std::cerr << "DEBUG MODE ACTIVATED" << std::endl;
#endif
    return BnZ::main(argc, argv);
}
