#pragma once

#include <bonez/viewer/Viewer.hpp>

#include <bonez/opengl/GLGBuffer.hpp>
#include <bonez/opengl/GLGBufferRenderPass.hpp>
#include <bonez/opengl/GLFlatShadingPass.hpp>
#include <bonez/opengl/GLScene.hpp>
#include <bonez/opengl/debug/GLDebugRenderer.hpp>
#include <bonez/opengl/debug/GLDebugOutputStream.hpp>
#include <bonez/opengl/GLScreenFramebuffer.hpp>

#include <bonez/maths/maths.hpp>

#include "SkelNodeDebugModule.hpp"

#include "GLSkeletonData.hpp"
#include "GLSkeletonMappingRenderPass.hpp"

namespace BnZ {

/***
 * Potentionally produce GL_OUT_OF_MEMORY, depending on the number of node of the processed skeleton.
 * Better strategy should be used to perform GPU skel mapping.
 */

class SceneViewer: public Viewer {
public:
    GLGBuffer m_GBuffer;
    Unique<GLScene> m_pGLScene;
    GLGBufferRenderPass m_GBufferRenderPass;
    GLFlatShadingPass m_FlatShadingPass;

    Shared<GLDebugRenderer> m_pGLDebugRenderer;
    GLImageRenderer m_GLImageRenderer;

    GLScreenTriangle m_ScreenTriangle;

    GLScreenFramebuffer m_GLFramebuffer;

    // On window viewports
    Vec4f m_FinalRenderViewport;
    Vec2f m_GBufferTexScreenSize;
    Vec4f m_GBufferViewport;
    Vec4f m_SideViewport;

    TwBar* m_pBar = nullptr;

    Vec4u m_SelectedObjectID = GLDebugRenderer::NULL_ID;
    Intersection m_PickedIntersection;
    Vec2u m_SelectedPixel;

    SkelNodeDebugModule m_SkelNodeDebugModule;

    enum { TOPO_NONE, TOPO_CURVSKEL, TOPO_SEGCURVSKEL } m_TopologyDisplayMode = TOPO_SEGCURVSKEL;
    bool m_bHighlightSkeleton = false;

    Shared<const CurvilinearSkeleton> m_pSkel;

    static const uint32_t ChannelCount = 1;

    GLSkeletonData<ChannelCount> m_GLSkelData;
    GLCubeMapRenderPass m_CubeMapRenderPass;

    uint32_t m_nNodeMapRes = 128;
    Vec2u m_SphereMapSize = Vec2u(256, 128);
    GLTexture2DArray m_SphereMaps[ChannelCount];

    GLSkeletonMappingRenderPass m_SkelMappingRenderPass;

    float m_fSMRenderTime = 0;
    float m_fSMBias = 0.001f;
    uint32_t m_nRequestedNode = UNDEFINED_NODE;
    uint32_t m_nComputeNodeWeightSubIndex = 0;
    bool m_bSwitch = true;
    uint32_t m_ChannelIdx = 0;
    bool m_bDisplayWeight = false;

    std::string m_ScreenshotPrefix = toString(getMicroseconds());
    bool m_bDoScreenshot = false;

    SceneViewer(const FilePath& applicationPath, const FilePath& settingsFilePath):
        Viewer("GPU Skeleton Mapping", applicationPath, settingsFilePath),
        m_GBufferRenderPass(m_ShaderManager),
        m_FlatShadingPass(m_ShaderManager),
        m_pGLDebugRenderer(makeShared<GLDebugRenderer>(m_ShaderManager)),
        m_GLImageRenderer(m_ShaderManager),
        m_SkelNodeDebugModule(m_ShaderManager),
        m_CubeMapRenderPass(m_ShaderManager),
        m_SkelMappingRenderPass(m_ShaderManager) {
    }

    void initGUI() {
        m_pBar = TwNewBar("Render Settings");

        // Scene rendering
        m_FlatShadingPass.exposeIO(m_pBar);

        atb::addSeparator(m_pBar);

        atb::addButton(m_pBar, "Screenshot", [this]() {
            m_bDoScreenshot = true;
        });

        atb::addSeparator(m_pBar);

        // Skeleton display
        atb::addVarRW(m_pBar, "Topology display mode", "None,CurvSkel,SegmentedCurvSkel",
                      m_TopologyDisplayMode);
        atb::addVarRW(m_pBar, ATB_VAR(m_bHighlightSkeleton));

        atb::addSeparator(m_pBar);

        // Selected node
        m_SkelNodeDebugModule.exposeIO(m_pBar);

        atb::addSeparator(m_pBar);

        atb::addButton(m_pBar, "Add Point Light", [&]() {
            m_pScene->addLight(makeShared<PointLight>(m_SkelNodeDebugModule.m_Node.P, zero<Vec3f>()));
        });
        atb::addButton(m_pBar, "Add Diffuse Surface Point Light", [&]() {
            m_pScene->addLight(makeShared<DiffuseSurfacePointLight>(m_SkelNodeDebugModule.m_Node.P, Vec3f(0, 1, 0), zero<Vec3f>()));
        });

        // Skeleton mapping
        auto pSkelMappingBar = TwNewBar("Skeleton Mapping");
        atb::addVarRO(pSkelMappingBar, "Node Map Render Time (ms)", m_fSMRenderTime);
        atb::addVarRW(pSkelMappingBar, "SM Bias", m_fSMBias);
        atb::addVarRO(pSkelMappingBar, "Requested Node", m_nRequestedNode);
        atb::addVarRW(pSkelMappingBar, "Compute Node Weight", m_nComputeNodeWeightSubIndex);
        atb::addVarRW(pSkelMappingBar, "Cube/Sphere Map", m_bSwitch);
        atb::addVarRW(pSkelMappingBar, ATB_VAR(m_ChannelIdx));
        atb::addVarRW(pSkelMappingBar, ATB_VAR(m_bDisplayWeight));
        atb::addVarRW(pSkelMappingBar, ATB_VAR(m_nNodeMapRes));
        atb::addVarRW(pSkelMappingBar, ATB_VAR(m_SphereMapSize.x));
        atb::addVarRW(pSkelMappingBar, ATB_VAR(m_SphereMapSize.y));
        atb::addButton(pSkelMappingBar, "Compute Maps", [this] {
            computeNodeMaps();
        });
    }

    void computeNodeMaps() {
        GLQuery timeQuery;
        {
            GLTimerScope timer(timeQuery);

            m_GLSkelData.init(*m_pSkel, m_nNodeMapRes);

            m_CubeMapRenderPass.render(*m_pGLScene,
                                       m_GLSkelData.m_NodeCubeMapContainer,
                                       m_GLSkelData.m_NodeViewMatrixBuffer.getGPUAddress(),
                                       m_pSkel->size(),
                                       m_ZNearFar.x, m_ZNearFar.y);

            // Conversion
            GLImage2DArrayHandle sphereMapImageHandles[ChannelCount];

            m_GLSkelData.m_NodeCubeMapContainer.makeResident();
            for (auto i = 0u; i < ChannelCount; ++i) {
                m_SphereMaps[i] = GLTexture2DArray();
                m_SphereMaps[i].setStorage(1, GL_RGBA32F,
                                           m_SphereMapSize.x, m_SphereMapSize.y,
                                           m_pSkel->size());

                m_SphereMaps[i].setMinFilter(GL_LINEAR);
                m_SphereMaps[i].setMagFilter(GL_LINEAR);
                m_SphereMaps[i].makeTextureHandleResident();
                sphereMapImageHandles[i] = m_SphereMaps[i].getImageHandle(0, GL_TRUE, 0, GL_RGBA32F);
                sphereMapImageHandles[i].makeResident(GL_WRITE_ONLY);
            }

            m_CubeMapRenderPass.convertToSphericalMaps(
                        m_GLSkelData.m_NodeCubeMapContainer,
                        sphereMapImageHandles,
                        m_SphereMapSize.x, m_SphereMapSize.y);

            m_GLSkelData.m_NodeCubeMapContainer.makeNonResident();

            for(auto handle: sphereMapImageHandles) {
                handle.makeNonResident();
            }
        }
        m_fSMRenderTime = ns2ms(timeQuery.waitResult());
    }

    void setUp() override {
        initGUI();

        m_GBuffer.init(m_Settings.m_FramebufferSize);
        m_pGLScene = makeUnique<GLScene>(m_pScene->getGeometry());

        m_GLFramebuffer.init(m_Settings.m_FramebufferSize);

        //m_pGLDebugRenderer->setOutputColorTexture(m_GLFramebuffer.getColorBuffer(0));

        // Setup the viewports depending on the window size and the framebuffer size
        float W = 0.75 * m_Settings.m_WindowSize.x;

        m_GBufferTexScreenSize.x = W / 5;
        m_GBufferTexScreenSize.y = m_GBufferTexScreenSize.x / m_Settings.m_fFramebufferRatio;

        m_GBufferViewport = Vec4f(0, 0, W, m_GBufferTexScreenSize.y);

        Vec4f mainViewport(0, m_GBufferTexScreenSize.y, W, m_Settings.m_WindowSize.y - m_GBufferTexScreenSize.y);

        m_FinalRenderViewport =
                Vec4f(mainViewport.x + 0.5 * (mainViewport.z - m_Settings.m_FramebufferSize.x),
                          mainViewport.y + 0.5 * (mainViewport.w - m_Settings.m_FramebufferSize.y),
                          m_Settings.m_FramebufferSize);

        m_SideViewport = Vec4f(mainViewport.x + mainViewport.z,
                                   mainViewport.y,
                                   m_Settings.m_WindowSize.x - mainViewport.z,
                                   mainViewport.w);

        // Compute segmented skeleton maps
//        m_pSkel = m_pScene->getSegmentedCurvSkeleton();
        m_pSkel = m_pScene->getCurvSkeleton();

        if(!m_pSkel) {
            throw std::runtime_error("No Segmented Skeleton for the scene");
        }

        computeNodeMaps();
    }

    void tearDown() override {
        // Do nothing
    }

    void drawGBuffer() {
        glViewport(m_GBufferViewport.x, m_GBufferViewport.y,
                   m_GBufferTexScreenSize.x, m_GBufferTexScreenSize.y);
        m_GLImageRenderer.drawNormalTexture(m_GBuffer);

        glViewport(m_GBufferViewport.x + m_GBufferTexScreenSize.x, m_GBufferViewport.y,
                   m_GBufferTexScreenSize.x, m_GBufferTexScreenSize.y);
        m_GLImageRenderer.drawDepthTexture(m_GBuffer);

        glViewport(m_GBufferViewport.x + 2 * m_GBufferTexScreenSize.x, m_GBufferViewport.y,
                   m_GBufferTexScreenSize.x, m_GBufferTexScreenSize.y);;
        m_GLImageRenderer.drawDiffuseTexture(m_GBuffer);

        glViewport(m_GBufferViewport.x + 3 * m_GBufferTexScreenSize.x, m_GBufferViewport.y,
                   m_GBufferTexScreenSize.x, m_GBufferTexScreenSize.y);
        m_GLImageRenderer.drawGlossyTexture(m_GBuffer);

        glViewport(m_GBufferViewport.x + 4 * m_GBufferTexScreenSize.x, m_GBufferViewport.y,
                   m_GBufferTexScreenSize.x, m_GBufferTexScreenSize.y);
        m_GLImageRenderer.drawShininessTexture(m_GBuffer);
    }

    void drawFinalRender() {
        m_GLFramebuffer.bindForReading();
        m_GLFramebuffer.setReadBuffer(0);

        glBlitFramebuffer(0, 0, m_GBuffer.getWidth(), m_GBuffer.getHeight(),
                          m_FinalRenderViewport.x, m_FinalRenderViewport.y, m_FinalRenderViewport.x + m_FinalRenderViewport.z, m_FinalRenderViewport.y + m_FinalRenderViewport.w,
                          GL_COLOR_BUFFER_BIT, GL_NEAREST);

        glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
    }

    void drawCurvSkel(GLDebugOutputStream& stream) {
        if(m_TopologyDisplayMode == TOPO_NONE) {
            return;
        }

        Shared<const CurvilinearSkeleton> pSkel;
        uint32_t objectTypeID = 0;

        if(m_TopologyDisplayMode == TOPO_CURVSKEL) {
            pSkel = m_pScene->getCurvSkeleton();
            objectTypeID = SKEL_NODE_ID;
        } else {
//            pSkel = m_pScene->getSegmentedCurvSkeleton();
            objectTypeID = SEG_SKEL_NODE_ID;
        }

        if(pSkel) {
            float lineWidth = 2.f;
            std::function<Vec3f(uint32_t)> getNodeColor = [](uint32_t i) {
                return getColor(i);
            };
            float scale = 0.0005f;

            if(m_bHighlightSkeleton) {
                lineWidth = 5.f;
                getNodeColor = [](uint32_t i) { return Vec3f(1, 0, 0); };
                scale = 0.001f;
            }

            for(auto i = 0u; i < pSkel->size(); ++i) {
                auto node = pSkel->getNode(i);
                stream.addSphere(node.P, m_ZNearFar.y * scale,
                                 getNodeColor(i), Vec4u(objectTypeID, i, 0, 0));

                for(auto j: pSkel->neighbours(i)) {
                    auto neighbour = pSkel->getNode(j);
                    stream.addLine(node.P, neighbour.P,
                                   getNodeColor(i), getNodeColor(j), lineWidth);
                }
            }
        }
    }

    void setSelectedPixel(const Vec2u& pixel) {
        m_SelectedPixel = pixel;
    }

    float getArrowLength() {
        return m_ZNearFar.y * 0.005f;
    }

    void doPicking(GLDebugOutputStream& stream) {
        if(m_WindowManager.hasClicked()) {
            m_SelectedObjectID = m_GLFramebuffer.getObjectID(m_WindowManager.getCursorPosition(Vec2f(m_FinalRenderViewport)));

            if(m_SelectedObjectID == GLDebugRenderer::NULL_ID) {
                auto ndcPosition = m_WindowManager.getCursorNDC(m_FinalRenderViewport);
                if(viewportContains(ndcPosition, Vec4f(-1, -1, 2, 2))) {
                    m_PickedIntersection =
                            m_pScene->intersect(m_Camera.getRay(ndcPosition));
                }
            } else {
                m_SkelNodeDebugModule.setObject(m_SelectedObjectID, *m_pScene);
            }

            auto pixel = m_WindowManager.getCursorPosition(Vec2f(m_FinalRenderViewport));
            setSelectedPixel(pixel);
        }

        if (m_PickedIntersection) {
            stream.addArrow(m_PickedIntersection.P, m_PickedIntersection.Ns, getArrowLength(), 0.5f * getArrowLength(), Vec3f(1.f));
        }
    }

    void drawSideView() {
        auto objectID = m_SelectedObjectID;
        if(objectID.x != SEG_SKEL_NODE_ID) {
            return;
        }

        glViewport(m_SideViewport.x, m_SideViewport.y, m_SideViewport.z, m_SideViewport.z * 0.5f);

        if (m_bSwitch) {
            GLResidentTextureScope scope(m_GLSkelData.m_NodeCubeMapContainer.getMapArray(m_ChannelIdx));
            m_CubeMapRenderPass.drawMap(m_GLSkelData.m_NodeCubeMapContainer, objectID.y, m_ChannelIdx, m_ZNearFar.x, m_ZNearFar.y, m_ScreenTriangle);
        }
        else {
            if (m_ChannelIdx == m_GLSkelData.m_NodeCubeMapContainer.getDepthMapChannelIndex()) {
                m_GLImageRenderer.drawDepthTexture(objectID.y, m_SphereMaps[m_ChannelIdx], m_ZNearFar.x, m_ZNearFar.y);
            }
            else {
                m_GLImageRenderer.drawTexture(objectID.y, m_SphereMaps[m_ChannelIdx]);
            }
        }
    }

    void doScreenshot() {
        FilePath screenshotsPath = m_Path + "screenshots";
        createDirectory(screenshotsPath);

        FilePath baseName = m_ScreenshotPrefix + "screenshot." + toString(getMicroseconds());
        FilePath pngFile = screenshotsPath + baseName.addExt(".png");

        Image image;
        fillImage(image, m_GLFramebuffer.getColorBuffer());
        image.flipY();

        storeImage(pngFile, image);
    }

    void drawFrame() override {
        auto debugStream = newGLDebugStream(m_pGLDebugRenderer, "default");

        m_GBufferRenderPass.render(m_Camera.getProjMatrix(), m_Camera.getViewMatrix(),
                                   m_ZNearFar.y, *m_pGLScene, m_GBuffer);

        m_GLFramebuffer.bindForDrawing();
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        m_GBuffer.setShadingViewport();

        m_FlatShadingPass.render(m_GBuffer, m_Camera.getRcpProjMatrix(),
                                 m_Camera.getViewMatrix(), m_ScreenTriangle);

        m_GLFramebuffer.blitFramebuffer(m_GBuffer, GL_DEPTH_BUFFER_BIT, GL_NEAREST);

        auto objectID = m_SelectedObjectID;
        int selectedNode = -1;
        if(objectID.x == SEG_SKEL_NODE_ID) {
            selectedNode = objectID.y;
        }

        glEnable(GL_BLEND);

        glBlendEquation(GL_FUNC_ADD);
        glBlendFunc(GL_ZERO, GL_SRC_COLOR);

        m_SkelMappingRenderPass.render(m_GBuffer,
                                       m_Camera,
                                       m_ScreenTriangle,
                                       m_GLSkelData,
                                       m_fSMBias, selectedNode,
                                       m_PickedIntersection,
                                       m_nRequestedNode,
                                       m_nComputeNodeWeightSubIndex,
                                       m_bDisplayWeight);

        glDisable(GL_BLEND);

        if(m_PickedIntersection && m_nRequestedNode < m_pSkel->size()) {
            m_pGLDebugRenderer->addLine(
                        "default",
                        m_PickedIntersection.P.xyz(),
                        m_pSkel->getNode(m_nRequestedNode).P,
                        Vec3f(1, 0, 0),
                        Vec3f(1, 0, 0),
                        5);
        }

        drawCurvSkel(debugStream);

        m_SkelNodeDebugModule.drawMaxball(debugStream);

        m_pGLDebugRenderer->render(m_Camera);

        doPicking(debugStream);

        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
        glClear(GL_COLOR_BUFFER_BIT);

        drawGBuffer();
        drawFinalRender();
        drawSideView();

        if(m_bDoScreenshot) {
            doScreenshot();
            m_bDoScreenshot = false;
        }
    }
};

}
