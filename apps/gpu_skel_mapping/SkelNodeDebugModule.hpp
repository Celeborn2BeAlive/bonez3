#pragma once

#include <bonez/viewer/Viewer.hpp>
#include <bonez/opengl/cube_mapping/GLCubeMapRenderPass.hpp>

namespace BnZ {

enum {
    NULL_ID = 0,
    SKEL_NODE_ID = 1,
    SEG_SKEL_NODE_ID = 2
};

struct SkelNodeDebugModule {
    uint32_t m_nIndex = UNDEFINED_NODE;
    CurvilinearSkeleton::Node m_Node;
    Col3f m_NodeColor;
    bool m_bDisplayMaxball = false;
    bool m_bDisplayDepthMap = true;

    GLCubeMapContainer<1> m_SMC;
    GLCubeMapRenderPass m_SMRP;

    SkelNodeDebugModule(const GLShaderManager& shaderManager):
        m_SMC(256, 1),
        m_SMRP(shaderManager) {
    }

    bool setObject(const Vec4u& objectID, const Scene& scene) {
        Shared<const CurvilinearSkeleton> pSkel;
        if(objectID.x == SKEL_NODE_ID) {
            pSkel = scene.getCurvSkeleton();
        } else if(objectID.x == SEG_SKEL_NODE_ID) {
//            pSkel = scene.getSegmentedCurvSkeleton();
        }
        if(!pSkel) {
            return false;
        }

        m_nIndex = objectID.y;
        m_Node = pSkel->getNode(m_nIndex);
        m_NodeColor = getColor(m_nIndex);

        return true;
    }

    void drawMaxball(GLDebugOutputStream& stream) {
        if(m_bDisplayMaxball && m_nIndex != UNDEFINED_NODE) {
            stream.addSphere(m_Node.P, m_Node.maxball, getColor(m_nIndex));
        }
    }

    void drawDepthMap(const GLScene& scene,
                      float zNear, float zFar, const GLScreenTriangle& triangle) {
        if(m_bDisplayDepthMap && m_nIndex != UNDEFINED_NODE) {
            Mat4f viewMatrix = getViewMatrix(m_Node);
            auto viewMatrixBuffer = genBufferStorage(1, &viewMatrix, 0, GL_READ_ONLY);

            m_SMRP.render(scene, m_SMC, viewMatrixBuffer.getGPUAddress(), 1, zNear, zFar);

            GLResidentTextureScope scope(m_SMC.getMapArray(0));
            m_SMRP.drawMap(m_SMC, 0, 0, zNear, zFar, triangle);
        }
    }

    void exposeIO(TwBar* bar) {
        atb::addVarRO(bar, ATB_VAR(m_nIndex));
        atb::addVarRO(bar, ATB_VAR(m_Node.P.x));
        atb::addVarRO(bar, ATB_VAR(m_Node.P.y));
        atb::addVarRO(bar, ATB_VAR(m_Node.P.z));
        atb::addVarRO(bar, ATB_VAR(m_Node.maxball));
        atb::addVarRO(bar, ATB_VAR(m_NodeColor));
        atb::addVarRW(bar, ATB_VAR(m_bDisplayMaxball));
        atb::addVarRW(bar, ATB_VAR(m_bDisplayDepthMap));
    }
};


}
