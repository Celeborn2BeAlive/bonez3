#version 430 core
#extension GL_NV_gpu_shader5 : enable
#extension GL_NV_bindless_texture : enable

struct GBuffer {
    sampler2D normalDepthSampler;
    sampler2D diffuseSampler;
    sampler2D glossyShininessSampler;
};

uniform GBuffer uGBuffer;
uniform mat4 uRcpViewMatrix;

uniform vec4* uNodeColorBuffer;

uniform bool uDisplayWeight;

uniform int uSelectedNode;

uniform bool uRequestNode;
uniform vec3 uRequestedP;
uniform vec3 uRequestedN;
coherent uniform uint* uRequestedNode;

int computeNearestNode(vec4 P_ws, vec3 N_ws);
ivec4 computeNearestNodes(vec4 P_ws, vec3 N_ws, out vec4 weights);
vec4 getNodesColor(ivec4 nodes);
float computeWeight(int nodeIdx, vec4 P_ws, vec3 N_ws);

in vec3 vFarPoint_vs;

out vec3 fColor;

void main() {
    ivec2 pixelCoords = ivec2(gl_FragCoord.xy);

    // Reconstruct fragment position from depth
    vec4 normalDepth = texelFetch(uGBuffer.normalDepthSampler, pixelCoords, 0);
    vec3 P_vs = normalDepth.w * vFarPoint_vs;

    vec4 P_ws = uRcpViewMatrix * vec4(P_vs, 1);

    if(uRequestNode) {
        *uRequestedNode = computeNearestNode(vec4(uRequestedP, 1), uRequestedN);
    }

    int bestMatch = computeNearestNode(P_ws, normalDepth.xyz);

    if(uSelectedNode < 0) {
        if(bestMatch >= 0) {
            fColor = uNodeColorBuffer[bestMatch].rgb;
        }
    } else {
        if(uDisplayWeight) {
            fColor = computeWeight(uSelectedNode, P_ws, normalDepth.xyz) * uNodeColorBuffer[uSelectedNode].rgb;
        } else {
            if(bestMatch != uSelectedNode) {
                discard;
            } else {
                fColor = uNodeColorBuffer[bestMatch].rgb;
            }
        }
    }
}
