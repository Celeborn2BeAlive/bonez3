#include "VoxelOpeningSegmentationGraphPathtraceRenderer.hpp"

#include <bonez/scene/shading/BSDF.hpp>
#include <bonez/voxskel/CubicalComplex3D.hpp>

#include <bonez/scene/sensors/PixelSensor.hpp>
#include <bonez/scene/sensors/SurfacePointCamera.hpp>

#include <bonez/opengl/debug/GLDebugRenderer.hpp>

namespace BnZ {

void VoxelOpeningSegmentationGraphPathtraceRenderer::preprocess() {
    m_LightSampler.initFrame(getScene());

    m_pPointLight = nullptr;

    getScene().getLightContainer().forEach<PointLight>([&](std::size_t index, const PointLight& light) {
       if(!m_pPointLight) {
           m_pPointLight = &light;
       }
    });

    if(!m_pPointLight) {
        throw std::runtime_error("No PointLight in scene");
    }

    auto voxel = m_OpeningGraph.getVoxel(m_pPointLight->m_Position);
    auto lightClusterIdx = m_OpeningGraph.getVoxelCluster(voxel);

    m_ShortestPaths = computeDijkstraShortestPaths(m_OpeningGraph.getGraph(),
                                                   lightClusterIdx,
                                                   [&](GraphNodeIndex n1, GraphNodeIndex n2) {
                                                        return 1.f;
                                                   });

    m_PortalRTScene = RTScene();
    m_RTScene = RTScene();
	m_PortalMeshs.clear();

	for (const auto portalIdx : range(m_OpeningGraph.getPortals().size())) {
        m_PortalMeshs.emplace_back(buildPortalMesh(m_OpeningGraph, portalIdx));
	}

    for (const auto& mesh : m_PortalMeshs) {
        m_PortalRTScene.addTriangleMesh(mesh);
    }
    m_PortalRTScene.commit();

    m_RTScene.addInstance(getScene().getRTScene());
    m_RTScene.addInstance(m_PortalRTScene);
    m_RTScene.commit();


}

void VoxelOpeningSegmentationGraphPathtraceRenderer::beginFrame() {
    m_SampledRays.clear();
    m_SampledRays.resize(getFramebuffer().getPixelCount());
}

Sample<TriangleMesh::Vertex> VoxelOpeningSegmentationGraphPathtraceRenderer::samplePortalVertex(std::size_t portalIdx, std::size_t threadID) const{
	// Sampling in grid space:
	//const auto& portals = m_OpeningGraph.getPortals();
	//auto faceCount = portals[portalIdx].size();
	//auto faceSample = min(size_t(getFloat(threadID) * faceCount), faceCount - 1);
	//// First attempt, just trace through the center of the voxel face
	//Vec3i u, v;
	//getFaceVectors(CC3DFaceBits::Value(portals[portalIdx][faceSample].m_VoxelFace.w), u, v);

	//auto uWs = Vec3f(m_OpeningGraph.getGridToWorldMatrix() * Vec4f(Vec3f(u), 0.f));
	//auto vWs = Vec3f(m_OpeningGraph.getGridToWorldMatrix() * Vec4f(Vec3f(v), 0.f));

	//auto pdf = 1.f / (faceCount * length(uWs) * length(vWs));

	//auto texCoords = Vec2f(getFloat(threadID), getFloat(threadID));

	//auto faceCenter = Vec3f(portals[portalIdx][faceSample].m_VoxelFace) + Vec3f(u) * texCoords.x + Vec3f(v) * texCoords.y;
	//auto faceCenterWorldSpace = Vec3f(m_OpeningGraph.getGridToWorldMatrix() * Vec4f(faceCenter, 1.f));
	//auto faceNormal = normalize(Vec3f(m_OpeningGraph.getGridToWorldMatrix() * Vec4f(Vec3f(portals[portalIdx][faceSample].m_OutgoingNormal), 0.f)));

	//return Sample<TriangleMesh::Vertex>(
	//	TriangleMesh::Vertex(faceCenterWorldSpace, faceNormal, texCoords), pdf);

	return sample(m_PortalMeshs[portalIdx], getFloat(threadID), getFloat2(threadID));
}

float VoxelOpeningSegmentationGraphPathtraceRenderer::pdfPortalVertex(std::size_t portalIdx, std::size_t triangleIdx) const {
    return 1.f / (m_PortalMeshs[portalIdx].getTriangleArea(triangleIdx) * m_PortalMeshs[portalIdx].getTriangleCount());
}

void VoxelOpeningSegmentationGraphPathtraceRenderer::processSampleSurfacePointCamera(
        const Ray& ray,
        uint32_t pixelID,
        const SurfacePointCamera* pCamera) const {
    if(pCamera) {
        const auto& camera = *pCamera;
        accumulate(PRIMARY_RAY_PDF_WRT_BRDF, pixelID, Vec4f(Vec3f(cosineSampleHemispherePDF(ray.dir, camera.getSurfacePoint().Ns)), 0.f));

        // Identify the portal to sample
        int portalIdx = -1;

        auto voxel = m_OpeningGraph.getVoxel(camera.getSurfacePoint());
        auto openingClusterIdx = m_OpeningGraph.getVoxelCluster(voxel);
        if(openingClusterIdx != VoxelOpeningSegmentationGraph::NO_CLUSTER()) {
            const auto& cluster = m_OpeningGraph.getCluster(openingClusterIdx);

            auto dijkstraNode = m_ShortestPaths[openingClusterIdx];

            int neighborIdx = -1;
            for(auto i = 0u; i < m_OpeningGraph.neighbours(openingClusterIdx).size(); ++i) {
                if(dijkstraNode.predecessor == m_OpeningGraph.neighbours(openingClusterIdx)[i]) {
                    neighborIdx = i;
                    break;
                }
            }

            if(neighborIdx >= 0) {
                auto portalSample = neighborIdx;
                portalIdx = cluster.m_PortalGlobalIndices[portalSample];
            }
        }

        float pdfWrtSolidAngle = 0.f;
        auto filter = [&](const RTScene::Hit& hit, uint32_t threadID) {
            if(int(hit.m_nMeshID) == portalIdx) {
                auto portalPdf = pdfPortalVertex(portalIdx, hit.m_nTriangleID);
                pdfWrtSolidAngle = distanceSquared(hit.m_P, camera.getSurfacePoint().P) * portalPdf / abs(dot(ray.dir, hit.m_Ng));
            }
            return true;
        };

        RTScene::Hit hit;
        m_PortalRTScene.intersect(ray, hit, filter);

        accumulate(PRIMARY_RAY_PDF_WRT_SEG, pixelID, Vec4f(Vec3f(pdfWrtSolidAngle), 0.f));
    }
}

void VoxelOpeningSegmentationGraphPathtraceRenderer::processSample1(uint32_t threadID, uint32_t pixelID, uint32_t sampleID, uint32_t x, uint32_t y) const {
    for(auto i = 0u; i < getFramebufferChannelCount(); ++i) {
        accumulate(i, pixelID, Vec4f(0.f, 0.f, 0.f, 1.f));
    }

    PixelSensor sensor(getSensor(), Vec2u(x, y), getFramebufferSize());

    auto pixelSample = getPixelSample(threadID, sampleID);
    auto lensSample = getFloat2(threadID);

    RaySample raySample;
    float positionPdf, directionPdf;
    sensor.sampleExitantRay(
                getScene(),
                lensSample,
                pixelSample,
                raySample,
                positionPdf,
                directionPdf);

    auto ray = raySample.value;

    if(!ray) {
        return;
    }

    processSampleSurfacePointCamera(ray, pixelID, dynamic_cast<const SurfacePointCamera*>(&getSensor()));

    Vec3f throughput = Vec3f(1.f);
    Vec3f L = Vec3f(0.f);

    Intersection I = getScene().intersect(ray);
    Vec3f wo = -ray.dir;

    if(acceptPathDepth(1)) {
        accumulate(FINAL_RENDER_DEPTH1, pixelID, Vec4f(I.Le, 0.f));
        L += I.Le;
    }

    uint32_t sampledEvent = ScatteringEvent::Emission;

    for(auto i = 1u; I && i <= m_nMaxPathDepth; ++i) {
        BSDF bsdf(wo, I, getScene());
        // Next event estimation
        if(i < m_nMaxPathDepth && acceptPathDepth(i + 1)) {
            RaySample shadowRay;

            auto Le = m_pPointLight->sampleDirectIllumination(getScene(), getFloat2(threadID), I, shadowRay);
            if(Le != zero<Vec3f>() && shadowRay.pdf) {
                float cosThetaOutDir;
                auto fr = bsdf.eval(shadowRay.value.dir, cosThetaOutDir);

                if(fr != zero<Vec3f>() && !getScene().occluded(shadowRay.value)) {
                    auto contrib = throughput * Le * fr * cosThetaOutDir / shadowRay.pdf;
                    accumulate(FINAL_RENDER_DEPTH1 + i, pixelID, Vec4f(contrib, 0.f));
                    if(i > 1u) {
                        if(sampledEvent == ScatteringEvent::Other) {
                            accumulate(PORTAL_CONTRIB, pixelID, Vec4f(contrib, 0.f));
                        } else {
                            accumulate(BSDF_CONTRIB, pixelID, Vec4f(contrib, 0.f));
                        }
                    }
                    L += contrib;
                }
            }
        }

        // If the scattering was specular, add Le
        if((sampledEvent & ScatteringEvent::Specular) && acceptPathDepth(i)) {
            auto contrib = throughput * I.Le;
            accumulate(FINAL_RENDER_DEPTH1 + i - 1, pixelID, Vec4f(contrib, 0.f));
            L += contrib;
        }

        if(i < m_nMaxPathDepth - 1 || bsdf.hasSpecularComponent()) {
            // Identify the portal to sample
            int portalIdx = -1;

            auto voxel = m_OpeningGraph.getVoxel(I, bsdf.getIncidentDirection());
            auto openingClusterIdx = m_OpeningGraph.getVoxelCluster(voxel);
            if(openingClusterIdx != VoxelOpeningSegmentationGraph::NO_CLUSTER()) {
                const auto& cluster = m_OpeningGraph.getCluster(openingClusterIdx);

                auto dijkstraNode = m_ShortestPaths[openingClusterIdx];

                int neighborIdx = -1;
                for(auto i = 0u; i < m_OpeningGraph.neighbours(openingClusterIdx).size(); ++i) {
                    if(dijkstraNode.predecessor == m_OpeningGraph.neighbours(openingClusterIdx)[i]) {
                        neighborIdx = i;
                        break;
                    }
                }

                if(neighborIdx >= 0) {
                    auto portalSample = neighborIdx;
                    portalIdx = cluster.m_PortalGlobalIndices[portalSample];
                }
            }

            Sample<TriangleMesh::Vertex> vertexSample;
            float useSegProb = 0.f;
            float dotAtPortal = 0.f;
            float distToSampledPoint = 0.f;
            float sqrDistToSampledPoint = 0.f;
            Vec3f dirIntersectionToPoint = zero<Vec3f>();
            float cosThetaOutDir = 0.f;
            Vec3f fr = zero<Vec3f>();

            if(!bsdf.isDelta() && portalIdx >= 0) {
                vertexSample = samplePortalVertex(portalIdx, threadID);
                dirIntersectionToPoint = vertexSample.value.position - I.P;
                distToSampledPoint = length(dirIntersectionToPoint);
                dirIntersectionToPoint /= distToSampledPoint;
                sqrDistToSampledPoint = sqr(distToSampledPoint);
                dotAtPortal = dot(dirIntersectionToPoint, vertexSample.value.normal);

                if(sqrDistToSampledPoint > 0.f &&
                        dotAtPortal > 0.f &&
                        (fr = bsdf.eval(dirIntersectionToPoint, cosThetaOutDir)) != zero<Vec3f>()) {
                    useSegProb = m_fUseSegProbability;
                }
            }

            //float useSegProb = !bsdf.isDelta() && portalIdx >= 0 ? m_fUseSegProbability : 0.f;

            auto strategySample = getFloat(threadID);
            if(strategySample <= useSegProb) {
                //auto vertexSample = sample(m_PortalMeshs[portalIdx], getFloat(threadID), getFloat2(threadID));
                sampledEvent = ScatteringEvent::Other;

                auto pdfWrtSolidAngle = sqrDistToSampledPoint * vertexSample.pdf / abs(dotAtPortal);

                auto value = fr * abs(cosThetaOutDir) / pdfWrtSolidAngle;

                auto weight = 1.f;
                //auto portalPdf = pdfPortal(portalIdx, I, dir);
                auto sum = (1 - useSegProb) * bsdf.pdf(dirIntersectionToPoint) + useSegProb * pdfWrtSolidAngle;
                if(sum > 0.f) {
                    weight = useSegProb * pdfWrtSolidAngle / sum;
                } else {
                    weight = 0.f;
                }

				if (i == 1) {
                    accumulate(PORTAL_RAY_PDF_BSDF_CONTRIB, pixelID, Vec4f(Vec3f(pdfWrtSolidAngle), 0.f));
                    accumulate(BSDF_RAY_PDF_BSDF_CONTRIB, pixelID, Vec4f(Vec3f(bsdf.pdf(dirIntersectionToPoint)), 0.f));
				}

                if (portalIdx == m_nForcedPortal) {
					accumulate(PORTAL42_CONTRIB, pixelID, Vec4f(Vec3f(1.f / pdfWrtSolidAngle), 0.f));
				}

                throughput *= weight * value / useSegProb;

                auto ray = Ray(I, dirIntersectionToPoint);

                I = getScene().intersect(ray);

                wo = -ray.dir;
            } else {
                // Use brdf to sample next direction
                Sample3f wi;
                float cosThetaOutDir;

                auto fr = bsdf.sample(Vec3f(getFloat(threadID), getFloat2(threadID)), wi, cosThetaOutDir, &sampledEvent);

                if(fr != zero<Vec3f>() && wi.pdf > 0.f) {
                    auto value = fr * abs(cosThetaOutDir) / wi.pdf;
                    auto ray = Ray(I, wi.value);

                    bool intersectPortal42 = false;

                    RTScene::Hit hit;

                    float sum = 0.f;
                    auto filter = [&](const RTScene::Hit& hit, uint32_t threadID) {
                          if(int(hit.m_nMeshID) == portalIdx) {
                              auto dirIntersectionToPoint = vertexSample.value.position - I.P;
                              auto distToSampledPoint = length(dirIntersectionToPoint);
                              dirIntersectionToPoint /= distToSampledPoint;
                              auto sqrDistToSampledPoint = sqr(distToSampledPoint);
                              auto dotAtPortal = dot(dirIntersectionToPoint, vertexSample.value.normal);

                              float cosThetaOutDir;
                              if(sqrDistToSampledPoint > 0.f &&
                                      dotAtPortal > 0.f &&
                                      bsdf.eval(dirIntersectionToPoint, cosThetaOutDir) != zero<Vec3f>()) {

                                  auto portalPdf = pdfPortalVertex(portalIdx, hit.m_nTriangleID);
                                  auto pdfWrtSolidAngle = sqrDistToSampledPoint * portalPdf / abs(dotAtPortal);

                                  sum += pdfWrtSolidAngle;
                              }
                          }

                          if(hit.m_nMeshID == m_nForcedPortal) {
                              intersectPortal42 = true;
                          }

                          return true;
                    };
                    m_PortalRTScene.intersect(ray, hit, filter);

                    float weight = 1.f;
                    if(!(sampledEvent & ScatteringEvent::Specular)) {
                        auto portalPdf = sum;
                        auto sum = (1 - useSegProb) * wi.pdf + useSegProb * portalPdf;
                        if(sum > 0.f) {
                            weight = (1 - useSegProb) * wi.pdf / sum;
                        } else {
                            weight = 0.f;
                        }
                    }

                    if(intersectPortal42) {
                        accumulate(PORTAL42_CONTRIB, pixelID, Vec4f(Vec3f(1.f / wi.pdf), 0.f));
                    }

                    throughput *= weight * value / (1 - useSegProb);
                    if(pixelID == 96730 && isInvalidMeasurementEstimate(throughput)) {
                        LOG(INFO) << "weight = " << weight;
                        LOG(INFO) << "value = " << value;
                        LOG(INFO) << "(1 - useSegProb) = " <<  (1 - useSegProb);
                        LOG(INFO) << "useSegProb = " <<  useSegProb;
                        LOG(INFO) << "depth = " <<  i;
                        LOG(INFO) << "strategySample = " << strategySample;
                    }

                    I = getScene().intersect(ray);
                    wo = -wi.value;
                } else {
                    break;
                }
            }
        }
    }

    accumulate(FINAL_RENDER, pixelID, Vec4f(L, 0.f));
}


void VoxelOpeningSegmentationGraphPathtraceRenderer::processSample2(uint32_t threadID, uint32_t pixelID, uint32_t sampleID, uint32_t x, uint32_t y) const {
	for (auto i = 0u; i < getFramebufferChannelCount(); ++i) {
		accumulate(i, pixelID, Vec4f(0.f, 0.f, 0.f, 1.f));
	}

	PixelSensor sensor(getSensor(), Vec2u(x, y), getFramebufferSize());

	auto pixelSample = getPixelSample(threadID, sampleID);
	auto lensSample = getFloat2(threadID);

	RaySample raySample;
	float positionPdf, directionPdf;
	sensor.sampleExitantRay(
		getScene(),
		lensSample,
		pixelSample,
		raySample,
		positionPdf,
		directionPdf);

	auto ray = raySample.value;

	if (!ray) {
		return;
	}

	processSampleSurfacePointCamera(ray, pixelID, dynamic_cast<const SurfacePointCamera*>(&getSensor()));

	Vec3f throughput = Vec3f(1.f);
	Vec3f L = Vec3f(0.f);

	Intersection I = getScene().intersect(ray);
	Vec3f wo = -ray.dir;

	if (acceptPathDepth(1)) {
		accumulate(FINAL_RENDER_DEPTH1, pixelID, Vec4f(I.Le, 0.f));
		L += I.Le;
	}

	uint32_t sampledEvent = ScatteringEvent::Emission;

    auto nextEventEstimation = [&](std::size_t depth, const Intersection& I, const BSDF& bsdf) {
        if (depth >= m_nMaxPathDepth || !acceptPathDepth(depth)) {
            return zero<Vec3f>();
        }

        RaySample shadowRay;

        auto Le = m_pPointLight->sampleDirectIllumination(getScene(), getFloat2(threadID), I, shadowRay);
        if (Le != zero<Vec3f>() && shadowRay.pdf) {
            float cosThetaOutDir;
            auto fr = bsdf.eval(shadowRay.value.dir, cosThetaOutDir);

            if (fr != zero<Vec3f>() && !getScene().occluded(shadowRay.value)) {
                auto contrib = throughput * Le * fr * cosThetaOutDir / shadowRay.pdf;
                accumulate(FINAL_RENDER_DEPTH1 + depth, pixelID, Vec4f(contrib, 0.f));
                if (depth > 1u) {
                    if (sampledEvent == ScatteringEvent::Other) {
                        accumulate(PORTAL_CONTRIB, pixelID, Vec4f(contrib, 0.f));
                    }
                    else {
                        accumulate(BSDF_CONTRIB, pixelID, Vec4f(contrib, 0.f));
                    }
                }
                return contrib;
            }
        }

        return zero<Vec3f>();
    };

    BSDF bsdf(wo, I, getScene());
    L += nextEventEstimation(1u, I, bsdf);

    SampledRay sampledRay;
    for (auto i = 1u; I && i <= m_nMaxPathDepth && throughput != zero<Vec3f>(); ++i) {
        //bsdf = BSDF(wo, I, getScene());

        //auto estim = nextEventEstimation(i, I, bsdf);
        //L += estim;

		// If the scattering was specular, add Le
		if ((sampledEvent & ScatteringEvent::Specular) && acceptPathDepth(i)) {
			auto contrib = throughput * I.Le;
			accumulate(FINAL_RENDER_DEPTH1 + i - 1, pixelID, Vec4f(contrib, 0.f));
			L += contrib;
		}

		if (i < m_nMaxPathDepth - 1 || bsdf.hasSpecularComponent()) {
			// Identify the portal to sample
            int portalIdx = m_nForcedPortal;

			Sample<TriangleMesh::Vertex> vertexSample;
			float useSegProb = 0.f;
			float dotAtPortal = 0.f;
			float distToSampledPoint = 0.f;
			float sqrDistToSampledPoint = 0.f;
			Vec3f dirIntersectionToPoint = zero<Vec3f>();
			float cosThetaOutDir = 0.f;
			Vec3f fr = zero<Vec3f>();

			if (!bsdf.isDelta() && portalIdx >= 0) {
				vertexSample = samplePortalVertex(portalIdx, threadID);
				dirIntersectionToPoint = vertexSample.value.position - I.P;
				distToSampledPoint = length(dirIntersectionToPoint);
				dirIntersectionToPoint /= distToSampledPoint;
				sqrDistToSampledPoint = sqr(distToSampledPoint);
				dotAtPortal = dot(dirIntersectionToPoint, vertexSample.value.normal);

				if (sqrDistToSampledPoint > 0.f &&
					dotAtPortal > 0.f &&
					(fr = bsdf.eval(dirIntersectionToPoint, cosThetaOutDir)) != zero<Vec3f>()) {
					useSegProb = m_fUseSegProbability;
				}
			}

			//float useSegProb = !bsdf.isDelta() && portalIdx >= 0 ? m_fUseSegProbability : 0.f;

			auto strategySample = getFloat(threadID);
			if (strategySample <= useSegProb) {
				//auto vertexSample = sample(m_PortalMeshs[portalIdx], getFloat(threadID), getFloat2(threadID));
				sampledEvent = ScatteringEvent::Other;

				auto pdfWrtSolidAngle = sqrDistToSampledPoint * vertexSample.pdf / abs(dotAtPortal);

				auto value = fr * abs(cosThetaOutDir) / pdfWrtSolidAngle;

				auto weight = 1.f;
                if(m_bUseMaxHeuristic) {
                    if(pdfWrtSolidAngle >= bsdf.pdf(dirIntersectionToPoint)) {
                        weight = 1.f;
                    } else {
                        weight = 0.f;
                    }
                } else {
                    auto sum = (1 - useSegProb) * bsdf.pdf(dirIntersectionToPoint) + useSegProb * pdfWrtSolidAngle;
                    if (sum > 0.f) {
                        weight = useSegProb * pdfWrtSolidAngle / sum;
                    }
                    else {
                        weight = 0.f;
                    }
                }

				if (i == 1) {
                    accumulate(PORTAL_RAY_PDF_PORTAL_CONTRIB, pixelID, Vec4f(Vec3f(pdfWrtSolidAngle), 0.f));
                    accumulate(BSDF_RAY_PDF_PORTAL_CONTRIB, pixelID, Vec4f(Vec3f(bsdf.pdf(dirIntersectionToPoint)), 0.f));
				}

                if (portalIdx == m_nForcedPortal) {
					accumulate(PORTAL42_CONTRIB, pixelID, Vec4f(Vec3f(1.f / pdfWrtSolidAngle), 0.f));
				}

				throughput *= weight * value / useSegProb;

                if(throughput == zero<Vec3f>()) {
                    break;
                }

				auto ray = Ray(I, dirIntersectionToPoint);

                sampledRay.fromPortals = true;
                sampledRay.origin = I;

				I = getScene().intersect(ray);

                sampledRay.destination = I;

				wo = -ray.dir;

                if(I) {
                    bsdf.init(wo, I, getScene());
                    L += nextEventEstimation(i + 1, I, bsdf);
                }
			}
			else {
				// Use brdf to sample next direction
				Sample3f wi;
				float cosThetaOutDir;

				auto fr = bsdf.sample(Vec3f(getFloat(threadID), getFloat2(threadID)), wi, cosThetaOutDir, &sampledEvent);

				if (fr != zero<Vec3f>() && wi.pdf > 0.f) {
					auto value = fr * abs(cosThetaOutDir) / wi.pdf;
					auto ray = Ray(I, wi.value);

					bool intersectPortal42 = false;

					RTScene::Hit hit;

//                    auto thePixelToTest = std::numeric_limits<uint32_t>::max();

                    float portalPdf = 0.f;
                    auto filter = [&, I, bsdf](const RTScene::Hit& hit, uint32_t threadID) {
                        if (int(hit.m_nMeshID) == portalIdx) {
							auto dirIntersectionToPoint = vertexSample.value.position - I.P;
							auto distToSampledPoint = length(dirIntersectionToPoint);
							dirIntersectionToPoint /= distToSampledPoint;
                            //dirIntersectionToPoint = wi.value;
							auto sqrDistToSampledPoint = sqr(distToSampledPoint);
							auto dotAtPortal = dot(dirIntersectionToPoint, vertexSample.value.normal);

//                            if(thePixelToTest == pixelID) {
//                                LOG(INFO) << bsdf.eval(dirIntersectionToPoint, cosThetaOutDir) << ", " << sqrDistToSampledPoint;
//                            }

							float cosThetaOutDir;
							if (sqrDistToSampledPoint > 0.f &&
                                dotAtPortal > 0.f &&
								bsdf.eval(dirIntersectionToPoint, cosThetaOutDir) != zero<Vec3f>()) {

                                auto portalPdfWrtArea = pdfPortalVertex(portalIdx, hit.m_nTriangleID);
                                auto pdfWrtSolidAngle = sqrDistToSampledPoint * portalPdfWrtArea / abs(dotAtPortal);

                                portalPdf += pdfWrtSolidAngle;

//                                if(thePixelToTest == pixelID) {
//                                    auto l = debugLock();
//                                    LOG(INFO) << "pdfs = " << pdfWrtSolidAngle << ", " << wi.pdf << ", " <<  bool((sampledEvent & ScatteringEvent::Specular));

//                                    float weight = 1.f;
//                                    if (!(sampledEvent & ScatteringEvent::Specular)) {
//                                        auto sum = (1 - useSegProb) * wi.pdf + useSegProb * portalPdf;
//                                        if (sum > 0.f) {
//                                            weight = (1 - useSegProb) * wi.pdf / sum;
//                                        }
//                                        else {
//                                            weight = 0.f;
//                                        }
//                                    }
//                                    LOG(INFO) << "weight = " << weight;
//                                }
							}
						}

                        if (hit.m_nMeshID == m_nForcedPortal) {
							intersectPortal42 = true;
						}

						return true;
					};
					m_PortalRTScene.intersect(ray, hit, filter);

					float weight = 1.f;
                    if (!(sampledEvent & ScatteringEvent::Specular)) {
                        if(m_bUseMaxHeuristic) {
                            if(wi.pdf > portalPdf) {
                                weight = 1.f;
                            } else {
                                weight = 0.f;
                            }
                        } else {
                            auto sum = (1 - useSegProb) * wi.pdf + useSegProb * portalPdf;
                            if (sum > 0.f) {
                                weight = (1 - useSegProb) * wi.pdf / sum;
                            }
                            else {
                                weight = 0.f;
                            }
                        }
					}

                    if (i == 1) {
                        accumulate(PORTAL_RAY_PDF_BSDF_CONTRIB, pixelID, Vec4f(Vec3f(portalPdf), 0.f));
                        accumulate(BSDF_RAY_PDF_BSDF_CONTRIB, pixelID, Vec4f(Vec3f(wi.pdf), 0.f));
                    }

					if (intersectPortal42) {
						accumulate(PORTAL42_CONTRIB, pixelID, Vec4f(Vec3f(1.f / wi.pdf), 0.f));
					}

					throughput *= weight * value / (1 - useSegProb);

                    if(throughput == zero<Vec3f>()) {
                        break;
                    }

					if (pixelID == 96730 && isInvalidMeasurementEstimate(throughput)) {
						LOG(INFO) << "weight = " << weight;
						LOG(INFO) << "value = " << value;
						LOG(INFO) << "(1 - useSegProb) = " << (1 - useSegProb);
						LOG(INFO) << "useSegProb = " << useSegProb;
						LOG(INFO) << "depth = " << i;
						LOG(INFO) << "strategySample = " << strategySample;
					}

                    sampledRay.fromPortals = false;
                    sampledRay.origin = I;
                    sampledRay.bsdfPdf = wi.pdf;
                    sampledRay.portalPdf = portalPdf;
                    sampledRay.weight = weight;

					I = getScene().intersect(ray);

                    sampledRay.destination = I;

					wo = -wi.value;

                    if(I) {
                        bsdf.init(wo, I, getScene());
                        auto estim = nextEventEstimation(i + 1, I, bsdf);

                        if(i == 1 && estim != zero<Vec3f>()) {
                            m_SampledRays[pixelID] = sampledRay;
//                            if(weight == 1.f && threadID == 12) {
//                                LOG(INFO) << "original weight = " << weight;
//                                LOG(INFO) << "original pdfs = " << portalPdf << ", " << wi.pdf;
//                                //thePixelToTest = pixelID;
//                                m_PortalRTScene.intersect(ray, hit, filter);
//                            }
                        }

                        L += estim;
                    }
				}
				else {
					break;
				}
            }
		}
	}

	accumulate(FINAL_RENDER, pixelID, Vec4f(L, 0.f));
}

void VoxelOpeningSegmentationGraphPathtraceRenderer::drawGLData(const ViewerData& viewerData) {
    viewerData.debugRenderer.addStream(&m_GLDebugStream);
    m_GLDebugStream.clearObjects();

    auto pixelID = getPixelIndex(viewerData.selectedPixel.x, viewerData.selectedPixel.y);

    if(pixelID >= 0u && pixelID < m_SampledRays.size()) {
        const auto& raySample = m_SampledRays[pixelID];
        if(raySample.origin && raySample.destination && !raySample.fromPortals) {
            m_GLDebugStream.addLine(raySample.origin.P, raySample.destination.P,
                                    Vec3f(1, 0, 0), Vec3f(1, 0, 0), 3);
        }
    }
//    for(const auto& raySample: m_SampledRays) {
//        if(raySample.origin && raySample.destination && !raySample.fromPortals) {
//            m_GLDebugStream.addLine(raySample.origin.P, raySample.destination.P,
//                                    Vec3f(1, 0, 0), Vec3f(1, 0, 0), 3);
//        }
//    }
}

void VoxelOpeningSegmentationGraphPathtraceRenderer::doExposeIO(GUI& gui) {
    gui.addVarRW(BNZ_GUI_VAR(m_nMaxPathDepth));
    gui.addVarRW(BNZ_GUI_VAR(m_fUseSegProbability));
    gui.addVarRW(BNZ_GUI_VAR(m_bUseUniformSphericalTriangleSampling));
    gui.addVarRW(BNZ_GUI_VAR(m_bUseMaxHeuristic));
    gui.addVarRW(BNZ_GUI_VAR(m_nForcedPortal));
}

void VoxelOpeningSegmentationGraphPathtraceRenderer::doLoadSettings(const tinyxml2::XMLElement& xml) {
    serialize(xml, "maxDepth", m_nMaxPathDepth);
    serialize(xml, "useSegProbability", m_fUseSegProbability);
    serialize(xml, "useMaxHeuristic", m_bUseMaxHeuristic);
    serialize(xml, "forcedPortal", m_nForcedPortal);
}

void VoxelOpeningSegmentationGraphPathtraceRenderer::doStoreSettings(tinyxml2::XMLElement& xml) const {
    serialize(xml, "maxDepth", m_nMaxPathDepth);
    serialize(xml, "useSegProbability", m_fUseSegProbability);
    serialize(xml, "useMaxHeuristic", m_bUseMaxHeuristic);
    serialize(xml, "forcedPortal", m_nForcedPortal);
}

void VoxelOpeningSegmentationGraphPathtraceRenderer::processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const {
    if(m_nForcedPortal >= 0) {
        processTileSamples(viewport, [this, threadID](uint32_t x, uint32_t y, uint32_t pixelID, uint32_t sampleID) {
            processSample2(threadID, pixelID, sampleID, x, y);
        });
    } else {
        processTileSamples(viewport, [this, threadID](uint32_t x, uint32_t y, uint32_t pixelID, uint32_t sampleID) {
            processSample1(threadID, pixelID, sampleID, x, y);
        });
    }
}

}
