#pragma once

#include "VoxelOpeningSegmentationGraph.hpp"

#include <bonez/rendering/renderers/TileProcessingRenderer.hpp>
#include <bonez/scene/lights/PowerBasedLightSampler.hpp>
#include <bonez/utils/DijkstraAlgorithm.hpp>

#include <bonez/opengl/debug/GLDebugStream.hpp>

namespace BnZ {

class SurfacePointCamera;
class BSDF;

class OpeningBorderBasedPathtraceRenderer: public TileProcessingRenderer {
public:
    float m_fSceneDiag = 0.f;

    RTScene m_RTScene;
    RTScene m_BordersRTScene;
    std::vector<TriangleMesh> m_BorderMeshs;
    std::vector<std::vector<Vec2f>> m_BorderFaceIncidentPower;
    std::vector<std::vector<float>> m_BorderFaceSamplingDistributions;
    const VoxelOpeningSegmentationGraph& m_OpeningGraph;

    const PointLight* m_pPointLight = nullptr;
    DijkstraShortestPathVector<float> m_ShortestPaths;

    PowerBasedLightSampler m_LightSampler;

    uint32_t m_nMaxPathDepth = 2;
    float m_fUseSegProbability = 0.5f;
    int m_nForcedPortal = -1; // 20 for veach door

    bool m_bUseUniformSphericalTriangleSampling = true;
    bool m_bUseMaxHeuristic = false;

    float m_fDistToPortalPercentageTreshold = 0.01f; // 1%
    float m_fPortalOrientationThreshold = 0.5f; // Compared with the cosine

    struct SampledRay {
        Intersection origin;
        Intersection destination;
        bool fromPortals = true;
        float portalPdf = 0.f;
        float bsdfPdf = 0.f;
        float weight = 0.f;
    };

    mutable std::vector<SampledRay> m_SampledRays;

    GLDebugStreamData m_GLDebugStream;

    enum Outputs {
        FINAL_RENDER,
        PRIMARY_RAY_PDF_WRT_SEG,
        PRIMARY_RAY_PDF_WRT_BRDF,
        PORTAL_RAY_PDF_PORTAL_CONTRIB,
        BSDF_RAY_PDF_PORTAL_CONTRIB,
        PORTAL_RAY_PDF_BSDF_CONTRIB,
        BSDF_RAY_PDF_BSDF_CONTRIB,
        PORTAL42_CONTRIB,
        PORTAL_CONTRIB,
        BSDF_CONTRIB,
        FINAL_RENDER_DEPTH1
    };

    OpeningBorderBasedPathtraceRenderer(
            const VoxelOpeningSegmentationGraph& openingGraph):
        m_OpeningGraph(openingGraph) {
    }

    void preprocess() override;

    void beginFrame() override;

    void processSampleSurfacePointCamera(
            const Ray& ray,
            uint32_t pixelID,
            const SurfacePointCamera* pCamera) const;

    void processSample(uint32_t threadID, uint32_t pixelID, uint32_t sampleID, uint32_t x, uint32_t y) const;

    void processSampleOnePortal(uint32_t threadID, uint32_t pixelID, uint32_t sampleID, uint32_t x, uint32_t y) const;

    void doExposeIO(GUI& gui) override;

    void doLoadSettings(const tinyxml2::XMLElement& xml) override;

    void doStoreSettings(tinyxml2::XMLElement& xml) const override;

    void processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const override;

    Sample1u sampleBorderTriangle(std::size_t clusterIdx, std::size_t threadID) const;

    float computeBorderTriangleSamplingProbability(std::size_t clusterIdx,
                                                   std::size_t triangleIdx,
                                                   const Intersection& intersection) const;

    Sample<TriangleMesh::Vertex> sampleBorderTriangleVertex(std::size_t clusterIdx,
                                                            std::size_t triangleIdx,
                                                            std::size_t threadID) const;

    Sample<TriangleMesh::Vertex> sampleBorderVertex(std::size_t clusterIdx, std::size_t threadID,
                                                    std::size_t& triangleIdx) const;

    void computeBorderVertexData(std::size_t clusterIdx, std::size_t triangleIdx,
                                 const Vec3f& borderPosition,
                                 const Vec3f& borderNormal,
                                 const Intersection& intersection,
                                 const BSDF& bsdf,
                                 float& useSegProb,
                                 float& dotAtPortal,
                                 float& distToSampledPoint,
                                 float& sqrDistToSampledPoint,
                                 Vec3f& dirIntersectionToPoint,
                                 float& cosThetaOutDir,
                                 Vec3f& fr) const;

    float pdfBorderVertex(std::size_t clusterIdx, std::size_t triangleIdx) const;

    void drawGLData(const ViewerData& viewerData) override;

    void initFramebuffer() {
        addFramebufferChannel("final_render");
        addFramebufferChannel("primary_ray_pdf_wrt_seg");
        addFramebufferChannel("primary_ray_pdf_wrt_brdf");
        addFramebufferChannel("portal_ray_pdf_portal_contrib");
        addFramebufferChannel("bsdf_ray_pdf_portal_contrib");
        addFramebufferChannel("portal_ray_pdf_bsdf_contrib");
        addFramebufferChannel("bsdf_ray_pdf_bsdf_contrib");
        addFramebufferChannel("portal42_contrib");
        addFramebufferChannel("portal_contrib");
        addFramebufferChannel("bsdf_contrib");
        for(auto i : range(m_nMaxPathDepth)) {
            addFramebufferChannel("depth_" + toString(i + 1));
        }
    }
};

}
