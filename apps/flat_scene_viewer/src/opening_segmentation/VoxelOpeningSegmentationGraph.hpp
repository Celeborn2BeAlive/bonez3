#pragma once

#include <vector>
#include <bonez/types.hpp>
#include <bonez/utils/Grid3D.hpp>
#include <bonez/utils/Graph.hpp>
#include <bonez/scene/SurfacePoint.hpp>
#include <bonez/scene/raytracing/RTScene.hpp>
#include <bonez/viewer/GUI.hpp>

#include <bonez/scene/topology/EmptySpaceVoxelMapping.hpp>
#include <bonez/voxskel/CubicalComplex3D.hpp>

namespace BnZ {

struct VoxelOpeningSegmentationGraph {
public:
    static uint32_t NO_CLUSTER() {
        return -1;
    }

    struct PortalFace {
        Vec4i m_VoxelFace; // A face is a voxel (x, y, z) and an element w from { FACEXY, FACEYZ, FACEXZ }
        Vec3i m_OutgoingNormal; // The normal leaving the region
        uint32_t m_nDestinationCluster;
    };

    using FaceArray = std::vector<PortalFace>;
    using Portal = std::vector<std::size_t>; // A portal is a list of indices of faces from a border of a region
    //using Portal = FaceArray; // A portal is a set of faces

    using PortalArray = std::vector<Portal>;

    struct Cluster {
        Vec3i m_SourceVoxel; // The voxel from which the depth first search has started
        // m_Portals[i] is the portal towards the i-th neighbour of this cluster
        //std::vector<uint32_t> m_Portals;

        FaceArray m_Border; // All face leading outside the regions
        Graph m_BorderGraph; //! TODO: compute this, not easy

        PortalArray m_Portals; // A portal is a list of indices of faces from the border
        std::vector<size_t> m_PortalGlobalIndices;

        uint32_t m_SourceVoxelOpening; // Opening of the source voxel

        Cluster() = default;

        Cluster(const Vec3i& srcVoxel, FaceArray border, Graph borderGraph,
                PortalArray portals, std::vector<size_t> portalGlobalIndices, uint32_t opening):
            m_SourceVoxel(srcVoxel), m_Border(move(border)), m_BorderGraph(move(borderGraph)),
            m_Portals(move(portals)), m_PortalGlobalIndices(portalGlobalIndices), m_SourceVoxelOpening(opening) {
        }
    };

    using ClusterArray = std::vector<Cluster>;

    struct PortalIndirection {
        size_t m_nClusterIndex;
        size_t m_nPortalIndex;
    };

    using PortalIndirectionArray = std::vector<PortalIndirection>;

    VoxelOpeningSegmentationGraph() = default;

    VoxelOpeningSegmentationGraph(const Mat4f& gridToWorld,
                                  Grid3D<uint32_t> distanceMap26,
                                  Grid3D<uint32_t> openingMap26,
                                  Grid3D<Vec3u> openingCenterMap);

    bool empty() const {
        return m_Clusters.empty();
    }

    size_t size() const {
        return m_Clusters.size();
    }

    const PortalIndirectionArray& getPortals() const {
        return m_Portals;
    }

    const Cluster& getCluster(uint32_t idx) const {
        assert(idx < size());
        return m_Clusters[idx];
    }

    uint32_t getVoxelCluster(const Vec3i& voxel) const {
        return m_OpeningSegmentation(voxel);
    }

    const Grid3D<uint32_t>& getOpeningSegmentation() const {
        return m_OpeningSegmentation;
    }

    const Grid3D<uint32_t>& getOpeningMap() const {
        return m_OpeningMap26;
    }

    const Grid3D<uint32_t>& getDistanceMap() const {
        return m_DistanceMap26;
    }

    const Grid3D<Vec3u>& getOpeningCenterMap() const {
        return m_OpeningCenterMap;
    }

    const GraphAdjacencyList& neighbours(uint32_t idx) const {
        return m_Graph[idx];
    }

    bool isClusterOnEmptySpace(size_t clusterIdx) const {
        return m_Clusters[clusterIdx].m_SourceVoxelOpening != 0u;
    }

    Vec3i getVoxel(const SurfacePoint& point, const Vec3f& incidentDirection) const {
        return m_VoxelMapping.getVoxel(point.P, point.Ns, incidentDirection, m_WorldToGrid, [&](const auto& voxel) {
            return this->isInEmptySpace(voxel);
        });
    }

    Vec3i getVoxel(const SurfacePoint& point) const {
        return m_VoxelMapping.getVoxel(point.P, point.Ns, m_WorldToGrid, [&](const auto& voxel) {
            return this->isInEmptySpace(voxel);
        });
    }

    Vec3i getVoxel(const Vec3f& position) const {
        return m_VoxelMapping.getVoxel(position, m_WorldToGrid);
    }

    bool isInEmptySpace(const Vec3i& voxel) const {
        return m_OpeningMap26.contains(voxel) && m_OpeningMap26(voxel) != 0u;
    }

    const Graph& getGraph() const {
        return m_Graph;
    }

    const Mat4f& getGridToWorldMatrix() const {
        return m_GridToWorld;
    }

    const CubicalComplex3D& getBordersCubicalComplex() const {
        return m_BordersCubicalComplex;
    }

    void exposeGUI(GUI& gui);

private:
    void build();

    void buildOpeningForstLabeling();

    void buildWatershedLabeling();

    void buildOpeningLabeling();

    void buildReducedOpeningLabeling();

    void buildMinTreeLabeling();

    void buildGraphAndPortalsFromSegmentation(uint32_t clusterCount, const Vec3i* seedVoxels,
        const uint32_t* clusterOpeningValues);

    Mat4f m_GridToWorld, m_WorldToGrid;
    Grid3D<uint32_t> m_DistanceMap26;
    Grid3D<uint32_t> m_OpeningMap26;
    Grid3D<Vec3u> m_OpeningCenterMap;

    Grid3D<uint32_t> m_OpeningSegmentation;
    CubicalComplex3D m_BordersCubicalComplex;

    enum SegmentationMethod {
        OPENING_FOREST_LABELING,
        OPENING_LABELING,
        REDUCED_OPENING_LABELING,
        WATERSHED_LABELING,
        MINTREE_LABELING,
        SEG_METHOD_COUNT
    };
    uint32_t m_SegmentationMethod = OPENING_FOREST_LABELING;

    //PortalArray m_Portals;

    ClusterArray m_Clusters;

    Graph m_Graph;

    std::vector<PortalIndirection> m_Portals;

//    Vec3i m_Offsets[6][9];
    EmptySpaceVoxelMapping m_VoxelMapping;
};

TriangleMesh buildPortalMesh(const VoxelOpeningSegmentationGraph& graph, std::size_t portalIdx);

TriangleMesh buildBorderMesh(const VoxelOpeningSegmentationGraph& graph, std::size_t clusterIdx);

}
