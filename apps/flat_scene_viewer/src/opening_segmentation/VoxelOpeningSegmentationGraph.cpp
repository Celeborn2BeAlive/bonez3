#include "VoxelOpeningSegmentationGraph.hpp"

#include <queue>
#include <stack>
#include <bonez/voxskel/CubicalComplex3D.hpp>

#include <bonez/voxskel/MinTreeNajmanCouprie2006.hpp>

namespace BnZ {

VoxelOpeningSegmentationGraph::VoxelOpeningSegmentationGraph(const Mat4f &gridToWorld,
                                                             Grid3D<uint32_t> distanceMap26,
                                                             Grid3D<uint32_t> openingMap26,
                                                             Grid3D<Vec3u> openingCenterMap):
    m_GridToWorld(gridToWorld),
    m_WorldToGrid(inverse(m_GridToWorld)),
    m_DistanceMap26(move(distanceMap26)),
    m_OpeningMap26(move(openingMap26)),
    m_OpeningCenterMap(move(openingCenterMap)) {

    build();
}

void VoxelOpeningSegmentationGraph::exposeGUI(GUI& gui) {
    if(auto window = gui.addWindow("VoxelOpeningSegmentationGraph")) {
        const char* allMethods[] = { "OPENING_FOREST_LABELING",
                                     "OPENING_LABELING",
                                     "REDUCED_OPENING_LABELING",
                                     "WATERSHED_LABELING",
                                     "MINTREE_LABELING"};
        static_assert((sizeof(allMethods) / sizeof(allMethods[0])) == SEG_METHOD_COUNT, "size(allMethods) != SEG_METHOD_COUNT");
        gui.addCombo("LabelingMethod", m_SegmentationMethod, SEG_METHOD_COUNT, [&](uint32_t idx) {
            return allMethods[idx];
        });
        gui.addButton("Rebuild", [&]() {
            build();
        });
    }
}

void VoxelOpeningSegmentationGraph::build() {
    switch (m_SegmentationMethod) {
    case OPENING_FOREST_LABELING:
        buildOpeningForstLabeling();
        break;
    case OPENING_LABELING:
        buildOpeningLabeling();
        break;
    case REDUCED_OPENING_LABELING:
        buildReducedOpeningLabeling();
        break;
    case WATERSHED_LABELING:
        buildWatershedLabeling();
    case MINTREE_LABELING:
        buildMinTreeLabeling();
    }
}

void VoxelOpeningSegmentationGraph::buildOpeningForstLabeling() {
    m_OpeningSegmentation = Grid3D<uint32_t>(m_OpeningMap26.resolution(), NO_CLUSTER());
    std::vector<uint32_t> labelOpeningValues; // The opening of each cluster
    std::vector<Vec3i> srcVoxels; // The source voxel of each cluster

    auto nextLabel = 0u;

    auto isCentered = [&](const Vec3i& voxel) {
        return m_OpeningCenterMap(voxel) == Vec3u(voxel);
    };

    // Associate a label to each centered connected component
    foreachVoxel(m_OpeningSegmentation.resolution(), [&](const Vec3i& voxel) {
        auto value = m_OpeningMap26(voxel);

        if (m_OpeningSegmentation(voxel) == NO_CLUSTER() &&
            isCentered(voxel)/* &&
            value != 0u*/) {
            std::queue<Vec3i> voxelQueue;

            m_OpeningSegmentation(voxel) = nextLabel;
            voxelQueue.push(voxel);
            while(!voxelQueue.empty()) {
                auto voxel = voxelQueue.front();
                voxelQueue.pop();

                foreach6Neighbour(m_OpeningSegmentation.resolution(), voxel, [&](const Vec3i& neighbour) {
                    auto neighborOpening = m_OpeningMap26(neighbour);
                    if(m_OpeningSegmentation(neighbour) == NO_CLUSTER()
                            && neighborOpening == value
                            && isCentered(neighbour)) {
                        m_OpeningSegmentation(neighbour) = nextLabel;
                        voxelQueue.push(neighbour);
                    }
                });
            }

            labelOpeningValues.push_back(value);
            srcVoxels.push_back(voxel);
            nextLabel += 1u;
        }
    });

    // Extend the label to children
    std::function<uint32_t (const Vec3i&)> resolveLabel = [&](const Vec3i& voxel) -> uint32_t {
        if (m_OpeningSegmentation(voxel) != NO_CLUSTER()) {
            return m_OpeningSegmentation(voxel);
        }

        m_OpeningSegmentation(voxel) = resolveLabel(Vec3i(m_OpeningCenterMap(voxel)));
        return m_OpeningSegmentation(voxel);
    };

    // Foreach voxel, assign it the label of its parent
    foreachVoxel(m_OpeningSegmentation.resolution(), [&](const Vec3i& voxel) {
        //if(m_OpeningMap26(voxel) != 0u) {
            resolveLabel(voxel);
        //}
    });

    buildGraphAndPortalsFromSegmentation(nextLabel, srcVoxels.data(), labelOpeningValues.data());
}

void VoxelOpeningSegmentationGraph::buildWatershedLabeling() {
    m_OpeningSegmentation = Grid3D<uint32_t>(m_OpeningMap26.resolution(), NO_CLUSTER());
    std::vector<uint32_t> labelOpeningValues; // The opening of each cluster
    std::vector<Vec3i> srcVoxels; // The source voxel of each cluster

    auto nextLabel = 0u;

    auto isCentered = [&](const Vec3i& voxel) {
        return m_OpeningCenterMap(voxel) == Vec3u(voxel);
    };

    auto maxOpening = 0u;
    std::vector<std::vector<Vec3i>> regionVoxels;
    std::vector<std::vector<Vec3i>> regionNeighborVoxels;
    bool borderIsEmpty = true;

    // Perform a depth first search from each voxel to compute the connected component
    // based on opening
    foreachVoxel(m_OpeningSegmentation.resolution(), [&](const Vec3i& voxel) {
        auto value = m_OpeningMap26(voxel);
        maxOpening = max(maxOpening, value);

        if (m_OpeningSegmentation(voxel) == NO_CLUSTER() &&
            m_OpeningCenterMap(voxel) == Vec3u(voxel) &&
            value != 0u) {
            std::queue<Vec3i> voxelQueue;

            m_OpeningSegmentation(voxel) = nextLabel;
            voxelQueue.push(voxel);

            regionVoxels.emplace_back();
            regionNeighborVoxels.emplace_back();

            while(!voxelQueue.empty()) {
                auto voxel = voxelQueue.front();
                voxelQueue.pop();

                regionVoxels.back().emplace_back(voxel);

                foreach6Neighbour(m_OpeningSegmentation.resolution(), voxel, [&](const Vec3i& neighbour) {
                    auto neighborOpening = m_OpeningMap26(neighbour);
                    if(m_OpeningSegmentation(neighbour) == NO_CLUSTER()
                            && neighborOpening == value
                            && isCentered(neighbour)) {
                        m_OpeningSegmentation(neighbour) = nextLabel;
                        voxelQueue.push(neighbour);
                    } else if(m_OpeningSegmentation(neighbour) == NO_CLUSTER() && neighborOpening != 0u) {
                        regionNeighborVoxels.back().push_back(neighbour);
                        borderIsEmpty = false;
                    }
                });
            }

            labelOpeningValues.push_back(value);
            srcVoxels.push_back(voxel);
            nextLabel += 1u;
        }
    });

    for(auto i = 1u; i <= maxOpening; ++i) {
        for(auto regionIdx = 0u; regionIdx < regionVoxels.size(); ++regionIdx) {
            for(auto& neighbor: regionNeighborVoxels[regionIdx]) {
                auto neighborOpening = m_OpeningMap26(neighbor);
                if(m_OpeningSegmentation(neighbor) == NO_CLUSTER()
                        /*&& neighborOpening <= i*/) {
                    m_OpeningSegmentation(neighbor) = regionIdx;
                }
            }

            std::vector<Vec3i> newNeighborVoxels;

            for(auto& voxel: regionNeighborVoxels[regionIdx]) {
                auto value = m_OpeningMap26(voxel);
                if(m_OpeningSegmentation(voxel) == regionIdx
                        && value == i) {
                    foreach6Neighbour(m_OpeningSegmentation.resolution(), voxel, [&](const Vec3i& neighbour) {
                        auto neighborOpening = m_OpeningMap26(neighbour);
                        if(m_OpeningSegmentation(neighbour) == NO_CLUSTER() && neighborOpening != 0u) {
                            newNeighborVoxels.push_back(neighbour);
                        }
                    });
                } else if(m_OpeningSegmentation(voxel) == NO_CLUSTER()/* && value > i*/) {
                    newNeighborVoxels.emplace_back(voxel);
                }
            }

            swap(newNeighborVoxels, regionNeighborVoxels[regionIdx]);
        }
    }

    buildGraphAndPortalsFromSegmentation(nextLabel, srcVoxels.data(), labelOpeningValues.data());
}

void VoxelOpeningSegmentationGraph::buildOpeningLabeling() {
    m_OpeningSegmentation = Grid3D<uint32_t>(m_OpeningMap26.resolution(), NO_CLUSTER());
    std::vector<uint32_t> labelOpeningValues; // The opening of each cluster
    std::vector<Vec3i> srcVoxels; // The source voxel of each cluster

    std::stack<Vec3i> voxelStack;
    auto nextLabel = 0u;
    auto threshold = 1;

    // Perform a depth first search from each voxel to compute the connected component
    // based on opening
    foreachVoxel(m_OpeningSegmentation.resolution(), [&](const Vec3i& voxel) {
        if (m_OpeningSegmentation(voxel) == NO_CLUSTER()) {
            auto value = m_OpeningMap26(voxel);
            m_OpeningSegmentation(voxel) = nextLabel;
            voxelStack.push(voxel);
            while(!voxelStack.empty()) {
                auto voxel = voxelStack.top();
                voxelStack.pop();

                foreach6Neighbour(m_OpeningSegmentation.resolution(), voxel, [&](const Vec3i& neighbour) {
                    auto diff = abs((int)m_OpeningMap26(neighbour) - (int)value);
                    if(m_OpeningSegmentation(neighbour) == NO_CLUSTER() && diff < threshold) {
                        m_OpeningSegmentation(neighbour) = nextLabel;
                        voxelStack.push(neighbour);
                    }
                });
            }

            labelOpeningValues.push_back(value);
            srcVoxels.push_back(voxel);
            nextLabel += 1u;
        }
    });

    buildGraphAndPortalsFromSegmentation(nextLabel, srcVoxels.data(), labelOpeningValues.data());
}

void VoxelOpeningSegmentationGraph::buildReducedOpeningLabeling() {
    m_OpeningSegmentation = Grid3D<uint32_t>(m_OpeningMap26.resolution(), NO_CLUSTER());
    std::vector<uint32_t> labelOpeningValues; // The opening of each cluster
    std::vector<Vec3i> srcVoxels; // The source voxel of each cluster

    std::stack<Vec3i> voxelStack;
    auto nextLabel = 0u;
    auto threshold = 1;

    // Perform a depth first search from each voxel to compute the connected component
    // based on opening
    foreachVoxel(m_OpeningSegmentation.resolution(), [&](const Vec3i& voxel) {
        if (m_OpeningSegmentation(voxel) == NO_CLUSTER()) {
            auto value = m_OpeningMap26(voxel);
            m_OpeningSegmentation(voxel) = nextLabel;
            voxelStack.push(voxel);
            while (!voxelStack.empty()) {
                auto voxel = voxelStack.top();
                voxelStack.pop();

                foreach6Neighbour(m_OpeningSegmentation.resolution(), voxel, [&](const Vec3i& neighbour) {
                    auto diff = abs((int)m_OpeningMap26(neighbour) - (int)value);
                    if (m_OpeningSegmentation(neighbour) == NO_CLUSTER() && diff < threshold) {
                        m_OpeningSegmentation(neighbour) = nextLabel;
                        voxelStack.push(neighbour);
                    }
                });
            }

            labelOpeningValues.push_back(value);
            srcVoxels.push_back(voxel);
            nextLabel += 1u;
        }
    });

    // Identify which cluster has a root (ie. at least one centered voxel)
    std::vector<bool> clusterHasRoot(nextLabel, false);

    {
        Grid3D<bool> visitMap(m_OpeningSegmentation.resolution(), false);

        for (auto i = 0u; i < nextLabel; ++i) {
            auto srcVoxel = srcVoxels[i];

            std::queue<Vec3i> queue;
            queue.push(srcVoxel);
            visitMap(srcVoxel) = true;

            while (!queue.empty()) {
                auto voxel = queue.front();
                queue.pop();

                if (m_OpeningCenterMap(voxel) == Vec3u(voxel)) {
                    clusterHasRoot[i] = true;
                    break;
                }

                foreach6Neighbour(m_OpeningSegmentation.resolution(), voxel, [&](Vec3i neighbour) {
                    if (m_OpeningSegmentation(neighbour) == i) {
                        if (!visitMap(neighbour)) {
                            queue.push(neighbour);
                            visitMap(neighbour) = true;
                        }
                    }
                });
            }
        }
    }

    // Replace the label of a voxel if its cluster has no root: research the label of the nearest parent
    // that have a root in its cluster
    std::function<uint32_t(const Vec3i&)> reduceLabel = [&](const Vec3i& voxel) -> uint32_t {
        if (clusterHasRoot[m_OpeningSegmentation(voxel)]) {
            return m_OpeningSegmentation(voxel);
        }
        m_OpeningSegmentation(voxel) = reduceLabel(Vec3i(m_OpeningCenterMap(voxel)));
        return m_OpeningSegmentation(voxel);
    };

    std::vector<uint32_t> newLabels(nextLabel, NO_CLUSTER()); // Will map old label to new label
    std::vector<Vec3i> newSrcVoxels;
    std::vector<uint32_t> newOpeningValues;

    // Foreach voxel, assign it the label of its parent
    auto newNextLabel = 0u;
    foreachVoxel(m_OpeningSegmentation.resolution(), [&](const Vec3i& voxel) {
        auto label = reduceLabel(voxel);
        if (newLabels[label] == NO_CLUSTER()) { // If not assigned new label yet, do it
            newLabels[label] = newNextLabel++;
            newSrcVoxels.push_back(srcVoxels[label]);
            newOpeningValues.push_back(labelOpeningValues[label]);
        }
    });

    // Reassign the labels in the segmentation
    for (auto& label : m_OpeningSegmentation) {
        label = newLabels[label];
    }

    buildGraphAndPortalsFromSegmentation(newNextLabel, newSrcVoxels.data(), newOpeningValues.data());
}

void VoxelOpeningSegmentationGraph::buildMinTreeLabeling() {
    m_OpeningSegmentation = Grid3D<uint32_t>(m_OpeningMap26.resolution(), NO_CLUSTER());
    std::vector<uint32_t> labelOpeningValues; // The opening of each cluster
    std::vector<Vec3i> srcVoxels; // The source voxel of each cluster

    MinTreeNajmanCouprie2006<uint32_t> minTree(m_OpeningMap26);

    auto nextLabel = 0u;
    const auto& voxelToNodeMapping = minTree.getVoxelToNodeMapping();
    foreachVoxel(voxelToNodeMapping.resolution(), [&](const Vec3i& voxel) {
        auto value = voxelToNodeMapping(voxel);

        if (m_OpeningSegmentation(voxel) == NO_CLUSTER()) {
            std::queue<Vec3i> voxelQueue;

            m_OpeningSegmentation(voxel) = nextLabel;
            voxelQueue.push(voxel);
            while(!voxelQueue.empty()) {
                auto voxel = voxelQueue.front();
                voxelQueue.pop();

                foreach6Neighbour(m_OpeningSegmentation.resolution(), voxel, [&](const Vec3i& neighbour) {
                    auto neighborValue = voxelToNodeMapping(neighbour);
                    if(m_OpeningSegmentation(neighbour) == NO_CLUSTER()
                            && neighborValue == value) {
                        m_OpeningSegmentation(neighbour) = nextLabel;
                        voxelQueue.push(neighbour);
                    }
                });
            }

            labelOpeningValues.push_back(m_OpeningMap26(voxel));
            srcVoxels.push_back(voxel);
            nextLabel += 1u;
        }
    });

    buildGraphAndPortalsFromSegmentation(nextLabel, srcVoxels.data(), labelOpeningValues.data());
}

void VoxelOpeningSegmentationGraph::buildGraphAndPortalsFromSegmentation(uint32_t clusterCount, const Vec3i* seedVoxels,
    const uint32_t* clusterOpeningValues) {
    Grid3D<bool> visitMap(m_OpeningSegmentation.resolution(), false);
    m_BordersCubicalComplex = CubicalComplex3D(m_OpeningMap26.resolution());

    for (auto i = 0u; i < clusterCount; ++i) {
        PortalArray portals(clusterCount); // A list of portals for each potential neighbour
        FaceArray border;
        auto srcVoxel = seedVoxels[i];

        std::queue<Vec3i> queue;
        queue.push(srcVoxel); // Start search from seed voxel
        visitMap(srcVoxel) = true;

        while (!queue.empty()) {
            auto voxel = queue.front();
            queue.pop();

            foreach6Neighbour(m_OpeningSegmentation.resolution(), voxel, [&](Vec3i neighbour) {
                if (m_OpeningSegmentation(neighbour) == i) { // Neighbour is inside the cluster
                    if (!visitMap(neighbour)) {
                        queue.push(neighbour);
                        visitMap(neighbour) = true;
                    }
                }
                else { // We find a neighbour outside the cluster
                    if (/*m_OpeningMap26(neighbour) != 0u && */m_OpeningSegmentation(neighbour) != NO_CLUSTER()) {
                        auto outgoingNormal = Vec3i(neighbour) - Vec3i(voxel); // Offset direction, allows to identify the face
                        Vec4i face;
                        if (outgoingNormal.x == -1) {
                            face = Vec4i(voxel, CC3DFaceBits::YZFACE);
                        }
                        else if (outgoingNormal.y == -1) {
                            face = Vec4i(voxel, CC3DFaceBits::XZFACE);
                        }
                        else if (outgoingNormal.z == -1) {
                            face = Vec4i(voxel, CC3DFaceBits::XYFACE);
                        }
                        else if (outgoingNormal.x == 1) {
                            face = Vec4i(neighbour, CC3DFaceBits::YZFACE);
                        }
                        else if (outgoingNormal.y == 1) {
                            face = Vec4i(neighbour, CC3DFaceBits::XZFACE);
                        }
                        else {
                            face = Vec4i(neighbour, CC3DFaceBits::XYFACE);
                        }

                        m_BordersCubicalComplex(Vec3i(face)).add(CC3DFace(face.w));

                        auto faceIdx = border.size();
                        border.emplace_back(PortalFace{ face, outgoingNormal,  m_OpeningSegmentation(neighbour) });

                        // Put the face in the portal leading to the neighbour cluster
                        portals[m_OpeningSegmentation(neighbour)].push_back(faceIdx);
                    }
                }
            });
        }

        // Compute the list of neighbours for each cluster
        GraphAdjacencyList neighbours;
        PortalArray newPortals;
        std::vector<size_t> portalGlobalIndices;
        for (auto k = 0u; k < portals.size(); ++k) {
            if (!portals[k].empty()) {
                portalGlobalIndices.emplace_back(m_Portals.size());
                m_Portals.emplace_back(PortalIndirection{ i, newPortals.size() });
                newPortals.emplace_back(portals[k]);
                neighbours.emplace_back(k);
            }
        }

        m_Clusters.emplace_back(Cluster(srcVoxel, border, Graph{}, newPortals,
                                        portalGlobalIndices, clusterOpeningValues[i]));
        m_Graph.emplace_back(neighbours);
    }
}

TriangleMesh buildPortalMesh(const VoxelOpeningSegmentationGraph& graph, std::size_t portalIdx) {
    const auto& portal = graph.getPortals()[portalIdx];
    auto clusterIndex = portal.m_nClusterIndex;
    auto portalIndex = portal.m_nPortalIndex;

    const auto& cluster = graph.getCluster(clusterIndex);

	auto gridToWorldMatrix = graph.getGridToWorldMatrix();    
	TriangleMesh mesh;
    for (auto& faceIndex : cluster.m_Portals[portalIndex]) {
        const auto& face = cluster.m_Border[faceIndex];
		Vec3i u, v;
		getFaceVectors(CC3DFaceBits::Value(face.m_VoxelFace.w), u, v);
		auto p1 = Vec3f(gridToWorldMatrix * Vec4f(Vec3i(face.m_VoxelFace), 1.f));
		auto p2 = Vec3f(gridToWorldMatrix * Vec4f(Vec3i(face.m_VoxelFace) + u, 1.f));
		auto p3 = Vec3f(gridToWorldMatrix * Vec4f(Vec3i(face.m_VoxelFace) + u + v, 1.f));
		auto p4 = Vec3f(gridToWorldMatrix * Vec4f(Vec3i(face.m_VoxelFace) + v, 1.f));

		auto n = normalize(Vec3f(gridToWorldMatrix * Vec4f(face.m_OutgoingNormal, 0.f)));

		uint32_t i1 = mesh.m_Vertices.size();
		mesh.m_Vertices.push_back({ p1, n, Vec2f(0, 0) });
		uint32_t i2 = mesh.m_Vertices.size();
		mesh.m_Vertices.push_back({ p2, n, Vec2f(0, 1) });
		uint32_t i3 = mesh.m_Vertices.size();
		mesh.m_Vertices.push_back({ p3, n, Vec2f(1, 1) });
		uint32_t i4 = mesh.m_Vertices.size();
		mesh.m_Vertices.push_back({ p4, n, Vec2f(1, 0) });

		if (face.m_OutgoingNormal.x == 1 || face.m_OutgoingNormal.y == 1 || face.m_OutgoingNormal.z == 1) {
			mesh.m_Triangles.emplace_back(TriangleMesh::Triangle{ i1, i4, i3 });
			mesh.m_Triangles.emplace_back(TriangleMesh::Triangle{ i1, i3, i2 });
		}
		else {
			mesh.m_Triangles.emplace_back(TriangleMesh::Triangle{ i1, i2, i3 });
			mesh.m_Triangles.emplace_back(TriangleMesh::Triangle{ i1, i3, i4 });
		}
	}

	return mesh;
}

TriangleMesh buildBorderMesh(const VoxelOpeningSegmentationGraph& graph, std::size_t clusterIdx) {
    const auto& cluster = graph.getCluster(clusterIdx);

    auto gridToWorldMatrix = graph.getGridToWorldMatrix();
    TriangleMesh mesh;
    for (auto& face : cluster.m_Border) {
        Vec3i u, v;
        getFaceVectors(CC3DFaceBits::Value(face.m_VoxelFace.w), u, v);
        auto p1 = Vec3f(gridToWorldMatrix * Vec4f(Vec3i(face.m_VoxelFace), 1.f));
        auto p2 = Vec3f(gridToWorldMatrix * Vec4f(Vec3i(face.m_VoxelFace) + u, 1.f));
        auto p3 = Vec3f(gridToWorldMatrix * Vec4f(Vec3i(face.m_VoxelFace) + u + v, 1.f));
        auto p4 = Vec3f(gridToWorldMatrix * Vec4f(Vec3i(face.m_VoxelFace) + v, 1.f));

        auto n = normalize(Vec3f(gridToWorldMatrix * Vec4f(face.m_OutgoingNormal, 0.f)));

        uint32_t i1 = mesh.m_Vertices.size();
        mesh.m_Vertices.push_back({ p1, n, Vec2f(0, 0) });
        uint32_t i2 = mesh.m_Vertices.size();
        mesh.m_Vertices.push_back({ p2, n, Vec2f(0, 1) });
        uint32_t i3 = mesh.m_Vertices.size();
        mesh.m_Vertices.push_back({ p3, n, Vec2f(1, 1) });
        uint32_t i4 = mesh.m_Vertices.size();
        mesh.m_Vertices.push_back({ p4, n, Vec2f(1, 0) });

        if (face.m_OutgoingNormal.x == 1 || face.m_OutgoingNormal.y == 1 || face.m_OutgoingNormal.z == 1) {
            mesh.m_Triangles.emplace_back(TriangleMesh::Triangle{ i1, i4, i3 });
            mesh.m_Triangles.emplace_back(TriangleMesh::Triangle{ i1, i3, i2 });
        }
        else {
            mesh.m_Triangles.emplace_back(TriangleMesh::Triangle{ i1, i2, i3 });
            mesh.m_Triangles.emplace_back(TriangleMesh::Triangle{ i1, i3, i4 });
        }
    }

    return mesh;
}

}
