//#include "VoxelOpeningSegmentationGraphPathtraceRenderer2.hpp"

//#include <bonez/sampling/patterns.hpp>
//#include <bonez/scene/shading/BSDF.hpp>
//#include <bonez/voxskel/CubicalComplex3D.hpp>

//#include <bonez/scene/sensors/PixelSensor.hpp>

//namespace BnZ {

//void VoxelOpeningSegmentationGraphPathtraceRenderer2::preprocess() {
//    m_LightSampler.initFrame(getScene());

//    m_pPointLight = nullptr;

//    getScene().getLightContainer().forEach<PointLight>([&](std::size_t index, const PointLight& light) {
//       if(!m_pPointLight) {
//           m_pPointLight = &light;
//       }
//    });

//    if(!m_pPointLight) {
//        throw std::runtime_error("No PointLight in scene");
//    }

//    auto voxel = m_OpeningGraph.getVoxel(m_pPointLight->m_Position);
//    auto lightClusterIdx = m_OpeningGraph.getVoxelCluster(voxel);

//    m_ShortestPaths = DijkstraAlgorithm().computeShortestPaths(m_OpeningGraph.getGraph(),
//                                                               lightClusterIdx,
//                                                               [&](GraphNodeIndex n1, GraphNodeIndex n2) {
//                                                                         return 1;
//                                                               });

//    m_fLookUpRadius = length(getScene().getBBox().size()) * 0.01f;

//    m_PortalLinks.clear();
//    RandomGenerator rng;

//    const auto& portals = m_OpeningGraph.getPortals();

//    for(auto clusterIdx = 0u; clusterIdx < m_OpeningGraph.size(); ++clusterIdx) {
//        const auto& cluster = m_OpeningGraph.getCluster(clusterIdx);
//        auto dijkstraNode = m_ShortestPaths[clusterIdx];

//        int neighborIdx = -1;
//        for(auto i = 0u; i < m_OpeningGraph.neighbours(clusterIdx).size(); ++i) {
//            if(dijkstraNode.predecessor == m_OpeningGraph.neighbours(clusterIdx)[i]) {
//                neighborIdx = i;
//                break;
//            }
//        }

//        if(neighborIdx >= 0) {
//            auto portalSample = neighborIdx;
//            auto portalIdx = cluster.m_PortalGlobalIndices[portalSample];
//            //auto faceCount = portals[portalIdx].size();

//            auto faceCount = cluster.m_Portals[portalSample].size();

//            if(faceCount > 0u) {
//                for(auto i = 0u; i < m_nPortalLinkCountPerPortal; ++i) {
//                    auto faceSample = min(size_t(rng.getFloat() * faceCount), faceCount - 1);
//                    // First attempt, just trace through the center of the voxel face
//                    Vec3i u, v;
//                    auto face = cluster.m_Border[cluster.m_Portals[portalSample][faceSample]];
//                    getFaceVectors(CC3DFaceBits::Value(face.m_VoxelFace.w), u, v);

//                    auto faceCenter = Vec3f(face.m_VoxelFace) + Vec3f(u) * rng.getFloat() + Vec3f(v) * rng.getFloat();
//                    auto faceCenterWorldSpace = Vec3f(m_OpeningGraph.getGridToWorldMatrix() * Vec4f(faceCenter, 1.f));
//                    auto faceNormal = Vec3f(m_OpeningGraph.getGridToWorldMatrix() * Vec4f(Vec3f(face.m_OutgoingNormal), 0.f));

//                    auto dirSample = uniformSampleHemisphere(rng.getFloat(), rng.getFloat(), -faceNormal);
//                    Ray ray(faceCenterWorldSpace, dirSample.value);
//                    auto I = getScene().intersect(ray);
//                    if(I) {
//                        m_PortalLinks.emplace_back(I, portalIdx);
//                    }
//                }
//            }
//        }
//    }

//    m_PortalBuffer.resize(m_PortalLinks.size() * getThreadCount());
//    m_PortalLinkHashGrid.Reserve(m_nPortalLinkCountPerPortal);
//    m_PortalLinkHashGrid.build(m_PortalLinks.data(), m_PortalLinks.size(), m_fLookUpRadius);

////    m_PortalLinkKdTree.build(m_PortalLinks.size(), [&](uint32_t idx) {
////        if(idx >= m_PortalLinks.size()) std::cerr << "error" << std::endl;
////        return m_PortalLinks[idx].m_Point.P;
////    });
//}

//void VoxelOpeningSegmentationGraphPathtraceRenderer2::processSample(uint32_t threadID, uint32_t pixelID, uint32_t sampleID, uint32_t x, uint32_t y) const {
//    for(auto i = 0u; i <= m_nMaxPathDepth; ++i) {
//        accumulate(i, pixelID, Vec4f(0.f, 0.f, 0.f, 1.f));
//    }

//    PixelSensor sensor(getSensor(), Vec2u(x, y), getFramebufferSize());

//    auto pixelSample = getPixelSample(threadID, sampleID);
//    auto lensSample = getFloat2(threadID);

//    RaySample raySample;
//    Intersection I;

//    sampleExitantRay(
//                sensor,
//                getScene(),
//                lensSample,
//                pixelSample,
//                raySample,
//                I);

//    auto ray = raySample.value;

//    Vec3f throughput = Vec3f(1.f);
//    Vec3f L = Vec3f(0.f);

//    Vec3f wo = -ray.dir;

//    if(acceptPathDepth(1)) {
//        accumulate(1, pixelID, Vec4f(I.Le, 0.f));
//        L += I.Le;
//    }

//    uint32_t sampledEvent = ScatteringEvent::Absorption;

//    for(auto i = 1u; I && i <= m_nMaxPathDepth; ++i) {
//        BSDF bsdf(wo, I, getScene());
//        // Next event estimation
//        if(i < m_nMaxPathDepth && acceptPathDepth(i + 1)) {
//            RaySample shadowRay;
//            auto Le = m_pPointLight->sampleDirectIllumination(getScene(), getFloat2(threadID), I, shadowRay);
//            if(Le != zero<Vec3f>() && shadowRay.pdf) {
//                float cosThetaOutDir;
//                auto fr = bsdf.eval(shadowRay.value.dir, cosThetaOutDir);

//                if(fr != zero<Vec3f>() && !getScene().occluded(shadowRay.value)) {
//                    auto contrib = throughput * Le * fr * cosThetaOutDir / shadowRay.pdf;
//                    accumulate(1 + i, pixelID, Vec4f(contrib, 0.f));
//                    L += contrib;
//                }
//            }
//        }

//        // If the scattering was specular, add Le
//        if((sampledEvent & ScatteringEvent::Specular) && acceptPathDepth(i)) {
//            auto contrib = throughput * I.Le;
//            accumulate(i, pixelID, Vec4f(contrib, 0.f));
//            L += contrib;
//        }

//        if(i < m_nMaxPathDepth) {
//            bool hasSampledPortal = false;

//            if(getFloat(threadID) < m_fUseSegProbability) {
//                auto* pPortalBuffer = m_PortalBuffer.data() + threadID * m_PortalLinks.size();

//                auto count = 0u;
//                m_PortalLinkHashGrid.process(m_PortalLinks.data(), I.P, [&](const PortalLink& link) {
//                    if(dot(I.Ns, link.m_Point.Ns) > 0.f) {
//                        pPortalBuffer[count] = link.m_nPortalIndex;
//                        ++count;
//                    }
//                });
////                m_PortalLinkKdTree.searchKNearestNeighbours(I.P, 16, [&](uint32_t idx, const Vec3f& P, float sqrDist) {
////                    const auto& link = m_PortalLinks[idx];
////                    if(dot(I.Ns, link.m_Point.Ns) > 0.f) {
////                        pPortalBuffer[count] = link.m_nPortalIndex;
////                        ++count;
////                    }
////                });

//                if(count > 0u) {
//                    const auto& portals = m_OpeningGraph.getPortals();

//                    auto linkIdx = min(uint32_t(getFloat(threadID) * count), count - 1);
//                    auto portalIdx = pPortalBuffer[linkIdx];
//                    auto faceCount = portals[portalIdx].size();
//                    if(faceCount > 0u) {
//                        auto faceSample = min(size_t(getFloat(threadID) * faceCount), faceCount - 1);
//                        // First attempt, just trace through the center of the voxel face
//                        Vec3i u, v;
//                        getFaceVectors(CC3DFaceBits::Value(portals[portalIdx][faceSample].m_VoxelFace.w), u, v);

//                        auto faceCenter = Vec3f(portals[portalIdx][faceSample].m_VoxelFace) + Vec3f(u) * getFloat(threadID) + Vec3f(v) * getFloat(threadID);
//                        auto faceCenterWorldSpace = Vec3f(m_OpeningGraph.getGridToWorldMatrix() * Vec4f(faceCenter, 1.f));
//                        auto faceNormal = Vec3f(m_OpeningGraph.getGridToWorldMatrix() * Vec4f(Vec3f(portals[portalIdx][faceSample].m_OutgoingNormal), 0.f));

//                        auto dir = normalize(faceCenterWorldSpace - I.P);

//                        if(dot(dir, faceNormal) > 0.f) {
//                            float cosThetaOutDir;
//                            auto fr = bsdf.eval(dir, cosThetaOutDir);

//                            if(fr != zero<Vec3f>()) {
//                                float pdfWrtArea = 1.f / faceCount;
//                                pdfWrtArea /= abs(determinant(m_OpeningGraph.getGridToWorldMatrix()));
//                                auto pdfWrtSolidAngle = distanceSquared(faceCenterWorldSpace, I.P) * pdfWrtArea / abs(dot(dir, faceNormal));

//                                throughput *= fr * abs(cosThetaOutDir) / pdfWrtSolidAngle;


//                                if(i == 1u) {
//                                    accumulate(m_nMaxPathDepth + 1, pixelID, Vec4f(getColor(portalIdx), 1.f));
//                                }

//                                auto ray = Ray(I, dir);
//                                I = getScene().intersect(ray);
//                                wo = -ray.dir;
//                            }
//                        }
//                    }
//                }
//                hasSampledPortal = true;
//            }


//            if(!hasSampledPortal) {
//                Sample3f wi;
//                float cosThetaOutDir;

//                auto fr = bsdf.sample(Vec3f(getFloat(threadID), getFloat2(threadID)), wi, cosThetaOutDir, &sampledEvent);

//                if(fr != zero<Vec3f>() && wi.pdf > 0.f) {
//                    throughput *= fr * abs(cosThetaOutDir) / wi.pdf;
//                    I = getScene().intersect(secondaryRay(I, wi.value));
//                    wo = -wi.value;
//                } else {
//                    break;
//                }
//            }
//        }
//    }

//    accumulate(0, pixelID, Vec4f(L, 0.f));
//}

//void VoxelOpeningSegmentationGraphPathtraceRenderer2::doExposeIO(GUI& gui) {
//    gui.addVarRW(BNZ_GUI_VAR(m_nMaxPathDepth));
//    gui.addVarRW(BNZ_GUI_VAR(m_fUseSegProbability));
//}

//void VoxelOpeningSegmentationGraphPathtraceRenderer2::doLoadSettings(const tinyxml2::XMLElement& xml) {
//    getAttribute(xml, "maxDepth", m_nMaxPathDepth);
//    getAttribute(xml, "useSegProbability", m_fUseSegProbability);
//}

//void VoxelOpeningSegmentationGraphPathtraceRenderer2::doStoreSettings(tinyxml2::XMLElement& xml) const {
//    setAttribute(xml, "maxDepth", m_nMaxPathDepth);
//    setAttribute(xml, "useSegProbability", m_fUseSegProbability);
//}

//void VoxelOpeningSegmentationGraphPathtraceRenderer2::processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const {
//    processTileSamples(viewport, [this, threadID](uint32_t x, uint32_t y, uint32_t pixelID, uint32_t sampleID) {
//        processSample(threadID, pixelID, sampleID, x, y);
//    });
//}

//}
