//#pragma once

//#include "VoxelOpeningSegmentationGraph.hpp"

//#include <bonez/rendering/renderers/TileProcessingRenderer.hpp>
//#include <bonez/scene/lights/PowerBasedLightSampler.hpp>
//#include <bonez/utils/DijkstraAlgorithm.hpp>
//#include <bonez/utils/HashGrid.hpp>
//#include <bonez/utils/KdTree.hpp>

//namespace BnZ {

//class VoxelOpeningSegmentationGraphPathtraceRenderer2: public TileProcessingRenderer {
//public:
//    const VoxelOpeningSegmentationGraph& m_OpeningGraph;
//    PowerBasedLightSampler m_LightSampler;

//    const PointLight* m_pPointLight = nullptr;

//    DijkstraAlgorithm::ShortestPathVector m_ShortestPaths;

//    struct PortalLink {
//        Intersection m_Point;
//        uint32_t m_nPortalIndex;

//        PortalLink(const Intersection& I, uint32_t portalIdx):
//            m_Point(I), m_nPortalIndex(portalIdx) {
//        }
//    };

//    friend bool isValid(const PortalLink& link) {
//        return true;
//    }

//    friend const Vec3f& getPosition(const PortalLink& link) {
//        return link.m_Point.P;
//    }

//    std::vector<PortalLink> m_PortalLinks;
//    HashGrid m_PortalLinkHashGrid;
//    //KdTree m_PortalLinkKdTree;
//    mutable std::vector<uint32_t> m_PortalBuffer;

//    float m_fLookUpRadius = 0.f;

//    uint32_t m_nMaxPathDepth = 2;
//    float m_fUseSegProbability = 0.5f;
//    uint32_t m_nPortalLinkCountPerPortal = 4096u;

//    VoxelOpeningSegmentationGraphPathtraceRenderer2(
//            const VoxelOpeningSegmentationGraph& openingGraph):
//        m_OpeningGraph(openingGraph) {
//    }

//    void preprocess() override;

//    void processSample(uint32_t threadID, uint32_t pixelID, uint32_t sampleID, uint32_t x, uint32_t y) const;

//    void doExposeIO(GUI& gui) override;

//    void doLoadSettings(const tinyxml2::XMLElement& xml) override;

//    void doStoreSettings(tinyxml2::XMLElement& xml) const override;

//    void processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const override;

//    void initFramebuffer() override {
//        addFramebufferChannel("final_render");
//        for(auto i : range(m_nMaxPathDepth)) {
//            addFramebufferChannel("depth_" + toString(i + 1));
//        }
//    }
//};

//}
