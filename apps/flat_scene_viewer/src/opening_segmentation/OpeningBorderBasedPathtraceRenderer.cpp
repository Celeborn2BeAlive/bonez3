#include "OpeningBorderBasedPathtraceRenderer.hpp"

#include <bonez/scene/shading/BSDF.hpp>
#include <bonez/voxskel/CubicalComplex3D.hpp>

#include <bonez/scene/sensors/PixelSensor.hpp>
#include <bonez/scene/sensors/SurfacePointCamera.hpp>

#include <bonez/opengl/debug/GLDebugRenderer.hpp>

#include <bonez/sampling/distribution1d.h>

namespace BnZ {

void OpeningBorderBasedPathtraceRenderer::preprocess() {
    m_LightSampler.initFrame(getScene());

    m_fSceneDiag = computeSceneDiag(getScene());

    m_pPointLight = nullptr;

    getScene().getLightContainer().forEach<PointLight>([&](std::size_t index, const PointLight& light) {
       if(!m_pPointLight) {
           m_pPointLight = &light;
       }
    });

    if(!m_pPointLight) {
        throw std::runtime_error("No PointLight in scene");
    }

    auto voxel = m_OpeningGraph.getVoxel(m_pPointLight->m_Position);
    auto lightClusterIdx = m_OpeningGraph.getVoxelCluster(voxel);

    m_ShortestPaths = computeDijkstraShortestPaths(m_OpeningGraph.getGraph(),
                                                               lightClusterIdx,
                                                               [&](GraphNodeIndex n1, GraphNodeIndex n2) {
                                                                         return 1.f;
                                                               });



    m_BordersRTScene = RTScene();
    m_RTScene = RTScene();
    m_BorderMeshs.clear();
    m_BorderFaceIncidentPower.clear();
    m_BorderFaceSamplingDistributions.clear();

    for (const auto clusterIdx : range(m_OpeningGraph.size())) {
        m_BorderMeshs.emplace_back(buildBorderMesh(m_OpeningGraph, clusterIdx));

        auto faceCount = m_BorderMeshs[clusterIdx].getTriangleCount();

        m_BorderFaceIncidentPower.emplace_back(faceCount, Vec2f(0.f));
        auto distribSize = getDistribution1DBufferSize(faceCount);
        m_BorderFaceSamplingDistributions.emplace_back(distribSize, 0.f);
    }

    for (const auto& mesh : m_BorderMeshs) {
        m_BordersRTScene.addTriangleMesh(mesh);
    }
    m_BordersRTScene.commit();

    m_RTScene.addInstance(getScene().getRTScene());
    m_RTScene.addInstance(m_BordersRTScene);
    m_RTScene.commit();
}

void OpeningBorderBasedPathtraceRenderer::beginFrame() {
    m_SampledRays.clear();
    m_SampledRays.resize(getFramebuffer().getPixelCount());

    const size_t maxDepth = m_nMaxPathDepth;

    auto estimateIrradiance = [&](size_t threadID, const TriangleMesh::Vertex& vertex) {
        auto irradiance = zero<Vec3f>();
        auto throughtput = Vec3f(1.f);

        auto wiSample = cosineSampleHemisphere(getFloat(threadID), getFloat(threadID), vertex.normal);
        if(wiSample.pdf) {
            Intersection I;
            auto ray = Ray(vertex.position, wiSample.value);
            throughtput *= abs(dot(wiSample.value, vertex.normal)) * wiSample.pdf;

            auto depth = 1;
            while(depth <= maxDepth && (I = getScene().intersect(ray))) {
                BSDF bsdf(-wiSample.value, I, getScene());
                RaySample shadowRay;

                auto lightPdf = 0.f;
                auto pLight = m_LightSampler.sample(getScene(), getFloat(threadID), lightPdf);

                auto Le = pLight->sampleDirectIllumination(getScene(), getFloat2(threadID), I, shadowRay);
                if(Le != zero<Vec3f>() && shadowRay.pdf) {
                    float cosThetaOutDir;
                    auto fr = bsdf.eval(shadowRay.value.dir, cosThetaOutDir);

                    if(fr != zero<Vec3f>() && !getScene().occluded(shadowRay.value)) {
                        auto contrib =  throughtput * Le * fr * cosThetaOutDir / (lightPdf * shadowRay.pdf);
                        irradiance += contrib;
                    }
                }

                float cosThetaOutDir;
                auto fr = bsdf.sample(getFloat3(threadID), wiSample, cosThetaOutDir);
                if(fr != zero<Vec3f>() && wiSample.pdf > 0.f) {
                    throughtput *= fr * abs(cosThetaOutDir) / wiSample.pdf;
                    ++depth;
                } else {
                    break;
                }
            }
        }

        return irradiance;
    };

    const size_t nbSamples = 1;

    for(auto clusterIdx : range(m_OpeningGraph.size())) {
        auto triangleCount = m_BorderMeshs[clusterIdx].getTriangleCount();

        processTasksDeterminist(triangleCount, [&](size_t triangleIdx, size_t threadID) {
            for(auto i: range(nbSamples)) {
                auto vertexSample = sampleTriangle(m_BorderMeshs[clusterIdx], triangleIdx, getFloat2(threadID));
                m_BorderFaceIncidentPower[clusterIdx][triangleIdx] +=
                        Vec2f(luminance(estimateIrradiance(0, vertexSample.value) / vertexSample.pdf), 1.f);
            }
        }, getThreadCount());
    }

    processTasksDeterminist (m_OpeningGraph.size(), [&](size_t clusterIdx, size_t threadID) {
        buildDistribution1D([&](size_t triangleIdx) {
            //auto faceIdx = triangleIdx / 2;
            //return m_OpeningGraph.isClusterOnEmptySpace(m_OpeningGraph.getCluster(clusterIdx).m_Border[faceIdx].m_nDestinationCluster) ? 1.f : 0.f;
            return m_BorderFaceIncidentPower[clusterIdx][triangleIdx].x / m_BorderFaceIncidentPower[clusterIdx][triangleIdx].y;
        }, m_BorderFaceSamplingDistributions[clusterIdx].data(), m_BorderMeshs[clusterIdx].getTriangleCount());
    }, getThreadCount());
}

Sample<TriangleMesh::Vertex> OpeningBorderBasedPathtraceRenderer::sampleBorderVertex(
        std::size_t clusterIdx,
        std::size_t threadID,
        std::size_t& triangleIdx) const{
	// Sampling in grid space:
	//const auto& portals = m_OpeningGraph.getPortals();
	//auto faceCount = portals[portalIdx].size();
	//auto faceSample = min(size_t(getFloat(threadID) * faceCount), faceCount - 1);
	//// First attempt, just trace through the center of the voxel face
	//Vec3i u, v;
	//getFaceVectors(CC3DFaceBits::Value(portals[portalIdx][faceSample].m_VoxelFace.w), u, v);

	//auto uWs = Vec3f(m_OpeningGraph.getGridToWorldMatrix() * Vec4f(Vec3f(u), 0.f));
	//auto vWs = Vec3f(m_OpeningGraph.getGridToWorldMatrix() * Vec4f(Vec3f(v), 0.f));

	//auto pdf = 1.f / (faceCount * length(uWs) * length(vWs));

	//auto texCoords = Vec2f(getFloat(threadID), getFloat(threadID));

	//auto faceCenter = Vec3f(portals[portalIdx][faceSample].m_VoxelFace) + Vec3f(u) * texCoords.x + Vec3f(v) * texCoords.y;
	//auto faceCenterWorldSpace = Vec3f(m_OpeningGraph.getGridToWorldMatrix() * Vec4f(faceCenter, 1.f));
	//auto faceNormal = normalize(Vec3f(m_OpeningGraph.getGridToWorldMatrix() * Vec4f(Vec3f(portals[portalIdx][faceSample].m_OutgoingNormal), 0.f)));

	//return Sample<TriangleMesh::Vertex>(
	//	TriangleMesh::Vertex(faceCenterWorldSpace, faceNormal, texCoords), pdf);

    auto triangleSample = sampleDiscreteDistribution1D(m_BorderFaceSamplingDistributions[clusterIdx].data(),
                                                   m_BorderMeshs[clusterIdx].getTriangleCount(),
                                                   getFloat(threadID));

    if(triangleSample.pdf > 0.f) {
        triangleIdx = triangleSample.value;

        auto vertex = sampleTriangle(m_BorderMeshs[clusterIdx], triangleSample.value,
                                     getFloat2(threadID));
        vertex.pdf *= triangleSample.pdf;

        return vertex;
    }
    return { TriangleMesh::Vertex{}, 0.f };
}

Sample1u OpeningBorderBasedPathtraceRenderer::sampleBorderTriangle(std::size_t clusterIdx, std::size_t threadID) const {
    return sampleDiscreteDistribution1D(m_BorderFaceSamplingDistributions[clusterIdx].data(),
                                        m_BorderMeshs[clusterIdx].getTriangleCount(),
                                        getFloat(threadID));
}

float OpeningBorderBasedPathtraceRenderer::computeBorderTriangleSamplingProbability(std::size_t clusterIdx,
                                               std::size_t triangleIdx,
                                               const Intersection& I) const {
    auto center = computeTriangleCenter(m_BorderMeshs[clusterIdx], triangleIdx);

    auto dirToCenter = center.position - I.P;
    auto distToCenter = length(dirToCenter);
    dirToCenter /= distToCenter;
    auto sqrDistToCenter = sqr(distToCenter);
    auto dotAtCenter = dot(dirToCenter, center.normal);

    if(sqrDistToCenter > sqr(m_fDistToPortalPercentageTreshold * m_fSceneDiag) && dotAtCenter > m_fPortalOrientationThreshold) {
        return m_fUseSegProbability;
    }
    return 0.f;
}

Sample<TriangleMesh::Vertex> OpeningBorderBasedPathtraceRenderer::sampleBorderTriangleVertex(
        std::size_t clusterIdx,
        std::size_t triangleIdx,
        std::size_t threadID) const {
    return sampleTriangle(m_BorderMeshs[clusterIdx], triangleIdx,
                          getFloat2(threadID));
}

float OpeningBorderBasedPathtraceRenderer::pdfBorderVertex(std::size_t clusterIdx, std::size_t triangleIdx) const {
    auto trianglePdf = pdfDiscreteDistribution1D(m_BorderFaceSamplingDistributions[clusterIdx].data(),
                                                 triangleIdx);

    return trianglePdf / m_BorderMeshs[clusterIdx].getTriangleArea(triangleIdx);
}

void OpeningBorderBasedPathtraceRenderer::processSampleSurfacePointCamera(
        const Ray& ray,
        uint32_t pixelID,
        const SurfacePointCamera* pCamera) const {
    if(pCamera) {
        const auto& camera = *pCamera;
        accumulate(PRIMARY_RAY_PDF_WRT_BRDF, pixelID, Vec4f(Vec3f(cosineSampleHemispherePDF(ray.dir, camera.getSurfacePoint().Ns)), 0.f));

        auto voxel = m_OpeningGraph.getVoxel(camera.getSurfacePoint());
        auto openingClusterIdx = m_OpeningGraph.getVoxelCluster(voxel);

        float pdfWrtSolidAngle = 0.f;
        auto filter = [&](const RTScene::Hit& hit, uint32_t threadID) {
            if(int(hit.m_nMeshID) == openingClusterIdx) {
                auto borderPdf = pdfBorderVertex(openingClusterIdx, hit.m_nTriangleID);
                pdfWrtSolidAngle = distanceSquared(hit.m_P, camera.getSurfacePoint().P) * borderPdf / abs(dot(ray.dir, hit.m_Ng));
            }
            return true;
        };

        RTScene::Hit hit;
        m_BordersRTScene.intersect(ray, hit, filter);

        accumulate(PRIMARY_RAY_PDF_WRT_SEG, pixelID, Vec4f(Vec3f(pdfWrtSolidAngle), 0.f));
    }
}

void OpeningBorderBasedPathtraceRenderer::computeBorderVertexData(
        std::size_t clusterIdx,
        std::size_t triangleIdx,
        const Vec3f& borderPosition,
        const Vec3f& borderNormal,
        const Intersection& I,
        const BSDF& bsdf,
        float& useSegProb,
        float& dotAtPortal,
        float& distToSampledPoint,
        float& sqrDistToSampledPoint,
        Vec3f& dirIntersectionToPoint,
        float& cosThetaOutDir,
        Vec3f& fr) const {
    useSegProb = computeBorderTriangleSamplingProbability(clusterIdx, triangleIdx, I);

    dirIntersectionToPoint = borderPosition - I.P;
    distToSampledPoint = length(dirIntersectionToPoint);
    dirIntersectionToPoint /= distToSampledPoint;
    sqrDistToSampledPoint = sqr(distToSampledPoint);
    dotAtPortal = dot(dirIntersectionToPoint, borderNormal);
    fr = bsdf.eval(dirIntersectionToPoint, cosThetaOutDir);

//    if(sqrDistToSampledPoint > sqr(m_fDistToPortalPercentageTreshold * m_fSceneDiag) &&
//            dotAtPortal > m_fPortalOrientationThreshold &&
//            fr != zero<Vec3f>()) {
//        useSegProb = m_fUseSegProbability;
//    } else {
//        useSegProb = 0.f;
//    }
}

void OpeningBorderBasedPathtraceRenderer::processSample(uint32_t threadID, uint32_t pixelID, uint32_t sampleID, uint32_t x, uint32_t y) const {
    for(auto i = 0u; i < getFramebufferChannelCount(); ++i) {
        accumulate(i, pixelID, Vec4f(0.f, 0.f, 0.f, 1.f));
    }

    PixelSensor sensor(getSensor(), Vec2u(x, y), getFramebufferSize());

    auto pixelSample = getPixelSample(threadID, sampleID);
    auto lensSample = getFloat2(threadID);

    RaySample raySample;
    float positionPdf, directionPdf;
    sensor.sampleExitantRay(
                getScene(),
                lensSample,
                pixelSample,
                raySample,
                positionPdf,
                directionPdf);

    auto ray = raySample.value;

    if(!ray) {
        return;
    }

    processSampleSurfacePointCamera(ray, pixelID, dynamic_cast<const SurfacePointCamera*>(&getSensor()));

    Vec3f throughput = Vec3f(1.f);
    Vec3f L = Vec3f(0.f);

    Intersection I = getScene().intersect(ray);
    Vec3f wo = -ray.dir;

    if(acceptPathDepth(1)) {
        accumulate(FINAL_RENDER_DEPTH1, pixelID, Vec4f(I.Le, 0.f));
        L += I.Le;
    }

    uint32_t sampledEvent = ScatteringEvent::Emission;

    for(auto i = 1u; I && i <= m_nMaxPathDepth; ++i) {
        BSDF bsdf(wo, I, getScene());
        // Next event estimation
        if(i < m_nMaxPathDepth && acceptPathDepth(i + 1)) {
            RaySample shadowRay;

            auto lightPdf = 0.f;
            auto pLight = m_LightSampler.sample(getScene(), getFloat(threadID), lightPdf);

            auto Le = pLight->sampleDirectIllumination(getScene(), getFloat2(threadID), I, shadowRay);
            if(Le != zero<Vec3f>() && shadowRay.pdf) {
                float cosThetaOutDir;
                auto fr = bsdf.eval(shadowRay.value.dir, cosThetaOutDir);

                if(fr != zero<Vec3f>() && !getScene().occluded(shadowRay.value)) {
                    auto contrib = throughput * Le * fr * cosThetaOutDir / (lightPdf * shadowRay.pdf);
                    accumulate(FINAL_RENDER_DEPTH1 + i, pixelID, Vec4f(contrib, 0.f));
                    if(i > 1u) {
                        if(sampledEvent == ScatteringEvent::Other) {
                            accumulate(PORTAL_CONTRIB, pixelID, Vec4f(contrib, 0.f));
                        } else {
                            accumulate(BSDF_CONTRIB, pixelID, Vec4f(contrib, 0.f));
                        }
                    }
                    L += contrib;
                }
            }
        }

        // If the scattering was specular, add Le
        if((sampledEvent & ScatteringEvent::Specular) && acceptPathDepth(i)) {
            auto contrib = throughput * I.Le;
            accumulate(FINAL_RENDER_DEPTH1 + i - 1, pixelID, Vec4f(contrib, 0.f));
            L += contrib;
        }

        if(i < m_nMaxPathDepth - 1 || bsdf.hasSpecularComponent()) {

            auto voxel = m_OpeningGraph.getVoxel(I, bsdf.getIncidentDirection());
            auto openingClusterIdx = m_OpeningGraph.getVoxelCluster(voxel);

            Sample<TriangleMesh::Vertex> vertexSample;
            float useSegProb = 0.f;
            float dotAtPortal = 0.f;
            float distToSampledPoint = 0.f;
            float sqrDistToSampledPoint = 0.f;
            Vec3f dirIntersectionToPoint = zero<Vec3f>();
            float cosThetaOutDir = 0.f;
            Vec3f fr = zero<Vec3f>();

            if(!bsdf.isDelta() && openingClusterIdx != VoxelOpeningSegmentationGraph::NO_CLUSTER()) {
                std::size_t triangleIdx;
                vertexSample = sampleBorderVertex(openingClusterIdx, threadID, triangleIdx);
                if(vertexSample.pdf > 0.f) {
                    computeBorderVertexData(
                                openingClusterIdx, triangleIdx,
                                vertexSample.value.position, vertexSample.value.normal, I, bsdf,
                                useSegProb, dotAtPortal,
                                distToSampledPoint, sqrDistToSampledPoint, dirIntersectionToPoint,
                                cosThetaOutDir, fr);

                    if(useSegProb == 0.f) {
                        auto faceIdx = triangleIdx / 2;
                        openingClusterIdx = m_OpeningGraph.getCluster(openingClusterIdx).m_Border[faceIdx].m_nDestinationCluster;
                        vertexSample = sampleBorderVertex(openingClusterIdx, threadID, triangleIdx);
                        if(vertexSample.pdf > 0.f) {
                            computeBorderVertexData(
                                        openingClusterIdx, triangleIdx,
                                        vertexSample.value.position, vertexSample.value.normal, I, bsdf,
                                        useSegProb, dotAtPortal,
                                        distToSampledPoint, sqrDistToSampledPoint, dirIntersectionToPoint,
                                        cosThetaOutDir, fr);
                        }
                    }
                }
            }

            //float useSegProb = !bsdf.isDelta() && portalIdx >= 0 ? m_fUseSegProbability : 0.f;

            auto strategySample = getFloat(threadID);
            if(strategySample <= useSegProb) {
                //auto vertexSample = sample(m_PortalMeshs[portalIdx], getFloat(threadID), getFloat2(threadID));
                sampledEvent = ScatteringEvent::Other;

                auto pdfWrtSolidAngle = sqrDistToSampledPoint * vertexSample.pdf / abs(dotAtPortal);

                auto value = fr * abs(cosThetaOutDir) / pdfWrtSolidAngle;

                auto weight = 1.f;
                //auto portalPdf = pdfPortal(portalIdx, I, dir);
                auto sum = (1 - useSegProb) * bsdf.pdf(dirIntersectionToPoint) + useSegProb * pdfWrtSolidAngle;
                if(sum > 0.f) {
                    weight = useSegProb * pdfWrtSolidAngle / sum;
                } else {
                    weight = 0.f;
                }

                if (i == 1) {
                    accumulate(PORTAL_RAY_PDF_PORTAL_CONTRIB, pixelID, Vec4f(Vec3f(pdfWrtSolidAngle), 0.f));
                    accumulate(BSDF_RAY_PDF_PORTAL_CONTRIB, pixelID, Vec4f(Vec3f(bsdf.pdf(dirIntersectionToPoint)), 0.f));
                }

                if (openingClusterIdx == m_nForcedPortal) {
					accumulate(PORTAL42_CONTRIB, pixelID, Vec4f(Vec3f(1.f / pdfWrtSolidAngle), 0.f));
				}

                throughput *= weight * value / useSegProb;

                auto ray = Ray(I, dirIntersectionToPoint);

                I = getScene().intersect(ray);

                wo = -ray.dir;
            } else {
                // Use brdf to sample next direction
                Sample3f wi;
                float cosThetaOutDir;

                auto fr = bsdf.sample(Vec3f(getFloat(threadID), getFloat2(threadID)), wi, cosThetaOutDir, &sampledEvent);

                if(fr != zero<Vec3f>() && wi.pdf > 0.f) {
                    auto value = fr * abs(cosThetaOutDir) / wi.pdf;
                    auto ray = Ray(I, wi.value);

                    bool intersectPortal42 = false;

                    RTScene::Hit hit;

                    float sum = 0.f;
                    float useSegProb = 0.f;
                    std::size_t intersectedFaceCount = 0u;
                    auto filter = [&](const RTScene::Hit& hit, uint32_t threadID) {
                          if(int(hit.m_nMeshID) == openingClusterIdx) {
                              float useSegProbLocal = 0.f;
                              float dotAtPortal, distToSampledPoint, sqrDistToSampledPoint,
                                      cosThetaOutDir;
                              Vec3f dirIntersectionToPoint;
                              Vec3f fr;
                              computeBorderVertexData(
                                          openingClusterIdx, hit.m_nTriangleID,
                                          hit.m_P,
                                          hit.m_Ng,
                                          I,
                                          bsdf,
                                          useSegProbLocal,
                                          dotAtPortal,
                                          distToSampledPoint,
                                          sqrDistToSampledPoint,
                                          dirIntersectionToPoint,
                                          cosThetaOutDir,
                                          fr);
                              if(useSegProbLocal > 0.f) {
                                  auto portalPdf = pdfBorderVertex(openingClusterIdx, hit.m_nTriangleID);
                                  auto pdfWrtSolidAngle = sqrDistToSampledPoint * portalPdf / abs(dotAtPortal);

                                  sum += pdfWrtSolidAngle;

                                  useSegProb = useSegProbLocal;

                                  ++intersectedFaceCount;
                              }
                          }

                          if(hit.m_nMeshID == m_nForcedPortal) {
                              intersectPortal42 = true;
                          }

                          return true;
                    };
                    m_BordersRTScene.intersect(ray, hit, filter);

                    if(useSegProb == 1.f) {
                        break;
                    }

                    float weight = 1.f;
                    if(!(sampledEvent & ScatteringEvent::Specular)) {
                        auto portalPdf = sum;
                        auto sum = (1 - useSegProb) * wi.pdf + useSegProb * portalPdf;
                        if(sum > 0.f) {
                            weight = (1 - useSegProb) * wi.pdf / sum;
                        } else {
                            weight = 0.f;
                        }
                    }

                    if (i == 1) {
                        accumulate(PORTAL_RAY_PDF_BSDF_CONTRIB, pixelID, Vec4f(Vec3f(sum), 0.f));
                        accumulate(BSDF_RAY_PDF_BSDF_CONTRIB, pixelID, Vec4f(Vec3f(wi.pdf), 0.f));
                    }

                    if(intersectPortal42) {
                        accumulate(PORTAL42_CONTRIB, pixelID, Vec4f(Vec3f(1.f / wi.pdf), 0.f));
                    }

                    throughput *= weight * value / (1 - useSegProb);
                    if(pixelID == 96730 && isInvalidMeasurementEstimate(throughput)) {
                        LOG(INFO) << "weight = " << weight;
                        LOG(INFO) << "value = " << value;
                        LOG(INFO) << "(1 - useSegProb) = " <<  (1 - useSegProb);
                        LOG(INFO) << "useSegProb = " <<  useSegProb;
                        LOG(INFO) << "depth = " <<  i;
                        LOG(INFO) << "strategySample = " << strategySample;
                        exit(0);
                    }

                    I = getScene().intersect(ray);
                    wo = -wi.value;
                } else {
                    break;
                }
            }
        }
    }

    accumulate(FINAL_RENDER, pixelID, Vec4f(L, 0.f));
}


void OpeningBorderBasedPathtraceRenderer::processSampleOnePortal(uint32_t threadID, uint32_t pixelID, uint32_t sampleID, uint32_t x, uint32_t y) const {
    for (auto i = 0u; i < getFramebufferChannelCount(); ++i) {
        accumulate(i, pixelID, Vec4f(0.f, 0.f, 0.f, 1.f));
    }

    PixelSensor sensor(getSensor(), Vec2u(x, y), getFramebufferSize());

    auto pixelSample = getPixelSample(threadID, sampleID);
    auto lensSample = getFloat2(threadID);

    RaySample raySample;
    float positionPdf, directionPdf;
    sensor.sampleExitantRay(
        getScene(),
        lensSample,
        pixelSample,
        raySample,
        positionPdf,
        directionPdf);

    auto ray = raySample.value;

    if (!ray) {
        return;
    }

    processSampleSurfacePointCamera(ray, pixelID, dynamic_cast<const SurfacePointCamera*>(&getSensor()));

    Vec3f throughput = Vec3f(1.f);
    Vec3f L = Vec3f(0.f);

    Intersection I = getScene().intersect(ray);
    Vec3f wo = -ray.dir;

    if (acceptPathDepth(1)) {
        accumulate(FINAL_RENDER_DEPTH1, pixelID, Vec4f(I.Le, 0.f));
        L += I.Le;
    }

    uint32_t sampledEvent = ScatteringEvent::Emission;

    auto nextEventEstimation = [&](std::size_t depth, const Intersection& I, const BSDF& bsdf) {
        if (depth >= m_nMaxPathDepth || !acceptPathDepth(depth)) {
            return zero<Vec3f>();
        }

        RaySample shadowRay;

        auto Le = m_pPointLight->sampleDirectIllumination(getScene(), getFloat2(threadID), I, shadowRay);
        if (Le != zero<Vec3f>() && shadowRay.pdf) {
            float cosThetaOutDir;
            auto fr = bsdf.eval(shadowRay.value.dir, cosThetaOutDir);

            if (fr != zero<Vec3f>() && !getScene().occluded(shadowRay.value)) {
                auto contrib = throughput * Le * fr * cosThetaOutDir / shadowRay.pdf;
                accumulate(FINAL_RENDER_DEPTH1 + depth, pixelID, Vec4f(contrib, 0.f));
                if (depth > 1u) {
                    if (sampledEvent == ScatteringEvent::Other) {
                        accumulate(PORTAL_CONTRIB, pixelID, Vec4f(contrib, 0.f));
                    }
                    else {
                        accumulate(BSDF_CONTRIB, pixelID, Vec4f(contrib, 0.f));
                    }
                }
                return contrib;
            }
        }

        return zero<Vec3f>();
    };

    BSDF bsdf(wo, I, getScene());
    L += nextEventEstimation(1u, I, bsdf);

    SampledRay sampledRay;
    for (auto i = 1u; I && i <= m_nMaxPathDepth && throughput != zero<Vec3f>(); ++i) {
        //bsdf = BSDF(wo, I, getScene());

        //auto estim = nextEventEstimation(i, I, bsdf);
        //L += estim;

        // If the scattering was specular, add Le
        if ((sampledEvent & ScatteringEvent::Specular) && acceptPathDepth(i)) {
            auto contrib = throughput * I.Le;
            accumulate(FINAL_RENDER_DEPTH1 + i - 1, pixelID, Vec4f(contrib, 0.f));
            L += contrib;
        }

        if (i < m_nMaxPathDepth - 1 || bsdf.hasSpecularComponent()) {
            // Identify the portal to sample
            int portalIdx = m_nForcedPortal;

            Sample<TriangleMesh::Vertex> vertexSample;
            float useSegProb = 0.f;
            float dotAtPortal = 0.f;
            float distToSampledPoint = 0.f;
            float sqrDistToSampledPoint = 0.f;
            Vec3f dirIntersectionToPoint = zero<Vec3f>();
            float cosThetaOutDir = 0.f;
            Vec3f fr = zero<Vec3f>();

            if (!bsdf.isDelta() && portalIdx >= 0) {
                size_t triangleIdx;
                vertexSample = sampleBorderVertex(portalIdx, threadID, triangleIdx);
                dirIntersectionToPoint = vertexSample.value.position - I.P;
                distToSampledPoint = length(dirIntersectionToPoint);
                dirIntersectionToPoint /= distToSampledPoint;
                sqrDistToSampledPoint = sqr(distToSampledPoint);
                dotAtPortal = dot(dirIntersectionToPoint, vertexSample.value.normal);

                if (sqrDistToSampledPoint > 0.f &&
                    dotAtPortal > 0.f &&
                    (fr = bsdf.eval(dirIntersectionToPoint, cosThetaOutDir)) != zero<Vec3f>()) {
                    useSegProb = m_fUseSegProbability;
                }
            }

            //float useSegProb = !bsdf.isDelta() && portalIdx >= 0 ? m_fUseSegProbability : 0.f;

            auto strategySample = getFloat(threadID);

            if (strategySample <= useSegProb) {
                //auto vertexSample = sample(m_PortalMeshs[portalIdx], getFloat(threadID), getFloat2(threadID));
                sampledEvent = ScatteringEvent::Other;

                auto pdfWrtSolidAngle = sqrDistToSampledPoint * vertexSample.pdf / abs(dotAtPortal);

                auto value = fr * abs(cosThetaOutDir) / pdfWrtSolidAngle;

                auto weight = 1.f;
                if(m_bUseMaxHeuristic) {
                    if(pdfWrtSolidAngle >= bsdf.pdf(dirIntersectionToPoint)) {
                        weight = 1.f;
                    } else {
                        weight = 0.f;
                    }
                } else {
                    auto sum = (1 - useSegProb) * bsdf.pdf(dirIntersectionToPoint) + useSegProb * pdfWrtSolidAngle;
                    if (sum > 0.f) {
                        weight = useSegProb * pdfWrtSolidAngle / sum;
                    }
                    else {
                        weight = 0.f;
                    }
                }

                if (i == 1) {
                    accumulate(PORTAL_RAY_PDF_PORTAL_CONTRIB, pixelID, Vec4f(Vec3f(pdfWrtSolidAngle), 0.f));
                    accumulate(BSDF_RAY_PDF_PORTAL_CONTRIB, pixelID, Vec4f(Vec3f(bsdf.pdf(dirIntersectionToPoint)), 0.f));
                }

                if (portalIdx == m_nForcedPortal) {
                    accumulate(PORTAL42_CONTRIB, pixelID, Vec4f(Vec3f(1.f / pdfWrtSolidAngle), 0.f));
                }

                throughput *= weight * value / useSegProb;

                if(throughput == zero<Vec3f>()) {
                    break;
                }

                auto ray = Ray(I, dirIntersectionToPoint);

                sampledRay.fromPortals = true;
                sampledRay.origin = I;

                I = getScene().intersect(ray);

                sampledRay.destination = I;

                wo = -ray.dir;

                if(I) {
                    bsdf.init(wo, I, getScene());
                    L += nextEventEstimation(i + 1, I, bsdf);
                }
            }
            else {
                // Use brdf to sample next direction
                Sample3f wi;
                float cosThetaOutDir;

                auto fr = bsdf.sample(Vec3f(getFloat(threadID), getFloat2(threadID)), wi, cosThetaOutDir, &sampledEvent);

                if (fr != zero<Vec3f>() && wi.pdf > 0.f) {
                    auto value = fr * abs(cosThetaOutDir) / wi.pdf;
                    auto ray = Ray(I, wi.value);

                    bool intersectPortal42 = false;

                    RTScene::Hit hit;

//                    auto thePixelToTest = std::numeric_limits<uint32_t>::max();

                    float portalPdf = 0.f;
                    auto filter = [&, I, bsdf](const RTScene::Hit& hit, uint32_t threadID) {
                        if (int(hit.m_nMeshID) == portalIdx) {
                            auto dirIntersectionToPoint = vertexSample.value.position - I.P;
                            auto distToSampledPoint = length(dirIntersectionToPoint);
                            dirIntersectionToPoint /= distToSampledPoint;
                            //dirIntersectionToPoint = wi.value;
                            auto sqrDistToSampledPoint = sqr(distToSampledPoint);
                            auto dotAtPortal = dot(dirIntersectionToPoint, vertexSample.value.normal);

//                            if(thePixelToTest == pixelID) {
//                                LOG(INFO) << bsdf.eval(dirIntersectionToPoint, cosThetaOutDir) << ", " << sqrDistToSampledPoint;
//                            }

                            float cosThetaOutDir;
                            if (sqrDistToSampledPoint > 0.f &&
                                dotAtPortal > 0.f &&
                                bsdf.eval(dirIntersectionToPoint, cosThetaOutDir) != zero<Vec3f>()) {

                                auto portalPdfWrtArea = pdfBorderVertex(portalIdx, hit.m_nTriangleID);
                                auto pdfWrtSolidAngle = sqrDistToSampledPoint * portalPdfWrtArea / abs(dotAtPortal);

                                portalPdf += pdfWrtSolidAngle;

//                                if(thePixelToTest == pixelID) {
//                                    auto l = debugLock();
//                                    LOG(INFO) << "pdfs = " << pdfWrtSolidAngle << ", " << wi.pdf << ", " <<  bool((sampledEvent & ScatteringEvent::Specular));

//                                    float weight = 1.f;
//                                    if (!(sampledEvent & ScatteringEvent::Specular)) {
//                                        auto sum = (1 - useSegProb) * wi.pdf + useSegProb * portalPdf;
//                                        if (sum > 0.f) {
//                                            weight = (1 - useSegProb) * wi.pdf / sum;
//                                        }
//                                        else {
//                                            weight = 0.f;
//                                        }
//                                    }
//                                    LOG(INFO) << "weight = " << weight;
//                                }
                            }
                        }

                        if (hit.m_nMeshID == m_nForcedPortal) {
                            intersectPortal42 = true;
                        }

                        return true;
                    };
                    m_BordersRTScene.intersect(ray, hit, filter);

                    float weight = 1.f;
                    if (!(sampledEvent & ScatteringEvent::Specular)) {
                        if(m_bUseMaxHeuristic) {
                            if(wi.pdf > portalPdf) {
                                weight = 1.f;
                            } else {
                                weight = 0.f;
                            }
                        } else {
                            auto sum = (1 - useSegProb) * wi.pdf + useSegProb * portalPdf;
                            if (sum > 0.f) {
                                weight = (1 - useSegProb) * wi.pdf / sum;
                            }
                            else {
                                weight = 0.f;
                            }
                        }
                    }

                    if (i == 1) {
                        accumulate(PORTAL_RAY_PDF_BSDF_CONTRIB, pixelID, Vec4f(Vec3f(portalPdf), 0.f));
                        accumulate(BSDF_RAY_PDF_BSDF_CONTRIB, pixelID, Vec4f(Vec3f(wi.pdf), 0.f));
                    }

                    if (intersectPortal42) {
                        accumulate(PORTAL42_CONTRIB, pixelID, Vec4f(Vec3f(1.f / wi.pdf), 0.f));
                    }

                    throughput *= weight * value / (1 - useSegProb);

                    if(throughput == zero<Vec3f>()) {
                        break;
                    }

                    if (pixelID == 96730 && isInvalidMeasurementEstimate(throughput)) {
                        LOG(INFO) << "weight = " << weight;
                        LOG(INFO) << "value = " << value;
                        LOG(INFO) << "(1 - useSegProb) = " << (1 - useSegProb);
                        LOG(INFO) << "useSegProb = " << useSegProb;
                        LOG(INFO) << "depth = " << i;
                        LOG(INFO) << "strategySample = " << strategySample;
                    }

                    sampledRay.fromPortals = false;
                    sampledRay.origin = I;
                    sampledRay.bsdfPdf = wi.pdf;
                    sampledRay.portalPdf = portalPdf;
                    sampledRay.weight = weight;

                    I = getScene().intersect(ray);

                    sampledRay.destination = I;

                    wo = -wi.value;

                    if(I) {
                        bsdf.init(wo, I, getScene());
                        auto estim = nextEventEstimation(i + 1, I, bsdf);

                        if(i == 1 && estim != zero<Vec3f>()) {
                            m_SampledRays[pixelID] = sampledRay;
//                            if(weight == 1.f && threadID == 12) {
//                                LOG(INFO) << "original weight = " << weight;
//                                LOG(INFO) << "original pdfs = " << portalPdf << ", " << wi.pdf;
//                                //thePixelToTest = pixelID;
//                                m_PortalRTScene.intersect(ray, hit, filter);
//                            }
                        }

                        L += estim;
                    }
                }
                else {
                    break;
                }
            }
        }
    }

    accumulate(FINAL_RENDER, pixelID, Vec4f(L, 0.f));
}

void OpeningBorderBasedPathtraceRenderer::drawGLData(const ViewerData& viewerData) {
    viewerData.debugRenderer.addStream(&m_GLDebugStream);
    m_GLDebugStream.clearObjects();

    auto pixelID = getPixelIndex(viewerData.selectedPixel.x, viewerData.selectedPixel.y);

    if(pixelID >= 0u && pixelID < m_SampledRays.size()) {
        const auto& raySample = m_SampledRays[pixelID];
        if(raySample.origin && raySample.destination && !raySample.fromPortals) {
            m_GLDebugStream.addLine(raySample.origin.P, raySample.destination.P,
                                    Vec3f(1, 0, 0), Vec3f(1, 0, 0), 3);
        }
    }
//    for(const auto& raySample: m_SampledRays) {
//        if(raySample.origin && raySample.destination && !raySample.fromPortals) {
//            m_GLDebugStream.addLine(raySample.origin.P, raySample.destination.P,
//                                    Vec3f(1, 0, 0), Vec3f(1, 0, 0), 3);
//        }
//    }
}

void OpeningBorderBasedPathtraceRenderer::doExposeIO(GUI& gui) {
    gui.addVarRW(BNZ_GUI_VAR(m_nMaxPathDepth));
    gui.addVarRW(BNZ_GUI_VAR(m_fUseSegProbability));
    gui.addVarRW(BNZ_GUI_VAR(m_bUseUniformSphericalTriangleSampling));
    gui.addVarRW(BNZ_GUI_VAR(m_bUseMaxHeuristic));
    gui.addVarRW(BNZ_GUI_VAR(m_fDistToPortalPercentageTreshold));
    gui.addVarRW(BNZ_GUI_VAR(m_fPortalOrientationThreshold));
    gui.addVarRW(BNZ_GUI_VAR(m_nForcedPortal));
}

void OpeningBorderBasedPathtraceRenderer::doLoadSettings(const tinyxml2::XMLElement& xml) {
    serialize(xml, "maxDepth", m_nMaxPathDepth);
    serialize(xml, "useSegProbability", m_fUseSegProbability);
    serialize(xml, "useMaxHeuristic", m_bUseMaxHeuristic);
    serialize(xml, "distToPortalPercentageTreshold", m_fDistToPortalPercentageTreshold);
    serialize(xml, "portalOrientationThreshold", m_fPortalOrientationThreshold);
    serialize(xml, "forcedPortal", m_nForcedPortal);
}

void OpeningBorderBasedPathtraceRenderer::doStoreSettings(tinyxml2::XMLElement& xml) const {
    serialize(xml, "maxDepth", m_nMaxPathDepth);
    serialize(xml, "useSegProbability", m_fUseSegProbability);
    serialize(xml, "useMaxHeuristic", m_bUseMaxHeuristic);
    serialize(xml, "distToPortalPercentageTreshold", m_fDistToPortalPercentageTreshold);
    serialize(xml, "portalOrientationThreshold", m_fPortalOrientationThreshold);
    serialize(xml, "forcedPortal", m_nForcedPortal);
}

void OpeningBorderBasedPathtraceRenderer::processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const {
    if(m_nForcedPortal >= 0) {
        processTileSamples(viewport, [this, threadID](uint32_t x, uint32_t y, uint32_t pixelID, uint32_t sampleID) {
            processSampleOnePortal(threadID, pixelID, sampleID, x, y);
        });
    } else {
        processTileSamples(viewport, [this, threadID](uint32_t x, uint32_t y, uint32_t pixelID, uint32_t sampleID) {
            processSample(threadID, pixelID, sampleID, x, y);
        });
    }
}

}
