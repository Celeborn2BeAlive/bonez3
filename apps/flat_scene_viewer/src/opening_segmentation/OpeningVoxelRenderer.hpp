#pragma once

#include <bonez/voxskel/CubicalComplex3D.hpp>
#include <bonez/rendering/renderers/TileProcessingRenderer.hpp>
#include "VoxelOpeningSegmentationGraph.hpp"

#include <bonez/scene/sensors/PixelSensor.hpp>

namespace BnZ {

class OpeningVoxelRenderer: public TileProcessingRenderer {
public:
    RTScene m_RTScene;
    RTScene m_PortalRTScene;
    std::vector<TriangleMesh> m_PortalMeshs;
    const VoxelOpeningSegmentationGraph& m_OpeningGraph;

    bool m_bDisplayOpeningCluster = false;
    bool m_bDisplayPortals = false;

    OpeningVoxelRenderer(const VoxelOpeningSegmentationGraph& graph):
        m_OpeningGraph(graph) {
    }

    void preprocess() override {
		m_PortalMeshs.clear();

        m_PortalRTScene = RTScene();
        m_RTScene = RTScene();

        for (const auto portalIdx : range(m_OpeningGraph.getPortals().size())) {
            m_PortalMeshs.emplace_back(buildPortalMesh(m_OpeningGraph, portalIdx));
        }
        for (const auto& mesh : m_PortalMeshs) {
            m_PortalRTScene.addTriangleMesh(mesh);
        }
        m_PortalRTScene.commit();

        m_RTScene.addInstance(getScene().getRTScene());
        m_RTScene.addInstance(m_PortalRTScene);
        m_RTScene.commit();
    }

    void processSample(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                       uint32_t x, uint32_t y) const {
        PixelSensor sensor(getSensor(), Vec2u(x, y), getFramebufferSize());

        auto pixelSample = getPixelSample(threadID, sampleID);
        auto lensSample = getFloat2(threadID);

        RaySample raySample;
        float positionPdf, directionPdf;
        sensor.sampleExitantRay(
                    getScene(),
                    lensSample,
                    pixelSample,
                    raySample,
                    positionPdf,
                    directionPdf);

        auto ray = raySample.value;

        bool hasBeenFiltered = false;
        auto filter = [&](const RTScene::Hit& hit, uint32_t threadID) {
              if(!m_bDisplayPortals) {
                  if(hit.m_nInstID == 1u) {
                      return true; // filter portals only
                  }
                  return false;
              }
              if(hit.m_nInstID == 1u) {
				  //auto portalID = hit.m_nMeshID;
				  //auto faceID = hit.m_nTriangleID / 2;
				  //const auto& portals = m_OpeningGraph.getPortals();
				  //auto outgoingNormal = Vec3f(m_OpeningGraph.getGridToWorldMatrix() * Vec4f(portals[portalID][faceID].m_OutgoingNormal, 0));
				  if (dot(raySample.value.dir, hit.m_Ng) > 0.f) { // Display only faces leading outside the region
					  accumulate(0, pixelID, Vec4f(getColor(hit.m_nMeshID), 1.f));
					  //accumulate(0, pixelID, Vec4f(outgoingNormal, 1.f));
					  hasBeenFiltered = true;
					  return false;
				  }
				  else {
					  return true;
				  }
              }
              return false;
        };

        RTScene::Hit hit;
        if (!m_RTScene.intersect(ray, hit, filter) || hasBeenFiltered) {
            return;
        }

        auto I = getScene().postIntersect(ray, hit);

        if(I) {
            auto voxel = m_OpeningGraph.getVoxel(I, -ray.dir);

            Vec3f color = zero<Vec3f>();
            if(m_bDisplayOpeningCluster && m_OpeningGraph.getOpeningSegmentation().contains(voxel)) {
                if(m_OpeningGraph.getOpeningMap()(voxel) != 0u) {
                    color = getColor(m_OpeningGraph.getOpeningSegmentation()(voxel));
                }
            } else {
                if(m_OpeningGraph.getOpeningMap()(voxel) != 0u) {
                    color = getColor(m_OpeningGraph.getOpeningSegmentation().offset(voxel));
                }
            }
            accumulate(0, pixelID, Vec4f(color, 1.f));
        }
    }

    void processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const override {
        processTileSamples(viewport, [this, threadID](uint32_t x, uint32_t y, uint32_t pixelID, uint32_t sampleID) {
            processSample(threadID, pixelID, sampleID, x, y);
        });
    }

    void doExposeIO(GUI &gui) override {
        gui.addVarRW(BNZ_GUI_VAR(m_bDisplayOpeningCluster));
        gui.addVarRW(BNZ_GUI_VAR(m_bDisplayPortals));
    }

    void initFramebuffer() override {
        addFramebufferChannel("final_render");
    }
};

}
