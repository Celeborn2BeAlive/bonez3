#pragma once

#include "VoxelOpeningSegmentationGraph.hpp"

#include <bonez/rendering/renderers/TileProcessingRenderer.hpp>
#include <bonez/scene/lights/PowerBasedLightSampler.hpp>
#include <bonez/utils/DijkstraAlgorithm.hpp>

#include <bonez/opengl/debug/GLDebugStream.hpp>

namespace BnZ {

class SurfacePointCamera;

class VoxelOpeningSegmentationGraphPathtraceRenderer: public TileProcessingRenderer {
public:
    RTScene m_RTScene;
    RTScene m_PortalRTScene;
    std::vector<TriangleMesh> m_PortalMeshs;
    const VoxelOpeningSegmentationGraph& m_OpeningGraph;

    const PointLight* m_pPointLight = nullptr;

    PowerBasedLightSampler m_LightSampler;

    DijkstraShortestPathVector<float> m_ShortestPaths;

    uint32_t m_nMaxPathDepth = 2;
    float m_fUseSegProbability = 0.5f;
    int m_nForcedPortal = -1; // 88 for veach door

    bool m_bUseUniformSphericalTriangleSampling = true;
    bool m_bUseMaxHeuristic = false;

    struct SampledRay {
        Intersection origin;
        Intersection destination;
        bool fromPortals = true;
        float portalPdf = 0.f;
        float bsdfPdf = 0.f;
        float weight = 0.f;
    };

    mutable std::vector<SampledRay> m_SampledRays;

    GLDebugStreamData m_GLDebugStream;

    enum Outputs {
        FINAL_RENDER,
        PRIMARY_RAY_PDF_WRT_SEG,
        PRIMARY_RAY_PDF_WRT_BRDF,
        PORTAL_RAY_PDF_PORTAL_CONTRIB,
        BSDF_RAY_PDF_PORTAL_CONTRIB,
        PORTAL_RAY_PDF_BSDF_CONTRIB,
        BSDF_RAY_PDF_BSDF_CONTRIB,
        PORTAL42_CONTRIB,
        PORTAL_CONTRIB,
        BSDF_CONTRIB,
        FINAL_RENDER_DEPTH1
    };

    VoxelOpeningSegmentationGraphPathtraceRenderer(
            const VoxelOpeningSegmentationGraph& openingGraph):
        m_OpeningGraph(openingGraph) {
    }

    void preprocess() override;

    void beginFrame() override;

    void processSampleSurfacePointCamera(
            const Ray& ray,
            uint32_t pixelID,
            const SurfacePointCamera* pCamera) const;

    void processSample1(uint32_t threadID, uint32_t pixelID, uint32_t sampleID, uint32_t x, uint32_t y) const;

	void processSample2(uint32_t threadID, uint32_t pixelID, uint32_t sampleID, uint32_t x, uint32_t y) const;

    void doExposeIO(GUI& gui) override;

    void doLoadSettings(const tinyxml2::XMLElement& xml) override;

    void doStoreSettings(tinyxml2::XMLElement& xml) const override;

    void processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const override;

    Sample<TriangleMesh::Vertex> samplePortalVertex(std::size_t portalIdx, std::size_t threadID) const;

    float pdfPortalVertex(std::size_t portalIdx, std::size_t triangleIdx) const;

    void drawGLData(const ViewerData& viewerData) override;

    void initFramebuffer() {
        addFramebufferChannel("final_render");
        addFramebufferChannel("primary_ray_pdf_wrt_seg");
        addFramebufferChannel("primary_ray_pdf_wrt_brdf");
        addFramebufferChannel("portal_ray_pdf_portal_contrib");
        addFramebufferChannel("bsdf_ray_pdf_portal_contrib");
        addFramebufferChannel("portal_ray_pdf_bsdf_contrib");
        addFramebufferChannel("bsdf_ray_pdf_bsdf_contrib");
        addFramebufferChannel("portal42_contrib");
        addFramebufferChannel("portal_contrib");
        addFramebufferChannel("bsdf_contrib");
        for(auto i : range(m_nMaxPathDepth)) {
            addFramebufferChannel("depth_" + toString(i + 1));
        }
    }
};

}
