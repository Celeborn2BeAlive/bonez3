#include "SceneViewer.hpp"

namespace BnZ {

SceneViewer::SceneViewer(const FilePath& applicationPath, const FilePath& settingsFilePath):
    Viewer("FlatSceneViewer", applicationPath, settingsFilePath),
    m_GBufferRenderPass(m_ShaderManager),
    m_FlatShadingPass(m_ShaderManager),
    m_pGLDebugRenderer(makeShared<GLDebugRenderer>(m_ShaderManager)),
    m_VoxelSceneAnalyzer(*this),
    m_CenteredPointsAnalyser(*this),
    m_SkeletonViewerModule(m_ShaderManager) {
    m_pGLDebugRenderer->setArrowSize(getArrowBase(), getArrowLength());
}

void SceneViewer::initViewports() {
    // Setup the viewports depending on the window size and the framebuffer size
    float W = 0.75 * m_Settings.m_WindowSize.x;

    m_GBufferTexScreenSize.x = W / 5;
    m_GBufferTexScreenSize.y = m_GBufferTexScreenSize.x / m_Settings.m_fFramebufferRatio;

    Vec4f mainViewport(0, m_GBufferTexScreenSize.y, W, m_Settings.m_WindowSize.y - m_GBufferTexScreenSize.y);

    m_FinalRenderViewport =
            Vec4f(0.5 * (m_Settings.m_WindowSize.x - m_Settings.m_FramebufferSize.x),
                  0.5 * (m_Settings.m_WindowSize.y - m_Settings.m_FramebufferSize.y),
                  m_Settings.m_FramebufferSize);
}

void SceneViewer::initGLData() {
    m_GBuffer.init(m_Settings.m_FramebufferSize);

    m_pGLScene = m_pScene->getGLScene();
    if(!m_pGLScene) {
        m_pGLScene = makeShared<GLScene>(m_pScene->getGeometry());
    }

    m_GLFramebuffer.init(m_Settings.m_FramebufferSize);
}

void SceneViewer::setUp() {
    initGLData();
    initViewports();

    GLenum format = GL_RGB32F;
    m_GBufferDisplayFramebuffer.init(5 * m_GBufferTexScreenSize.x, m_GBufferTexScreenSize.y, &format, GL_NEAREST);

    m_SkeletonVisibilityCorrelationAnalizer.initialize(m_pScene.get());
    m_VoxelSceneAnalyzer.setUp();
    m_SkeletonViewerModule.setUp(*this);
    m_KdTreeTestViewerModule.setUp(*this);
}

void SceneViewer::tearDown() {
}

void SceneViewer::drawFinalRender() {
    m_GLFramebuffer.bindForReading();
    m_GLFramebuffer.setReadBuffer(0);

    glBlitFramebuffer(0, 0, m_GBuffer.getWidth(), m_GBuffer.getHeight(),
                      m_FinalRenderViewport.x, m_FinalRenderViewport.y, m_FinalRenderViewport.x + m_FinalRenderViewport.z, m_FinalRenderViewport.y + m_FinalRenderViewport.w,
                      GL_COLOR_BUFFER_BIT, GL_NEAREST);

    glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
}

void SceneViewer::setSelectedPixel(const Vec2u& pixel) {
    m_SelectedPixel = pixel;

    if(m_RenderModule.m_bCPURendering) {
        m_RenderModule.setSelectedPixel(pixel);
    }
}

float SceneViewer::getArrowLength() const {
    return length(size(getScene()->getBBox())) * 0.005f;
}

float SceneViewer::getArrowBase() const {
    return (1.f / 8) * getArrowLength();
}

void SceneViewer::doPicking() {
    if(!m_bGUIHasFocus && m_WindowManager.hasClicked()) {
        m_SelectedObjectID = m_GLFramebuffer.getObjectID(m_WindowManager.getCursorPosition(Vec2f(m_FinalRenderViewport)));

        if(m_SelectedObjectID == GLScreenFramebuffer::NULL_OBJECT_ID) {
            auto ndcPosition = m_WindowManager.getCursorNDC(m_FinalRenderViewport);
            if(viewportContains(ndcPosition, Vec4f(-1, -1, 2, 2))) {
                RaySample raySample;
                m_PickedIntersection =
                        tracePrimaryRay(getCamera(), *getScene(), Vec2f(0.5f), ndcToUV(ndcPosition), raySample);
                if(m_PickedIntersection) {
                    m_VoxelSceneAnalyzer.onPickIntersection();
                    m_PickedIntersectionIncidentDirection = -raySample.value.dir;
                }
            }
        } else {
            m_VoxelSceneAnalyzer.handlePicking(m_SelectedObjectID);
        }

        auto pixel = m_WindowManager.getCursorPosition(Vec2f(m_FinalRenderViewport));
        if(viewportContains(Vec2f(pixel), Vec4f(0, 0, m_FinalRenderViewport.z, m_FinalRenderViewport.w))) {
            setSelectedPixel(pixel);
        }
    }
}

void SceneViewer::storeScreenshotImage(const Image& image) {
    FilePath screenshotsPath = m_Path + "screenshots";
    createDirectory(screenshotsPath);
    FilePath baseName = m_ScreenshotPrefix + "screenshot_" + toString(getMicroseconds());
    FilePath pngFile = screenshotsPath + baseName.addExt(".png");

    storeImage(pngFile, image);
}

void SceneViewer::doScreenshot() {
    Image image;
    fillImage(image, m_GLFramebuffer.getColorBuffer());
    image.flipY();

    storeScreenshotImage(image);
}

void SceneViewer::drawFrame() {
    // Draw GUI
    exposeIO();
    m_RenderModule.exposeIO(m_GUI, m_GLImageRenderer);
    m_SkeletonViewerModule.drawGUI();
    m_KdTreeTestViewerModule.drawGUI();

    m_GBufferRenderPass.render(m_Camera.getProjMatrix(), m_Camera.getViewMatrix(),
                               m_ZNearFar.y, *m_pGLScene, m_GBuffer);
    m_GLFramebuffer.bindForDrawing();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glViewport(0, 0, m_GBuffer.getWidth(), m_GBuffer.getHeight());

    if(m_RenderModule.m_bCPURendering) {
        m_RenderModule.doCPURendering(m_CameraUpdateFlag, m_Camera, *m_pScene, m_GLImageRenderer);
    } else {
        m_FlatShadingPass.render(m_GBuffer, m_Camera.getRcpProjMatrix(),
                                 m_Camera.getViewMatrix(), m_ScreenTriangle);
    }

    m_GLFramebuffer.blitFramebuffer(m_GBuffer, GL_DEPTH_BUFFER_BIT, GL_NEAREST);

    m_DisplayStream.clearObjects();

    if (m_PickedIntersection) {
        m_DisplayStream.addArrow(m_PickedIntersection.P, m_PickedIntersection.Ns, getArrowLength(), getArrowBase(), Vec3f(1.f));
    }

    m_SkeletonVisibilityCorrelationAnalizer.drawPoints(*m_pGLDebugRenderer, m_ZNearFar.y);
    m_VoxelSceneAnalyzer.draw();
    m_CenteredPointsAnalyser.draw();
    m_SkeletonViewerModule.draw();
    m_KdTreeTestViewerModule.draw();

    m_RenderModule.drawGLData(m_PickedIntersection, m_PickedIntersectionIncidentDirection, *m_pGLDebugRenderer);

    m_pGLDebugRenderer->addStream(&m_DisplayStream);
    m_pGLDebugRenderer->render(m_Camera);

    doPicking();

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    glClear(GL_COLOR_BUFFER_BIT);

    drawFinalRender();

    if(m_bDoScreenshot) {
        doScreenshot();
        m_bDoScreenshot = false;
    }

    m_pGLDebugRenderer->clearStreams();
}

void SceneViewer::handleGlobalPreprocessParameters(const tinyxml2::XMLElement& input, tinyxml2::XMLElement& output) {
    auto pThinning = input.FirstChildElement("Thinning");
    if(pThinning) {
        uint32_t gridResolution;
        getChildAttribute(*pThinning, "VoxelGridResolution", gridResolution);
        bool useSegmentedSkeleton = false;
        getChildAttribute(*pThinning, "UseSegmentedSkeleton", useSegmentedSkeleton);

        m_VoxelSceneAnalyzer.resetSceneSkeleton(gridResolution, useSegmentedSkeleton, output);
    }
}

void SceneViewer::exposeIO() {
    if(auto window = m_GUI.addWindow("SceneViewer")) {
        m_GUI.addButton("Screenshot", [this]() {
            m_bDoScreenshot = true;
        });

        m_GUI.addSeparator();

        // Scene rendering
        m_FlatShadingPass.exposeIO(m_GUI);

        m_GUI.addSeparator();

        if(m_PickedIntersection) {
            m_GUI.addValue(BNZ_GUI_VAR(m_PickedIntersection.P));
            m_GUI.addValue(BNZ_GUI_VAR(m_PickedIntersection.Ns));
            m_GUI.addValue(BNZ_GUI_VAR(m_PickedIntersection.Ng));
            m_GUI.addValue(BNZ_GUI_VAR(m_PickedIntersection.meshID));
            m_GUI.addValue(BNZ_GUI_VAR(m_PickedIntersection.triangleID));
            m_GUI.addValue("isEmissive",
                           isEmissive(getScene()->getMaterial(getScene()->getGeometry().getMesh(m_PickedIntersection.meshID).m_MaterialID)));
        }
    }

    if(auto window = m_GUI.addWindow("GBuffer", false, m_GBufferDisplayFramebuffer.getSize())) {
        m_GBufferDisplayFramebuffer.bindForDrawing();

        glViewport(0, 0,
                   m_GBufferTexScreenSize.x, m_GBufferTexScreenSize.y);
        m_GLImageRenderer.drawNormalTexture(m_GBuffer);

        glViewport(0 + m_GBufferTexScreenSize.x, 0,
                   m_GBufferTexScreenSize.x, m_GBufferTexScreenSize.y);
        m_GLImageRenderer.drawDepthTexture(m_GBuffer);

        glViewport(0 + 2 * m_GBufferTexScreenSize.x, 0,
                   m_GBufferTexScreenSize.x, m_GBufferTexScreenSize.y);;
        m_GLImageRenderer.drawDiffuseTexture(m_GBuffer);

        glViewport(0 + 3 * m_GBufferTexScreenSize.x, 0,
                   m_GBufferTexScreenSize.x, m_GBufferTexScreenSize.y);
        m_GLImageRenderer.drawGlossyTexture(m_GBuffer);

        glViewport(0 + 4 * m_GBufferTexScreenSize.x, 0,
                   m_GBufferTexScreenSize.x, m_GBufferTexScreenSize.y);
        m_GLImageRenderer.drawShininessTexture(m_GBuffer);

        m_GUI.addImage(m_GBufferDisplayFramebuffer.getColorBuffer(0));
    }

    m_SkeletonVisibilityCorrelationAnalizer.exposeGUI(m_GUI, m_SkeletonViewerModule.getSelectedNodeIndex());
    m_VoxelSceneAnalyzer.exposeGUI(m_GUI);
    m_CenteredPointsAnalyser.exposeGUI(m_GUI);
}

}
