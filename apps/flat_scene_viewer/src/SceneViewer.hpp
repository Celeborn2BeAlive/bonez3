#pragma once

#include <bonez/viewer/Viewer.hpp>

#include <bonez/opengl/GLGBuffer.hpp>
#include <bonez/opengl/GLGBufferRenderPass.hpp>
#include <bonez/opengl/GLFlatShadingPass.hpp>
#include <bonez/opengl/GLScene.hpp>
#include <bonez/opengl/debug/GLDebugRenderer.hpp>
#include <bonez/opengl/GLScreenFramebuffer.hpp>

#include <bonez/maths/maths.hpp>

#include <bonez/rendering/RenderModule.hpp>

#include "SkeletonVisibilityCorrelationAnalyzer.hpp"
#include "VoxelSceneAnalyzer.hpp"
#include "CenteredPointsAnalyzer.hpp"
#include "SkeletonViewerModule.hpp"
#include "KdTreeTestViewerModule.hpp"

namespace BnZ {

class SceneViewer: public Viewer {
public:
    SceneViewer(const FilePath& applicationPath, const FilePath& settingsFilePath);

    GLDebugRenderer& getGLDebugRenderer() {
        assert(m_pGLDebugRenderer);
        return *m_pGLDebugRenderer;
    }

    const Vec4u& getSelectedObjectID() const {
        return m_SelectedObjectID;
    }

    const Intersection& getPickedIntersection() const {
        return m_PickedIntersection;
    }

    const Vec3f& getPickedIntersectionIncidentDirection() const {
        return m_PickedIntersectionIncidentDirection;
    }

    const Vec2u& getSelectedPixel() const {
        return m_SelectedPixel;
    }

    GLScreenFramebuffer& getScreenFramebuffer() {
        return m_GLFramebuffer;
    }

    const GLScene& getGLScene() const {
        assert(m_pGLScene);
        return *m_pGLScene;
    }

    const GLScreenTriangle& getScreenTriangle() const {
        return m_ScreenTriangle;
    }

    bool isSkeletonDisplayed() const {
        return m_SkeletonViewerModule.isSkeletonDisplayed();
    }

    void storeScreenshotImage(const Image& image);

    float getArrowLength() const;

    float getArrowBase() const;

    void setUp() override;

    void tearDown() override;

private:
    void initViewports();

    void initGLData();

    void drawFinalRender();

    void setSelectedPixel(const Vec2u& pixel);

    void doPicking();

    void doScreenshot();

    void drawFrame() override;

    void handleGlobalPreprocessParameters(const tinyxml2::XMLElement& input, tinyxml2::XMLElement& output) override;

    void exposeIO();

    GLGBuffer m_GBuffer;
    Shared<const GLScene> m_pGLScene;
    GLGBufferRenderPass m_GBufferRenderPass;
    GLFlatShadingPass m_FlatShadingPass;

    Shared<GLDebugRenderer> m_pGLDebugRenderer;

    GLScreenTriangle m_ScreenTriangle;

    GLScreenFramebuffer m_GLFramebuffer;

    // On window viewports
    Vec4f m_FinalRenderViewport;

    Vec4u m_SelectedObjectID = GLScreenFramebuffer::NULL_OBJECT_ID;
    Intersection m_PickedIntersection;
    Vec3f m_PickedIntersectionIncidentDirection;
    Vec2u m_SelectedPixel;

    std::string m_ScreenshotPrefix = toString(getMicroseconds());
    bool m_bDoScreenshot = false;

    SkeletonVisibilityCorrelationAnalyzer m_SkeletonVisibilityCorrelationAnalizer;
    VoxelSceneAnalyzer m_VoxelSceneAnalyzer;
    CenteredPointsAnalyzer m_CenteredPointsAnalyser;
    SkeletonViewerModule m_SkeletonViewerModule;
    KdTreeTestViewerModule m_KdTreeTestViewerModule;

    GLDebugStreamData m_DisplayStream;

    GLFramebuffer2D<1, false> m_GBufferDisplayFramebuffer;
    Vec2f m_GBufferTexScreenSize;
};

}
