#pragma once

#include <bonez/viewer/GUI.hpp>

#include <bonez/opengl/debug/GLDebugStream.hpp>

#include <bonez/voxskel/GLVoxelizer.hpp>
#include <bonez/voxskel/GLCubicalComplexRenderer.hpp>
#include <bonez/voxskel/GLVoxelGridRenderer.hpp>
#include <bonez/voxskel/CubicalComplex3D.hpp>
#include <bonez/voxskel/discrete_functions.hpp>
#include <bonez/voxskel/GLVoxelizerTripiana2009.hpp>
#include <bonez/voxskel/ThinningProcessDGCI2013.hpp>
#include <bonez/voxskel/ThinningProcessDGCI2013_2.hpp>
#include <bonez/voxskel/GLCubicalComplexFaceListRenderer.hpp>

#include <bonez/image/Image.hpp>

#include <bonez/sys/time.hpp>

#include <stack>
#include <map>

#include <bonez/rendering/renderers/TileProcessingRenderer.hpp>

#include <bonez/utils/DijkstraAlgorithm.hpp>

#include <bonez/voxskel/MinTreeNajmanCouprie2006.hpp>

#include "opening_segmentation/VoxelOpeningSegmentationGraph.hpp"

namespace BnZ {

class SceneViewer;
class Scene;
class GLScene;
class GLDebugRenderer;

Grid3D<uint32_t> computeOpeningMap26Experiment(Grid3D<uint32_t> distanceMap, Grid3D<Vec3u>& centerMap,
                                               std::vector<int32_t> directions, bool iterative = false);

class VoxelSceneAnalyzer {
public:
    VoxelSceneAnalyzer(SceneViewer& viewer);

    const Grid3D<uint32_t>& getOpeningSegmentation() const {
        return m_VoxelOpeningSegmentationGraph.getOpeningSegmentation();
    }

    void computeCenterOfMaxballsVoxelGrid();

    void computeCutVoxelGrid();

    void setOpeningSegmentationGridColor();

    void setMinTreeComponentColor();

    void computeSceneDiscreteData(tinyxml2::XMLElement* output = nullptr);

    void setMappedNodeGridColor();

    void setSceneSkeletonFromDiscreteData();

    void computeOpeningSegmentBorder();

    void computeMaxballExpension();

    void computeMaxballExpensionStepByStep();

    void exposeGUI(GUI& gui);

    void setUp();

    bool handlePicking(const Vec4u& selectedObjectID);

    bool drawSideView(const Vec4f& viewport);

    void onPickIntersection();

    void draw();

    void resetSceneSkeleton(uint32_t gridResolution,
                            bool segmented,
                            tinyxml2::XMLElement& output);

private:
    void drawRecursiveGUIMinTree(size_t node);

    SceneViewer& m_Viewer;
    Scene* m_pScene = nullptr;
    const GLScene* m_pGLScene = nullptr;
    GLDebugRenderer* m_pGLDebugRenderer = nullptr;
    GLDebugStreamData m_DisplayStream;

    GLVoxelizerTripiana2009 m_GLVoxelizerTripiana2009;
    uint32_t m_nVoxelGridRes = 128;
    Mat4f m_GridToWorldMatrix;
    GLVoxelFramebuffer m_GLVoxelFramebuffer;

    bool m_bIsVoxelized = false;
    bool m_bDisplaySkeleton = true;
    bool m_bDisplayCC = true;
    VoxelGrid m_SceneVoxelGrid;
    VoxelGrid m_EmptySpaceVoxelGrid;

    GLVoxelGrid m_GLSceneVoxelGrid;
    GLVoxelGridRenderer m_GLVoxelGridRenderer;
    GLTexture3D m_VoxelGridColorTexture;
    bool m_bDisplaySceneVoxelGrid = false;

    ThinningProcessDGCI2013_2 m_ThinningProcess;

    CubicalComplex3D m_SceneCubicalComplex;
    GLCubicalComplex m_GLSceneCubicalComplex;
    CubicalComplex3D m_EmptySpaceCubicalComplex;
    CubicalComplex3D m_SkeletonCubicalComplex;
    GLCubicalComplex m_GLSkeletonCubicalComplex;
    GLCubicalComplexRenderer m_VoxSkelRenderer;

    CubicalComplex3D m_ConstrainedCubicalComplex;

    Grid3D<uint32_t> m_EmptySpaceDistanceMap;
    Grid3D<uint32_t> m_EmptySpaceOpeningMap;
    Grid3D<Vec3u> m_EmptySpaceOpeningCenterMap;

    MinTreeNajmanCouprie2006<uint32_t> m_OpeningMinTree;

    Vec3i m_OpeningDirections = Vec3i(0, 1, 2);

    uint32_t m_CameraDistance = 0u;
    Vec3i m_CameraVoxel = Vec3i(0);
    bool m_bDrawMaxball = false;
    float m_fGridToWorldScale = 0.f;
    uint32_t m_nThinningIterCount = 1u;
    bool m_bComputeFilteredSkeleton = false;

    bool m_bDisplayOpeningSegmentation = true;
    bool m_bDisplayCut = false;
    uint32_t m_nCutAxis = 0u;
    uint32_t m_nCutPosition = 0u;
    Image m_CutImage;
    GLTexture2D m_CutTexture;

    bool m_bDisplaySelectedVoxel = true;
    bool m_bIsVoxelSelected = false;
    Vec3i m_SelectedVoxel = Vec3i(0);
    uint32_t m_nSelectedVoxelDistance26 = 0u;
    uint32_t m_nSelectedVoxelOpening26 = 0u;
    bool m_bDrawSelectedVoxelDiscreteMaxball = false;
    bool m_bDrawSelectedVoxelOpeningSegmentBorder = false;
    std::vector<Vec4i> m_SelectedVoxelOpeningSegmentBorder;
    std::vector<Vec3f> m_SelectedVoxelOpeningSegmentBorderColors;

    GLCubicalComplexFaceListRenderer m_GLCubicalComplexFaceListRenderer;
    std::vector<GLCubicalComplexFaceList::Vertex> m_SelectedVoxelOpeningSegmentBorderFaceVertices;
    GLCubicalComplexFaceList m_GLSelectedVoxelOpeningSegmentBorderFaceList;

    VoxelOpeningSegmentationGraph m_VoxelOpeningSegmentationGraph;
    GLCubicalComplex m_GLBordersCubicalComplex;
    bool m_bDisplayBordersCubicalComplex = false;

    VoxelGrid m_CenterOfMaxballsVoxelGrid;
    GLVoxelGrid m_GLCenterOfMaxballsVoxelGrid;
    bool m_bDisplayCenterOfMaxballs = false;

    GLVoxelGrid m_GLCutVoxelGrid;
    bool m_bDisplayCutVoxelGrid = false;

    bool m_bDisplayMaxballExpension = false;
    std::vector<Vec3i> m_MaxballExpension;
    std::vector<Vec3i> m_MaxballExpensionBorder;
    Grid3D<bool> m_MaxballExpensionVisitGrid;
    std::queue<Vec3i> m_MaxballExpensionQueue;

    bool m_bRayTracePortals = false;
    uint32_t m_nPortalRayCount = 128;
    enum RaySamplingMethod {
        RSM_RANDOM_PORTAL,
        RSM_AREA_BASED_PORTAL,
        RSM_TARGET_PORTAL,
        RSM_LOCAL,
        RSM_TOWARD_POINT_LIGHT,
        RSM_COUNT
    };
    RaySamplingMethod m_nRaySamplingMethod = RSM_RANDOM_PORTAL;
    uint32_t m_nSelectedPortalIndex = 0u;


    Unique<Renderer> m_pPickedPointRenderer;
    Unique<Sensor> m_PickedPointCamera;
    Framebuffer m_PickedPointFramebuffer;
    GLFramebuffer2D<1, false> m_PickedPointGLFramebuffer;
    uint32_t m_nPickedPointChannel = 0u;
    float m_fPickedPointGamma = 1.f;
    uint32_t m_nPickedPointCurrentRendererIdx = 0u;
};

}
