#include "CenteredPointsAnalyzer.hpp"
#include "SceneViewer.hpp"

#include <bonez/sampling/Random.hpp>
#include <bonez/scene/SurfacePoint.hpp>
#include <bonez/scene/Scene.hpp>

namespace BnZ {

CenteredPointsAnalyzer::CenteredPointsAnalyzer(SceneViewer& viewer):
    m_Viewer(viewer),
    m_pScene(m_Viewer.getScene()) {
}

void CenteredPointsAnalyzer::exposeGUI(GUI& gui) {
    if(auto window = gui.addWindow("CenteredPointsAnalyzer")) {
        gui.addVarRW(BNZ_GUI_VAR(m_nSurfacePointCount));
        gui.addButton("Compute centered points", [&]() {
            RandomGenerator rng;

            m_CenteredPoints.clear();

            std::vector<SurfacePointSample> points(m_nSurfacePointCount);
            std::vector<float> buffer1(m_nSurfacePointCount);
            std::vector<float> buffer2(m_nSurfacePointCount);
            std::vector<float> buffer3(m_nSurfacePointCount);
            std::vector<Vec2f> buffer4(m_nSurfacePointCount);

            for(auto i = 0u; i < m_nSurfacePointCount; ++i) {
                buffer1[i] = rng.getFloat();
                buffer2[i] = rng.getFloat();
                buffer3[i] = rng.getFloat();
                buffer4[i] = rng.getFloat2();
            }

            m_pScene->uniformSampleSurfacePoints(m_nSurfacePointCount,
                                                 buffer1.data(),
                                                 buffer2.data(),
                                                 buffer3.data(),
                                                 buffer4.data(),
                                                 points.data());

            for(auto i = 0u; i < m_nSurfacePointCount; ++i) {
                Ray ray(points[i].value, points[i].value.Ns);
                auto I = m_pScene->intersect(ray);
                if(I) {
                    m_CenteredPoints.push_back({ I.P, points[i].value.P, 0.5f * (I.P + points[i].value.P) });
                }
            }
            m_bDisplayCenteredPoints = true;
        });
        gui.addVarRW(BNZ_GUI_VAR(m_bDisplayCenteredPoints));
        gui.addVarRW(BNZ_GUI_VAR(m_bDisplayCenteredLines));
    }
}

void CenteredPointsAnalyzer::draw() {
    if(m_bDisplayCenteredPoints) {
        m_DisplayStream.clearObjects();

        //float scale = 0.0005f;
        auto i = 0u;
        for(const auto& point: m_CenteredPoints) {
            auto color = getColor(i);
            m_DisplayStream.addPoints(1, &point.center, &color);
            if(m_bDisplayCenteredLines) {
                m_DisplayStream.addLine(point.org, point.dst, getColor(i), getColor(i), 2);
            }
            ++i;
        }
        m_Viewer.getGLDebugRenderer().addStream(&m_DisplayStream);
    }
}

}
