#include "KdTreeTestViewerModule.hpp"

#include "SceneViewer.hpp"

namespace BnZ {

void KdTreeTestViewerModule::setUp(SceneViewer& viewer) {
    m_pViewer = &viewer;
}

void KdTreeTestViewerModule::drawGUI() {
    auto& gui = m_pViewer->getGUI();

    if(auto window = gui.addWindow("KdTreeTestViewerModule")) {
        gui.addVarRW(BNZ_GUI_VAR(m_nPointCount));
        gui.addVarRW(BNZ_GUI_VAR(m_bJitteredSampling));
        gui.addButton("Sample points", [&]() {
            m_PointBuffer.clear();

            RandomGenerator rng;

            if(!m_bJitteredSampling) {
                auto getSample = [&](uint32_t idx) {
                    return Scene::SurfacePointSampleParams{ rng.getFloat(), rng.getFloat(), rng.getFloat(), rng.getFloat2() };
                };

                m_pViewer->getScene()->sampleSurfacePointsWrtArea(m_nPointCount, std::back_inserter(m_PointBuffer), getSample);
            } else {
                std::vector<float> s1, s2, s3;
                std::vector<Vec2f> s4;

                auto pointCountSqrt = size_t(sqrt(m_nPointCount));
                m_nPointCount = sqr(pointCountSqrt);

                std::random_device rd;
                std::mt19937 g(rd());

                generateDistribution(m_nPointCount, { m_nPointCount }, rng, std::back_inserter(s1));
                std::shuffle(begin(s1), end(s1), g);
                generateDistribution(m_nPointCount, { m_nPointCount }, rng, std::back_inserter(s2));
                std::shuffle(begin(s2), end(s2), g);
                generateDistribution(m_nPointCount, { m_nPointCount }, rng, std::back_inserter(s3));
                std::shuffle(begin(s3), end(s3), g);
                generateDistribution(m_nPointCount, { pointCountSqrt, pointCountSqrt }, rng, std::back_inserter(s4));
                std::shuffle(begin(s4), end(s4), g);

                auto getSample = [&](uint32_t i) {
                    return Scene::SurfacePointSampleParams{ s1[i], s2[i], s3[i], s4[i] };
                };

                m_pViewer->getScene()->sampleSurfacePointsWrtArea(m_nPointCount, std::back_inserter(m_PointBuffer), getSample);
            }

            m_KdTree.build(m_PointBuffer.size(), [&](uint32_t i) {
               return m_PointBuffer[i].value.P;
            }, [&](uint32_t i) { return m_PointBuffer[i].pdf > 0.f; });
        });
        gui.addVarRW(BNZ_GUI_VAR(m_fLookupRadius));
        gui.addVarRW(BNZ_GUI_VAR(m_bGetNearest));
        gui.addVarRW(BNZ_GUI_VAR(m_nK));
        gui.addVarRW(BNZ_GUI_VAR(m_bDisplayLookupSphere));
    }
}

void KdTreeTestViewerModule::draw() {
    if(!m_PointBuffer.empty()) {
        m_DisplayStream.clearObjects();
        m_pViewer->getGLDebugRenderer().addStream(&m_DisplayStream);

        std::vector<Vec3f> pointColors(m_PointBuffer.size(), Vec3f(1));

        if(m_pViewer->getPickedIntersection()) {
            if(!m_bGetNearest) {
                m_KdTree.search(m_pViewer->getPickedIntersection().P, sqr(m_fLookupRadius),
                    [&](uint32_t i, Vec3f position, float distSquared, float& maxDistSquared) {
                        pointColors[i] = Vec3f(1, 0, 0);
                    });

                if(m_bDisplayLookupSphere) {
                    m_DisplayStream.addSphere(m_pViewer->getPickedIntersection().P, m_fLookupRadius,
                                              Vec3f(1.f));
                }
            } else {
                if(m_nK == 1u) {
                    float distSquared;
                    auto i = m_KdTree.searchNearestNeighbour(m_pViewer->getPickedIntersection().P, distSquared);
                    pointColors[i] = Vec3f(1, 0, 0);

                    if(m_bDisplayLookupSphere) {
                        m_DisplayStream.addSphere(m_pViewer->getPickedIntersection().P, sqrt(distSquared),
                                                  Vec3f(1.f));
                    }
                } else {
                    BBox3f bbox;
                    float maxDistSquared = 0.f;
                    m_KdTree.searchKNearestNeighbours(m_pViewer->getPickedIntersection().P, m_nK,
                        [&](uint32_t i, Vec3f position, float distSquared) {
                            pointColors[i] = Vec3f(1, 0, 0);
                            maxDistSquared = max(maxDistSquared, distSquared);
                            bbox.grow(position);
                        });
                    if(m_bDisplayLookupSphere) {
                        m_DisplayStream.addSphere(m_pViewer->getPickedIntersection().P, sqrt(maxDistSquared),
                                                  Vec3f(1.f));
                    }
                    m_DisplayStream.addBBox(bbox.upper, bbox.lower, Vec3f(1.f), 3.f);
                }
            }
        }

        auto i = 0u;
        for(const auto& pointSample: m_PointBuffer) {
            m_DisplayStream.addArrow(pointSample.value.P,
                                     pointSample.value.Ns,
                                     m_pViewer->getArrowLength(),
                                     m_pViewer->getArrowBase(),
                                     pointColors[i]);
            ++i;
        }
    }
}

}
