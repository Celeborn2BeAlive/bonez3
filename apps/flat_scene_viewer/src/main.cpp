#include <iostream>

#include "SceneViewer.hpp"
#include "bonez/sys/DebugLog.hpp"

INITIALIZE_EASYLOGGINGPP

namespace BnZ {

int offlineRendering(const tinyxml2::XMLDocument& inputDocument, const FilePath& inputDocumentPath, const FilePath& applicationPath);

int main(int argc, char** argv) {
    if(argc < 2) {
        std::cerr << "Usage: " << argv[0] << " < XML configuration filepath >" << std::endl;
        return -1;
    }

    initEasyLoggingpp(argc, argv);
	
    tinyxml2::XMLDocument inputDocument;
    if(tinyxml2::XML_NO_ERROR != inputDocument.LoadFile(argv[1])) {
        std::cerr << "Unable to open input file " << argv[1] << std::endl;
        return -1;
    }

    auto pRootElement = inputDocument.RootElement();

    if(!pRootElement) {
        std::cerr << "No root element in input file " << argv[1] << std::endl;
        return -1;
    }
    auto rootName = std::string(pRootElement->Name());

    if(rootName == "Viewer") {
        // Run interactive viewer
        SceneViewer viewer(argv[0], argv[1]);
        viewer.run();
    } else if(rootName == "RenderBatch") {
        return offlineRendering(inputDocument, argv[1], argv[0]);
    }

	return 0;
}

int offlineRendering(const tinyxml2::XMLDocument& inputDocument, const FilePath& inputDocumentPath, const FilePath& applicationPath) {
    auto basePath = inputDocumentPath.directory();

    auto pRootElement = inputDocument.RootElement();

    bool loop = false;
    bool keepRunning = true;
    getAttribute(*pRootElement, "loop", loop);

    do {
        // Get relative directory path containing the scenes
        std::string sceneDirectoryPathAttr;
        if(!getAttribute(*pRootElement, "sceneDirectoryPath", sceneDirectoryPathAttr)) {
            std::cerr << "Missing 'sceneDirectoryPath' attribute on RenderBatch tag" << std::endl;
            return -1;
        }
        // Build absolute path
        FilePath sceneDirectoryPath = basePath + sceneDirectoryPathAttr;
        if(!exists(sceneDirectoryPath) || !isDirectory(sceneDirectoryPath)) {
            std::cerr << "Scene directory " << sceneDirectoryPath << " does not exist or is not a directory" << std::endl;
            return -1;
        }

        // Extract shared properties: renderers file, session name and reference computation algorithm
        std::string renderBatchRenderersFileName;
        std::string renderBatchSessionName;

        getAttribute(*pRootElement, "renderers", renderBatchRenderersFileName);
        getAttribute(*pRootElement, "session", renderBatchSessionName);

        auto pRenderBatchReferenceElement = pRootElement->FirstChildElement("Reference");

        // Run render batch
        for(auto pRenderConfigs = pRootElement->FirstChildElement("RenderConfigs");
            keepRunning && pRenderConfigs != nullptr;
            pRenderConfigs = pRenderConfigs->NextSiblingElement("RenderConfigs")) {
            std::string sceneName;
            if(!getAttribute(*pRenderConfigs, "scene", sceneName)) {
                std::cerr << "Missing 'scene' attribute on RenderConfigs tag" << std::endl;
                continue;
            }

            auto viewerSettingsFilePath = sceneDirectoryPath + FilePath(sceneName) + "viewer.bnz.xml";
            if(!exists(viewerSettingsFilePath)) {
                std::cerr << "Viewer settings file not found (" << viewerSettingsFilePath << ")" << std::endl;
                continue;
            }

            SceneViewer viewer(applicationPath, viewerSettingsFilePath);
            viewer.setUp();

            // Extract shared properties: renderers file, session name and reference computation algorithm
            std::string renderConfigsRenderersFileName = renderBatchRenderersFileName;
            std::string renderConfigsSessionName = renderBatchSessionName;

            getAttribute(*pRenderConfigs, "renderers", renderConfigsRenderersFileName);
            getAttribute(*pRenderConfigs, "session", renderConfigsSessionName);

            auto pRenderConfigsReferenceElement = pRenderConfigs->FirstChildElement("Reference");

            for(auto pRender = pRenderConfigs->FirstChildElement("Render"); keepRunning && pRender != nullptr; pRender = pRender->NextSiblingElement("Render")) {

                // Extract shared properties: renderers file, session name and reference computation algorithm
                std::string renderersFileName = renderConfigsRenderersFileName;
                std::string configSessionName = renderConfigsSessionName;

                getAttribute(*pRender, "renderers", renderersFileName);
                getAttribute(*pRender, "session", configSessionName);

                auto pRenderTagReferenceElement = pRender->FirstChildElement("Reference");

                std::string configName;
                if(!getAttribute(*pRender, "config", configName)) {
                    std::cerr << "Missing 'config'' attribute on Render tag" << std::endl;
                    continue;
                }

                auto pReferenceElement = pRenderTagReferenceElement ? pRenderTagReferenceElement :
                                                                      pRenderConfigsReferenceElement ? pRenderConfigsReferenceElement :
                                                                                                     pRenderBatchReferenceElement;
                auto renderersFilePath = basePath + renderersFileName;

                tinyxml2::XMLDocument renderersDocument;
                auto pRenderersDocument = &renderersDocument;
                if(!renderersFileName.empty()) {
                    if(tinyxml2::XML_NO_ERROR != renderersDocument.LoadFile(renderersFilePath.c_str())) {
                        std::cerr << "Unable to load file " << renderersFilePath << std::endl;
                        continue;
                    }
                } else {
                    pRenderersDocument = nullptr;
                }

                std::cout << "Start rendering config = " << configName << ", scene = " << sceneName << ", renderers = "
                    << renderersFileName << ", session = " << configSessionName << std::endl;

                keepRunning = viewer.renderConfig(basePath + sceneName, configName, configSessionName, pRenderersDocument, pReferenceElement);
                loop = loop && keepRunning;
            }

            viewer.tearDown();
        }
    } while(loop); // Loop infinitely if the user wants to.

    return 0;
}

}

int main(int argc, char** argv) {
#ifdef _DEBUG
        std::cerr << "DEBUG MODE ACTIVATED" << std::endl;
#endif

#ifdef DEBUG
        std::cerr << "DEBUG MODE ACTIVATED" << std::endl;
#endif
    return BnZ::main(argc, argv);
}
