#include "VoxelSceneAnalyzer.hpp"

#include "SceneViewer.hpp"

#include "opening_segmentation/OpeningVoxelRenderer.hpp"
#include "opening_segmentation/VoxelOpeningSegmentationGraphPathtraceRenderer.hpp"
#include "opening_segmentation/VoxelOpeningSegmentationGraphPathtraceRenderer2.hpp"
#include "opening_segmentation/OpeningBorderBasedPathtraceRenderer.hpp"

#include <bonez/sampling/distribution1d.h>
#include <bonez/scene/shading/BSDF.hpp>

#include <bonez/rendering/renderers/GeometryRenderer.hpp>

#include <bonez/scene/sensors/ParaboloidCamera.hpp>
#include <bonez/scene/sensors/HemisphereCamera.hpp>
#include <bonez/scene/sensors/SurfacePointCamera.hpp>

namespace BnZ {

VoxelSceneAnalyzer::VoxelSceneAnalyzer(SceneViewer& viewer):
    m_Viewer(viewer),
    m_pScene(viewer.getScene()),
    m_pGLDebugRenderer(&viewer.getGLDebugRenderer()),
    m_GLVoxelizerTripiana2009(viewer.getShaderManager()),
    m_GLVoxelGridRenderer(viewer.getShaderManager()),
    m_VoxSkelRenderer(viewer.getShaderManager()),
    m_GLCubicalComplexFaceListRenderer(viewer.getShaderManager()) {
}

void VoxelSceneAnalyzer::setUp() {
    m_pGLScene = &m_Viewer.getGLScene();

    m_Viewer.getRenderModule().m_RendererManager.addRenderer("OpeningVoxelRenderer",
                                                             [&]() {
        return makeUnique<OpeningVoxelRenderer>(m_VoxelOpeningSegmentationGraph);
    });
    m_Viewer.getRenderModule().m_RendererManager.addRenderer("VoxelOpeningSegmentationGraphPathtraceRenderer",
                                                             [&]() {
        return makeUnique<VoxelOpeningSegmentationGraphPathtraceRenderer>(m_VoxelOpeningSegmentationGraph);
    });
    m_Viewer.getRenderModule().m_RendererManager.addRenderer("OpeningBorderBasedPathtraceRenderer",
                                                             [&]() {
        return makeUnique<OpeningBorderBasedPathtraceRenderer>(m_VoxelOpeningSegmentationGraph);
    });
//    m_Viewer.getRenderModule().m_RendererManager.addRenderer("VoxelOpeningSegmentationGraphPathtraceRenderer2",
//                                                             [&]() {
//        return makeUnique<VoxelOpeningSegmentationGraphPathtraceRenderer2>(m_VoxelOpeningSegmentationGraph);
//    });

    auto pDiscreteSceneSettings = m_Viewer.getSettingsRoot()->FirstChildElement("DiscreteSceneSettings");
    if(pDiscreteSceneSettings) {
        getChildAttribute(*pDiscreteSceneSettings, "VoxelGridResolution", m_nVoxelGridRes);
        getChildAttribute(*pDiscreteSceneSettings, "FilterSkeleton", m_bComputeFilteredSkeleton);
    }

    if(m_nVoxelGridRes == 0u) {
        return;
    }

    computeSceneDiscreteData();
    setSceneSkeletonFromDiscreteData();

    //setMappedNodeGridColor();

    m_bDisplayCC = false;

    setOpeningSegmentationGridColor();

    //m_pPickedPointRenderer = makeUnique<VoxelOpeningSegmentationGraphPathtraceRenderer>(m_VoxelOpeningSegmentationGraph);
    m_pPickedPointRenderer = makeUnique<GeometryRenderer>();
    m_PickedPointFramebuffer = Framebuffer(512, 512);
    GLenum format = GL_RGB32F;
    m_PickedPointGLFramebuffer.init(m_PickedPointFramebuffer.getSize(), &format);
}

void VoxelSceneAnalyzer::resetSceneSkeleton(uint32_t gridResolution,
                                            bool segmented,
                                            tinyxml2::XMLElement& output) {
    m_nVoxelGridRes = gridResolution;
    computeSceneDiscreteData(&output);

    auto skeleton = getCurvilinearSkeleton(m_SkeletonCubicalComplex, m_EmptySpaceCubicalComplex,
                                           m_EmptySpaceDistanceMap, m_EmptySpaceOpeningMap, m_GridToWorldMatrix);
    if(!segmented) {
        m_pScene->setCurvSkeleton(skeleton);
        setChildAttribute(output, "UseSegmentedSkeleton", false);
    } else {
        Timer timer;
        auto segSkeleton = computeMaxballBasedSegmentedSkeleton(skeleton);
        setChildAttribute(output, "SkelSegTime", timer.getMicroEllapsedTime());
        setChildAttribute(output, "UseSegmentedSkeleton", true);

        m_pScene->setCurvSkeleton(segSkeleton);
    }
    setChildAttribute(output, "SkeletonNodeCount", m_pScene->getCurvSkeleton()->size());
}

void VoxelSceneAnalyzer::setOpeningSegmentationGridColor() {
    Grid3D<Vec3f> colorGrid(m_SceneVoxelGrid.resolution(), Vec3f(1));

    foreachVoxel(colorGrid.resolution(), [&](Vec3i voxel) {
       colorGrid(voxel) = getColor(
                   m_VoxelOpeningSegmentationGraph.getOpeningSegmentation()(voxel));
    });

    m_VoxelGridColorTexture.setImage(0, GL_RGB, m_SceneVoxelGrid.width(), m_SceneVoxelGrid.height(), m_SceneVoxelGrid.depth(),
                                     0, GL_RGB, GL_FLOAT, colorGrid.data());
    m_VoxelGridColorTexture.setMagFilter(GL_NEAREST);
    m_VoxelGridColorTexture.setMinFilter(GL_NEAREST);
}

void VoxelSceneAnalyzer::computeCenterOfMaxballsVoxelGrid() {
    m_CenterOfMaxballsVoxelGrid = VoxelGrid(m_SceneVoxelGrid.resolution(), false);

    auto isCentered = [&](const Vec3i& voxel) {
        return m_VoxelOpeningSegmentationGraph.getOpeningCenterMap()(voxel) == Vec3u(voxel);

//        auto value = m_VoxelOpeningSegmentationGraph.getOpeningMap()(voxel) - m_VoxelOpeningSegmentationGraph.getDistanceMap()(voxel);
//        bool result = true;
//        foreach6Neighbour(m_VoxelOpeningSegmentationGraph.getOpeningMap().resolution(), voxel, [&](const Vec3i& neighbour) {
//            if(result && m_VoxelOpeningSegmentationGraph.getOpeningMap()(neighbour) - m_VoxelOpeningSegmentationGraph.getDistanceMap()(neighbour) < value) {
//                result = false;
//            }
//        });
//        return result;
    };

    foreachVoxel(m_SceneVoxelGrid.resolution(), [&](const Vec3i& coords) {
        if(!m_CenterOfMaxballsVoxelGrid(coords) &&
                isCentered(coords) &&
                m_EmptySpaceDistanceMap(coords)) {
            m_CenterOfMaxballsVoxelGrid(coords) = true;
        }
    });

    m_GLCenterOfMaxballsVoxelGrid.init(m_CenterOfMaxballsVoxelGrid);
}

void VoxelSceneAnalyzer::computeCutVoxelGrid() {
    auto cutVoxelGrid = VoxelGrid(m_SceneVoxelGrid.resolution(), false);

    foreachVoxel(m_SceneVoxelGrid.resolution(), [&](const Vec3i& coords) {
        if(m_EmptySpaceDistanceMap(coords) && (
                    (m_nCutAxis == 0 && coords.x == m_nCutPosition) ||
                    (m_nCutAxis == 1 && coords.y == m_nCutPosition) ||
                    (m_nCutAxis == 2 && coords.z == m_nCutPosition)
                )) {
            cutVoxelGrid(coords) = true;
        }
    });

    m_GLCutVoxelGrid.init(cutVoxelGrid);
}

void VoxelSceneAnalyzer::computeSceneDiscreteData(tinyxml2::XMLElement* output) {
    Timer timer;

    TaskTimer thinningTimer({ "Voxelize",
                              "GetVoxelGridFromGPU",
                              "InverseToGetEmptySpace",
                              "ConvertToCubicalComplex",
                              "ComputeDistanceMap",
                              "ComputeOpeningMap",
                              "InitThinning",
                              "PerformThinning" }, 1u);

    {
        auto timer = thinningTimer.start(0);
        std::clog << "Voxelizing the scene; resolution = " << m_nVoxelGridRes << std::endl;

        m_GLVoxelizerTripiana2009.initGLState(m_nVoxelGridRes, m_pScene->getBBox(),
            m_GLVoxelFramebuffer, m_GridToWorldMatrix);
        m_pGLScene->render();
        m_GLVoxelizerTripiana2009.restoreGLState();
    }

    std::clog << "0. Time = " << us2sec(timer.getMicroEllapsedTime()) << " sec." << std::endl;

    {
        auto timer = thinningTimer.start(1);
        std::clog << "Get scene voxel grid from GPU" << std::endl;

        m_SceneVoxelGrid = getVoxelGrid(m_GLVoxelFramebuffer);
        m_SceneVoxelGrid = deleteEmptyBorder(m_SceneVoxelGrid, m_GridToWorldMatrix, m_GridToWorldMatrix);
    }

    std::clog << "1. Time = " << us2sec(timer.getMicroEllapsedTime()) << " sec." << std::endl;

    {
        auto timer = thinningTimer.start(2);
        std::clog << "Inverse scene voxel grid to get empty space voxel grid" << std::endl;
        m_EmptySpaceVoxelGrid = inverse(m_SceneVoxelGrid);
    }

    std::clog << "2. Time = " << us2sec(timer.getMicroEllapsedTime()) << " sec." << std::endl;

    {
        auto timer = thinningTimer.start(3);
        std::clog << "Convert empty space voxel grid to CC3D" << std::endl;
        m_EmptySpaceCubicalComplex = getCubicalComplex(m_EmptySpaceVoxelGrid);
    }

    std::clog << "3. Time = " << us2sec(timer.getMicroEllapsedTime()) << " sec." << std::endl;

    {
        auto timer = thinningTimer.start(4);
        std::clog << "Compute Distance Map" << std::endl;
        m_EmptySpaceDistanceMap = computeDistanceMap26(m_EmptySpaceCubicalComplex, CubicalComplex3D::isInObject, true);
    }

    std::clog << "4. Time = " << us2sec(timer.getMicroEllapsedTime()) << " sec." << std::endl;

    {
        auto timer = thinningTimer.start(5);
        std::clog << "Compute Opening Map" << std::endl;
        m_EmptySpaceOpeningMap = parallelComputeOpeningMap26(m_EmptySpaceDistanceMap,
                                                             m_EmptySpaceOpeningCenterMap);
//        m_EmptySpaceOpeningMap = computeOpeningMap26Experiment(m_EmptySpaceDistanceMap,
//                                                               m_EmptySpaceOpeningCenterMap,
//            { m_OpeningDirections.x, m_OpeningDirections.y, m_OpeningDirections.z });
    }

    std::clog << "5. Time = " << us2sec(timer.getMicroEllapsedTime()) << " sec." << std::endl;

    {
        m_SkeletonCubicalComplex = m_EmptySpaceCubicalComplex;

        {
            auto timer = thinningTimer.start(6);
            std::clog << "Initialize the thinning process" << std::endl;
            m_ThinningProcess.init(m_SkeletonCubicalComplex, &m_EmptySpaceDistanceMap, &m_EmptySpaceOpeningMap);
        }

        {
            auto timer = thinningTimer.start(7);
            std::clog << "Run the thinning process" << std::endl;
            m_ThinningProcess.directionalCollapse();
        }

		auto hasSurfacicParts = [&]() {
			std::clog << "Pierce surface parts" << std::endl;
			return m_SkeletonCubicalComplex.pierceSurfacicParts();
		}();

		if (hasSurfacicParts) {
			std::clog << "Run the thinning process to remove surfacic parts" << std::endl;
			m_ThinningProcess.directionalCollapse();
		}

        std::clog << us2sec(thinningTimer.getEllapsedTime<Microseconds>(7)) << std::endl;
    }

    std::clog << "6. Time = " << us2sec(timer.getMicroEllapsedTime()) << " sec." << std::endl;

    std::clog << "Thinning over. Time = " << us2sec(timer.getMicroEllapsedTime()) << " sec." << std::endl;

    if(output) {
        setChildAttribute(*output, "VoxelGridResolution", m_SceneVoxelGrid.resolution());
        setChildAttribute(*output, "ThinningTime", thinningTimer);
    }

    {
        std::clog << "Convert scene space voxel grid to CC3D" << std::endl;
        m_SceneCubicalComplex = getCubicalComplex(m_SceneVoxelGrid);
    }

    {
        Timer timer(true);
        std::clog << "Initialize m_GLSceneCubicalComplex" << std::endl;
        m_GLSceneCubicalComplex.init(m_SceneCubicalComplex);
    }

    {
        Timer timer(true);
        std::clog << "Initialize m_GLSkeletonCubicalComplex" << std::endl;
        m_GLSkeletonCubicalComplex.init(m_SkeletonCubicalComplex);
    }

    {
        Timer timer(true);
        std::clog << "Initialize m_GLSceneVoxelGrid" << std::endl;
        m_GLSceneVoxelGrid.init(m_SceneVoxelGrid);
    }

    {
        Timer timer(true);
        std::clog << "Compute opening segmentation" << std::endl;
        m_VoxelOpeningSegmentationGraph = VoxelOpeningSegmentationGraph(m_GridToWorldMatrix,
                                                                        m_EmptySpaceDistanceMap,
                                                                        m_EmptySpaceOpeningMap,
                                                                        m_EmptySpaceOpeningCenterMap);
        m_GLBordersCubicalComplex.init(m_VoxelOpeningSegmentationGraph.getBordersCubicalComplex());
        std::clog << "Number of opening clusters = " << m_VoxelOpeningSegmentationGraph.size() << std::endl;
    }

    m_OpeningMinTree = decltype(m_OpeningMinTree)(m_EmptySpaceOpeningMap);

    computeCenterOfMaxballsVoxelGrid();

    computeCutVoxelGrid();

    // Experimentation: thinning of the centered voxels
    // Care for this: opening and distance map are not correct for computing skeleton after

//    m_SkeletonCubicalComplex = getCubicalComplex(m_CenterOfMaxballsVoxelGrid);
//    {
//        Timer timer(true);
//        std::clog << "Initialize the thinning process" << std::endl;
//        m_ThinningProcess.init(m_SkeletonCubicalComplex, &m_EmptySpaceDistanceMap, &m_EmptySpaceOpeningMap);
//    }

//    {
//        Timer timer(true);
//        std::clog << "Run the thinning process" << std::endl;
//        m_ThinningProcess.directionalCollapse();
//    }

////    m_SkeletonCubicalComplex = filterIsolatedPoints(m_SkeletonCubicalComplex);

//    {
//        Timer timer(true);
//        std::clog << "Initialize m_GLSkeletonCubicalComplex" << std::endl;
//        m_GLSkeletonCubicalComplex.init(m_SkeletonCubicalComplex);
//    }


    // refine thinning
//    {
//        m_ConstrainedCubicalComplex = getCubicalComplex(m_CenterOfMaxballsVoxelGrid);
//        {
//            Timer timer(true);
//            std::clog << "Initialize the thinning process" << std::endl;
//            m_ThinningProcess.init(m_ConstrainedCubicalComplex, &m_EmptySpaceDistanceMap, &m_EmptySpaceOpeningMap);
//        }

//        {
//            Timer timer(true);
//            std::clog << "Run the thinning process" << std::endl;
//            m_ThinningProcess.directionalCollapse();
//        }

//        m_ConstrainedCubicalComplex = filterIsolatedPoints(m_ConstrainedCubicalComplex);

//        m_SkeletonCubicalComplex = m_EmptySpaceCubicalComplex;

//        {
//            Timer timer(true);
//            std::clog << "Initialize the thinning process" << std::endl;
//            m_ThinningProcess.init(m_SkeletonCubicalComplex, &m_EmptySpaceDistanceMap, &m_EmptySpaceOpeningMap, &m_ConstrainedCubicalComplex);
//        }


//        {
//            Timer timer(true);
//            std::clog << "Run the thinning process" << std::endl;
//            m_ThinningProcess.directionalCollapse();
//        }

//        auto skeletonWithoutSurfaces = filterSurfacicParts(m_SkeletonCubicalComplex);

//        {
//            Timer timer(true);
//            std::clog << "Initialize the thinning process" << std::endl;
//            m_ThinningProcess.init(m_SkeletonCubicalComplex, &m_EmptySpaceDistanceMap, &m_EmptySpaceOpeningMap, &skeletonWithoutSurfaces);
//        }

//        {
//            Timer timer(true);
//            std::clog << "Run the thinning process" << std::endl;
//            m_ThinningProcess.directionalCollapse();
//        }

//        {
//            Timer timer(true);
//            std::clog << "Initialize m_GLSkeletonCubicalComplex" << std::endl;
//            m_GLSkeletonCubicalComplex.init(m_SkeletonCubicalComplex);
//        }
//    }

    m_bIsVoxelized = true;
}

void VoxelSceneAnalyzer::setMappedNodeGridColor() {
    Grid3D<Vec3f> colorGrid(m_pScene->getCurvSkeleton()->getGrid().resolution());
    for(auto i = 0u; i < colorGrid.size(); ++i) {
        colorGrid[i] = getColor(m_pScene->getCurvSkeleton()->getGrid()[i]);
    }

    m_VoxelGridColorTexture.setImage(0, GL_RGB, colorGrid.width(), colorGrid.height(), colorGrid.depth(),
                                     0, GL_RGB, GL_FLOAT, colorGrid.data());
    m_VoxelGridColorTexture.setMagFilter(GL_NEAREST);
    m_VoxelGridColorTexture.setMinFilter(GL_NEAREST);
}

void VoxelSceneAnalyzer::setMinTreeComponentColor() {
    const auto& voxelToNode = m_OpeningMinTree.getVoxelToNodeMapping();

    Grid3D<Vec3f> colorGrid(m_pScene->getCurvSkeleton()->getGrid().resolution());
    for(auto i = 0u; i < colorGrid.size(); ++i) {
        colorGrid[i] = getColor(voxelToNode[i]);
    }

    m_VoxelGridColorTexture.setImage(0, GL_RGB, colorGrid.width(), colorGrid.height(), colorGrid.depth(),
                                     0, GL_RGB, GL_FLOAT, colorGrid.data());
    m_VoxelGridColorTexture.setMagFilter(GL_NEAREST);
    m_VoxelGridColorTexture.setMinFilter(GL_NEAREST);
}

void VoxelSceneAnalyzer::setSceneSkeletonFromDiscreteData() {
    {
        Timer timer(true);

        auto computeSkeleton = [&]() {
            if(m_bComputeFilteredSkeleton) {
                std::clog << "Convert skeleton CC3D to Curvilinear Skeleton (segmented)" << std::endl;
                return getSegmentedCurvilinearSkeleton(m_SkeletonCubicalComplex, m_EmptySpaceCubicalComplex,
                                                                m_EmptySpaceDistanceMap, m_EmptySpaceOpeningMap, m_GridToWorldMatrix);
            } else {
                std::clog << "Convert skeleton CC3D to Curvilinear Skeleton (unsegmented)" << std::endl;
                return getCurvilinearSkeleton(m_SkeletonCubicalComplex, m_EmptySpaceCubicalComplex,
                                                       m_EmptySpaceDistanceMap, m_EmptySpaceOpeningMap, m_GridToWorldMatrix);
            }
        };

        m_pScene->setCurvSkeleton(computeSkeleton());
    }
}

void VoxelSceneAnalyzer::computeOpeningSegmentBorder() {
    auto segmentIdx = getOpeningSegmentation()(m_SelectedVoxel);

    m_SelectedVoxelOpeningSegmentBorder.clear();
    m_SelectedVoxelOpeningSegmentBorderColors.clear();

    const auto& cluster = m_VoxelOpeningSegmentationGraph.getCluster(segmentIdx);
    const auto& neighbours = m_VoxelOpeningSegmentationGraph.neighbours(segmentIdx);

    // Neighbour cluster color:
    for (auto i = 0u; i < neighbours.size(); ++i) {
        for (const auto& faceIdx : cluster.m_Portals[i]) {
            auto face = cluster.m_Border[faceIdx];
            m_SelectedVoxelOpeningSegmentBorder.push_back(face.m_VoxelFace);
            m_SelectedVoxelOpeningSegmentBorderColors.push_back(getColor(neighbours[i]));
        }
    }

    // Is "approximate portal area equal to opening" color:
    //std::vector<uint32_t> portalFaceCount(neighbours.size(), 0u);
    //for(auto i = 0u; i < neighbours.size(); ++i) {
    //    auto firstColorIdx = m_SelectedVoxelOpeningSegmentBorderColors.size();
    //    for(const auto& face: m_VoxelOpeningSegmentationGraph.getPortals()[cluster.m_Portals[i]]) {
    //        m_SelectedVoxelOpeningSegmentBorder.push_back(face);
    //        m_SelectedVoxelOpeningSegmentBorderColors.push_back(getColor(neighbours[i]));
    //        ++portalFaceCount[i];
    //    }
    //    bool isMergeable = false;
    //    if (sqrt(float(portalFaceCount[i])) > cluster.m_Opening && m_VoxelOpeningSegmentationGraph.getCluster(neighbours[i]).m_Opening == cluster.m_Opening) {
    //        isMergeable = true;
    //    }
    //    for (auto j = firstColorIdx; j < m_SelectedVoxelOpeningSegmentBorderColors.size(); ++j) {
    //        if (isMergeable) {
    //            m_SelectedVoxelOpeningSegmentBorderColors[j] = getColor(segmentIdx);
    //        }
    //    }
    //}

    // Is "bounding box opening equal to opening" color:
    //for (auto i = 0u; i < neighbours.size(); ++i) {
    //    BBox<Vec3i> bboxOfPortal(m_VoxelOpeningSegmentationGraph.getPortals()[cluster.m_Portals[i]][0].xyz());
    //    auto firstColorIdx = m_SelectedVoxelOpeningSegmentBorderColors.size();
    //    for (const auto& face : m_VoxelOpeningSegmentationGraph.getPortals()[cluster.m_Portals[i]]) {
    //        m_SelectedVoxelOpeningSegmentBorder.push_back(face);
    //        m_SelectedVoxelOpeningSegmentBorderColors.push_back(getColor(neighbours[i]));
    //        bboxOfPortal.grow(face.xyz());
    //    }
    //    auto size = bboxOfPortal.size();
    //    auto bboxOpening = max(max(size.x, size.y), size.z);
    //    bool isMergeable = false;
    //    if (bboxOpening > cluster.m_Opening && m_VoxelOpeningSegmentationGraph.getCluster(neighbours[i]).m_Opening == cluster.m_Opening) {
    //        isMergeable = true;
    //    }
    //    for (auto j = firstColorIdx; j < m_SelectedVoxelOpeningSegmentBorderColors.size(); ++j) {
    //        if (isMergeable) {
    //            m_SelectedVoxelOpeningSegmentBorderColors[j] = getColor(segmentIdx);
    //        }
    //    }
    //}

    // If "it exists a face with equal opening separing neighbour cluster" color
//    for (auto i = 0u; i < neighbours.size(); ++i) {
//        bool faceWithEqualOpeningExists = false;
//        auto firstColorIdx = m_SelectedVoxelOpeningSegmentBorderColors.size();
//        for (const auto& face : m_VoxelOpeningSegmentationGraph.getPortals()[cluster.m_Portals[i]]) {
//            m_SelectedVoxelOpeningSegmentBorder.push_back(face);
//            m_SelectedVoxelOpeningSegmentBorderColors.push_back(getColor(neighbours[i]));
//            if (m_VoxelOpeningSegmentationGraph.getOpeningMap()(face.xyz()) == cluster.m_Opening) {
//                faceWithEqualOpeningExists = true;
//            }
//        }
//        bool isMergeable = false;
//        if (faceWithEqualOpeningExists && m_VoxelOpeningSegmentationGraph.getCluster(neighbours[i]).m_Opening == cluster.m_Opening) {
//            isMergeable = true;
//        }
//        for (auto j = firstColorIdx; j < m_SelectedVoxelOpeningSegmentBorderColors.size(); ++j) {
//            if (isMergeable) {
//                m_SelectedVoxelOpeningSegmentBorderColors[j] = getColor(segmentIdx);
//            }
//        }
//    }

    m_SelectedVoxelOpeningSegmentBorderFaceVertices.clear();
    for(auto i = 0u; i < m_SelectedVoxelOpeningSegmentBorder.size(); ++i) {
        m_SelectedVoxelOpeningSegmentBorderFaceVertices.emplace_back(m_SelectedVoxelOpeningSegmentBorder[i],
                                                                     m_SelectedVoxelOpeningSegmentBorderColors[i],
                                                                     m_SelectedVoxelOpeningSegmentBorderColors[i],
                                                                     m_SelectedVoxelOpeningSegmentBorderColors[i]);
    }

    m_GLSelectedVoxelOpeningSegmentBorderFaceList.init(m_SelectedVoxelOpeningSegmentBorderFaceVertices.data(), m_SelectedVoxelOpeningSegmentBorderFaceVertices.size());
}

void VoxelSceneAnalyzer::onPickIntersection() {
    m_PickedPointCamera = makeUnique<SurfacePointCamera>(m_Viewer.getPickedIntersection());
    m_pPickedPointRenderer->init(*m_pScene, *m_PickedPointCamera, m_PickedPointFramebuffer);
    m_pPickedPointRenderer->render();
}

void VoxelSceneAnalyzer::drawRecursiveGUIMinTree(size_t node) {
    if (ImGui::TreeNode((toString(node) + " | " + toString(m_OpeningMinTree.getLevel(node))).c_str()))
    {
        for(const auto& child: m_OpeningMinTree.getChildren(node)) {
            drawRecursiveGUIMinTree(child);
        }
        ImGui::TreePop();
    }
}

void VoxelSceneAnalyzer::exposeGUI(GUI& gui) {
    if(auto window = gui.addWindow("VoxelSceneAnalyzer")) {
        if(ImGui::CollapsingHeader("Selected Voxel")) {
            gui.addVarRW(BNZ_GUI_VAR(m_bDisplaySelectedVoxel));

            gui.addButton("SetSelectedVoxelFromPickedPoint", [&]() {
                if (m_Viewer.getPickedIntersection()) {
                    m_SelectedVoxel = m_VoxelOpeningSegmentationGraph.getVoxel(m_Viewer.getPickedIntersection(),
                        m_Viewer.getCamera().getOrigin() - m_Viewer.getPickedIntersection().P);
                    m_bIsVoxelSelected = true;
                }
            });

            gui.addSeparator();

            gui.addVarRW(BNZ_GUI_VAR(m_SelectedVoxel));
            gui.addButton("XPos", [&]() {
                ++m_SelectedVoxel.x;
            });
            gui.sameLine();
            gui.addButton("XNeg", [&]() {
                --m_SelectedVoxel.x;
            });
            gui.sameLine();
            gui.addButton("YPos", [&]() {
                ++m_SelectedVoxel.y;
            });
            gui.sameLine();
            gui.addButton("YNeg", [&]() {
                --m_SelectedVoxel.y;
            });
            gui.sameLine();
            gui.addButton("ZPos", [&]() {
                ++m_SelectedVoxel.z;
            });
            gui.sameLine();
            gui.addButton("ZNeg", [&]() {
                --m_SelectedVoxel.z;
            });

            gui.addButton("Select parent", [&]() {
                m_SelectedVoxel = m_EmptySpaceOpeningCenterMap(m_SelectedVoxel);
            });

            gui.addSeparator();

            gui.addValue("Distance 26", m_nSelectedVoxelDistance26);
            gui.addValue("Opening 26", m_nSelectedVoxelOpening26);

            gui.addValue("Edge Distance 26", m_ThinningProcess.m_EdgeDistanceMap(m_SelectedVoxel));
            gui.addValue("Edge Opening 26", m_ThinningProcess.m_EdgeOpeningMap(m_SelectedVoxel));
            gui.addValue("Thinning Birth Date", m_ThinningProcess.m_BirthMap(m_SelectedVoxel));

            gui.addSeparator();

            gui.addValue("Opening Cluster Index", m_VoxelOpeningSegmentationGraph.getVoxelCluster(m_SelectedVoxel));

            gui.addSeparator();

            gui.addVarRW(BNZ_GUI_VAR(m_bDrawSelectedVoxelDiscreteMaxball));

            gui.addSeparator();

            gui.addVarRW(BNZ_GUI_VAR(m_bDrawSelectedVoxelOpeningSegmentBorder));
            gui.addButton("Compute opening segment border", [&]() {
                computeOpeningSegmentBorder();
            });
            gui.addVarRW(BNZ_GUI_VAR(m_bDisplayBordersCubicalComplex));

            gui.addSeparator();

            gui.addVarRW(BNZ_GUI_VAR(m_bDisplayMaxballExpension));
            gui.addButton("computeMaxballExpension()", [&]() {
                computeMaxballExpension();
            });
        }

        gui.addSeparator();

        if(ImGui::CollapsingHeader("Voxelization and Thinning")) {
            gui.addVarRW(BNZ_GUI_VAR(m_nVoxelGridRes));
            gui.addButton("Voxelize and Skeletonize", [&]() {
                computeSceneDiscreteData();
                setOpeningSegmentationGridColor();
            });

            gui.addVarRW(BNZ_GUI_VAR(m_bDisplaySceneVoxelGrid));
            gui.addVarRW(BNZ_GUI_VAR(m_bDisplayCenterOfMaxballs));
            gui.addVarRW(BNZ_GUI_VAR(m_bDisplayCutVoxelGrid));
            gui.addButton("Compute Cut Voxel Grid", [&]() {
                computeCutVoxelGrid();
            });
            gui.addVarRW(BNZ_GUI_VAR(m_bDisplayCC));

            gui.addButton("Filter surface from skeleton", [&]() {
                m_SkeletonCubicalComplex = filterSurfacicParts(m_SkeletonCubicalComplex);
                m_GLSkeletonCubicalComplex.init(m_SkeletonCubicalComplex);
            });

            gui.addVarRW(BNZ_GUI_VAR(m_nThinningIterCount));
            gui.addButton("Thinning", [&]() {
                m_ThinningProcess.directionalCollapse(m_nThinningIterCount);
                m_GLSkeletonCubicalComplex.init(m_SkeletonCubicalComplex);
            });
            gui.addVarRW(BNZ_GUI_VAR(m_bComputeFilteredSkeleton));
            gui.addButton("Set Scene Skeleton from CC3D", [&]() {
                setSceneSkeletonFromDiscreteData();
            });
            gui.addButton("setMappedNodeGridColor()", [&]() {
                setMappedNodeGridColor();
            });

            gui.addSeparator();

            gui.addValue("GridSize", m_SceneVoxelGrid.resolution());
            gui.addValue(BNZ_GUI_VAR(m_fGridToWorldScale));
            gui.addValue(BNZ_GUI_VAR(m_CameraVoxel));
            gui.addValue(BNZ_GUI_VAR(m_CameraDistance));
            gui.addVarRW(BNZ_GUI_VAR(m_bDisplaySkeleton));

            gui.addButton("Draw maxball", [&]() {
                m_bDrawMaxball = !m_bDrawMaxball;
            });
        }

        gui.addSeparator();

        if(ImGui::CollapsingHeader("Opening Map and Segmentation")) {
            gui.addButton("setOpeningSegmentationGridColor()", [&]() {
                setOpeningSegmentationGridColor();
            });

            gui.addButton("setMinTreeComponentColor()", [&]() {
                setMinTreeComponentColor();
            });

            gui.addVarRW(BNZ_GUI_VAR(m_OpeningDirections));
            gui.addButton("ResetOpeningMap", [&]() {
                m_EmptySpaceOpeningMap = computeOpeningMap26Experiment(m_EmptySpaceDistanceMap, m_EmptySpaceOpeningCenterMap, {});
                m_VoxelOpeningSegmentationGraph = VoxelOpeningSegmentationGraph(m_GridToWorldMatrix,
                    m_EmptySpaceDistanceMap,
                    m_EmptySpaceOpeningMap,
                    m_EmptySpaceOpeningCenterMap);
                setOpeningSegmentationGridColor();
            });
            gui.addButton("ComputeOpeningMapIterative", [&]() {
                m_EmptySpaceOpeningMap = computeOpeningMap26Experiment(m_EmptySpaceOpeningMap, m_EmptySpaceOpeningCenterMap,
                    { m_OpeningDirections.x, m_OpeningDirections.y, m_OpeningDirections.z }, true);
                m_VoxelOpeningSegmentationGraph = VoxelOpeningSegmentationGraph(m_GridToWorldMatrix,
                    m_EmptySpaceDistanceMap,
                    m_EmptySpaceOpeningMap,
                    m_EmptySpaceOpeningCenterMap);
                setOpeningSegmentationGridColor();
            });
        }

        gui.addSeparator();

        if(ImGui::CollapsingHeader("2D Cuts")) {
            gui.addButton("Display ES Cut", [&]() {
                auto size = getCutSize(m_EmptySpaceCubicalComplex.resolution(), Grid3DCutAxis(m_nCutAxis));

                Unique<uint32_t[]> array = makeUniqueArray<uint32_t>(size.x * size.y);

                processCut(m_EmptySpaceCubicalComplex.resolution(),
                    m_nCutPosition, Grid3DCutAxis(m_nCutAxis), [&](uint32_t x, uint32_t y, Vec3i voxel) {
                    array[x + y * size.x] = CubicalComplex3D::isInObject(voxel.x, voxel.y, voxel.z, m_EmptySpaceCubicalComplex);
                });

                m_CutImage = Image(size.x, size.y);
                for (auto y = 0u; y < size.y; ++y) {
                    for (auto x = 0u; x < size.x; ++x) {
                        m_CutImage(x, y) = Vec4f(array[x + y * size.x]);
                    }
                }
                m_bDisplayCut = true;
            });

            gui.addButton("Display D1 Cut", [&]() {
                auto size = getCutSize(m_SkeletonCubicalComplex.resolution(), Grid3DCutAxis(m_nCutAxis));

                Unique<uint32_t[]> array = makeUniqueArray<uint32_t>(size.x * size.y);
                uint32_t maxValue = 0u;

                processCut(m_SkeletonCubicalComplex.resolution(),
                    m_nCutPosition, Grid3DCutAxis(m_nCutAxis), [&](uint32_t x, uint32_t y, Vec3i voxel) {
                    array[x + y * size.x] = m_EmptySpaceDistanceMap(voxel.x, voxel.y, voxel.z);
                    maxValue = max(maxValue, array[x + y * size.x]);
                });

                m_CutImage = Image(size.x, size.y);
                for (auto y = 0u; y < size.y; ++y) {
                    for (auto x = 0u; x < size.x; ++x) {
                        m_CutImage(x, y) = Vec4f(array[x + y * size.x]) / float(maxValue);
                    }
                }
                m_bDisplayCut = true;
            });

            gui.addButton("Display Opening Cut", [&]() {
                auto size = getCutSize(m_SkeletonCubicalComplex.resolution(), Grid3DCutAxis(m_nCutAxis));

                m_CutImage = Image(size.x, size.y);

                if (m_bDisplayOpeningSegmentation) {
                    processCut(getOpeningSegmentation().resolution(),
                        m_nCutPosition, Grid3DCutAxis(m_nCutAxis), [&](uint32_t x, uint32_t y, Vec3i voxel) {
                        m_CutImage(x, y) = Vec4f(getColor(getOpeningSegmentation()(voxel.x, voxel.y, voxel.z)), 1.f);
                    });
                }
                else {
                    Unique<uint32_t[]> array = makeUniqueArray<uint32_t>(size.x * size.y);
                    uint32_t maxValue = 0u;

                    processCut(m_SkeletonCubicalComplex.resolution(),
                        m_nCutPosition, Grid3DCutAxis(m_nCutAxis), [&](uint32_t x, uint32_t y, Vec3i voxel) {
                        array[x + y * size.x] = m_EmptySpaceOpeningMap(voxel.x, voxel.y, voxel.z);
                        maxValue = max(maxValue, array[x + y * size.x]);
                    });

                    for (auto y = 0u; y < size.y; ++y) {
                        for (auto x = 0u; x < size.x; ++x) {
                            m_CutImage(x, y) = Vec4f(array[x + y * size.x]) / float(maxValue);
                        }
                    }
                }

                m_bDisplayCut = true;
            });
            gui.addVarRW(BNZ_GUI_VAR(m_bDisplayOpeningSegmentation));

            gui.addButton("Display Decenterness Cut", [&]() {
                auto size = getCutSize(m_SkeletonCubicalComplex.resolution(), Grid3DCutAxis(m_nCutAxis));

                Unique<uint32_t[]> array = makeUniqueArray<uint32_t>(size.x * size.y);
                uint32_t maxValue = 0u;

                processCut(m_SkeletonCubicalComplex.resolution(),
                    m_nCutPosition, Grid3DCutAxis(m_nCutAxis), [&](uint32_t x, uint32_t y, Vec3i voxel) {
                    array[x + y * size.x] = m_EmptySpaceOpeningMap(voxel.x, voxel.y, voxel.z);
                    array[x + y * size.x] -= m_EmptySpaceDistanceMap(voxel.x, voxel.y, voxel.z);
                    maxValue = max(maxValue, array[x + y * size.x]);
                });

                m_CutImage = Image(size.x, size.y);
                for (auto y = 0u; y < size.y; ++y) {
                    for (auto x = 0u; x < size.x; ++x) {
                        m_CutImage(x, y) = Vec4f(array[x + y * size.x]) / float(maxValue);
                    }
                }
                m_bDisplayCut = true;
            });

            if(gui.addVarRW(BNZ_GUI_VAR(m_nCutPosition))) {
                computeCutVoxelGrid();
            }
            const char* cutAxes[] = { "X", "Y", "Z" };
            gui.addRadioButtons("cutAxis", m_nCutAxis, 3, [&](uint32_t idx) { return cutAxes[idx]; });

            gui.addButton("SaveCutImage", [&]() {
                auto copy = m_CutImage;
                for (auto& value : copy) {
                    value.w = 1.f;
                }
                copy.flipY();
                m_Viewer.storeScreenshotImage(copy);
            });
        }

        gui.addSeparator();

        if(ImGui::CollapsingHeader("Opening Portal Ray Tracing")) {
            gui.addVarRW(BNZ_GUI_VAR(m_bRayTracePortals));
            gui.addVarRW(BNZ_GUI_VAR(m_nPortalRayCount));


            const char* rsmLabels[] = {
                "RSM_RANDOM_PORTAL",
                "RSM_AREA_BASED_PORTAL",
                "RSM_TARGET_PORTAL",
                "RSM_LOCAL",
                "RSM_TOWARD_POINT_LIGHT"
            };
            gui.addCombo(BNZ_GUI_VAR(m_nRaySamplingMethod), RSM_COUNT, rsmLabels);

            gui.addVarRW(BNZ_GUI_VAR(m_nSelectedPortalIndex));
        }
    }
    m_VoxelOpeningSegmentationGraph.exposeGUI(gui);

    if(auto window = gui.addWindow("OpeningMinTree")) {
        auto root = m_OpeningMinTree.getRoot();
        drawRecursiveGUIMinTree(root);
    }

    if(m_Viewer.getPickedIntersection()) {
        if(auto pickedViewWindow = gui.addWindow("VoxelSceneAnalyzer::PickedView")) {
            if(gui.addCombo("Renderer", m_nPickedPointCurrentRendererIdx, m_Viewer.getRenderModule().m_RendererManager.getRendererNames().size(), [&](uint32_t idx) {
                    return m_Viewer.getRenderModule().m_RendererManager.getRendererNames()[idx].c_str();
                })) {
                m_pPickedPointRenderer = m_Viewer.getRenderModule().m_RendererManager.createRenderer(m_Viewer.getRenderModule().m_RendererManager.getRendererNames()[m_nPickedPointCurrentRendererIdx]);

                onPickIntersection();
            }

            gui.addVarRW(BNZ_GUI_VAR(m_nPickedPointChannel));
            gui.addVarRW(BNZ_GUI_VAR(m_fPickedPointGamma));
            gui.addButton("Render", [&]() {
                m_pPickedPointRenderer->render();
            });
            gui.addButton("Clear", [&]() {
                m_PickedPointFramebuffer.clear();
            });

            gui.addSeparator();

            m_pPickedPointRenderer->exposeIO(gui);

            gui.addSeparator();

            m_PickedPointGLFramebuffer.bindForDrawing();
            m_PickedPointGLFramebuffer.setDrawingViewport();
            m_Viewer.getGLImageRenderer().drawFramebuffer(m_fPickedPointGamma, m_PickedPointFramebuffer, m_nPickedPointChannel, true);

            glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
            ImVec2 canvas_pos = ImGui::GetCursorScreenPos();
            ImVec2 canvas_size = ImVec2(max((float)m_PickedPointGLFramebuffer.getWidth(), ImGui::GetWindowContentRegionMax().x - ImGui::GetCursorPos().x),
                                        max((float)m_PickedPointGLFramebuffer.getHeight(),ImGui::GetWindowContentRegionMax().y - ImGui::GetCursorPos().y));
            ImVec2 mouse_pos_in_canvas = ImVec2(ImGui::GetIO().MousePos.x - canvas_pos.x, ImGui::GetIO().MousePos.y - canvas_pos.y);

            gui.addImage(m_PickedPointGLFramebuffer.getColorBuffer(0));

            gui.addValue("mouse pos_x", mouse_pos_in_canvas.x);
            gui.addValue("mouse pos_y", mouse_pos_in_canvas.y);

            if(m_nPickedPointChannel < m_PickedPointFramebuffer.getChannelCount()) {
                auto sum = zero<Vec3f>();
                auto valueCount = 0u;
                for(auto j = 0u; j < m_PickedPointFramebuffer.getHeight(); ++j) {
                    for(auto i = 0u; i < m_PickedPointFramebuffer.getWidth(); ++i) {
                        auto value = m_PickedPointFramebuffer.getChannel(m_nPickedPointChannel)(i, j);
                        if(value.w) {
                            auto uv = getUV(i, j, m_PickedPointFramebuffer.getSize());
                            float jacobian = 0.f;
                            jacobian = paraboloidMappingJacobian(uv);
                            sum += (Vec3f(value) / value.w) * abs(jacobian);
                            ++valueCount;
                        }
                    }
                }
                sum /= valueCount;
                gui.addValue("IntegralOverHemisphere", sum);
            }
        }
    }

    drawSideView(Vec4f(0));
}

void VoxelSceneAnalyzer::computeMaxballExpension() {
    m_MaxballExpension.clear();

    Grid3D<bool> visitGrid(m_VoxelOpeningSegmentationGraph.getOpeningMap().resolution(), false);

    std::queue<Vec3i> queue;
    queue.push(m_SelectedVoxel);

    while(!queue.empty()) {
        auto voxel = queue.front();
        queue.pop();

        auto distance = m_VoxelOpeningSegmentationGraph.getDistanceMap()(voxel);

        BBox<Vec3i> ballBBox(voxel - Vec3i(distance - 1), voxel + Vec3i(distance - 1));
        for(auto z = ballBBox.lower.z; z <= ballBBox.upper.z; ++z) {
            for(auto y = ballBBox.lower.y; y <= ballBBox.upper.y; ++y) {
                for(auto x = ballBBox.lower.x; x <= ballBBox.upper.x; ++x) {
                    if(!visitGrid(x, y, z)) {
                        if(m_VoxelOpeningSegmentationGraph.getOpeningMap()(x, y, z) == distance) {
                            visitGrid(x, y, z) = true;
                            queue.push(Vec3i(x, y, z));
                            m_MaxballExpension.emplace_back(Vec3i(x, y, z));
                        }
                    }
                }
            }
        }
    }

    m_MaxballExpensionBorder.clear();

    for(const auto& voxel: m_MaxballExpension) {
        bool isBorder = false;
        foreach6Neighbour(m_VoxelOpeningSegmentationGraph.getOpeningMap().resolution(), voxel, [&](const Vec3i& neighbour) {
            if(!visitGrid(neighbour)) {
                isBorder = true;
            }
        });
        if(isBorder) {
            m_MaxballExpensionBorder.emplace_back(voxel);
        }
    }
}

void VoxelSceneAnalyzer::computeMaxballExpensionStepByStep() {
    if(m_MaxballExpensionVisitGrid.empty()) {
        m_MaxballExpensionVisitGrid = Grid3D<bool>(m_VoxelOpeningSegmentationGraph.getOpeningMap().resolution(), false);
    }

    if(m_MaxballExpensionQueue.empty()) {
        m_MaxballExpensionQueue.push(m_SelectedVoxel);
    }

    auto voxel = m_MaxballExpensionQueue.front();
    m_MaxballExpensionQueue.pop();

    auto distance = m_VoxelOpeningSegmentationGraph.getDistanceMap()(voxel);
    auto opening = m_VoxelOpeningSegmentationGraph.getOpeningMap()(m_SelectedVoxel);

    BBox<Vec3i> ballBBox(voxel - Vec3i(distance - 1), voxel + Vec3i(distance - 1));
    for(auto z = ballBBox.lower.z; z <= ballBBox.upper.z; ++z) {
        for(auto y = ballBBox.lower.y; y <= ballBBox.upper.y; ++y) {
            for(auto x = ballBBox.lower.x; x <= ballBBox.upper.x; ++x) {
                if(!m_MaxballExpensionVisitGrid(x, y, z)) {
                    if(m_VoxelOpeningSegmentationGraph.getOpeningMap()(x, y, z) <= opening) {
                        m_MaxballExpensionVisitGrid(x, y, z) = true;
                        m_MaxballExpensionQueue.push(Vec3i(x, y, z));
                        m_MaxballExpension.emplace_back(Vec3i(x, y, z));
                    }
                }
            }
        }
    }

    decltype(m_MaxballExpension) maxballExpensionBorder;

    for(const auto& voxel: m_MaxballExpension) {
        bool isBorder = false;
        foreach6Neighbour(m_VoxelOpeningSegmentationGraph.getOpeningMap().resolution(), voxel, [&](const Vec3i& neighbour) {
            if(!m_MaxballExpensionVisitGrid(neighbour)) {
                isBorder = true;
            }
        });
        if(isBorder) {
            maxballExpensionBorder.emplace_back(voxel);
        }
    }

    swap(maxballExpensionBorder, m_MaxballExpension);
}

bool VoxelSceneAnalyzer::handlePicking(const Vec4u& selectedObjectID) {
    if(selectedObjectID.x == 42) {
        // Pick a voxel
        m_bIsVoxelSelected = true;
        m_SelectedVoxel = Vec3i(selectedObjectID.y, selectedObjectID.z, selectedObjectID.w);

        return true;
    }
    return false;
}

bool VoxelSceneAnalyzer::drawSideView(const Vec4f& viewport) {
    if(m_bDisplayCut) {
        if(auto cutImageWindow = m_Viewer.getGUI().addWindow("CutImage", false)) {
            fillTexture(m_CutTexture, m_CutImage);
            m_CutTexture.setMinFilter(GL_LINEAR);
            m_CutTexture.setMagFilter(GL_LINEAR);
            m_Viewer.getGUI().addImage(m_CutTexture, 2.f);

            return true;
        }
    }
    return false;
}

void VoxelSceneAnalyzer::draw() {
    m_DisplayStream.clearObjects();
    m_pGLDebugRenderer->addStream(&m_DisplayStream);

    if (m_bIsVoxelized) {
        // Draw selected voxel
        if(m_bIsVoxelSelected) {
            if (m_bDisplaySelectedVoxel) {
                // Display voxel
                auto lower = Vec3f(m_GridToWorldMatrix * Vec4f(m_SelectedVoxel.x, m_SelectedVoxel.y, m_SelectedVoxel.z, 1.f));
                auto upper = Vec3f(m_GridToWorldMatrix * Vec4f(m_SelectedVoxel.x + 1, m_SelectedVoxel.y + 1, m_SelectedVoxel.z + 1, 1.f));

                m_DisplayStream.addBBox(lower, upper, Vec3f(0, 0, 1), 3);

                // Display linked node on the skeleton
                if (m_Viewer.isSkeletonDisplayed() &&
                    m_pScene->getCurvSkeleton()) {
                    auto pSkel = m_pScene->getCurvSkeleton();
                    auto node = pSkel->getGrid()(m_SelectedVoxel);

                    if (node != uint32_t(-1)) {
                        m_DisplayStream.addLine(
                            0.5f * (lower + upper),
                            pSkel->getNode(node).P,
                            Vec3f(1, 0, 0),
                            Vec3f(1, 0, 0),
                            5);
                    }
                }

                // Display opening tree branch
                {
                    auto voxel = m_SelectedVoxel;
                    Vec3u openingCenter;
                    while((openingCenter = m_EmptySpaceOpeningCenterMap(voxel)) != Vec3u(voxel)) {
                        auto lower = Vec3f(m_GridToWorldMatrix * Vec4f(voxel.x, voxel.y, voxel.z, 1.f));
                        auto upper = Vec3f(m_GridToWorldMatrix * Vec4f(voxel.x + 1, voxel.y + 1, voxel.z + 1, 1.f));

                        auto lower2 = Vec3f(m_GridToWorldMatrix * Vec4f(openingCenter.x, openingCenter.y, openingCenter.z, 1.f));
                        auto upper2 = Vec3f(m_GridToWorldMatrix * Vec4f(openingCenter.x + 1, openingCenter.y + 1, openingCenter.z + 1, 1.f));

                        m_DisplayStream.addBBox(lower2, upper2, Vec3f(1, 0, 0), 3);

                        m_DisplayStream.addLine(
                            0.5f * (lower + upper),
                            0.5f * (lower2 + upper2),
                            Vec3f(0, 0, 1),
                            Vec3f(0, 0, 1),
                            5);

                        voxel = openingCenter;
                    }
                }


            }

            m_nSelectedVoxelDistance26 = m_EmptySpaceDistanceMap(m_SelectedVoxel);
            m_nSelectedVoxelOpening26 = m_EmptySpaceOpeningMap(m_SelectedVoxel);

            if(m_bDrawSelectedVoxelDiscreteMaxball) {
                BBox<Vec3i> ballBBox(m_SelectedVoxel - Vec3i(m_nSelectedVoxelDistance26 - 1), m_SelectedVoxel + Vec3i(m_nSelectedVoxelDistance26 - 1));

                auto lower = Vec3f(m_GridToWorldMatrix * Vec4f(ballBBox.lower.x, ballBBox.lower.y, ballBBox.lower.z, 1.f));
                auto upper = Vec3f(m_GridToWorldMatrix * Vec4f(ballBBox.upper.x + 1, ballBBox.upper.y + 1, ballBBox.upper.z + 1, 1.f));

                m_DisplayStream.addBBox(lower, upper, Vec3f(1, 0, 0), 3);
            }

            if(m_bDrawSelectedVoxelOpeningSegmentBorder) {
                m_GLCubicalComplexFaceListRenderer.setMVMPatrix(m_Viewer.getCamera().getViewProjMatrix() * m_GridToWorldMatrix);
                m_GLCubicalComplexFaceListRenderer.render(m_GLSelectedVoxelOpeningSegmentBorderFaceList);
            }

            if(m_bDisplayMaxballExpension) {
                for(const auto& voxel: m_MaxballExpensionBorder) {
                    auto lower = Vec3f(m_GridToWorldMatrix * Vec4f(voxel.x, voxel.y, voxel.z, 1.f));
                    auto upper = Vec3f(m_GridToWorldMatrix * Vec4f(voxel.x + 1, voxel.y + 1, voxel.z + 1, 1.f));

                    m_DisplayStream.addBBox(lower, upper, Vec3f(1, 1, 0), 3);
                }
            }
        }

        if(m_bDisplayCC) {
            m_VoxSkelRenderer.setProjectionMatrix(m_Viewer.getCamera().getProjMatrix());
            m_VoxSkelRenderer.setViewMatrix(m_Viewer.getCamera().getViewMatrix());

            if(m_bDisplaySkeleton) {
                m_VoxSkelRenderer.drawCubicalComplex(m_GridToWorldMatrix, m_GLSkeletonCubicalComplex);
            } else {
                m_VoxSkelRenderer.drawCubicalComplex(m_GridToWorldMatrix, m_GLSceneCubicalComplex);
            }
        }

        if(m_bDisplayBordersCubicalComplex) {
            m_VoxSkelRenderer.setProjectionMatrix(m_Viewer.getCamera().getProjMatrix());
            m_VoxSkelRenderer.setViewMatrix(m_Viewer.getCamera().getViewMatrix());

            m_VoxSkelRenderer.drawCubicalComplex(m_GridToWorldMatrix, m_GLBordersCubicalComplex);
        }

        auto worldToGrid = inverse(m_GridToWorldMatrix);
        auto camPos = worldToGrid * Vec4f(m_Viewer.getCamera().getOrigin(), 1.f);
        m_CameraVoxel = Vec3i(camPos.x, camPos.y, camPos.z);
        if(m_EmptySpaceDistanceMap.contains(m_CameraVoxel)) {
            m_CameraDistance = m_EmptySpaceDistanceMap(m_CameraVoxel);
        }

        m_fGridToWorldScale = m_GridToWorldMatrix[0][0];

        if(m_bDrawMaxball) {
            Vec3f voxelPos = Vec3f(m_GridToWorldMatrix * Vec4f(m_CameraVoxel.x + 0.5f,
                                                               m_CameraVoxel.y + 0.5f,
                                                               m_CameraVoxel.z + 0.5f,
                                                               1.f));
            if(m_EmptySpaceDistanceMap.contains(m_CameraVoxel)) {
                float dist = (m_EmptySpaceDistanceMap(m_CameraVoxel) - 0.5f) * m_GridToWorldMatrix[0][0];
                m_DisplayStream.addSphere(voxelPos, dist, Vec3f(m_CameraVoxel) / Vec3f(m_EmptySpaceDistanceMap.resolution()));
            }
        }

        m_GLVoxelGridRenderer.setProjectionMatrix(m_Viewer.getCamera().getProjMatrix());
        m_GLVoxelGridRenderer.setViewMatrix(m_Viewer.getCamera().getViewMatrix());

        if(m_bDisplayCenterOfMaxballs) {
            m_GLVoxelGridRenderer.render(m_GridToWorldMatrix, m_GLCenterOfMaxballsVoxelGrid, m_VoxelGridColorTexture.glId(), false);
        }

        if(m_bDisplayCutVoxelGrid) {
            m_GLVoxelGridRenderer.render(m_GridToWorldMatrix, m_GLCutVoxelGrid, m_VoxelGridColorTexture.glId(), false);
        }

        if(m_bDisplaySceneVoxelGrid) {
            m_GLVoxelGridRenderer.render(m_GridToWorldMatrix, m_GLSceneVoxelGrid, m_VoxelGridColorTexture.glId());
        }

        if(m_Viewer.getPickedIntersection()) {
            auto wi = m_Viewer.getCamera().getOrigin() - m_Viewer.getPickedIntersection().P;
            auto voxel = m_VoxelOpeningSegmentationGraph.getVoxel(m_Viewer.getPickedIntersection(), wi);
            auto openingClusterIdx = m_VoxelOpeningSegmentationGraph.getVoxelCluster(voxel);

            auto lower = Vec3f(m_GridToWorldMatrix * Vec4f(voxel.x, voxel.y, voxel.z, 1.f));
            auto upper = Vec3f(m_GridToWorldMatrix * Vec4f(voxel.x + 1, voxel.y + 1, voxel.z + 1, 1.f));

            m_DisplayStream.addBBox(lower, upper, getColor(openingClusterIdx), 3);

            if (m_bRayTracePortals) {
                RandomGenerator rng;

                const auto& portals = m_VoxelOpeningSegmentationGraph.getPortals();
                
                if (openingClusterIdx != VoxelOpeningSegmentationGraph::NO_CLUSTER()) {
                    const auto& cluster = m_VoxelOpeningSegmentationGraph.getCluster(openingClusterIdx);
                    auto portalCount = cluster.m_Portals.size();

                    if (portalCount > 0u) {
                        std::vector<Ray> rays;
                        std::vector<Col3f> colors;

                        switch(m_nRaySamplingMethod) {
                        default:
                            break;
                        case RSM_RANDOM_PORTAL:
                            for (auto i = 0u; i < m_nPortalRayCount; ++i) {
                                auto portalSample = min(size_t(rng.getFloat() * portalCount), portalCount - 1);
                                //auto portalIdx = cluster.m_Portals[portalSample];
                                auto faceCount = cluster.m_Portals[portalSample].size();
                                if (faceCount > 0u) {
                                    auto faceSample = min(size_t(rng.getFloat() * faceCount), faceCount - 1);
                                    // First attempt, just trace through the center of the voxel face
                                    Vec3i u, v;
                                    auto face = cluster.m_Border[cluster.m_Portals[portalSample][faceSample]];
                                    getFaceVectors(CC3DFaceBits::Value(face.m_VoxelFace.w), u, v);

                                    auto faceCenter = Vec3f(face.m_VoxelFace) + Vec3f(u) * rng.getFloat() + Vec3f(v) * rng.getFloat();
                                    auto faceCenterWorldSpace = Vec3f(m_GridToWorldMatrix * Vec4f(faceCenter, 1.f));

                                    auto dir = faceCenterWorldSpace - m_Viewer.getPickedIntersection().P;
                                    rays.emplace_back(Ray(m_Viewer.getPickedIntersection(), dir));
                                    auto neighbour = m_VoxelOpeningSegmentationGraph.neighbours(openingClusterIdx)[portalSample];
                                    colors.emplace_back(getColor(neighbour));
                               }
                            }
                            break;
                        case RSM_AREA_BASED_PORTAL:
                            {
                                std::vector<float> values(getDistribution1DBufferSize(portalCount));
                                for(auto i = 0u; i < portalCount; ++i) {
                                    values[i] = cluster.m_Portals[i].size();
                                }

                                buildDistribution1D([&](uint32_t i) {
                                    return values[i];
                                }, values.data(), portalCount);

                                for (auto i = 0u; i < m_nPortalRayCount; ++i) {
                                    auto portalSample = sampleDiscreteDistribution1D(values.data(), portalCount, rng.getFloat());
                                    //auto portalIdx = cluster.m_Portals[portalSample];
                                    auto faceCount = cluster.m_Portals[portalSample.value].size();
                                    if (faceCount > 0u) {
                                        auto faceSample = min(size_t(rng.getFloat() * faceCount), faceCount - 1);
                                        // First attempt, just trace through the center of the voxel face
                                        Vec3i u, v;
                                        auto face = cluster.m_Border[cluster.m_Portals[portalSample.value][faceSample]];
                                        getFaceVectors(CC3DFaceBits::Value(face.m_VoxelFace.w), u, v);

                                        auto faceCenter = Vec3f(face.m_VoxelFace) + Vec3f(u) * rng.getFloat() + Vec3f(v) * rng.getFloat();
                                        auto faceCenterWorldSpace = Vec3f(m_GridToWorldMatrix * Vec4f(faceCenter, 1.f));

                                        auto dir = faceCenterWorldSpace - m_Viewer.getPickedIntersection().P;
                                        rays.emplace_back(Ray(m_Viewer.getPickedIntersection(), dir));
                                        auto neighbour = m_VoxelOpeningSegmentationGraph.neighbours(openingClusterIdx)[portalSample.value];
                                        colors.emplace_back(getColor(neighbour));
                                    }
                                }
                            }
                            break;
                        case RSM_TARGET_PORTAL:
                            if(m_nSelectedPortalIndex < portalCount) {
                                auto portalSample = m_nSelectedPortalIndex;
//                                auto portalIdx = cluster.m_Portals[portalSample];
//                                auto faceCount = portals[portalIdx].size();
                                auto faceCount = cluster.m_Portals[portalSample].size();
                                LOG(INFO) << "global index = " << cluster.m_PortalGlobalIndices[portalSample];
                                if(faceCount > 0u) {
                                    for (auto i = 0u; i < m_nPortalRayCount; ++i) {
                                        auto faceSample = min(size_t(rng.getFloat() * faceCount), faceCount - 1);
                                        // First attempt, just trace through the center of the voxel face
                                        Vec3i u, v;
                                        auto face = cluster.m_Border[cluster.m_Portals[portalSample][faceSample]];
                                        getFaceVectors(CC3DFaceBits::Value(face.m_VoxelFace.w), u, v);

                                        auto faceCenter = Vec3f(face.m_VoxelFace) + Vec3f(u) * rng.getFloat() + Vec3f(v) * rng.getFloat();
                                        auto faceCenterWorldSpace = Vec3f(m_GridToWorldMatrix * Vec4f(faceCenter, 1.f));

                                        auto dir = faceCenterWorldSpace - m_Viewer.getPickedIntersection().P;
                                        rays.emplace_back(Ray(m_Viewer.getPickedIntersection(), dir));
                                        auto neighbour = m_VoxelOpeningSegmentationGraph.neighbours(openingClusterIdx)[portalSample];
                                        colors.emplace_back(getColor(neighbour));
                                    }
                                }
                            } else {
                                std::cerr << "Error: m_nSelectedPortalIndex >= portalCount" << std::endl;
                            }
                            break;
                        case RSM_LOCAL:
                            for (auto i = 0u; i < m_nPortalRayCount; ++i) {
                                auto dirSample = uniformSampleHemisphere(rng.getFloat(), rng.getFloat(), m_Viewer.getPickedIntersection().Ns);
                                rays.emplace_back(Ray(m_Viewer.getPickedIntersection(), dirSample.value));
                                colors.emplace_back(Vec3f(1, 0, 0));
                            }
                            break;
                        case RSM_TOWARD_POINT_LIGHT:
                        {
                            const PointLight* pPointLight = nullptr;
                            m_Viewer.getScene()->getLightContainer().forEach<PointLight>([&](std::size_t index, const PointLight& light) {
                               if(!pPointLight) {
                                   pPointLight = &light;
                               }
                            });

                            if(!pPointLight) {
                                std::cerr << "No PointLight in the scene" << std::endl;
                                break;
                            }

                            m_DisplayStream.addLine(
                                m_Viewer.getPickedIntersection().P,
                                pPointLight->m_Position,
                                Vec3f(0, 1, 0),
                                Vec3f(0, 1, 0),
                                5);

                            auto voxel = m_VoxelOpeningSegmentationGraph.getVoxel(pPointLight->m_Position);
                            auto lightClusterIdx = m_VoxelOpeningSegmentationGraph.getVoxelCluster(voxel);

                            auto shortestPaths = computeDijkstraShortestPaths(m_VoxelOpeningSegmentationGraph.getGraph(),
                                                                                          lightClusterIdx,
                                                                                          [&](GraphNodeIndex n1, GraphNodeIndex n2) {
                                                                                                    return 1;
                                                                                          });

                            for (auto i = 0u; i < m_nPortalRayCount; ++i) {
                                auto currentPoint = m_Viewer.getPickedIntersection();
                                auto incidendDir = normalize(wi);

                                for(auto depth = 1u; currentPoint && depth < 2u; ++depth) {
                                    bool hasSampledPortal = false;
                                    auto voxel = m_VoxelOpeningSegmentationGraph.getVoxel(currentPoint, wi);
                                    auto openingClusterIdx = m_VoxelOpeningSegmentationGraph.getVoxelCluster(voxel);

                                    if(openingClusterIdx != VoxelOpeningSegmentationGraph::NO_CLUSTER()) {
                                        const auto& cluster = m_VoxelOpeningSegmentationGraph.getCluster(openingClusterIdx);

                                        if(openingClusterIdx == lightClusterIdx) {
                                            break;
                                        }

                                        auto dijkstraNode = shortestPaths[openingClusterIdx];
                                        int neighborIdx = -1;
                                        for(auto i = 0u; i < m_VoxelOpeningSegmentationGraph.neighbours(openingClusterIdx).size(); ++i) {
                                            if(dijkstraNode.predecessor == m_VoxelOpeningSegmentationGraph.neighbours(openingClusterIdx)[i]) {
                                                neighborIdx = i;
                                                break;
                                            }
                                        }

                                        if(neighborIdx >= 0) {
                                            auto portalSample = neighborIdx;
//                                            auto portalIdx = cluster.m_Portals[portalSample];
//                                            auto faceCount = portals[portalIdx].size();
                                            auto faceCount = cluster.m_Portals[portalSample].size();
                                            if(faceCount > 0u) {
                                                auto faceSample = min(size_t(rng.getFloat() * faceCount), faceCount - 1);
                                                // First attempt, just trace through the center of the voxel face
                                                Vec3i u, v;
                                                auto face = cluster.m_Border[cluster.m_Portals[portalSample][faceSample]];
                                                getFaceVectors(CC3DFaceBits::Value(face.m_VoxelFace.w), u, v);

                                                auto faceCenter = Vec3f(Vec3f(face.m_VoxelFace)) + Vec3f(u) * rng.getFloat() + Vec3f(v) * rng.getFloat();
                                                auto faceCenterWorldSpace = Vec3f(m_GridToWorldMatrix * Vec4f(faceCenter, 1.f));
                                                auto faceNormal = normalize(Vec3f(m_GridToWorldMatrix * Vec4f(Vec3f(face.m_OutgoingNormal), 0.f)));

                                                auto dir = normalize(faceCenterWorldSpace - currentPoint.P);

                                                if(dot(dir, faceNormal) > 0.f) {
                                                    auto ray = Ray(currentPoint, dir);
                                                    rays.emplace_back(Ray(ray));
                                                    auto neighbour = m_VoxelOpeningSegmentationGraph.neighbours(openingClusterIdx)[portalSample];
                                                    colors.emplace_back(getColor(neighbour));

                                                    auto vo = Vec3f(face.m_VoxelFace);
                                                    auto lower = Vec3f(m_GridToWorldMatrix * Vec4f(vo.x, vo.y, vo.z, 1.f));
                                                    auto upper = Vec3f(m_GridToWorldMatrix * Vec4f(vo.x + 1, vo.y + 1, vo.z + 1, 1.f));

                                                    m_DisplayStream.addBBox(upper, lower, getColor(neighbour), 4.f);

//                                                    m_DisplayStream.addLine(faceCenterWorldSpace, faceCenterWorldSpace + m_Viewer.getArrowLength() * faceNormal, getColor(neighbour),
//                                                                                getColor(neighbour), 5);

                                                    currentPoint = m_Viewer.getScene()->intersect(ray);
                                                    incidendDir = -ray.dir;
                                                    hasSampledPortal = true;
                                                }
                                            }
                                        }
                                    }

//                                    if(!hasSampledPortal) {
//                                        auto bsdf = BSDF(incidendDir, currentPoint, *m_Viewer.getScene());
//                                        Sample3f outDir;
//                                        float costOutDir;
//                                        bsdf.sample(rng.getFloat3(), outDir, costOutDir);
//                                        auto dir = outDir.value;

//                                        auto ray = Ray(currentPoint, dir);
//                                        rays.emplace_back(Ray(ray));
//                                        colors.emplace_back(Vec3f(0.f));

//                                        currentPoint = m_Viewer.getScene()->intersect(ray);
//                                        incidendDir = -ray.dir;
//                                    }
                                }
                            }
                        }
                            break;
                        }

                        for(auto i = 0u; i < rays.size(); ++i) {
                            auto I = m_Viewer.getScene()->intersect(rays[i]);

                            if(I) {
                               m_DisplayStream.addLine(
                                   rays[i].org,
                                   I.P,
                                   colors[i],
                                   colors[i],
                                   5);
                            }
                        }
                    }
                }
            }
        }
    }
}

void dilateLineExperimental(uint32_t* radiusLine, Vec3u* centerLine, int stride, uint32_t size, Ball26* pBuffer) {
	DeQueue<Ball26> queue(pBuffer);

	// Invariants for the queue:
	// queue[i].radius > queue[i+1].radius (1)
	// queue[i+1].pixelIndex + queue[i+1].radius > queue[i].pixelIndex + queue[i].radius (2)

	for (auto i = 0u; i < size; ++i) {
		int offset = int(i) * stride;
		Ball26 currentBall = { i, radiusLine[offset], centerLine[offset] };

		if (currentBall.radius == 0u) {
			// We are outside the object, reset the dilation from there
			queue.clear();
		}
		else if (!queue.empty() && queue.front().endOfRange() == i) {
			// We reach the first pixel outside the range
			queue.pop_front();
		}

		if (queue.empty()) {
			queue.push_back(currentBall);
		}
		else {
			// The equals creates non connex regions
			if (currentBall.radius >= queue.front().radius) {
				// If the current range is bigger than the first one to dilate, it becomes the new to dilate
				// and all the stored range are discarded
				queue.clear();
				queue.push_back(currentBall);
			}
			else {
				// The equals creates non connex regions
				// Check if the current pixel need to be dilated outside the stored ranges
				while (currentBall.radius >= queue.back().radius) {
					// Remove the ranges lower than the current pixel one
					queue.pop_back();
				}

				// The queue cannot be empty because we have at least pixelRange.rangeRadius < queue.front().rangeRadius
				if (queue.back().endOfRange() < currentBall.endOfRange()) {
					queue.push_back(currentBall);
				}
			}
		}

		// Dilate
		radiusLine[offset] = queue.front().radius;
		centerLine[offset] = queue.front().center;
	}
}

// SPECIALIZED VERSION FOR EXPERIMENTATIONS

Grid3D<uint32_t> computeOpeningMap26Experiment(Grid3D<uint32_t> distanceMap, Grid3D<Vec3u>& centerMap,
                                               std::vector<int32_t> directions, bool iterative) {
    auto width = distanceMap.width();
    auto height = distanceMap.height();
    auto depth = distanceMap.depth();

    auto threadCount = getSystemThreadCount();

	if (!iterative) {
		centerMap = Grid3D<Vec3u>(width, height, depth);

		processTasks(width * height * depth, [&](uint32_t voxelID, uint32_t threadID) {
			auto voxel = centerMap.coords(voxelID);
			centerMap[voxelID] = voxel;
		}, threadCount);
	}

    auto rangeListSize = max(max(width, height), depth) + 1;
    auto rangeList = makeUniqueArray<Ball26>(rangeListSize * threadCount);

    for(auto dir: directions) {
        switch(dir) {
        default:
            break;
        case 0: // X
            processTasks(depth * height, [&](uint32_t lineID, uint32_t threadID) {
                auto j = lineID % height;
                auto k = lineID / height;

                //auto pRangeList = rangeList.get() + threadID * rangeListSize;
                auto pBuffer = rangeList.get() + threadID * rangeListSize;

				dilateLineExperimental(distanceMap.data() + j * width + k * width * height,
					centerMap.data() + j * width + k * width * height,
					1, width, pBuffer);
				dilateLineExperimental(distanceMap.data() + j * width + k * width * height + width - 1,
                    centerMap.data() + j * width + k * width * height + width - 1,
                    -1, width, pBuffer);
            }, getSystemThreadCount());
            break;
        case 1:
            processTasks(height * width, [&](uint32_t lineID, uint32_t threadID) {
                auto i = lineID % width;
                auto j = lineID / width;

                auto pBuffer = rangeList.get() + threadID * rangeListSize;

				dilateLineExperimental(distanceMap.data() + i + j * width,
                    centerMap.data() + i + j * width,
                    width * height, depth, pBuffer);
				dilateLineExperimental(distanceMap.data() + i + j * width + (depth - 1) * width * height,
                    centerMap.data() + i + j * width + (depth - 1) * width * height,
                    -(int)(width * height), depth, pBuffer);
            }, getSystemThreadCount());
            break;
        case 2:
            processTasks(depth * width, [&](uint32_t lineID, uint32_t threadID) {
                auto i = lineID % width;
                auto k = lineID / width;

                auto pBuffer = rangeList.get() + threadID * rangeListSize;

				dilateLineExperimental(distanceMap.data() + i + k * width * height,
                    centerMap.data() + i + k * width * height,
                    width, height, pBuffer);
				dilateLineExperimental(distanceMap.data() + i + k * width * height + (height - 1) * width,
                    centerMap.data() + i + k * width * height + (height - 1) * width,
                    -(int)width, height, pBuffer);
            }, getSystemThreadCount());
        }
    }

    return distanceMap;
}

}
