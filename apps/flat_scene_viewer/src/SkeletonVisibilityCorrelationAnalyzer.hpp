#pragma once

#include <vector>

#include <bonez/viewer/GUI.hpp>

#include <bonez/scene/SurfacePoint.hpp>
#include <bonez/scene/Scene.hpp>

#include <bonez/sampling/shapes.hpp>

#include <bonez/sys/threads.hpp>

#include <bonez/opengl/debug/GLDebugRenderer.hpp>

#include <bonez/utils/ColorMap.hpp>

#include <Eigen/Dense>
#include <Eigen/Sparse>

namespace BnZ {

class SkeletonVisibilityCorrelationAnalyzer {
public:
    void initialize(const Scene* pScene) {
        m_pScene = pScene;
    }

    void sampleSurfacePoints() {
        m_SurfacePointSamples.resize(m_nSurfacePointSampleCount);
        std::vector<float> s1DMeshBuffer;
        std::vector<float> s1DTriangleBuffer;
        std::vector<float> s1DTriangleSideBuffer;
        std::vector<Vec2f> s2DTriangleBuffer;

        for(auto i = 0u; i < m_nSurfacePointSampleCount; ++i) {
            s1DMeshBuffer.emplace_back(m_Rng.getFloat());
            s1DTriangleBuffer.emplace_back(m_Rng.getFloat());
            s1DTriangleSideBuffer.emplace_back(m_Rng.getFloat());
            s2DTriangleBuffer.emplace_back(m_Rng.getFloat2());
        }
        m_pScene->uniformSampleSurfacePoints(m_nSurfacePointSampleCount,
                                             s1DMeshBuffer.data(),
                                             s1DTriangleBuffer.data(),
                                             s1DTriangleSideBuffer.data(),
                                             s2DTriangleBuffer.data(),
                                             m_SurfacePointSamples.data());

        m_SurfacePointMappedNode.reserve(m_nSurfacePointSampleCount);
        for(const auto& pointSample: m_SurfacePointSamples) {
            auto nodeIndex = m_pScene->getCurvSkeleton()->getNearestNode(pointSample.value);
            m_SurfacePointMappedNode.emplace_back(nodeIndex);
        }
    }

    void computeVisibilityMatrices() {
        {
            std::vector<Eigen::Triplet<float>> allTriplets;
            std::mutex mutex;
            auto nbPointProcessed = 0u;

            processTasks(m_nSurfacePointSampleCount, [&](uint32_t pointID, uint32_t threadID) {
                std::vector<Eigen::Triplet<float>> triplets;
                for(auto i: range(m_nSurfacePointSampleCount)) {
                    auto shadowRay = Ray(m_SurfacePointSamples[pointID].value, m_SurfacePointSamples[i].value);
                    if(!m_pScene->occluded(shadowRay)) {
                        triplets.emplace_back(Eigen::Triplet<float>(pointID, i, 1.f));
                    }
                }

                {
                    std::unique_lock<std::mutex> l(mutex);
                    allTriplets.reserve(allTriplets.size() + triplets.size());
                    std::copy(begin(triplets), end(triplets), std::back_inserter(allTriplets));

                    std::clog << "Number of triplets = " << allTriplets.size() << std::endl;
                    ++nbPointProcessed;
                    std::clog << nbPointProcessed << " / " << m_nSurfacePointSampleCount << std::endl;
                }

            }, getSystemThreadCount());

            m_PointToPointVisibilityMatrix.resize(m_nSurfacePointSampleCount, m_nSurfacePointSampleCount);
            m_PointToPointVisibilityMatrix.setFromTriplets(begin(allTriplets), end(allTriplets));
        }

        {
            std::vector<Eigen::Triplet<float>> allTriplets;
            std::mutex mutex;
            auto nbNodeProcessed = 0u;

            processTasks(m_pScene->getCurvSkeleton()->size(), [&](uint32_t nodeID, uint32_t threadID) {
                std::vector<Eigen::Triplet<float>> triplets;
                for(auto i: range(m_nSurfacePointSampleCount)) {
                    auto node = m_pScene->getCurvSkeleton()->getNode(nodeID);
                    auto dir = m_SurfacePointSamples[i].value.P - node.P;
                    auto l = length(dir);
                    dir /= l;
                    auto shadowRay = Ray(node.P, m_SurfacePointSamples[i].value, dir, l);
                    if(!m_pScene->occluded(shadowRay)) {
                        triplets.emplace_back(Eigen::Triplet<float>(nodeID, i, 1.f));
                    }
                }

                {
                    std::unique_lock<std::mutex> l(mutex);
                    allTriplets.reserve(allTriplets.size() + triplets.size());
                    std::copy(begin(triplets), end(triplets), std::back_inserter(allTriplets));

                    std::clog << "Number of triplets = " << allTriplets.size() << std::endl;
                    ++nbNodeProcessed;
                    std::clog << nbNodeProcessed << " / " << m_pScene->getCurvSkeleton()->size() << std::endl;
                }

            }, getSystemThreadCount());

            m_NodeToPointVisibilityMatrix.resize(m_pScene->getCurvSkeleton()->size(), m_nSurfacePointSampleCount);
            m_NodeToPointVisibilityMatrix.setFromTriplets(begin(allTriplets), end(allTriplets));
        }
    }

    void computeSkeletonVisibilityCorrelations() {
        m_PerNodeVisibilityMeanCorrelation.resize(m_pScene->getCurvSkeleton()->size());
        std::fill(begin(m_PerNodeVisibilityMeanCorrelation),
                  end(m_PerNodeVisibilityMeanCorrelation), 0.f);

        m_PerNodeMappedPointsCount.resize(m_pScene->getCurvSkeleton()->size());
        std::fill(begin(m_PerNodeMappedPointsCount), end(m_PerNodeMappedPointsCount), 0.f);

        for(auto pointID: range(m_nSurfacePointSampleCount)) {
            auto nodeID = m_SurfacePointMappedNode[pointID];

            if(nodeID != UNDEFINED_NODE) {
                auto correlationForThisPoint = 0.f;
                for(auto j: range(m_nSurfacePointSampleCount)) {
                    auto diff = m_PointToPointVisibilityMatrix.coeff(pointID, j) - m_PointToPointVisibilityMatrix.coeff(nodeID, j);
                    correlationForThisPoint += sqr(diff);
                }
                m_PerNodeVisibilityMeanCorrelation[nodeID] += correlationForThisPoint / float(m_nSurfacePointSampleCount);
                m_PerNodeMappedPointsCount[nodeID] += 1.f;
            }
        }

        m_fGlobalMeanCorrelation = 0.f;
        m_fGlobalCorrelationWeightedPerMappedNodeCount = 0.f;
        m_fMaxMappedPointsCount = 0.f;
        auto pointCount = 0u;
        auto nodeCount = 0u;
        for(auto nodeID: range(m_pScene->getCurvSkeleton()->size())) {
            if(m_PerNodeMappedPointsCount[nodeID] > 0.f) {
                m_fGlobalCorrelationWeightedPerMappedNodeCount += m_PerNodeVisibilityMeanCorrelation[nodeID];
                pointCount += m_PerNodeMappedPointsCount[nodeID];
                m_PerNodeVisibilityMeanCorrelation[nodeID] /= m_PerNodeMappedPointsCount[nodeID];
                m_fGlobalMeanCorrelation += m_PerNodeVisibilityMeanCorrelation[nodeID];
                nodeCount += 1;
            }
            m_fMaxMappedPointsCount = max(m_fMaxMappedPointsCount, m_PerNodeMappedPointsCount[nodeID]);
        }

        m_fGlobalCorrelationWeightedPerMappedNodeCount /= float(pointCount);
        m_fGlobalMeanCorrelation /= float(nodeCount);
    }

    void exposeGUI(GUI& gui, GraphNodeIndex selectedNode) {
        if(auto window = gui.addWindow("SkeletonVisibilityCorrelationAnalizer")) {
            gui.addVarRW(BNZ_GUI_VAR(m_nSurfacePointSampleCount));
            gui.addButton("sample surface points", [&]() {
                sampleSurfacePoints();
            });

            gui.addSeparator();
            gui.addButton("compute visibility matrices", [&]() {
                computeVisibilityMatrices();
            });

            gui.addSeparator();
            gui.addButton("compute skeleton visibility correlation", [&]() {
                computeSkeletonVisibilityCorrelations();
            });

            gui.addSeparator();

            gui.addValue(BNZ_GUI_VAR(m_fGlobalMeanCorrelation));
            gui.addValue(BNZ_GUI_VAR(m_fGlobalCorrelationWeightedPerMappedNodeCount));
            if(!m_PerNodeVisibilityMeanCorrelation.empty()) {
                ImGui::PlotHistogram("skeleton visibility correlation",
                                 m_PerNodeVisibilityMeanCorrelation.data(),
                                 m_PerNodeVisibilityMeanCorrelation.size(),
                                 0, nullptr, 0, 1, ImVec2(0, 128));
            }

            if(!m_PerNodeMappedPointsCount.empty()) {
                ImGui::PlotHistogram("skeleton mapped point count",
                                 m_PerNodeMappedPointsCount.data(),
                                 m_PerNodeMappedPointsCount.size(),
                                 0, nullptr, 0.0f, m_fMaxMappedPointsCount, ImVec2(0,128));
            }
        }
    }

    void drawPoints(GLDebugRenderer& renderer, float zFar) {
        m_DisplayStream.clearObjects();
        renderer.addStream(&m_DisplayStream);

        std::vector<Vec3f> points, colors;
        for(const auto& point: m_SurfacePointSamples) {
            points.emplace_back(point.value.P);
            colors.emplace_back(point.value.Ns);
        }
        m_DisplayStream.addPoints(points.size(), points.data(), colors.data());
    }

private:
    RandomGenerator m_Rng;

    const Scene* m_pScene = nullptr;

    Eigen::SparseMatrix<float> m_PointToPointVisibilityMatrix;
    Eigen::SparseMatrix<float> m_NodeToPointVisibilityMatrix;

    // A set of random points distributed over the surfaces of the scene
    std::vector<SurfacePointSample> m_SurfacePointSamples;
    std::vector<GraphNodeIndex> m_SurfacePointMappedNode;
    uint32_t m_nSurfacePointSampleCount = 512;

    std::vector<float> m_PerNodeVisibilityMeanCorrelation;
    float m_fGlobalMeanCorrelation = 0.f;
    float m_fGlobalCorrelationWeightedPerMappedNodeCount = 0.f;

    std::vector<float> m_PerNodeMappedPointsCount;
    float m_fMaxMappedPointsCount = 0.f;


    // A set of surface points visible from the last node of interest
    //std::vector<SurfacePointSample> m_VisibleFromNode;
    //std::vector<SurfacePoint> m_MappedToNode;
    //std::vector<float> m_MappedToNodeVisibilityCorrelation;
    //uint32_t m_nVisibleFromNodeSampleCount = 16000;
    //uint32_t m_nNodeOfInterest = UNDEFINED_NODE;

    GLDebugStreamData m_DisplayStream;
};

}
