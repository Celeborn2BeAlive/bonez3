#pragma once

#include <bonez/common.hpp>
#include <bonez/types.hpp>
#include <bonez/viewer/GUI.hpp>
#include <bonez/opengl/debug/GLDebugStream.hpp>

namespace BnZ {

class SceneViewer;
class Scene;

class CenteredPointsAnalyzer {
public:
    SceneViewer& m_Viewer;
    const Scene* m_pScene;

    uint32_t m_nSurfacePointCount = 0u;
    bool m_bDisplayCenteredPoints = false;
    bool m_bDisplayCenteredLines = true;

   GLDebugStreamData m_DisplayStream;

    struct CenteredPoint {
        Vec3f org, dst, center;
    };

    std::vector<CenteredPoint> m_CenteredPoints;

    CenteredPointsAnalyzer(SceneViewer& viewer);

    void exposeGUI(GUI& gui);

    void draw();
};

}
