#pragma once

#include <bonez/opengl/debug/GLDebugStream.hpp>
#include <bonez/utils/KdTree.hpp>
#include <bonez/scene/SurfacePoint.hpp>

namespace BnZ {

class SceneViewer;
class Scene;

class KdTreeTestViewerModule {
public:
    void setUp(SceneViewer& viewer);

    void drawGUI();

    void draw();

private:
    SceneViewer* m_pViewer = nullptr;

    size_t m_nPointCount = 128;
    bool m_bJitteredSampling = false;
    std::vector<SurfacePointSample> m_PointBuffer;

    float m_fLookupRadius = 1.f;
    bool m_bGetNearest = false;
    uint32_t m_nK = 1u;
    KdTree m_KdTree;

    bool m_bDisplayLookupSphere = false;

    GLDebugStreamData m_DisplayStream;
};

}
